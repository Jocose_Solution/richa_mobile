﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace IRCTC_RDS
//{
//    class EncrytionDecryptionCBCMode
//    {
//    }
//}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IRCTC_RDS
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Security.Cryptography;

    public class EncryptDecryptString
    {
       public string EncryptString(string str)
        {
            string encr = null;
            try
            {
                //string textp = @"merchantCode=IR_CRIS|reservationId=1100012345|txnAmount=458.00|currencyType=INR|appCode=BOOKING|pymtMode=Internet|txnDate=20111206|securityId=CRIS|RU=https://localhost:8080/eticketing/BankResponse|checkSum=1EF7C9AA4C9CF912C1539AE332E997E15019E40010A59363064C9C308EA138";
                //byte[] key =  Encoding.ASCII.GetBytes("abfb7c8d48dfc4f1ce7ed92a44989f25");
                string hexKey = Convert.ToString(ConfigurationManager.AppSettings["RDSKEY"]);
                string textp = str;
                byte[] key = Encoding.ASCII.GetBytes(hexKey);
                byte[] aa =   EncryptStringToBytes_Aes(textp, key);
                //string ddr=  DecryptStringFromBytes_Aes(aa, key);
                encr = BitConverter.ToString(aa).Replace("-", "");
                if(encr.Length>=32)
                {
                    encr = encr.Substring(32);
                }                
            }
            catch (Exception e)
            {
                //Console.WriteLine("Error: {0}", e.Message);
            }
            return encr;
        }


       public string DecryptString(string str)
        {
            string strDecrypt = null;
            try
            {               
                string EncrypteRDSKEY = Convert.ToString(ConfigurationManager.AppSettings["EncrypteRDSKEY"]);
                str = EncrypteRDSKEY + str;   //Add RDSKEY ENCRIPT 32 LENGTH

                //string textp = @"merchantCode=IR_CRIS|reservationId=1100012345|txnAmount=458.00|currencyType=INR|appCode=BOOKING|pymtMode=Internet|txnDate=20111206|securityId=CRIS|RU=https://localhost:8080/eticketing/BankResponse|checkSum=1EF7C9AA4C9CF912C1539AE332E997E15019E40010A59363064C9C308EA138";
                //byte[] key = Encoding.ASCII.GetBytes("abfb7c8d48dfc4f1ce7ed92a44989f25");
                //string textp = str;
                string hexKey = Convert.ToString(ConfigurationManager.AppSettings["RDSKEY"]);
                byte[] key = Encoding.ASCII.GetBytes(hexKey);
                //byte[] aa = EncryptStringToBytes_Aes(textp, key);
                //string ddr = DecryptStringFromBytes_Aes(aa, key);
                //string encr = BitConverter.ToString(aa).Replace("-", "");

                string encr = str;
                byte[] Eaa = StringToByteArray(encr);
                //string ddr1 = DecryptStringFromBytes_Aes(Eaa, key);
               strDecrypt = DecryptStringFromBytes_Aes(Eaa, key);
            }
            catch (Exception e)
            {
                //Console.WriteLine("Error: {0}", e.Message);
            }
            return strDecrypt;
        }


        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key)
        {
            byte[] encrypted;
            byte[] IV;


            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;

                //aesAlg.GenerateIV();
                //IV = aesAlg.IV;

                string strIV = Convert.ToString(ConfigurationManager.AppSettings["IVKey"]);
                IV = Encoding.ASCII.GetBytes(strIV);
                aesAlg.IV = IV;
                aesAlg.Mode = CipherMode.CBC;

               var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            var combinedIvCt = new byte[IV.Length + encrypted.Length];
            Array.Copy(IV, 0, combinedIvCt, 0, IV.Length);
            Array.Copy(encrypted, 0, combinedIvCt, IV.Length, encrypted.Length);

            // Return the encrypted bytes from the memory stream. 
            return combinedIvCt;

        }

        static string DecryptStringFromBytes_Aes(byte[] cipherTextCombined, byte[] Key)
        {

            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an Aes object 
            // with the specified key and IV. 
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;

                string strIV = Convert.ToString(ConfigurationManager.AppSettings["IVKey"]);
                byte[] IV1 = Encoding.ASCII.GetBytes(strIV);
                
                byte[] IV = new byte[aesAlg.BlockSize / 8];
                byte[] cipherText = new byte[cipherTextCombined.Length - IV.Length];

                Array.Copy(cipherTextCombined, IV, IV.Length);
                Array.Copy(cipherTextCombined, IV.Length, cipherText, 0, cipherText.Length);

                aesAlg.IV = IV;

                aesAlg.Mode = CipherMode.CBC;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }
    }
}