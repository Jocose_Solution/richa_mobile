﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using HotelShared;
using HotelDAL;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data;
namespace HotelBAL
{
  public class GTAHotelResponse
    {
      GTARequestXML HtlReq = new GTARequestXML();
      public HotelComposite GTAHotels(HotelSearch SearchDetails)
      {
          HotelComposite obgHotelsCombo = new HotelComposite();
          try
          {
              if (SearchDetails.SearchCityCode.Length < 5 && SearchDetails.CurrancyRate > 0 && SearchDetails.GTAURL != null && (SearchDetails.GTATrip == SearchDetails.HtlType || SearchDetails.GTATrip == "ALL"))
              {
                    SearchDetails = HtlReq.SeachHotelPriceRequestXml(SearchDetails);
                    SearchDetails.GTA_HotelSearchRes = GTAPostXml(SearchDetails.GTAURL, SearchDetails.GTA_HotelSearchReq);
                    obgHotelsCombo.Hotelresults = GetGTAHotelsPrice(SearchDetails.GTA_HotelSearchRes, SearchDetails);
                  //XDocument document1;
                  //string XMLFile = "D:\\HotelPriject\\New folder\\B2BHote_RezNext_Expedia\\HotelGTAXML\\Home\\GTA.xml";
                  //  document1 = XDocument.Load(XMLFile);
                  //  SearchDetails.GTA_HotelSearchRes = document1.ToString();
                  //  obgHotelsCombo.Hotelresults = GetGTAHotelsPrice(SearchDetails.GTA_HotelSearchRes, SearchDetails); 
              }
              else
              {
                  SearchDetails.GTA_HotelSearchRes = "Hotel Not available for " + SearchDetails.SearchCity + ", " + SearchDetails.Country;
                  SearchDetails.GTA_HotelSearchReq = "not allow";
                  List<HotelResult> objHotellist = new List<HotelResult>();
                  objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = SearchDetails.GTA_HotelSearchRes });
                  obgHotelsCombo.Hotelresults = objHotellist;
              }
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "GTAHotels");
              HotelDA objhtlDa = new HotelDA();
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.GTA_HotelSearchReq, SearchDetails.GTA_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
              SearchDetails.GTA_HotelSearchRes = ex.Message;
          }
          obgHotelsCombo.HotelSearchDetail = SearchDetails;
          return obgHotelsCombo;
      }
      protected string GTAPostXml(string url, string xml)
      {
          StringBuilder sbResult = new StringBuilder();
          try
          {
              HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
              if (!string.IsNullOrEmpty(xml))
              {
                  Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                  Http.Method = "POST";
                  byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                  Http.ContentLength = lbPostBuffer.Length;
                  Http.ContentType = "text/xml";
                  Http.Timeout = 56000;
                  using (Stream PostStream = Http.GetRequestStream())
                  {
                      PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                  }
              }

              using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
              {
                  if (WebResponse.StatusCode != HttpStatusCode.OK)
                  {
                      string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                      throw new ApplicationException(message);
                  }
                  else
                  {
                      Stream responseStream = WebResponse.GetResponseStream();
                      if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                      {
                          responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                      }
                      else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                      {
                          responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                      }
                      StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                      sbResult.Append(reader.ReadToEnd());
                      responseStream.Close();
                  }
              }
          }
          catch (WebException WebEx)
          {
              HotelDA objhtlDa = new HotelDA();
              HotelDA.InsertHotelErrorLog(WebEx, "GTAPostXml");
              WebResponse response = WebEx.Response;
              if (response != null)
              {
                  Stream stream = response.GetResponseStream();
                  string responseMessage = new StreamReader(stream).ReadToEnd();
                  sbResult.Append(responseMessage);                 
                  int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, responseMessage, "GTA", "HotelInsert");
              }
              else
              {
                  int n = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, WebEx.Message, "GTA", "HotelInsert");
                  sbResult.Append(WebEx.Message + "<Errors>");
              }
          }
          catch (Exception ex)
          {
              sbResult.Append(ex.Message + "<Errors>");
              HotelDA.InsertHotelErrorLog(ex, "GTAPostXml");
              HotelDA objhtlDa = new HotelDA();
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, ex.Message, "GTA", "HotelInsert");
          }
          return sbResult.ToString();
      }
      protected List<HotelResult> GetGTAHotelsPrice(string XmlRes, HotelSearch SearchDetails)
      {
          HotelDA objhtlDa = new HotelDA(); HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();
          List<HotelResult> objHotellist = new List<HotelResult>();
          try
          {
              if (XmlRes.Contains("<Errors>"))
              {
                  int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.GTA_HotelSearchReq, XmlRes, SearchDetails.AgentID, "HotelInsert");

                  XDocument document = XDocument.Parse(XmlRes);
                  var Hotels_temps = (from Hotel in document.Descendants("Errors").Descendants("Error") select new { Errormsg = Hotel.Element("ErrorText"), ErrorID = Hotel.Element("ErrorId") }).ToList();
                  objHotellist.Add(new HotelResult { HtlError = Hotels_temps[0].ErrorID.Value + " : " + Hotels_temps[0].Errormsg.Value });
              }
              else
              {
                  XDocument document = XDocument.Parse(XmlRes.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                  var Hotels_temps = (from Hotel in document.Descendants("HotelDetails").Descendants("Hotel")
                      select new
                      {
                          citycode = Hotel.Element("City").Attribute("Code").Value.Trim(),city = Hotel.Element("City").Value.Trim(),
                          HotelID = Hotel.Element("Item").Attribute("Code").Value.Trim(), HtlName = Hotel.Element("Item").Value.Trim(),
                          StarRating = Hotel.Element("StarRating").Value.Trim(),Location = Hotel.Element("LocationDetails"),
                          HotelRooms = Hotel.Element("HotelRooms"), RoomCategory = Hotel.Element("RoomCategories").Element("RoomCategory")
                      }).ToList();
                  if (Hotels_temps.Count > 0)
                  {
                      int k = 740;
                      foreach (var Hotels in Hotels_temps)
                      {
                          try
                          {

                              decimal orgrates = (Convert.ToDecimal(Hotels.RoomCategory.Element("ItemPrice").Value) * SearchDetails.CurrancyRate) / (SearchDetails.NoofNight * SearchDetails.NoofRoom);
                              MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, Hotels.StarRating, SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, orgrates, SearchDetails.GTA_servicetax);
                              //// Hotel Discount strat
                              string discMsg = "", Inclusion = "", Locations=""; decimal discAmt = 0; int i = 0;
                              if (Hotels.RoomCategory.Element("Offer") != null)
                                  discMsg = "<span class='discounttag'>" + Hotels.RoomCategory.Element("Offer").Value.Trim() + "</span>";

                              if (Hotels.RoomCategory.Element("ItemPrice").Attribute("GrossWithoutDiscount") != null)
                                  discAmt = objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, ((Convert.ToDecimal(Hotels.RoomCategory.Element("ItemPrice").Attribute("GrossWithoutDiscount").Value.Trim()) * SearchDetails.CurrancyRate) / SearchDetails.NoofNight * SearchDetails.NoofRoom), SearchDetails.GTA_servicetax);
                              //Hotel Discount End
                              HotelInformation HotelInfo = new HotelInformation();
                              HotelInfo = GetGTAHotelInformation(SearchDetails, Hotels.HotelID, Hotels.citycode);
                              //Display Hotel Inclution strat  
                              try
                              {
                                  if (Hotels.RoomCategory.Element("Meals").Element("Basis") != null)
                                  {
                                      if (Hotels.RoomCategory.Element("Meals").Element("Basis").Value == "None" || Hotels.RoomCategory.Element("Meals").Element("Basis").Value == "Room only")
                                          Inclusion = "Room Only";
                                      else
                                          Inclusion = Hotels.RoomCategory.Element("Meals").Element("Basis").Value + ", " + Hotels.RoomCategory.Element("Meals").Element("Breakfast").Value;
                                  }
                              }
                              catch (Exception ex)
                              {
                                  Inclusion = Hotels.RoomCategory.Element("Meals").Element("Basis").Value;
                                  HotelDA.InsertHotelErrorLog(ex, "Meals");
                              }
                              if (Hotels.Location.Element("Location") != null)
                                  Locations = Hotels.Location.Element("Location").Value;
                              //Display Hotel Inclution End
                              //Add Hotels Detaisl in List start
                              objHotellist.Add(new HotelResult
                              {
                                  RatePlanCode = Hotels.RoomCategory.Attribute("Id").Value,
                                  HotelCityCode = Hotels.citycode,
                                  HotelCity = Hotels.city,
                                  HotelCode = Hotels.HotelID,
                                  HotelName = Hotels.HtlName.Trim().Replace("'", ""),
                                  StarRating = Hotels.StarRating,
                                  Location = Locations,
                                  hotelPrice = MarkList.TotelAmt,
                                  AgtMrk=MarkList.AgentMrkAmt,
                                  DiscountMsg = discMsg,
                                  hotelDiscoutAmt = discAmt,
                                  HotelAddress = HotelInfo.Address,
                                  HotelThumbnailImg = HotelInfo.ThumbImg,
                                  HotelServices = HotelInfo.HotelServices,
                                  HotelDescription = HotelInfo.HotelDiscription,
                                  Lati_Longi = HotelInfo.Latitude + "##" + HotelInfo.Longitude,
                                  inclusions=Inclusion,
                                  Provider = "GTA",
                                  ReviewRating = "",
                                  PopulerId = k++
                              });
                              //Add Hotels Detaisl in List end
                          }
                          catch (Exception ex)
                          {
                              objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = ex.Message });
                              HotelDA.InsertHotelErrorLog(ex, "adding Hotel list  " + SearchDetails.SearchCity);
                          }
                      }
                  }
                  else
                  {
                      objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search." });
                      int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.GTA_HotelSearchReq, SearchDetails.GTA_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                  }
              }
          }
          catch (Exception ex)
          {
              objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = ex.Message });
              HotelDA.InsertHotelErrorLog(ex, "GetGTAHotelsPrice  " + SearchDetails.SearchCity);
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.GTA_HotelSearchReq, SearchDetails.GTA_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
          }
          return objHotellist;
      }
      protected HotelInformation GetGTAHotelInformation(HotelSearch SearchDetails, string HotelCode, string CityCode)
      {
          HotelInformation HotelInfo = new HotelInformation();
          XDocument document;
          try
          {
              string XMLFile = ConfigurationManager.AppSettings["ExtractFilePath"] + "\\" + CityCode + "_" + HotelCode + ".xml";
              if (File.Exists(XMLFile))
                  document = XDocument.Load(XMLFile);
              else
              {
                  string GTAResponce = GTAItemInformationRespnse(SearchDetails, HotelCode, CityCode);
                  document = XDocument.Parse(GTAResponce);
                  document.Save(XMLFile);
                 HotelDA objhtlDa = new HotelDA();
                  int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", SearchDetails.GTAItemInformationReq, GTAResponce, "GTA", "HotelInsert");
              }
              var ItemInformations2 = (from Hotel in document.Descendants("ItemDetail")
                  where Hotel.Element("City").Attribute("Code").Value == CityCode && Hotel.Element("Item").Attribute("Code").Value == HotelCode
                   select new {HotelDetails = Hotel.Element("HotelInformation") }).ToList();
              if (ItemInformations2.Count > 0)
              {
                  foreach (var Htl in ItemInformations2)
                  {
                      string Addresss = "Address Not Found";
                      try
                      {
                          Addresss = Htl.HotelDetails.Element("AddressLines").Element("AddressLine1").Value;
                          if (Htl.HotelDetails.Element("AddressLines").Element("AddressLine2") != null)
                              Addresss += ", " + Htl.HotelDetails.Element("AddressLines").Element("AddressLine2").Value;
                          if (Htl.HotelDetails.Element("AddressLines").Element("AddressLine3") != null)
                              Addresss += ", " + Htl.HotelDetails.Element("AddressLines").Element("AddressLine3").Value;
                          if (Htl.HotelDetails.Element("AddressLines").Element("AddressLine4") != null)
                              Addresss += ", " + Htl.HotelDetails.Element("AddressLines").Element("AddressLine4").Value;
                      }
                      catch (Exception ex)
                      {
                          HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode + "-Addresss");
                      }
                      HotelInfo.Address = Addresss;

                      HotelInfo.HotelDiscription = "<strong>Description</strong>: ";
                      try
                      {
                          if (Htl.HotelDetails.Element("Reports") != null)
                              foreach (var Report in Htl.HotelDetails.Element("Reports").Descendants("Report"))
                              {
                                  HotelInfo.HotelDiscription += Report.Value + " ";
                              }
                      }
                      catch (Exception ex)
                      {
                          HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode + "-HotelDiscription");
                      }

                      //Hotel facility Facilities
                       HotelInfo.HotelServices = "";
                       try
                       {
                           if (Htl.HotelDetails.Element("Facilities") != null)
                               foreach (var flt in Htl.HotelDetails.Element("Facilities").Descendants("Facility"))
                               {
                                   HotelInfo.HotelServices += flt.Attribute("Code").Value + "#";
                               }
                           if (Htl.HotelDetails.Element("RoomFacilities") != null)
                               foreach (var flt in Htl.HotelDetails.Element("RoomFacilities").Descendants("Facility"))
                               {
                                   HotelInfo.HotelServices += flt.Attribute("Code").Value + "#";
                               }
                       }
                       catch (Exception ex)
                       {
                           HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode + "-HotelServices");
                       }
                      //Links  ImageLinks
                      HotelInfo.ThumbImg = "Images/Hotel/NoImage.jpg"; HotelInfo.Latitude = ""; HotelInfo.Longitude = "";
                      try
                      {
                          if (Htl.HotelDetails.Element("Links") != null)
                          {
                              if (Htl.HotelDetails.Element("Links").Element("ImageLinks") != null)
                                  HotelInfo.ThumbImg = Htl.HotelDetails.Element("Links").Element("ImageLinks").Element("ImageLink").Element("ThumbNail").Value;
                          }
                          if (Htl.HotelDetails.Element("GeoCodes") != null)
                          {
                              if (Htl.HotelDetails.Element("GeoCodes").Element("Latitude") != null)
                              {
                                  HotelInfo.Latitude = Htl.HotelDetails.Element("GeoCodes").Element("Latitude").Value;
                                  HotelInfo.Longitude = Htl.HotelDetails.Element("GeoCodes").Element("Longitude").Value;
                              }
                          }
                      }
                      catch (Exception ex)
                      {
                          HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode + "-Images");
                      }
                  }
              }
              else
              {
                  HotelInfo.Address = "Address not found";
                  HotelInfo.HotelServices = "";
                  HotelInfo.ThumbImg = "Images/Hotel/NoImage.jpg";
                  HotelInfo.Latitude = ""; HotelInfo.Longitude = "";
                  HotelInfo.HotelDiscription = "";
                  HotelDA objhtlDa = new HotelDA();
                  int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", CityCode + "_" + HotelCode, document.ToString(), "GetGTAHotelInformation_Nodata", "HotelInsert");
              }
          }
          catch (Exception ex)
          {
              HotelInfo.Address = "Address not found";
              HotelInfo.HotelServices = "";
              HotelInfo.ThumbImg = "Images/Hotel/NoImage.jpg";
              HotelInfo.Latitude = ""; HotelInfo.Longitude = "";
              HotelInfo.HotelDiscription = "";

              HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode);
              HotelDA objhtlDa = new HotelDA();
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", CityCode + "_" + HotelCode, "GetGTAHotelInformation", "GetGTAHotelInformation_Exception", "HotelInsert");
          }
          return HotelInfo;
      }
      private string GTAItemInformationRespnse(HotelSearch SearchDetails, string ItemCode, string CityCode)
      {
          string GTAResponse = "";
          try
          {
              SearchDetails = HtlReq.SearchItemInformationRequest(SearchDetails, ItemCode, CityCode);
              GTAResponse = GTAPostXml(SearchDetails.GTAURL, SearchDetails.GTAItemInformationReq);
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + ItemCode);
              HotelDA objhtlDa = new HotelDA();
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.GTAItemInformationReq, GTAResponse, "GTA", "HotelInsert");

          }
          return GTAResponse;
      }
      public RoomComposite GTARoomDetals(HotelSearch SearchDetails)
      {
          RoomComposite objRoomDetals = new RoomComposite();
          List<RoomList> objRoomList = new List<RoomList>(); SelectedHotel HotelDetail = new SelectedHotel();
          HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();
          try
          {
              XDocument document = XDocument.Parse(SearchDetails.GTA_HotelSearchRes.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
              var RoomsDetails = (from Hotel in document.Descendants("HotelDetails").Elements("Hotel")
                                  where Hotel.Element("Item").Attribute("Code").Value == SearchDetails.HtlCode && Hotel.Element("City").Attribute("Code").Value == SearchDetails.HotelCityCode
                                  select new { HotelName = Hotel.Element("Item").Value, RoomCategory = Hotel.Element("RoomCategories") }).ToList();

              if (RoomsDetails.Count > 0)
              {
                  #region Hotel Details
                  HotelDetail = GetGTAHotelInformation_Room(SearchDetails.HtlCode, SearchDetails.HotelCityCode);
                  #endregion
                  #region Room Details
                  int j = 0;
                  foreach (var Rooms in RoomsDetails[0].RoomCategory.Descendants("RoomCategory"))
                  {
                      try
                      {
                          string sharebeding = "";
                          if (Rooms.Element("SharingBedding") != null)
                              sharebeding = Rooms.Element("SharingBedding").Value;
                          string Org_RoomRateStr = "", MrkRoomrateStr = "";
                          //Hotel Price
                          decimal Rate_Org = Convert.ToDecimal(Rooms.Element("ItemPrice").Value) * SearchDetails.CurrancyRate;
                          MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, SearchDetails.StarRating.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, Rate_Org, SearchDetails.GTA_servicetax);

                          //Hotel Discount strat
                          decimal GrassDiscount = 0; string discountmsg = "", Inclusion = "";
                          if (Rooms.Element("ItemPrice").Attribute("GrossWithoutDiscount") != null)
                          {
                              GrassDiscount = objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, Convert.ToDecimal(Rooms.Element("ItemPrice").Attribute("GrossWithoutDiscount").Value) * SearchDetails.CurrancyRate, SearchDetails.GTA_servicetax);
                          }
                          if (Rooms.Element("Offer") != null)
                              discountmsg = Rooms.Element("Offer").Value;
                          //Hotel Discount End
                          foreach (var roomPric in Rooms.Elements("HotelRoomPrices").Elements("HotelRoom"))
                          {
                              try
                              {
                                  if (roomPric.Attribute("NumberOfRooms") != null)
                                  {
                                      Org_RoomRateStr += roomPric.Attribute("Code").Value + "-" + roomPric.Attribute("NumberOfRooms").Value + "-" + roomPric.Element("RoomPrice").Attribute("Gross").Value + "/";
                                      MrkRoomrateStr += roomPric.Attribute("Code").Value + "-" + roomPric.Attribute("NumberOfRooms").Value + "-" + MarkList.TotelAmt + "/";
                                  }
                                  else
                                  {
                                     Org_RoomRateStr += roomPric.Attribute("Code").Value + "-1-" + roomPric.Element("RoomPrice").Attribute("Gross").Value + "/";
                                     MrkRoomrateStr += roomPric.Attribute("Code").Value + "-1-" + MarkList.TotelAmt + "/";
                                  }
                              }
                              catch (Exception ex)
                              {
                                  HotelDA.InsertHotelErrorLog(ex, "roomPric " + SearchDetails.HotelCityCode + "_" + RoomsDetails[0].HotelName);
                              }
                          }
                          //Display Hotel Inclution strat  
                          try
                          {
                              if (Rooms.Element("Meals").Element("Basis") != null)
                              {
                                  if (Rooms.Element("Meals").Element("Basis").Value == "None" || Rooms.Element("Meals").Element("Basis").Value == "Room only")
                                      Inclusion = "Room Only";
                                  else
                                      Inclusion = Rooms.Element("Meals").Element("Basis").Value + ", " + Rooms.Element("Meals").Element("Breakfast").Value;
                              }
                          }
                          catch (Exception ex)
                          {
                              Inclusion = Rooms.Element("Meals").Element("Basis").Value;
                              HotelDA.InsertHotelErrorLog(ex, "RoomMeals_" + SearchDetails.HotelCityCode  + "_" + RoomsDetails[0].HotelName);
                          }
                          //Display Hotel Inclution End
                          //Display Hotel Essential Information start 
                          string EssentialInformation = ""; int i = 0;
                          foreach (var EssentialInfo in Rooms.Elements("EssentialInformation").Elements("Information"))
                          {
                              if (EssentialInfo.Element("Text").Value != null)
                              {
                                  i++;
                                  if (i > 1)
                                      EssentialInformation += "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                  EssentialInformation += EssentialInfo.Element("Text").Value;
                                  if (EssentialInfo.Element("DateRange").Element("FromDate") != null)
                                      EssentialInformation += " From " + EssentialInfo.Element("DateRange").Element("FromDate").Value;

                                  if (EssentialInfo.Element("DateRange").Element("ToDate").Value != null)
                                      EssentialInformation += " To " + EssentialInfo.Element("DateRange").Element("ToDate").Value;
                              }
                          }
                          if (EssentialInformation != "")
                          {
                              EssentialInformation = "<span class='bld lft' >Essential Information: </span>&nbsp; " + EssentialInformation;
                          }
                          //Display Hotel Essential Information End
                          string RoomDiscription = "";
                          //Room Disciption
                          try
                          {
                              if (j <= HotelDetail.RoomcatList.Count - 1)
                              {
                                  string[] roomdisArr = (string[])HotelDetail.RoomcatList[j];
                                  roomdisArr.Select(x => x[0].ToString() == Rooms.Attribute("Id").Value);
                                  RoomDiscription += roomdisArr[2];
                              }
                          }
                          catch (Exception ex)
                          {
                              HotelDA.InsertHotelErrorLog(ex, "RoomcatList " + SearchDetails.HotelCityCode + "_" + RoomsDetails[0].HotelName);
                          }
                          j++;

                          objRoomList.Add(new RoomList
                          {
                              HotelCode=SearchDetails.HtlCode,
                              RatePlanCode = Rooms.Attribute("Id").Value,
                              RoomTypeCode = SearchDetails.HotelCityCode,
                              RoomName = Rooms.Element("Description").Value,
                              discountMsg = discountmsg,
                              DiscountAMT = GrassDiscount,
                              Total_Org_Roomrate = Rate_Org,
                              TotalRoomrate = MarkList.TotelAmt,
                              AdminMarkupPer = MarkList.AdminMrkPercent,
                              AdminMarkupAmt = MarkList.AdminMrkAmt,
                              AdminMarkupType = MarkList.AdminMrkType,
                              AgentMarkupPer = MarkList.AgentMrkPercent,
                              AgentMarkupAmt = MarkList.AgentMrkAmt,
                              AgentMarkupType = MarkList.AgentMrkType,
                              AgentServiseTaxAmt = MarkList.AgentServiceTaxAmt,
                              V_ServiseTaxAmt = MarkList.VenderServiceTaxAmt,
                              AmountBeforeTax = 0, Taxes = 0, MrkTaxes = 0, ExtraGuest_Charge = 0,
                              Smoking = sharebeding,
                              inclusions = Inclusion,
                              CancelationPolicy = SetHotelPolicyForRoom(Rooms.Element("ChargeConditions"), SearchDetails.CurrancyRate),
                              OrgRateBreakups = Org_RoomRateStr,
                              MrkRateBreakups = MrkRoomrateStr,
                              DiscRoomrateBreakups ="",
                              EssentialInformation = EssentialInformation,
                              RoomDescription = RoomDiscription,
                              Provider = "GTA", RoomImage = ""
                          });
                      }
                      catch (Exception ex)
                      {
                          objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
                          HotelDA.InsertHotelErrorLog(ex, "Roomsadding " + SearchDetails.HotelCityCode + "_" + RoomsDetails[0].HotelName);
                      }
                  }
                  #endregion
                  objRoomDetals.RoomDetails = objRoomList;
                  objRoomDetals.SelectedHotelDetail = HotelDetail;
              }
              else
              {
                  objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = "Room Details Not found" });
                  HotelDA objhtlDa = new HotelDA();
                  int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.GTA_HotelSearchReq, SearchDetails.GTA_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
              }
          }
          catch (Exception ex)
          {
              objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
              HotelDA objhtlDa = new HotelDA();
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.GTA_HotelSearchReq, SearchDetails.GTA_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
              HotelDA.InsertHotelErrorLog(ex, "GTARoomDetals " + SearchDetails.HotelCityCode);
          }
          return objRoomDetals;
      }
      public SelectedHotel GetGTAHotelInformation_Room(string HotelCode, string CityCode)
      {
          SelectedHotel HotelDetail = new SelectedHotel();
          XDocument document;
          try
          {
              document = XDocument.Load(System.Configuration.ConfigurationManager.AppSettings["ExtractFilePath"] + "\\" + CityCode + "_" + HotelCode + ".xml");
              var ItemInformations = (from Hotel in document.Descendants("ItemDetail")
                  where Hotel.Element("City").Attribute("Code").Value == CityCode && Hotel.Element("Item").Attribute("Code").Value == HotelCode
                                      select new { HotelDetails = Hotel.Element("HotelInformation") }).ToList();
              ArrayList RoomcatList = new ArrayList();
              if (ItemInformations.Count > 0)
              {
                  foreach (var Htl in ItemInformations)
                  {
                      string Adress = "Address Not Found", Telephones = "";
                      try
                      {
                        Adress = Htl.HotelDetails.Element("AddressLines").Element("AddressLine1").Value;
                        if (Htl.HotelDetails.Element("AddressLines").Element("AddressLine2") != null)
                            Adress += ", " + Htl.HotelDetails.Element("AddressLines").Element("AddressLine2").Value;
                        if (Htl.HotelDetails.Element("AddressLines").Element("AddressLine3") != null)
                            Adress += ", " + Htl.HotelDetails.Element("AddressLines").Element("AddressLine3").Value;
                        if (Htl.HotelDetails.Element("AddressLines").Element("AddressLine4") != null)
                            Adress += ", " + Htl.HotelDetails.Element("AddressLines").Element("AddressLine4").Value;

                        if (Htl.HotelDetails.Element("AddressLines").Element("Telephone") != null)
                            Telephones = Htl.HotelDetails.Element("AddressLines").Element("Telephone").Value;
                      }
                      catch (Exception ex)
                      {
                          HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode + "-Addresss");
                      }
                      HotelDetail.HotelContactNo = Telephones;
                      HotelDetail.HotelAddress = Adress;

                      string hoteldiscription = "", Attract = "", RoomfltStr = "", facliStr = "";
                      try
                      {
                          if (Htl.HotelDetails.Element("AreaDetails") != null)
                          foreach (var areas in Htl.HotelDetails.Element("AreaDetails").Descendants("AreaDetail"))
                          {
                              Attract += "<div class='check'>" + areas.Value + "</div>";
                          }
                      }
                      catch (Exception ex)
                      {
                          HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode + "-AreaDetails");
                      }
                      HotelDetail.Attraction = Attract;
                      try
                      {
                          if (Htl.HotelDetails.Element("Reports") != null)
                              foreach (var Report in Htl.HotelDetails.Element("Reports").Descendants("Report"))
                              {
                                  hoteldiscription += "<div style='padding: 4px 0;'><span style='text-transform:uppercase; font-size:13px;font-weight: bold;color:#B21E55;'>" + Report.Attribute("Type").Value + ": </span> " + Report.Value + "</div><hr />";
                              }
                      }
                      catch (Exception ex)
                      {
                          HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode + "-Reports");
                      }
                      HotelDetail.HotelDescription = hoteldiscription;

                      //Room Catagary and discription 
                      try
                      {
                          if (Htl.HotelDetails.Element("RoomCategories") != null)
                              foreach (var RoomCategary in Htl.HotelDetails.Element("RoomCategories").Descendants("RoomCategory"))
                              {
                                  string RoomDisc = "";
                                  if (RoomCategary.Element("RoomDescription") != null)
                                      RoomDisc = RoomCategary.Element("RoomDescription").Value;

                                  string[] arrNumbers = new string[] { RoomCategary.Attribute("Id").Value, RoomCategary.Element("Description").Value, RoomDisc };
                                  RoomcatList.Add(arrNumbers);
                                  //objRoomCatList.Add(new RoomCatgryDetails
                                  //{
                                  //    Id = RoomCategary.Attribute("Id").Value,
                                  //    Description = RoomCategary.Element("Description").Value,
                                  //    RoomDescription = RoomDisc
                                  //});
                              }
                      }
                      catch (Exception ex)
                      {
                          HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode + "-RoomCategories");
                      }
                      HotelDetail.RoomcatList = RoomcatList;
                      //Room Facility RoomFacilities
                      try
                      {
                          if (Htl.HotelDetails.Element("RoomFacilities") != null)
                              foreach (var Roomflt in Htl.HotelDetails.Element("RoomFacilities").Descendants("Facility"))
                              {
                                  RoomfltStr += "<div class='check'>" + Roomflt.Value.Trim() + "</div>";
                              }
                      }
                      catch (Exception ex)
                      {
                          HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode + "-RoomFacilities");
                      }
                      HotelDetail.RoomAmenities = RoomfltStr;
                      //Hotel facility Facilities
                      try
                      {
                          if (Htl.HotelDetails.Element("Facilities") != null)
                              foreach (var flt in Htl.HotelDetails.Element("Facilities").Descendants("Facility"))
                              {
                                  facliStr += "<div class='check1'>" + flt.Value.Trim() + "</div>";
                                 // HotelService += SetHotelService(flt.Attribute("Code").Value);
                              }
                      }
                      catch (Exception ex)
                      {
                          HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode + "-Facilities");
                      }
                      HotelDetail.HotelAmenities = facliStr;
                      string imgdiv = "", LatiLongi = "";
                      //Images Links ImageLinks
                     ArrayList Images = new ArrayList();
                      try
                      {
                         int im = 0;
                         if (Htl.HotelDetails.Element("Links") != null)
                         {
                             if (Htl.HotelDetails.Element("Links").Element("ImageLinks") != null)
                             {
                                 foreach (var img in Htl.HotelDetails.Element("Links").Element("ImageLinks").Descendants("ImageLink"))
                                 {
                                     im++;
                                     imgdiv += "<img id='img" + im + "' src='" + img.Element("Image").Value + "' onmouseover='return ShowHtlImg(this);' title='" + img.Element("Text").Value + "' alt='' class='imageHtlDetailsshow' />";
                                     Images.Add(img.Element("Image").Value);
                                 }
                             }
                             else
                                 Images.Add("Images/Hotel/NoImage.jpg");
                         }
                         else
                             Images.Add("Images/Hotel/NoImage.jpg");
                          HotelDetail.ThumbnailUrl = Images[0].ToString();
                      }
                      catch (Exception ex)
                      {
                          HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode + "-ImageLinks");
                      }
                       HotelDetail.HotelImage = imgdiv;
                       //GeoCodes
                       if (Htl.HotelDetails.Element("GeoCodes") != null)
                           if (Htl.HotelDetails.Element("GeoCodes").Element("Latitude") != null)
                              LatiLongi = Htl.HotelDetails.Element("GeoCodes").Element("Latitude").Value + "," + Htl.HotelDetails.Element("GeoCodes").Element("Longitude").Value;
                      HotelDetail.Lati_Longi = LatiLongi;
                      HotelDetail.StarRating = Htl.HotelDetails.Element("StarRating").Value;
                  }
              }
              else
              {
                HotelDetail.HotelAddress = "";
                HotelDetail.HotelContactNo = "";
                HotelDetail.Attraction = "";
                HotelDetail.HotelDescription = "";
                HotelDetail.RoomAmenities = "";
                HotelDetail.HotelAmenities = "";
                HotelDetail.RoomcatList = RoomcatList;
                HotelDetail.Lati_Longi = "";
                HotelDetail.HotelImage = "<li><a href='Images/NoImage.jpg'><img src='Images/NoImage.jpg' width='40px' height='40px' title='Image not found' /></a></li>";

                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", CityCode, HotelCode, "GetGTAHotelInformation_Room_NoData", "HotelInsert");
              }
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, CityCode + "_" + HotelCode + "-GetGTAHotelInformation_Room");
              HotelDA objhtlDa = new HotelDA();
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", CityCode, HotelCode, "GetGTAHotelInformation_Room_Exception", "HotelInsert");

              HotelDetail.HotelAddress = "";
              HotelDetail.HotelContactNo = "";
              HotelDetail.Attraction = "";
              HotelDetail.HotelDescription = "";
              HotelDetail.RoomAmenities = "";
              HotelDetail.HotelAmenities = "";
              HotelDetail.Lati_Longi = "";
              HotelDetail.HotelImage = "<li><a href='Images/NoImage.jpg'><img src='Images/NoImage.jpg' width='40px' height='40px' title='Image not found' /></a></li>";

          }
          return HotelDetail;
      }
      public HotelBooking GTAHotelsPreBooking(HotelBooking HotelDetail)
      {
          try
          {
              if (HotelDetail.GTAURL != null)
              {
                 HotelDetail = HtlReq.PreBookHotelPriceRequestXml(HotelDetail);
                 HotelDetail.ProBookingRes = GTAPostXml(HotelDetail.GTAURL, HotelDetail.ProBookingReq);

                  if (!HotelDetail.ProBookingRes.Contains("<Errors>"))
                  {
                      XDocument document = XDocument.Parse(HotelDetail.ProBookingRes.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                      var Hotels_temps = (from Hotel in document.Descendants("RoomCategory")
                                          select new { ItemPrice = Hotel.Element("ItemPrice"), Confirmation = Hotel.Element("Confirmation") }).ToList();

                      foreach (var Htl in Hotels_temps)
                      {
                          if (HotelDetail.Total_Org_Roomrate >= Convert.ToDecimal(Htl.ItemPrice.Value) * HotelDetail.CurrancyRate && Htl.Confirmation.Value == "AVAILABLE")
                          {
                              HotelDetail.ProBookingID = "true";
                          }
                          else
                          {
                           //   HotelDetail.ReferenceNo = "Rate Not matching";
                              HotelDetail.BookingID = "";
                              HotelDetail.ProBookingID = "false";
                          }
                      }
                  }
                  else
                  {
                    //  HotelDetail.ReferenceNo = "Exception";
                      HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
                  } 
              }
              else
              {
                   // HotelDetail.ReferenceNo = "BookingNotAlowed";
                    HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
              }
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "GTAHotelsPreBooking");
             // HotelDetail.ReferenceNo = ex.Message;
              HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
          }
          return HotelDetail;
      }
      public HotelBooking GTAHotelsBooking(HotelBooking HotelDetail)
      {
          try
          {
              HotelDetail = HtlReq.GtaHotelBookingReuestXML(HotelDetail);
              HotelDetail.BookingConfRes = GTAPostXml(HotelDetail.GTAURL, HotelDetail.BookingConfReq);
              if (!HotelDetail.BookingConfRes.Contains("<Errors>"))
              {
                  XDocument document = XDocument.Parse(HotelDetail.BookingConfRes.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                  var HtlBooking = (from book in document.Descendants("BookingResponse")
                                    where book.Element("BookingItems").Element("BookingItem").Element("ItemConfirmationReference").Value != ""
                                    select new
                                    {
                                        BookingStatus = book.Element("BookingStatus"), BookingReferences = book.Element("BookingReferences"),
                                        bookConfirmation = book.Element("BookingItems").Element("BookingItem").Element("ItemConfirmationReference")
                                    }).ToList();

                  foreach (var Htlbook in HtlBooking)
                  {
                      var abc = (from Bid in Htlbook.BookingReferences.Descendants("BookingReference") where Bid.Attribute("ReferenceSource").Value == "api" select new { BookingID = Bid }).ToList();
                      foreach (var BookingRef in abc)
                      {
                          HotelDetail.BookingID = BookingRef.BookingID.Value;
                      }
                      HotelDetail.Status = Htlbook.BookingStatus.Value;
                      HotelDetail.ProBookingID = Htlbook.bookConfirmation.Value;
                  }
              }
              else
              {
                  HotelDetail.ProBookingID = "Exception";
                  HotelDetail.BookingID = "";
              }             
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "GTAHotelsBooking");
              HotelDetail.ProBookingID = ex.Message;
          }
          return HotelDetail;
      }
      protected string SetHotelService(string Services)
      {
          string InclImg = "";
          try
          {
              switch (Services)
              {
                  case "*CP":
                  case "*HP":
                      InclImg = "<img src='../Images/Hotel/Facility/Parking.png' title='Parking' style='margin:2px;' />";
                      break;
                  case "*PT":
                  case "*RS":
                      InclImg = "<img src='../Images/Hotel/Facility/lounge.png' title='Room Service' style='margin:2px;' />";
                      break;
                  case "*DD":
                      InclImg = "<img src='../Images/Hotel/Facility/Phone.png' title='Direct dial phone' style='margin:2px;' />";
                      break;
                  case "*OP":
                      InclImg = "<img src='../Images/Hotel/Facility/swimming.png' title='Outdoor Swimming Pool' style='margin:2px;' />";
                      break;
                  case "*IP":
                      InclImg = "<img src='../Images/Hotel/Facility/jacuzzi.png' title='Indoor Swimming Pool' style='margin:2px;' />";
                      break;
                  case "*TA":
                      InclImg = "<img src='../Images/Hotel/Facility/travel_desk.png' title='Travel agency facilities' style='margin:2px;' />";
                      break;
                  case "*LY":
                      InclImg = "<img src='../Images/Hotel/Facility/laundary.png' title='Laundry facilities' style='margin:2px;' />";
                      break;
                  case "*CR":
                      InclImg = "<img src='../Images/Hotel/Facility/Airport_transfer.png' title='Car rental facilities' style='margin:2px;' />";
                      break;
                  case "*BC":
                      InclImg = "<img src='../Images/Hotel/Facility/Banquet_hall.png' title='Business centre' style='margin:2px;' />";
                      break;
                  case "*DF":
                      InclImg = "<img src='../Images/Hotel/Facility/handicap.png' title='Disabled facilities' style='margin:2px;' />";
                      break;
                  case "*GY":
                      InclImg = "<img src='../Images/Hotel/Facility/health_club.png' title='Gymnasium' style='margin:2px;' />";
                      break;
                  case "*LF":
                      InclImg = "<img src='../Images/Hotel/Facility/elevator.png' title='Lifts' style='margin:2px;' />";
                      break;
                  case "*IN":
                      InclImg = "<img src='../Images/Hotel/Facility/Internet.png' title='Internet' style='margin:2px;' />";
                      break;
                  case "*MB":
                      InclImg = "<img src='../Images/Hotel/Facility/bar.png' title='Mini bar' style='margin:2px;' />";
                      break;
                  case "*BS":
                      InclImg = "<img src='../Images/Hotel/Facility/babysitting.png' title='Baby sitting' style='margin:2px;' />";
                      break;
                  case "*BP":
                      InclImg = "<img src='../Images/Hotel/Facility/beauty.png' title='Beauty parlour' style='margin:2px;' />";
                      break;
                  case "*SA":
                      InclImg = "<img src='../Images/Hotel/Facility/sauna.png' title='Sauna' style='margin:2px;' />";
                      break;
                  case "*LS":
                      InclImg = "<img src='../Images/Hotel/Facility/lobby.png' title='Lobby' style='margin:2px;' />";
                      break;
                  case "*TE":
                      InclImg = "<img src='../Images/Hotel/Facility/tennis.png' title='Tennis' style='margin:2px;' />";
                      break;
                  case "*GF":
                      InclImg = "<img src='../Images/Hotel/Facility/golf.png' title='Golf' style='margin:2px;' />";
                      break;
              }
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "");
          }
          return InclImg;
      }
     
      // public string SetHotelPolicy(XElement ChargeCondition, decimal CurrancyRate, decimal servicetax, decimal AdmMrkPer, decimal agtMrkPer, string AdMrkType, string AgtMrkType)
      public string SetHotelPolicy(XElement ChargeCondition, HotelSearch SearchDetails)
      {
          string StrPolicy = "HotelSearch SearchDetails", StrPoli = "", StrPoli2 = "";
          HotelMarkups objHtlMrk = new HotelMarkups(); HotelDA objhtlDa = new HotelDA(); DataSet MarkupDS = new DataSet();
          try
          {
            //  MarkupDS = objhtlDa.GetHtlMarkup("", SearchDetails.AgentID, SearchDetails.Country, SearchDetails.SearchCity, "", SearchDetails.HtlType, "", "", 0, "SEL");
        
              XDocument document = XDocument.Parse(ChargeCondition.ToString());
              var Htlpoli = (from Policy in document.Descendants("ChargeConditions") select new { Policies = Policy }).ToList();

              foreach (var Htl in Htlpoli)
              {
                  foreach (var Htl1 in Htl.Policies.Descendants("ChargeCondition"))
                  {
                      #region  cancellation
                      if (Htl1.Attribute("Type").Value == "cancellation")
                      {
                          foreach (var canPoly in Htl1.Descendants("Condition"))
                          {
                              if (canPoly.Attribute("Charge") != null)
                              {
                                  if (canPoly.Attribute("Charge").Value == "true")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                      {
                                         // decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * SearchDetails.CurrancyRate, SearchDetails.servicetax);
                                          StrPoli2 += "<li> Cancellation charges <img src='Images/htlrs.png' class='pagingSize' /> " + Math.Round(Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * SearchDetails.CurrancyRate).ToString() + " /- will be applicable.</li>";
                                          //StrPoli2 += "<li> Cancellation charges  " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + " /- will be applicable.</li>";
                                      }
                                      else
                                      {
                                         // decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * SearchDetails.CurrancyRate, SearchDetails.servicetax);
                                          StrPoli += "<li> Cancellation done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                          StrPoli += " the cancellation charge will be <img src='Images/htlrs.png' class='pagingSize' /> " +  Math.Round(Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * SearchDetails.CurrancyRate).ToString() + "/-.</li>";
                                         // StrPoli += " the cancellation charge will be " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + "/-.</li>";
                                      }
                                  }
                                  else if (canPoly.Attribute("Charge").Value == "false")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2 += "<li> Cancellation done on " + canPoly.Attribute("FromDate").Value + " or earlier, cancellation charges will not applicable.</li>";
                                      else
                                      {
                                          StrPoli += "<li> Cancellation done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                          StrPoli += " cancellation charges will not applicable.</li>";
                                      }
                                  }
                              }
                              if (canPoly.Attribute("Allowable") != null)
                              {
                                  if (canPoly.Attribute("Allowable").Value == "false")
                                  {
                                      StrPoli += "<li> Cancellation not allowed";
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2 += ".</li>";
                                      else
                                          StrPoli += " " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value + " </li>";
                                  }
                              }
                          }

                      }
                      #endregion
                  }
              }
              StrPolicy = "<span style='font-size:13px;font-weight: bold;'>Cancellation Policies</span><span>" + StrPoli.Replace("amp;", "and") + StrPoli2 + "</span>";
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "SetHotelPolicy");
              StrPolicy += ex.Message;
          }
          return StrPolicy.Replace("amp;", "and");
      }
      protected string SetHotelPolicyForRoom(XElement ChargeCondition, decimal CurrancyRate)
      {
          string StrPolicy = "", StrPoli = "", StrPoli2 = "", StrPoliM = "", StrPoli2M = ""; HotelMarkups objHtlMrk = new HotelMarkups();
          try
          {
              XDocument document = XDocument.Parse(ChargeCondition.ToString());
              var Htlpoli = (from Policy in document.Descendants("ChargeConditions") select new { Policies = Policy }).ToList();

              foreach (var Htl in Htlpoli)
              {
                  foreach (var Htl1 in Htl.Policies.Descendants("ChargeCondition").TakeWhile(x => x.Attribute("Type").Value == "cancellation"))
                  {
                      #region  cancellation
                          foreach (var canPoly in Htl1.Descendants("Condition"))
                          {
                              if (canPoly.Attribute("Charge") != null)
                              {
                                  if (canPoly.Attribute("Charge").Value == "true")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                      {
                                         // decimal cancelAmt = objHtlMrk.DiscountMarkupCalculation(AdmMrkPer, agtMrkPer, AdMrkType, AgtMrkType, Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * CurrancyRate, servicetax);
                                          StrPoli2 += "<li> Cancellation charges Rs. " + Math.Round(Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * CurrancyRate).ToString() + " /- will be applicable.</li>";
                                         // StrPoli2 += "<li> Cancellation charges  " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + " /- will be applicable.</li>";
                                      }
                                      else
                                      {
                                         // decimal cancelAmt = objHtlMrk.DiscountMarkupCalculation(AdmMrkPer, agtMrkPer, AdMrkType, AgtMrkType, Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * CurrancyRate, servicetax);
                                          StrPoli += "<li> Cancellation done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                          StrPoli += " the cancellation charge will be Rs. " + Math.Round(Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * CurrancyRate).ToString() + "/-.</li>";
                                         //StrPoli += " the cancellation charge will be " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + "/-.</li>";
                                      }
                                  }
                                  else if (canPoly.Attribute("Charge").Value == "false")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2 += "<li> Cancellation done on " + canPoly.Attribute("FromDate").Value + " or earlier, cancellation charges will not applicable.</li>";
                                      else
                                      {
                                          StrPoli += "<li> Cancellation done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                          StrPoli += " cancellation charges will not applicable.</li>";
                                      }
                                  }
                              }
                              if (canPoly.Attribute("Allowable") != null)
                              {
                                  if (canPoly.Attribute("Allowable").Value == "false")
                                  {
                                      StrPoli += "<li> Cancellation not allowed";
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2 += ".</li>";
                                      else
                                          StrPoli += " " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value + " </li>";
                                  }
                              }
                          }
                      #endregion  
                  }
                  foreach (var Htl1 in Htl.Policies.Descendants("ChargeCondition").TakeWhile(x => x.Attribute("Type").Value == "amendment"))
                  {
                      #region  Modification
                          foreach (var canPoly in Htl1.Descendants("Condition"))
                          {
                              if (canPoly.Attribute("Charge") != null)
                              {
                                  if (canPoly.Attribute("Charge").Value == "true")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                      {
                                         // decimal cancelAmt = objHtlMrk.DiscountMarkupCalculation(AdmMrkPer, agtMrkPer, AdMrkType, AgtMrkType, Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * CurrancyRate, servicetax);
                                          StrPoli2M += " <li> Modification charges Rs. " + Math.Round(Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * CurrancyRate).ToString() + "/- will be applicable.</li>";
                                          // StrPoli2M += " <li> Modification charges " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + "/- will be applicable.</li>";
                                      }
                                      else
                                      {
                                          //decimal cancelAmt = objHtlMrk.DiscountMarkupCalculation(AdmMrkPer, agtMrkPer, AdMrkType, AgtMrkType, Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * CurrancyRate, servicetax);
                                          StrPoliM += " <li> Modification done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                          StrPoliM += " modification charge will be Rs. " + Math.Round(Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * CurrancyRate).ToString() + "/-.</li>";
                                          //StrPoliM += " modification charge will be " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + "/-.</li>";
                                      }
                                  }
                                  else if (canPoly.Attribute("Charge").Value == "false")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2M += "<li> Modification done on " + canPoly.Attribute("FromDate").Value + " or earlier, modification charges will not applicable.</li>";
                                      else
                                      {
                                          StrPoliM += "<li> Modification done between " + canPoly.Attribute("ToDate") + "  to " + canPoly.Attribute("FromDate");
                                          StrPoliM += " modification charges will not applicable.</li>";
                                      }
                                  }
                              }
                              if (canPoly.Attribute("Allowable") != null)
                              {
                                  if (canPoly.Attribute("Allowable").Value == "false")
                                  {
                                      StrPoli += "<li> Modification not allowed";
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2 += ".</li>";
                                      else
                                          StrPoli += " " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value + " </li>";
                                  }
                              }
                          }
                      #endregion
                  }
              }
              StrPolicy = "<div><span>" + StrPoli.Replace("amp;", "and") + StrPoli2 + "</span> <span>" + StrPoliM + StrPoli2M + "</span></div>";
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "");
              StrPolicy += ex.Message;
          }
          return StrPolicy.Replace("amp;", "and");
      }

      public string SearchHotelBookingItem(HotelSearch SearchDetails) 
       {
        string GTAResponse= "";
           try
           {
                SearchDetails = HtlReq.SearchBookingItemRequest(SearchDetails);
                   if(SearchDetails.HtlType=="XMLtype")
                       GTAResponse = GTAPostXml(SearchDetails.GTAURL, SearchDetails.GTA_HotelSearchReq);
                   else
                       GTAResponse = SearchGTABooking(GTAPostXml(SearchDetails.GTAURL, SearchDetails.GTA_HotelSearchReq));

           }
           catch(Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "SearchHotelBookingItem");
           }
           return GTAResponse;
      }
      public string SearchGTABooking(string GTAResponse)
    {
	    string my_table = "<table border='1' cellspacing='4' cellpadding='4' align='center' style='border: thin solid #999999; border=collapse:collapse;width:100%;' >";
        try
        {
            my_table += "<tr>";
	        my_table += "<td style='padding: 4px;  background-color: #eee;font-weight: bold; font-family: arial, Helvetica, sans-serif;' align='center'>Order Id</td>";
	        my_table += "<td style='padding: 4px;  background-color: #eee;font-weight: bold; font-family: arial, Helvetica, sans-serif;' align='center'>Booking ID</td>";
	        my_table += "<td style='padding: 4px;  background-color: #eee;font-weight: bold; font-family: arial, Helvetica, sans-serif;' align='center'>Booking Date</td>";
	        my_table += "<td style='padding: 4px;  background-color: #eee;font-weight: bold; font-family: arial, Helvetica, sans-serif;' align='center'>Checkin Date</td>";
	        my_table += "<td style='padding: 4px;  background-color: #eee;font-weight: bold; font-family: arial, Helvetica, sans-serif;' align='center'>Purchase Cost</td>";
	        my_table += "<td style='padding: 4px;  background-color: #eee;font-weight: bold; font-family: arial, Helvetica, sans-serif;' align='center'>Status</td>";
	        my_table += "<td style='padding: 4px;  background-color: #eee;font-weight: bold; font-family: arial, Helvetica, sans-serif;' align='center'>Primary Guest Name</td>";
	        my_table += "</tr>";
            //.Elements("Booking")
		    XDocument document = XDocument.Parse(GTAResponse);
            var Hotel_Search = (from BookedHotel in document.Descendants("Response").Descendants("ResponseDetails").Descendants("SearchBookingResponse").Elements("Bookings")
                                select new { BookedHotel }).ToList();
             foreach (var Htlbook in Hotel_Search[0].BookedHotel.Descendants("Booking")) 
            {
			    my_table += "<tr>";
                foreach (var BookingRef in Htlbook.Elements("BookingReferences").Elements("BookingReference")) 
                {
				    if (BookingRef.Value == "client")
					    my_table += "<td style='padding:4px;' >" + BookingRef.Value + "</td>";
                    if (BookingRef.Value == "api")
					    my_table += "<td style='padding:4px;' >" + BookingRef.Value + "</td>";
			    }
			    my_table += "<td style='padding: 4px;' >" + Htlbook.Element("BookingCreationDate").Value + "</td>";
                my_table += "<td style='padding: 4px;' >" + Htlbook.Element("BookingDepartureDate").Value + "</td>";
                my_table += "<td style='padding: 4px;' >" + Htlbook.Element("BookingPrice").Attribute("Currency").Value + " " + Htlbook.Element("BookingPrice").Attribute("Nett").Value + "</td>";
                my_table += "<td style='padding: 4px;' >" + Htlbook.Element("BookingStatus").Value + "</td>";
                my_table += "<td style='padding: 2px;' >" + Htlbook.Element("BookingName").Value + "</td>";
			    my_table += "</tr>";
		    }
		    my_table += "<tr><td style='padding: 4px; font-size: 13px;font-weight: bold;'> Totel Hotel: " + Hotel_Search.Count + "</td></tr>";
		    my_table += "</table>";
	    }
        catch (Exception ex) 
        {
            HotelDA.InsertHotelErrorLog(ex, "");
		    my_table += "<tr><td>No records Found<td/></tr></table>";
	    }
        return my_table;
}

      public string HotelItemDownlode(HotelSearch SearchDetails)
      {
          return ItemDownLoad_PostXml(SearchDetails.GTAURL, HtlReq.ItemInformationDownloadRequest(SearchDetails));
      }
      protected string ItemDownLoad_PostXml(string url, string xml)
      {
          try
          {
              HttpWebRequest HttpWebReq = (HttpWebRequest)WebRequest.Create(url);
              if (!string.IsNullOrEmpty(xml))
              {
                  HttpWebReq.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                  HttpWebReq.Method = "POST";
                  HttpWebReq.ContentType = "text/xml";
                  HttpWebReq.Timeout = 600000;
                  byte[] requestXML = Encoding.UTF8.GetBytes(xml);
                  Stream PostStream = HttpWebReq.GetRequestStream();
                  PostStream.Write(requestXML, 0, requestXML.Length);
                  PostStream.Close();
              }
              //** get response  text/xml;charset=UTF-8     application/download
              System.Net.HttpWebResponse HttpWRes = (HttpWebResponse)HttpWebReq.GetResponse();
              if ((HttpWRes.StatusCode == System.Net.HttpStatusCode.OK) && (HttpWRes.ContentType == "application/download"))
              {
                  //**  Process Zip Download
                  System.IO.Stream responseStream = null;
                  //** If the response is gzip encoded, then wrap the stream in a gzip decoding stream
                  if ((HttpWRes.Headers.Get("Content-Encoding") == "gzip"))
                  {
                      System.IO.Stream zipStream = HttpWRes.GetResponseStream();
                      responseStream = new System.IO.Compression.GZipStream(zipStream, System.IO.Compression.CompressionMode.Decompress);
                  }
                  else
                      responseStream = HttpWRes.GetResponseStream();
                  //** Process the response stream, streaming it directly to a file. 
                  System.IO.FileStream fs = new System.IO.FileStream(ConfigurationManager.AppSettings["ItemDownloadPath"] + ".zip", System.IO.FileMode.Create);
                  //Temporary Buffer and block size 4097  32768
                  byte[] Buffer = new byte[32768];
                  int BlockSize = 0;
                  do
                  {
                      BlockSize = responseStream.Read(Buffer, 0, Buffer.Length);
                      if (BlockSize > 0)
                          fs.Write(Buffer, 0, BlockSize);
                  } while (BlockSize > 0);
                  fs.Flush();
                  fs.Close();
              }
              HttpWRes.Close();
              return "Downloaded";
          }
          catch (WebException WebException)
          {
              WebResponse response = WebException.Response;
              Stream stream = response.GetResponseStream();
              String responseMessage = new StreamReader(stream).ReadToEnd();
              HotelDA.InsertHotelErrorLog(WebException, "");
              return responseMessage;
          }
      }
      public HotelCancellation GTAHotelCancalltion(HotelCancellation SearchDetails)
      {
          SearchDetails.CancellationCharge = 0; SearchDetails.CancellationID = "";
          try
          {
              SearchDetails = HtlReq.CancelBookingItemRequest(SearchDetails);
              SearchDetails.BookingCancelRes = GTAPostXml(SearchDetails.HotelUrl, SearchDetails.BookingCancelReq);
              
              if (!SearchDetails.BookingCancelRes.Contains("<Errors>"))
               {
                    XDocument document = XDocument.Parse(SearchDetails.BookingCancelRes);
                    var CancelHotel = (from Hotel in document.Element("Response").Element("ResponseDetails").Descendants("BookingResponse")
                         select new
                         {
                             BookingPrice = Hotel.Element("BookingItems").Element("BookingItem").Element("ItemPrice").Attribute("Nett").Value,
                             BookingFees = Hotel.Element("BookingPrice").Attribute("Nett").Value,Status = Hotel.Element("BookingStatus").Value
                         }).ToList();
               
                      foreach (var Htl in CancelHotel)
                      {
                          SearchDetails.CancelStatus = Htl.Status;
                          SearchDetails.CancellationCharge = Convert.ToDecimal(Htl.BookingFees) * SearchDetails.CurrancyRate;
                          //SearchDetails.Total_Org_Roomrate = Convert.ToDecimal(Htl.BookingPrice);
                      }
               }
           }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "GTAHotelCancalltion");
          }
          return SearchDetails;
      }
     
      public string GTACountryResponse(HotelSearch SearchDetails)
      {
          string GTAResponse = "", GTAResponse1 = ""; HotelDA objDa = new HotelDA();
          try
          {
              HotelAvailabilitySearch objAuthenticat = new HotelAvailabilitySearch();
              SearchDetails =objAuthenticat.SetAuthentication(SearchDetails, "");

              GTAResponse = GTAPostXml(SearchDetails.GTAURL, HtlReq.Country_Req(SearchDetails));
              DataSet dsVMB = new DataSet();
              dsVMB.ReadXml(new System.IO.StringReader(GTAResponse));
              foreach (DataRow dtrow in dsVMB.Tables[4].Rows)
              {
                  GTAResponse1 = GTAPostXml(SearchDetails.GTAURL, HtlReq.City_Req(SearchDetails, dtrow["Code"].ToString()));
                  DataSet dsVMB1 = new DataSet();
                  dsVMB1.ReadXml(new System.IO.StringReader(GTAResponse1));

                  if (dsVMB1.Tables.Count > 4)
                  {
                      foreach (DataRow cityrow in dsVMB1.Tables[4].Rows)
                      {
                          string a = dtrow["Country_Text"].ToString();
                          try
                          {
                              objDa.insertGTACity(cityrow["City_Text"].ToString().Replace("'", " ").Replace(",", " "), cityrow["Code"].ToString().Replace("'", " ").Replace(",", " "), dtrow["Country_Text"].ToString().Replace("'", " ").Replace(",", " "), dtrow["Code"].ToString().Replace("'", " ").Replace(",", " "));
                              //SqlCommand cmd = new SqlCommand("insert into GTACity (CityName, CityCode, Country, CountryCode)values('" + cityrow["City_Text"].ToString().Replace("'", " ").Replace(",", " ") + "','" + cityrow["Code"].ToString().Replace("'", " ").Replace(",", " ") + "','" + dtrow["Country_Text"].ToString().Replace("'", " ").Replace(",", " ") + "','" + dtrow["Code"].ToString().Replace("'", " ").Replace(",", " ") + "')", con);
                              //con.Open();
                              //int i = cmd.ExecuteNonQuery();
                              //con.Close();
                          }
                          catch (Exception ex)
                          {
                              GTAResponse = ex.Message;
                          }
                      }
                  }

              }
          }
          catch (Exception ex)
          {
              GTAResponse = ex.Message;
          }
          return GTAResponse;
      }

      public string GTACityResponse(HotelSearch SearchDetails)
      {
          string GTAResponse = "";
          try
          {
              HotelAvailabilitySearch objAuthenticat = new HotelAvailabilitySearch();
              SearchDetails = objAuthenticat.SetAuthentication(SearchDetails, "");
              GTAResponse = GTAPostXml(SearchDetails.GTAURL, HtlReq.City_Req(SearchDetails, "US"));

          }
          catch (Exception ex)
          {
              GTAResponse = ex.Message;
          }
          return GTAResponse;
      }

      public string GTAAreaResponse(HotelSearch SearchDetails)
      {
          string GTAResponse = "";
          try
          {
              HotelAvailabilitySearch objAuthenticat = new HotelAvailabilitySearch();
              SearchDetails = objAuthenticat.SetAuthentication(SearchDetails, "");
              GTAResponse = GTAPostXml(SearchDetails.GTAURL, HtlReq.Area_Req(SearchDetails));
              DataSet dsVMB = new DataSet();
              dsVMB.ReadXml(new System.IO.StringReader(GTAResponse));
              HotelDA HTLST = new HotelDA();

              foreach (DataRow cityrow in dsVMB.Tables[4].Rows)
              {
                  try
                  {
                      string a = cityrow["Area_Text"].ToString();
                      if ((a.Contains(",")))
                      {
                          string[] b = a.Split(',');
                          if (b.Length > 1)
                          {
                              if (b.Length > 2)
                              {
                                  string c = b[0].Replace("'", " ").Replace(",", " ") + " " + b[1].Replace("'", " ").Replace(",", " ");
                                  if (b.Length > 3)
                                  {
                                      c = c + " " + b[2].Replace("'", " ").Replace(",", " ");
                                      HTLST.InsertGTAArea(c.Trim().Trim(), cityrow["Code"].ToString().Trim(), b[3].Replace("'", " ").Replace(",", " ").Trim());
                                  }
                                  else
                                  {
                                      HTLST.InsertGTAArea(c.Trim().Trim(), cityrow["Code"].ToString().Trim(), b[2].Replace("'", " ").Replace(",", " ").Trim());
                                  }
                              }
                              else
                              {
                                  HTLST.InsertGTAArea(b[0].Replace("'", " ").Replace(",", " ").Trim(), cityrow["Code"].ToString().Trim(), b[1].Replace("'", " ").Replace(",", " ").Trim());
                              }
                          }
                          else
                          {
                              HTLST.InsertGTAArea(b[0].Replace("'", " ").Replace(",", " ").Trim(), cityrow["Code"].ToString().Trim(), b[1].Replace("'", " ").Replace(",", " ").Trim());
                          }
                      }
                      else
                      {
                          HTLST.InsertGTAArea(a.Replace("'", " ").Replace(",", " ").Trim(), cityrow["Code"].ToString().Trim(), a.Replace("'", " ").Replace(",", " ").Trim());
                      }
                  }
                  catch (Exception ex)
                  {
                      GTAResponse = ex.Message;
                  }
              }

          }
          catch (Exception ex)
          {
              GTAResponse = ex.Message;
          }
          SearchDetails.GTA_HotelSearchRes = GTAResponse;
          return GTAResponse;
      }

      public List<HotelResult> Promotion_GTAHotels(HotelSearch SearchDetails)
      {
          //HotelComposite obgHotelsList = new HotelComposite();
          List<HotelResult> objHotellist = new List<HotelResult>();
          try
          {
              if (SearchDetails.SearchCityCode.Length < 5 && SearchDetails.CurrancyRate > 0)
              {
                  SearchDetails = HtlReq.Promotion_SeachHotelPriceRequestXml(SearchDetails);
                  SearchDetails.GTA_HotelSearchRes = GTAPostXml(SearchDetails.GTAURL, SearchDetails.GTA_HotelSearchReq);
                  objHotellist = GetGTAHotelsPrice(SearchDetails.GTA_HotelSearchRes, SearchDetails);
              }
              else
              {
                  SearchDetails.GTA_HotelSearchRes = "Hotel Not available for " + SearchDetails.SearchCity + ", " + SearchDetails.Country;
                  SearchDetails.GTA_HotelSearchReq = "not allow";
                  objHotellist.Add(new HotelResult { HtlError = SearchDetails.GTA_HotelSearchRes });
                 // obgHotelsList.Hotelresults = objHotellist;
              }
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "GTAHotels");
              HotelDA objhtlDa = new HotelDA();
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.GTA_HotelSearchReq, SearchDetails.GTA_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
              SearchDetails.GTA_HotelSearchRes = ex.Message;
          }
          //obgHotelsList.HotelSearchDetail = SearchDetails;
          return objHotellist;
      }
      protected List<HotelResult> Promotion_GetGTAHotelsPrice(string XmlRes, HotelSearch SearchDetails)
      {
          HotelDA objhtlDa = new HotelDA(); HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();
          List<HotelResult> objHotellist = new List<HotelResult>();
          try
          {
              if (XmlRes.Contains("<Errors>"))
              {
                  int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.GTA_HotelSearchReq, XmlRes, SearchDetails.AgentID, "HotelInsert");

                  XDocument document = XDocument.Parse(XmlRes);
                  var Hotels_temps = (from Hotel in document.Descendants("Errors").Descendants("Error") select new { Errormsg = Hotel.Element("ErrorText"), ErrorID = Hotel.Element("ErrorId") }).ToList();
                  objHotellist.Add(new HotelResult { HtlError = Hotels_temps[0].ErrorID.Value + " : " + Hotels_temps[0].Errormsg.Value });
              }
              else
              {
                  XDocument document = XDocument.Parse(XmlRes.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                 var Hotels_temps = (from Hotel in document.Descendants("HotelDetails").Descendants("Hotel")
                                      select new
                                      {
                                          citycode = Hotel.Element("City").Attribute("Code").Value.Trim(),
                                          city = Hotel.Element("City").Value.Trim(),
                                          HotelID = Hotel.Element("Item").Attribute("Code").Value.Trim(),
                                          HtlName = Hotel.Element("Item").Value.Trim(),
                                          StarRating = Hotel.Element("StarRating").Value.Trim(),
                                          //HotelRooms = Hotel.Element("HotelRooms"),
                                          RoomCategory = Hotel.Element("RoomCategories").Element("RoomCategory")
                                      }).ToList();
                  if (Hotels_temps.Count > 0)
                  {
                      foreach (var Hotels in Hotels_temps)
                      {
                          try
                          {
                              decimal orgrates = (Convert.ToDecimal(Hotels.RoomCategory.Element("ItemPrice").Value) * SearchDetails.CurrancyRate) / (SearchDetails.NoofNight * SearchDetails.NoofRoom);
                              MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, Hotels.StarRating, SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, orgrates, SearchDetails.servicetax);
                              //// Hotel Discount strat
                              string discMsg = ""; decimal discAmt = 0;
                              if (Hotels.RoomCategory.Element("Offer") != null)
                                  discMsg = "<span class='discounttag'>" + Hotels.RoomCategory.Element("Offer").Value.Trim() + "</span>";

                              if (Hotels.RoomCategory.Element("ItemPrice").Attribute("GrossWithoutDiscount") != null)
                                  discAmt = objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, ((Convert.ToDecimal(Hotels.RoomCategory.Element("ItemPrice").Attribute("GrossWithoutDiscount").Value.Trim()) * SearchDetails.CurrancyRate) / SearchDetails.NoofNight * SearchDetails.NoofRoom), SearchDetails.servicetax);

                              //Hotel Discount End
                              //HotelInformation HotelInfo = new HotelInformation();
                              //HotelInfo = GetGTAHotelInformation(SearchDetails, Hotels.HotelID, Hotels.citycode);
                              
                              //Add Hotels Detaisl in List start
                              objHotellist.Add(new HotelResult
                              {
                                  HotelCityCode = Hotels.citycode,
                                  HotelCity = Hotels.city,
                                  HotelCode = Hotels.HotelID,
                                  HotelName = Hotels.HtlName.Trim().Replace("'", ""),
                                  StarRating = Hotels.StarRating,
                                  Location = "",
                                  hotelPrice = MarkList.TotelAmt,
                                  AgtMrk = MarkList.AgentMrkAmt,
                                  DiscountMsg = discMsg,
                                  hotelDiscoutAmt = discAmt,
                                  HotelAddress = "",
                                  HotelThumbnailImg = "b2b.looknbook.com/Hotel/Images/NoImage.jpg",//HotelInfo.ThumbImg,
                                  HotelServices = "",
                                  HotelDescription = "",
                                  Lati_Longi = "",
                                  Provider = "GTA",
                                  PopulerId = 0
                              });
                              //Add Hotels Detaisl in List end
                          }
                          catch (Exception ex)
                          {
                              objHotellist.Add(new HotelResult { HtlError = ex.Message });
                              HotelDA.InsertHotelErrorLog(ex, "");
                          }
                      }
                  }
                  else
                  {
                      objHotellist.Add(new HotelResult { HtlError = "Hotel not found for given search criteria. Please modify your search." });
                      int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.GTA_HotelSearchReq, SearchDetails.GTA_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                  }
              }
          }
          catch (Exception ex)
          {
              objHotellist.Add(new HotelResult { HtlError = ex.Message });
              HotelDA.InsertHotelErrorLog(ex, "");
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.GTA_HotelSearchReq, SearchDetails.GTA_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
          }
          return objHotellist;
      }

      protected string SetHotelPolicyForRoom_withMrk(XElement ChargeCondition, decimal CurrancyRate, decimal servicetax, decimal AdmMrkPer, decimal agtMrkPer, string AdMrkType, string AgtMrkType)
      {
          string StrPolicy = "", StrPoli = "", StrPoli2 = "", StrPoliM = "", StrPoli2M = ""; HotelMarkups objHtlMrk = new HotelMarkups();
          try
          {
              XDocument document = XDocument.Parse(ChargeCondition.ToString());
              var Htlpoli = (from Policy in document.Descendants("ChargeConditions") select new { Policies = Policy }).ToList();

              foreach (var Htl in Htlpoli)
              {
                  foreach (var Htl1 in Htl.Policies.Descendants("ChargeCondition").TakeWhile(x => x.Attribute("Type").Value == "cancellation"))
                  {
                      #region  cancellation
                      foreach (var canPoly in Htl1.Descendants("Condition"))
                      {
                          if (canPoly.Attribute("Charge") != null)
                          {
                              if (canPoly.Attribute("Charge").Value == "true")
                              {
                                  if (canPoly.Attribute("ToDate") == null)
                                  {
                                      decimal cancelAmt = objHtlMrk.DiscountMarkupCalculation(AdmMrkPer, agtMrkPer, AdMrkType, AgtMrkType, Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * CurrancyRate, servicetax);
                                      StrPoli2 += "<li> Cancellation charges Rs. " + cancelAmt.ToString() + " /- will be applicable.</li>";
                                      // StrPoli2 += "<li> Cancellation charges  " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + " /- will be applicable.</li>";
                                  }
                                  else
                                  {
                                      decimal cancelAmt = objHtlMrk.DiscountMarkupCalculation(AdmMrkPer, agtMrkPer, AdMrkType, AgtMrkType, Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * CurrancyRate, servicetax);
                                      StrPoli += "<li> Cancellation done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                      StrPoli += " the cancellation charge will be Rs. " + cancelAmt.ToString() + "/-.</li>";
                                      //StrPoli += " the cancellation charge will be " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + "/-.</li>";
                                  }
                              }
                              else if (canPoly.Attribute("Charge").Value == "false")
                              {
                                  if (canPoly.Attribute("ToDate") == null)
                                      StrPoli2 += "<li> Cancellation done on " + canPoly.Attribute("FromDate").Value + " or earlier, cancellation charges will not applicable.</li>";
                                  else
                                  {
                                      StrPoli += "<li> Cancellation done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                      StrPoli += " cancellation charges will not applicable.</li>";
                                  }
                              }
                          }
                          if (canPoly.Attribute("Allowable") != null)
                          {
                              if (canPoly.Attribute("Allowable").Value == "false")
                              {
                                  StrPoli += "<li> Cancellation not allowed";
                                  if (canPoly.Attribute("ToDate") == null)
                                      StrPoli2 += ".</li>";
                                  else
                                      StrPoli += " " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value + " </li>";
                              }
                          }
                      }
                      #endregion
                  }
                  foreach (var Htl1 in Htl.Policies.Descendants("ChargeCondition").TakeWhile(x => x.Attribute("Type").Value == "amendment"))
                  {
                      #region  Modification
                      foreach (var canPoly in Htl1.Descendants("Condition"))
                      {
                          if (canPoly.Attribute("Charge") != null)
                          {
                              if (canPoly.Attribute("Charge").Value == "true")
                              {
                                  if (canPoly.Attribute("ToDate") == null)
                                  {
                                      decimal cancelAmt = objHtlMrk.DiscountMarkupCalculation(AdmMrkPer, agtMrkPer, AdMrkType, AgtMrkType, Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * CurrancyRate, servicetax);
                                      StrPoli2M += " <li> Modification charges Rs. " + cancelAmt.ToString() + "/- will be applicable.</li>";
                                      // StrPoli2M += " <li> Modification charges " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + "/- will be applicable.</li>";
                                  }
                                  else
                                  {
                                      decimal cancelAmt = objHtlMrk.DiscountMarkupCalculation(AdmMrkPer, agtMrkPer, AdMrkType, AgtMrkType, Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * CurrancyRate, servicetax);
                                      StrPoliM += " <li> Modification done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                      StrPoliM += " modification charge will be Rs. " + cancelAmt.ToString() + "/-.</li>";
                                      //StrPoliM += " modification charge will be " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + "/-.</li>";
                                  }
                              }
                              else if (canPoly.Attribute("Charge").Value == "false")
                              {
                                  if (canPoly.Attribute("ToDate") == null)
                                      StrPoli2M += "<li> Modification done on " + canPoly.Attribute("FromDate").Value + " or earlier, modification charges will not applicable.</li>";
                                  else
                                  {
                                      StrPoliM += "<li> Modification done between " + canPoly.Attribute("ToDate") + "  to " + canPoly.Attribute("FromDate");
                                      StrPoliM += " modification charges will not applicable.</li>";
                                  }
                              }
                          }
                          if (canPoly.Attribute("Allowable") != null)
                          {
                              if (canPoly.Attribute("Allowable").Value == "false")
                              {
                                  StrPoli += "<li> Modification not allowed";
                                  if (canPoly.Attribute("ToDate") == null)
                                      StrPoli2 += ".</li>";
                                  else
                                      StrPoli += " " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value + " </li>";
                              }
                          }
                      }
                      #endregion
                  }
              }
              StrPolicy = "<div><span>" + StrPoli + StrPoli2 + "</span> <span>" + StrPoliM + StrPoli2M + "</span></div>";
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "");
              StrPolicy += ex.Message;
          }
          return StrPolicy;
      }

      public string SetHotelPolicy_withMrk(XElement ChargeCondition, HotelSearch SearchDetails, string StarRating)
      {
          string StrPolicy = "HotelSearch SearchDetails", StrPoli = "", StrPoli2 = "";
          HotelMarkups objHtlMrk = new HotelMarkups(); HotelDA objhtlDa = new HotelDA(); DataSet MarkupDS = new DataSet();
          try
          {
              MarkupDS = objhtlDa.GetHtlMarkup("", SearchDetails.AgentID, SearchDetails.Country, SearchDetails.SearchCity, "", SearchDetails.HtlType, "", "", 0, "SEL");

              XDocument document = XDocument.Parse(ChargeCondition.ToString());
              var Htlpoli = (from Policy in document.Descendants("ChargeConditions") select new { Policies = Policy }).ToList();

              foreach (var Htl in Htlpoli)
              {
                  foreach (var Htl1 in Htl.Policies.Descendants("ChargeCondition"))
                  {
                      #region  cancellation
                      if (Htl1.Attribute("Type").Value == "cancellation")
                      {
                          foreach (var canPoly in Htl1.Descendants("Condition"))
                          {
                              if (canPoly.Attribute("Charge") != null)
                              {
                                  if (canPoly.Attribute("Charge").Value == "true")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                      {
                                          decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * SearchDetails.CurrancyRate, SearchDetails.servicetax);
                                          StrPoli2 += "<li> Cancellation charges <img src='Images/htlrs.png' class='pagingSize' /> " + cancelAmt.ToString() + " /- will be applicable.</li>";
                                          //StrPoli2 += "<li> Cancellation charges  " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + " /- will be applicable.</li>";
                                      }
                                      else
                                      {
                                          decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, Convert.ToDecimal(canPoly.Attribute("ChargeAmount").Value) * SearchDetails.CurrancyRate, SearchDetails.servicetax);
                                          StrPoli += "<li> Cancellation done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                          StrPoli += " the cancellation charge will be <img src='Images/htlrs.png' class='pagingSize' /> " + cancelAmt.ToString() + "/-.</li>";
                                          // StrPoli += " the cancellation charge will be " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + "/-.</li>";
                                      }
                                  }
                                  else if (canPoly.Attribute("Charge").Value == "false")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2 += "<li> Cancellation done on " + canPoly.Attribute("FromDate").Value + " or earlier, cancellation charges will not applicable.</li>";
                                      else
                                      {
                                          StrPoli += "<li> Cancellation done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                          StrPoli += " cancellation charges will not applicable.</li>";
                                      }
                                  }
                              }
                              if (canPoly.Attribute("Allowable") != null)
                              {
                                  if (canPoly.Attribute("Allowable").Value == "false")
                                  {
                                      StrPoli += "<li> Cancellation not allowed";
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2 += ".</li>";
                                      else
                                          StrPoli += " " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value + " </li>";
                                  }
                              }
                          }

                      }
                      #endregion
                  }
              }
              StrPolicy = "<span style='font-size:13px;font-weight: bold;'>Cancellation Policies</span><span>" + StrPoli + StrPoli2 + "</span>";
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "SetHotelPolicy");
              StrPolicy += ex.Message;
          }
          return StrPolicy;
      }
      public string SetHotelPolicyOld(XElement ChargeCondition)
      {
          string StrPolicy = "", StrPoli = "", StrPoli2 = "";
          try
          {
              XDocument document = XDocument.Parse(ChargeCondition.ToString());
              var Htlpoli = (from Policy in document.Descendants("ChargeConditions") select new { Policies = Policy }).ToList();

              foreach (var Htl in Htlpoli)
              {
                  foreach (var Htl1 in Htl.Policies.Descendants("ChargeCondition"))
                  {
                      #region  cancellation
                      if (Htl1.Attribute("Type").Value == "cancellation")
                      {
                          foreach (var canPoly in Htl1.Descendants("Condition"))
                          {
                              if (canPoly.Attribute("Charge") != null)
                              {
                                  if (canPoly.Attribute("Charge").Value == "true")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                      {
                                          StrPoli2 += "<li> Cancellation charges  " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + " /- will be applicable.</li>";
                                      }
                                      else
                                      {
                                          StrPoli += "<li> Cancellation done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                          StrPoli += " the cancellation charge will be " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + "/-.</li>";
                                      }
                                  }
                                  else if (canPoly.Attribute("Charge").Value == "false")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2 += "<li> Cancellation done on " + canPoly.Attribute("FromDate").Value + " or earlier, cancellation charges will not applicable.</li>";
                                      else
                                      {
                                          StrPoli += "<li> Cancellation done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                          StrPoli += " cancellation charges will not applicable.</li>";
                                      }
                                  }
                              }
                              if (canPoly.Attribute("Allowable") != null)
                              {
                                  if (canPoly.Attribute("Allowable").Value == "false")
                                  {
                                      StrPoli += "<li> Cancellation not allowed";
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2 += ".</li>";
                                      else
                                          StrPoli += " " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value + " </li>";
                                  }
                              }
                          }

                      }
                      #endregion
                  }
              }
              StrPolicy = "<span style='font-size:13px;font-weight: bold;'>Cancellation Policies</span><span>" + StrPoli + StrPoli2 + "</span>";
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "");
              StrPolicy += ex.Message;
          }
          return StrPolicy;
      }
      protected string SetHotelPolicyForRoomOld(XElement ChargeCondition)
      {
          string StrPolicy = "", StrPoli = "", StrPoli2 = "", StrPoliM = "", StrPoli2M = "";
          try
          {
              XDocument document = XDocument.Parse(ChargeCondition.ToString());
              var Htlpoli = (from Policy in document.Descendants("ChargeConditions") select new { Policies = Policy }).ToList();

              foreach (var Htl in Htlpoli)
              {
                  foreach (var Htl1 in Htl.Policies.Descendants("ChargeCondition"))
                  {
                      #region  cancellation
                      if (Htl1.Attribute("Type").Value == "cancellation")
                      {
                          foreach (var canPoly in Htl1.Descendants("Condition"))
                          {
                              if (canPoly.Attribute("Charge") != null)
                              {
                                  if (canPoly.Attribute("Charge").Value == "true")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                      {
                                          StrPoli2 += "<li> Cancellation charges  " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + " /- will be applicable.</li>";
                                      }
                                      else
                                      {
                                          StrPoli += "<li> Cancellation done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                          StrPoli += " the cancellation charge will be " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + "/-.</li>";
                                      }
                                  }
                                  else if (canPoly.Attribute("Charge").Value == "false")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2 += "<li> Cancellation done on " + canPoly.Attribute("FromDate").Value + " or earlier, cancellation charges will not applicable.</li>";
                                      else
                                      {
                                          StrPoli += "<li> Cancellation done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                          StrPoli += " cancellation charges will not applicable.</li>";
                                      }
                                  }
                              }
                              if (canPoly.Attribute("Allowable") != null)
                              {
                                  if (canPoly.Attribute("Allowable").Value == "false")
                                  {
                                      StrPoli += "<li> Cancellation not allowed";
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2 += ".</li>";
                                      else
                                          StrPoli += " " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value + " </li>";
                                  }
                              }
                          }

                      }
                      #endregion
                      #region  Modification
                      if (Htl1.Attribute("Type").Value == "amendment")
                      {
                          foreach (var canPoly in Htl1.Descendants("Condition"))
                          {
                              if (canPoly.Attribute("Charge") != null)
                              {
                                  if (canPoly.Attribute("Charge").Value == "true")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2M += " <li> Modification charges " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + "/- will be applicable.</li>";
                                      else
                                      {
                                          StrPoliM += " <li> Modification done between " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value;
                                          StrPoliM += " modification charge will be " + canPoly.Attribute("Currency").Value + " " + canPoly.Attribute("ChargeAmount").Value + "/-.</li>";
                                      }
                                  }
                                  else if (canPoly.Attribute("Charge").Value == "false")
                                  {
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2M += "<li> Modification done on " + canPoly.Attribute("FromDate").Value + " or earlier, modification charges will not applicable.</li>";
                                      else
                                      {
                                          StrPoliM += "<li> Modification done between " + canPoly.Attribute("ToDate") + "  to " + canPoly.Attribute("FromDate");
                                          StrPoliM += " modification charges will not applicable.</li>";
                                      }
                                  }
                              }
                              if (canPoly.Attribute("Allowable") != null)
                              {
                                  if (canPoly.Attribute("Allowable").Value == "false")
                                  {
                                      StrPoli += "<li> Modification not allowed";
                                      if (canPoly.Attribute("ToDate") == null)
                                          StrPoli2 += ".</li>";
                                      else
                                          StrPoli += " " + canPoly.Attribute("ToDate").Value + "  to " + canPoly.Attribute("FromDate").Value + " </li>";
                                  }
                              }
                          }
                      }
                      #endregion
                  }
              }
              StrPolicy = "<div><span>" + StrPoli + StrPoli2 + "</span><br /><span>" + StrPoliM + StrPoli2M + "</span></div>";
          }
          catch (Exception ex)
          {
              HotelDA.InsertHotelErrorLog(ex, "");
              StrPolicy += ex.Message;
          }
          return StrPolicy;
      }
  }
}
