﻿using COMN_SHARED.Flight;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STD.BAL;
using System.Collections;
using EXCEPTION_LOG;

namespace COMN_API.Flight
{

    public class GalApi : IFltPNRAPI, IFltTicketAPI
    {
        public string Constr { get; set; }
        public GalApi(string connectionString)
        {
            Constr = connectionString;
        }
        public FltPNR GetPNR(DataSet PaxDs, DataSet FltHdrDs, DataSet FltDs, string OrderId)
        {
            FltPNR objPNR = new FltPNR();
            COMN_BAL.Flight.FlighBal objBal = new COMN_BAL.Flight.FlighBal(Constr);
            STD.BAL.GALTransanctions objGalT = new GALTransanctions();
            objGalT.connectionString = Constr;
            try
            {
               // FltDs.Tables(0).Rows(0)("RESULTTYPE")
                DataSet TktDs = objBal.GetTktCredentials_GAL(FltHdrDs.Tables[0].Rows[0]["VC"].ToString().Trim(), FltHdrDs.Tables[0].Rows[0]["Trip"].ToString().Trim(),Convert.ToString(FltDs.Tables[0].Rows[0]["RESULTTYPE"]));
                Hashtable PnrHT = objGalT.CreateGdsPnrGAL(FltDs, FltHdrDs, PaxDs, TktDs);
                objPNR.GdsPNR = Convert.ToString(PnrHT["ADTPNR"]);
                objPNR.AirlinePNR = Convert.ToString(PnrHT["ADTAIRPNR"]);
                objBal.InsertGdsBkgLogs(FltHdrDs.Tables[0].Rows[0]["OrderId"].ToString(), PnrHT);
            }
            catch (Exception ex)
            {
                ErrorLog.FileHandling("Flight", "Error_101", ex, "COMN_API.Flight.GalApi.GetPNR");
            }
            return objPNR;
        }
    }
}
