﻿using IRCTC_RDS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Eticketing_EncryptDecrypt : System.Web.UI.Page
{
    EncryptDecryptString ENDE = new EncryptDecryptString();
    protected void Page_Load(object sender, EventArgs e)
    {
        LblMessage.Text = "";

    }
    protected void BtnClear_Click(object sender, EventArgs e)
    {
        TxtString.Text = "";
        LblMessage.Text = "";
    }
    protected void BtnEncrypt_Click(object sender, EventArgs e)
    {
        LblMessage.Text = "";
        if (!string.IsNullOrEmpty(TxtString.Text))
        {
            try
            {
                LblMessage.Text = ENDE.EncryptString(TxtString.Text);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('" + ex.Message + "');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Enter Text ..');", true);
        }
    }
    protected void BtnDecrypt_Click(object sender, EventArgs e)
    {
        LblMessage.Text = "";
        if (!string.IsNullOrEmpty(TxtString.Text))
        {
            try
            {
                LblMessage.Text = ENDE.DecryptString(TxtString.Text);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('"+ex.Message+"');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Enter Text ..');", true);
        }
        
    }


    protected void btnSha256_Click(object sender, EventArgs e)
    {
        LblMessage.Text = sha256(TxtString.Text);
    }
    public static string sha256(string randomString)
    {
        SHA256Managed crypt = new SHA256Managed();
        string hash = String.Empty;
        try
        {
            byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(randomString), 0, Encoding.ASCII.GetByteCount(randomString));
            foreach (byte theByte in crypto)
            {
                hash += theByte.ToString("x2");
            }
        }
        catch (Exception ex)
        {
            //int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "GetRdsPaymentDetails", "DataTable", ex.Message, ex.StackTrace); 
        }
        return hash.ToUpper();
    }
}