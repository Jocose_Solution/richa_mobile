﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace STD.Shared
{
    public class FlightSearch : ICloneable
    {
        public Trip Trip { get; set; }
        public TripType TripType { get; set; }  
        public string Trip1 { get; set; }
        public string TripType1 { get; set; }  
        public string DepartureCity {get;set;}
        public string ArrivalCity { get; set; }
        public string HidTxtDepCity {get;set;}
        public string HidTxtArrCity {get;set;}
        public int Adult {get;set;}
        public int Child {get;set;}
        public int Infant {get;set;}
        public string Cabin {get;set;} //For Class Economy  Business
        public string AirLine {get;set;}
        public string HidTxtAirLine {get;set;}
        public string DepDate { get; set; }
        public string RetDate { get; set; }
        public bool RTF { get; set; }        
        public bool GDSRTF { get; set; }
        public bool NStop { get; set; }
        public string UID { get; set; }
        public string DISTRID { get; set; }
        public string UserType { get; set; }
        public string TypeId { get; set; }
        public string OwnerId { get; set; }
        public string TDS { get; set; }
        public string AgentType { get; set; }
        public bool IsCorp { get; set; }
        public string Provider { get; set; }


        //Add for Multicity

        public string DepartureCity2 { get; set; }
        public string ArrivalCity2 { get; set; }
        public string HidTxtDepCity2 { get; set; }
        public string HidTxtArrCity2 { get; set; }
        public string DepDate2 { get; set; }

        public string DepartureCity3 { get; set; }
        public string ArrivalCity3 { get; set; }
        public string HidTxtDepCity3 { get; set; }
        public string HidTxtArrCity3 { get; set; }
        public string DepDate3 { get; set; }

        public string DepartureCity4 { get; set; }
        public string ArrivalCity4 { get; set; }
        public string HidTxtDepCity4 { get; set; }
        public string HidTxtArrCity4 { get; set; }
        public string DepDate4 { get; set; }

        public string DepartureCity5 { get; set; }
        public string ArrivalCity5 { get; set; }
        public string HidTxtDepCity5 { get; set; }
        public string HidTxtArrCity5 { get; set; }
        public string DepDate5 { get; set; }

        public string DepartureCity6 { get; set; }
        public string ArrivalCity6 { get; set; }
        public string HidTxtDepCity6 { get; set; }
        public string HidTxtArrCity6 { get; set; }
        public string DepDate6 { get; set; }

        public bool CheckReprice { get; set; }
        public List<string> JSK { get; set; }
        public List<string> FSK { get; set; }

        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }


        #endregion
        
    }

    public class fareinfo
    {
        public string StartPt { get; set; }
        public string EndPt { get; set; }
        public string FirstTravDt { get; set; }
        public string AirV { get; set; }
        public string FIC { get; set; }
        public string TotFareComponent { get; set; }
        public string FareAmt { get; set; }
        public string RuleNumOrdinal { get; set; }
        public string RuleTextOrdinalNum { get; set; }
        public string OTWTransportingAirV { get; set; }
        public string Key { get; set; }
        public string UniqueKey { get; set; }
        public string RulesText { get; set; }
    }
}
