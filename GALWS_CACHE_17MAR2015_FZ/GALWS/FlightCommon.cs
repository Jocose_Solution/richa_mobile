﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace STD.Shared
{
    /// <summary>
    /// 
    /// </summary>
   public class FlightCommon
   {
   //To Get CityName
       public string City { get; set; }
       public string AirportCode { get; set; }
       public string AirportName { get; set; }
       public string CountryCode { get; set; }
       public string Trip { get; set; }
      
       //To Get Airline Name
       public string AlilineName { get; set; }
       public string AirlineCode { get; set; }


   
   }
    /// <summary>
    /// 
    /// </summary>
    public class FltPaxDetails
       {
           public string OrderID { get; set; }
           public string Title { get; set; }
           public string FName { get; set; }
           public string MName { get; set; }
           public string LName { get; set; }
           public string PaxType { get; set; }
           public string TicketNo { get; set; }
           public string DOB { get; set; }
           public string FFNumber { get; set; }
           public string FFAirline { get; set; }
           public string MealType { get; set; }
           public string SeatType { get; set; }
           public bool IsPrimary { get; set; }
           public string InfAssociatePaxName { get; set; }

       }

    public class FltHeaderDetails
       {

           public string ORDERID { get; set; }
           public string SECTOR { get; set; }
           public string STATUS { get; set; }
           public string MORDIFYSTATUS { get; set; }
           public string GDSPNR { get; set; }
           public string AIRLINEPNR { get; set; }
           public string VC { get; set; }
           public string DURATION { get; set; }
           public string TRIPTYPE { get; set; }
           public string TRIP { get; set; }
           public string TOURCODE { get; set; }
           public double TA_TOTALBOOKINGCOST { get; set; }
           public double DI_TOTALBOOKINGCOST { get; set; }
           public double TA_TOTALAFTERDIS { get; set; }
           public double DI_TOTALAFTERDIS { get; set; }
           public double SFDIS { get; set; }
           public double ADDITIONALMARKUP { get; set; }
           public int ADULT { get; set; }
           public int CHILD { get; set; }
           public int INFANT { get; set; }
           public string AGENTID { get; set; }
           public string AGENCYNAME { get; set; }
           public string AGENCYTYPE { get; set; }
           public string PROVIDER { get; set; }
           public string DISTRID { get; set; }
           public string STAFFID { get; set; }
           public string EXECUTIVEID { get; set; }
           public string PAYMENTTYPE { get; set; }
           public string PGTITLE { get; set; }
           public string PGFNAME { get; set; }
           public string PGLNAME { get; set; }
           public string PGMOBILE { get; set; }
           public string PGEMAIL { get; set; }
           public string REMARK { get; set; }
           public string REJECTEDREMARK { get; set; }
           public string RESUID { get; set; }
           public double RESUCHARGE { get; set; }
           public double RESUSERVICECHARGE { get; set; }
           public double RESUFAREDIFF { get; set; }
           public int PAXID { get; set; }
           public double IMPORTCHARGE { get; set; }
           public double ADDISCPRCNT { get; set; }
           public string ADDISCPRCNTON { get; set; }
           public double DISTRDISCPRCNT { get; set; }
           public string DISTRISCPRCNTON { get; set; }


       }

    public class FltDetails
    {

        public string ORDERID { get; set; }
        public string DEPCITYORAIRPORTCODE { get; set; }
        public string DEPCITYORAIRPORTNAME { get; set; }
        public string ARRCITYORAIRPORTCODE { get; set; }
        public string ARRCITYORAIRPORTNAME { get; set; }
        public string DEPDATE { get; set; }
        public string DEPTIME { get; set; }
        public string ARRDATE { get; set; }
        public string ARRTIME { get; set; }
        public string AIRLINECODE { get; set; }
        public string AIRLINENAME { get; set; }
        public string FLTNUMBER { get; set; }
        public string AIRCRAFT { get; set; }
        public string ADTFAREBASIS { get; set; }
        public string CHDFAREBASIS { get; set; }
        public string INFFAREBASIS { get; set; }
        public string ADTRBD { get; set; }
        public string CHDRBD { get; set; }
        public string INFRBD { get; set; }
        public int  AVLSEAT { get; set; }
        

    }

    public class FltFareDetails
    {
        public string ORDERID { get; set; }
        public double BASEFARE { get; set; }
        public double YQ { get; set; }
        public double YR { get; set; }
        public double WO { get; set; }
        public double OT { get; set; }
        public double TOTALTAX { get; set; }
        public double DI_SERVICETAX { get; set; }
        public double TA_SERVICETAX { get; set; }
        public double DI_TRANFEE { get; set; }
        public double TA_TRANFEE { get; set; }
        public double AD_ADMINMRK { get; set; }
        public double TA_AGENTMRK { get; set; }
        public double DI_DISTRMRK { get; set; }
        //public double TOTALDISCOUNT { get; set; }
        public double DI_TOTALDISCOUNT { get; set; } //Added on 25/07/2012 By Manish Sharma
        public double TA_TOTALDISCOUNT { get; set; } //Added on 25/07/2012 By Manish Sharma
        public double DI_PLB { get; set; }
        public double TA_PLB { get; set; }
        public double DI_DISCOUNT { get; set; }
        public double TA_DISCOUNT { get; set; }
        public double DI_CASHBACK { get; set; }
        public double TA_CASHBACK { get; set; }
        public double DI_TDS { get; set; }
        public double TA_TDS { get; set; }
        public double DI_TDSON { get; set; }
        public double TA_TDSON { get; set; }
        //public double TOTALAFTERDIS { get; set; }
        public double DI_TOTALAFTERDIS { get; set; } //Added on 25/07/2012 By Manish Sharma
        public double TA_TOTALAFTERDIS { get; set; } //Added on 25/07/2012 By Manish Sharma
        public double DI_TOTALFARE { get; set; }
        public double TA_TOTALFARE { get; set; }
        public double DI_AVLBALANCE { get; set; }
        public double TA_AVLBALANCE { get; set; }
        public string PAXTYPE { get; set; }
        public int PAXID { get; set; }       

    }

    public class FlightCityList
    {
        public string City { get; set; }
        public string AirportCode { get; set; }
        public string AirportName { get; set; }
        public string CountryCode { get; set; }
        public string Trip { get; set; }

    }

    public class AirlineList
    {
        public string AlilineName { get; set; }
        public string AirlineCode { get; set; }

    }

    public class SelectedFlightDetail
    {
        //public string DISTRID { get; set; }
        public string ORGDESTFROM { get; set; }
        public string ORGDESTTO { get; set; }
        public string DEPARTURELOCATION { get; set; }
        public string DEPARTURECITYNAME { get; set; }
        public string DEPAIRPORTCODE { get; set; }
        public string DEPARTURETERMINAL { get; set; }
        public string ARRIVALLOCATION { get; set; }
        public string ARRIVALCITYNAME { get; set; }
        public string ARRAIRPORTCODE { get; set; }
        public string ARRIVALTERMINAL { get; set; }
        public string DEPARTUREDATE { get; set; }
        public string DEPARTURE_DATE { get; set; }
        public string DEPARTURETIME { get; set; }
        public string ARRIVALDATE { get; set; }
        public string ARRIVAL_DATE { get; set; }
        public string ARRIVALTIME { get; set; }
        public int ADULT { get; set; } //Data Type Changed
        public int CHILD { get; set; }
        public int INFANT { get; set; }
        public int TOTPAX { get; set; }
        public string MARKETINGCARRIER { get; set; }
        public string OPERATINGCARRIER  { get; set; }
        public string FLIGHTIDENTIFICATION { get; set; }
        public string VALIDATINGCARRIER { get; set; }
        public string AIRLINENAME { get; set; }
        public string AVAILABLESEATS { get; set; }
        public string ADTCABIN { get; set; }
        public string CHDCABIN { get; set; }
        public string INFCABIN { get; set; }
        public string ADTRBD { get; set; }
        public string CHDRBD { get; set; }
        public string INFRBD { get; set; }
        public string RBD { get; set; }
        public string ADTFARE { get; set; }
        public string ADTBFARE { get; set; }
        public string ADTTAX { get; set; }
        public string CHDFARE { get; set; }
        public string CHDBFARE { get; set; }
        public string CHDTAX { get; set; }
        public string INFFARE { get; set; }
        public string INFBFARE { get; set; }
        public string INFTAX { get; set; }
        public string TOTALBFARE { get; set; }
        public string TOTALFARE { get; set; }
        public string TOTALTAX { get; set; }
        public decimal NETFARE { get; set; }
        public string STAX { get; set; }
        public string TFEE { get; set; }
        public string DISCOUNT { get; set; }
        public string SEARCHVALUE { get; set; }
        public string LINEITEMNUMBER { get; set; }
        public string LEG { get; set; }
        public string FLIGHT { get; set; }
        public string TOT_DUR { get; set; }
        public string TRIPTYPE { get; set; }
        public string EQ { get; set; }
        public string STOPS { get; set; }
        public string TRIP { get; set; }
        public string SECTOR { get; set; }
        public string TRIPCNT { get; set; }
        //public string CURRENCY { get; set; }
        public string ADTADMINMRK { get; set; }
        public string ADTAGENTMRK { get; set; }
        //public string ADTDISTMRK { get; set; }
        public string CHDADMINMRK { get; set; }
        public string CHDAGENTMRK { get; set; }
        //public string CHDDISTMRK { get; set; } 
        public string INFADMINMRK { get; set; }
        public string INFAGENTMRK { get; set; }
        public string IATACOMM { get; set; }
        public string ADTFARETYPE { get; set; }
        public string ADTFAREBASIS { get; set; }
        public string CHDFARETYPE { get; set; }
        public string CHDFAREBASIS { get; set; }
        public string INFFARETYPE { get; set; }
        public string INFFAREBASIS { get; set; }
        public string FAREBASIS { get; set; }
        public string FBPAXTYPE { get; set; }
        public decimal ADTFSUR { get; set; }
        public decimal CHDFSUR { get; set; }
        public decimal INFFSUR { get; set; }
        public decimal TOTALFUELSUR { get; set; }
        public string SNO { get; set; }
        public string DEPDATELCC { get; set; }
        public string ARRDATELCC { get; set; }
        public decimal ORIGINALTF { get; set; }
        public decimal ORIGINALTT { get; set; }
        public string TRACK_ID { get; set; }
        public string FLIGHTSTATUS { get; set; }
        public string ADT_TAX { get; set; }
        public string ADTOT { get; set; }
        public string ADTSRVTAX { get; set; }
        public string ADTSRVTAX1 { get; set; }
        public string CHD_TAX { get; set; }
        public string CHDOT { get; set; }
        public string CHDSRVTAX { get; set; }
        public string CHDSRVTAX1 { get; set; }
        public string INF_TAX { get; set; }
        public string INFOT { get; set; }
        public string INFSRVTAX { get; set; }
        public decimal SRVTAX { get; set; }
        public decimal TF { get; set; }
        public decimal TC { get; set; }
        public decimal ADTTDS { get; set; }
        public decimal CHDTDS { get; set; }
        public decimal INFTDS { get; set; }
        public decimal ADTCOMM { get; set; }
        public decimal ADTCOMM1 { get; set; }
        public decimal CHDCOMM { get; set; }
        public decimal CHDCOMM1 { get; set; }
        public decimal INFCOMM { get; set; }
        public decimal ADTCB { get; set; }
        public decimal CHDCB { get; set; }
        public decimal INFCB { get; set; }
        public string ElectronicTicketing { get; set; }
        public string USERID { get; set; }
        public string PROVIDER { get; set; }
        public decimal ADTMGTFEE { get; set; }
        public decimal CHDMGTFEE { get; set; }
        public decimal INFMGTFEE { get; set; }
        public decimal TOTMGTFEE { get; set; }
        public bool ISCORP { get; set; }
        public string RESULTTYPE { get; set; }
        public string AIRLINEREMARK { get; set; }
        public string SEARCHID { get; set; }//    Supplier UserID .i.e 1G,Indigo   add 10-07-2017

        #region Baggage        
        public bool IsBagFare { get; set; }
        public string SSRCode { get; set; }
        #endregion

        #region SME FARE IsSMEFare
        public bool IsSMEFare { get; set; }
        #endregion
    }

    public class GetFtlCommission
    {
        public double BaseFare { get; set; }
        public double YQ { get; set; }
        public string OwnerId { get; set; }
        public string VC { get; set; }
        public string Trip { get; set; }
        public int PaxCnt { get; set; }

        public double ADCOMM { get; set; }
        public double ADCB { get; set; }
        public double DICOMM { get; set; }
        public double DICB { get; set; }


    }

    public class GetFtlServiceTaxTFee
    {
        public string VC { get; set; }
        public double ADCOMM { get; set; }
        public double DICOMM { get; set; }
        public double BaseFare { get; set; }
        public double YQ { get; set; }
        public string Trip { get; set; }
        public double ADSTax { get; set; }
        public double DISTax { get; set; }
        public double TFee { get; set; }



    }

    public class GetTDS
    {
        public string DistrId { get; set; }
        public string AgentId { get; set; }
        public string UserType { get; set; }
        public double DistTdsOn { get; set; }
        public double TdsOn { get; set; }
        public string Trip { get; set; }
        public double ADSTax { get; set; }
        public double DISTax { get; set; }
        public double TFee { get; set; }
        public string TdsPrcnt { get; set; }
        public string DTdsPrcnt { get; set; }




    }

    public class GetFareDetailsByPaxId
    {

        public double TA_TOTALAFTERDIS { get; set; }
        public double DI_TOTALAFTERDIS { get; set; }
        
}

    public class BaggageList
    {
        public string BaggageName { get; set; }
        public string Weight { get; set; }
        public string Trip { get; set; }
        public string Airline { get; set; }
        public string Class { get; set; }

    }
    //Cache Update
    public class MISCCharges
    {

        public string  Airline { get; set; }
        public float   Amount { get; set; }        
        public float CommisionOnBasic { get; set; }
        public float CommissionOnYq { get; set; }
        public float CommisionOnBasicYq { get; set; }
        public string MarkupType { get; set; }
        public string FareType { get; set; }

    }
    public class CacheList
    {

        public string Airline { get; set; }
        public string  Json { get; set; }

    }
    public class CacheFinalList
    {

        public string Airline { get; set; }
        public string Json { get; set; }

    }
    public class UserFlightSearch
    {

        public string UserId { get; set; }
        public string TypeId { get; set; }
        public string UserType { get; set; }
        public string TDS { get; set; }
        public string AgentType { get; set; }

    }

    public class FareTypeSettings
    {

        public string AirCode { get; set; }
        public string Trip { get; set; }
        public string FareClass_Req { get; set; }
        public string FareClass_Res { get; set; }
        public string FareType { get; set; }
        public string ProductClass_Req { get; set; }
        public string ProductClass_Res { get; set; }
        public string FSK { get; set; }
        public decimal CancellationPanelty { get; set; }
        public decimal ReIssuePanelty { get; set; }
        public bool IsRefundable { get; set; }
        public string Provider { get; set; }
        public string IdType { get; set; }
        public string FareBasis { get; set; }
        public bool IsChangeable { get; set; }
        public string CreatedDate { get; set; }
        public string Cabin { get; set; }
        #region Baggage
        public bool IsBagFare { get; set; }
        public string SSRCode { get; set; }
        #endregion

        #region SME FARE IsSMEFare
        public bool IsSMEFare { get; set; }
        #endregion

    }


    public class SearchResultBlock
    {
        public string Airline { get; set; }
        public string Provider { get; set; }
        public bool RTF { get; set; }
        public string Trip { get; set; }
        public string AirlineType { get; set; }
        public bool Status { get; set; }
        public string GroupType { get; set; }
        public string AgentId { get; set; }
        public string FareType { get; set; }
        public string AgentIdExclude { get; set; }
        public string ExcludeStatus { get; set; }
        
    }
    #region Seat
    [DataContract]
    public class AssignSeat
    {
        [DataMember]
        public string Airline { get; set; }
        [DataMember]
        public int Columns { get; set; }
        [DataMember]
        public int Rows { get; set; }
        [DataMember]
        public List<SeatMapFinal> SeatMapAll { get; set; }
        [DataMember]
        public List<TDPaxInfo> PaxListDetails { get; set; }
        [DataMember]
        public string Error { get; set; }
        [DataMember]
        public string Provider { get; set; }
    }

    [DataContract]
    public class SeatMapFinal
    {
        [DataMember]
        public List<SeatMap> SeatMapDetails_Final { get; set; }
        [DataMember]
        public int Columns { get; set; }
        [DataMember]
        public int Rows { get; set; }
        [DataMember]
        public List<Alpha> Alpha { get; set; }
    }
    [DataContract]
    public class Alpha
    {
        [DataMember]
        public string Value { get; set; }
    }

    [DataContract]
    public class SeatMap
    {
        [DataMember]
        public string ArrivalStation { get; set; }
        [DataMember]
        public string DepartureStation { get; set; }
        [DataMember]
        public string EquipmentType { get; set; }
        [DataMember]
        public string AvailableUnits { get; set; }
        [DataMember]
        public string EquipmentCategory { get; set; }
        [DataMember]
        public string Aircraft { get; set; }
        [DataMember]
        public string FlightNumber { get; set; }
        [DataMember]
        public string FlightTime { get; set; }
        [DataMember]
        public List<SeatMapDetails> SeatMapDetails { get; set; }
        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public int Columns { get; set; }
        [DataMember]
        public int Rows { get; set; }
        [DataMember]
        public string EquipmentTypeSuffix { get; set; }
        [DataMember]
        public string ClassOfService { get; set; }
    }
    [DataContract]
    public class SeatMapDetails
    {
        [DataMember]
        public short Deck { get; set; }
        [DataMember]
        public int MaxRows { get; set; }
        [DataMember]
        public int MaxColumn { get; set; }
        [DataMember]
        public string AircraftName { get; set; }
        [DataMember]
        public string EquipmentCategory { get; set; }
        [DataMember]
        public int AvailableUnits { get; set; }
        [DataMember]
        public List<SeatList> SeatListDetails { get; set; }
    }
    [DataContract]
    public class SeatList
    {
        [DataMember]
        public int RowNo { get; set; }
        [DataMember]
        public int ColumnNo { get; set; }
        [DataMember]
        public bool Assignable { get; set; }
        [DataMember]
        public int SeatSet { get; set; }
        [DataMember]
        public short SeatAngle { get; set; }
        [DataMember]
        public string SeatAvailability { get; set; }
        [DataMember]
        public string SeatDesignator { get; set; }
        [DataMember]
        public string SeatType { get; set; }
        [DataMember]
        public string TravelClassCode { get; set; }
        [DataMember]
        public short SeatGroup { get; set; }
        [DataMember]
        public bool PremiumSeatIndicator { get; set; }
        [DataMember]
        public decimal SeatFee { get; set; }
        [DataMember]
        public decimal CGST { get; set; }
        [DataMember]
        public decimal SGST { get; set; }
        [DataMember]
        public decimal IGST { get; set; }
        [DataMember]
        public decimal UGST { get; set; }
        [DataMember]
        public string SeatStatus { get; set; }
        [DataMember]
        public string ExitSeats { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string SeatAlignment { get; set; }
        [DataMember]
        public string FlightNumber { get; set; }
        [DataMember]
        public string FlightTime { get; set; }
        [DataMember]
        public bool Paid { get; set; }
        [DataMember]
        public List<Characteristic> Characteristic { get; set; }
        [DataMember]
        public string OptionalServiceRef { get; set; }
        [DataMember]
        public string Group { get; set; }
        [DataMember]
        public string ClassOfService { get; set; }
        [DataMember]
        public string Equipment { get; set; }
        [DataMember]
        public string Carrier { get; set; }
        [DataMember]
        public string SegmentRef { get; set; }
    }
    public class MySeatDetails
    {
        public string Pax { get; set; }
        public string Sector { get; set; }
        public string Seat { get; set; }
        public string Amount { get; set; }
        public string HtmlID { get; set; }
        public string SeatAlignment { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Title { get; set; }
        public string Tpcode { get; set; }
        public string FlightNumber { get; set; }
        public string FlightTime { get; set; }

        public string OptionalServiceRef { get; set; }
        public string Group { get; set; }
        public string ClassOfService { get; set; }
        public string Equipment { get; set; }
        public string Carrier { get; set; }
        public bool Paid { get; set; }
        // Fill the missing properties for your data
    }
    public class Seat
    {
        [DataMember]
        public int Counter { get; set; }
        [DataMember]
        public int PaxId { get; set; }
        [DataMember]
        public string OrderId { get; set; }
        [DataMember]
        public string SeatDesignator { get; set; }
        [DataMember]
        public int Amount { get; set; }
        [DataMember]
        public string Origin { get; set; }
        [DataMember]
        public string Destination { get; set; }
        [DataMember]
        public string VC { get; set; }
        [DataMember]
        public string CreatedDate { get; set; }
        [DataMember]
        public string FlightNumber { get; set; }
        [DataMember]
        public string FlightTime { get; set; }
        [DataMember]
        public string SeatAlignment { get; set; }
        [DataMember]
        public string OptionalServiceRef { get; set; }
        [DataMember]
        public string Group { get; set; }
        [DataMember]
        public string ClassOfService { get; set; }
        [DataMember]
        public string Equipment { get; set; }
        [DataMember]
        public string Carrier { get; set; }
        [DataMember]
        public bool Paid { get; set; }
        [DataMember]
        public bool IsSeat { get; set; }
        [DataMember]
        public string SeatStatus { get; set; }
    }
    public class SeatRequest
    {
        public int Adult { get; set; }
        public int Child { get; set; }
        public int Infant { get; set; }
        public string Provider { get; set; }
        public string ValidatingCarrier { get; set; }
        public string Trip { get; set; }
        public List<SeatRequestSegment> AirSegList { get; set; }
        public List<UAPIPrvtFareCode> PrvtFareCodeList { get; set; }
        public float AdtFare { get; set; }
        public float ChdFare { get; set; }
        public float TotalFare { get; set; }
        public string TCCode { get; set; }
        public string IDType { get; set; }
        public string AdtCabin { get; set; }
        public string ChdCabin { get; set; }
        public string InfCabin { get; set; }
    }
    public class SeatRequestSegment
    {
        public string Key { get; set; }
        public string Group { get; set; }
        public string Conx { get; set; }
        public string Carrier { get; set; }
        public string FlightNumber { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string DepartureTime { get; set; }
        public string ArrivalTime { get; set; }
        public string ETicketability { get; set; }
        public string Equipment { get; set; }
        public string Provider { get; set; }
        public string AdtRbd { get; set; }
        public string AdtFBCode { get; set; }
        public string ChdRbd { get; set; }
        public string AChdFBCode { get; set; }
        public string InfRbd { get; set; }
        public string InfFBCode { get; set; }
    }
    public class SRS
    {
        [DataMember]
        public int RowNo { get; set; }
        [DataMember]
        public int ColumnNo { get; set; }
        [DataMember]
        public bool Assignable { get; set; }
        [DataMember]
        public int SeatSet { get; set; }
        [DataMember]
        public short SeatAngle { get; set; }
        [DataMember]
        public string SeatAvailability { get; set; }
        [DataMember]
        public string SeatDesignator { get; set; }
        [DataMember]
        public string SeatType { get; set; }
        [DataMember]
        public string TravelClassCode { get; set; }
        [DataMember]
        public short SeatGroup { get; set; }
        [DataMember]
        public bool PremiumSeatIndicator { get; set; }
        [DataMember]
        public decimal SeatFee { get; set; }
        [DataMember]
        public decimal CGST { get; set; }
        [DataMember]
        public decimal SGST { get; set; }
        [DataMember]
        public decimal IGST { get; set; }
        [DataMember]
        public decimal UGST { get; set; }
        [DataMember]
        public string SeatStatus { get; set; }
        [DataMember]
        public string ExitSeats { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public string SeatAlignment { get; set; }
        [DataMember]
        public string FlightNumber { get; set; }
        [DataMember]
        public string FlightTime { get; set; }
        [DataMember]
        public bool Paid { get; set; }
        [DataMember]
        public List<Characteristic> Characteristic { get; set; }
        [DataMember]
        public string OptionalServiceRef { get; set; }
        [DataMember]
        public string Group { get; set; }
        [DataMember]
        public string ClassOfService { get; set; }
        [DataMember]
        public string Equipment { get; set; }
        [DataMember]
        public string Carrier { get; set; }
    }
    public class Characteristic
    {
        public string Value { get; set; }
    }
    public class SeatPrice
    {
        public string TotalPrice { get; set; }
        public string SupplierCode { get; set; }
        public string SequenceNumber { get; set; }
        public string IssuanceReason { get; set; }
        public string Key { get; set; }
        public string BasePrice { get; set; }
        public string Taxes { get; set; }
    }
    [DataContract]
    public class SeatSegment
    {
        [DataMember]
        public string Key { get; set; }
        [DataMember]
        public string Group { get; set; }
        [DataMember]
        public string Carrier { get; set; }
        [DataMember]
        public string FlightNumber { get; set; }
        [DataMember]
        public string Origin { get; set; }
        [DataMember]
        public string Destination { get; set; }
        [DataMember]
        public string DepartureTime { get; set; }
        [DataMember]
        public string ArrivalTime { get; set; }
        [DataMember]
        public string ClassOfService { get; set; }
        [DataMember]
        public string Equipment { get; set; }
        [DataMember]
        public string SupplierCode { get; set; }
    }
    #endregion

    #region paxinfo
    [DataContract]
    public class TDPaxInfo : ICloneable
    {
        #region Seat
        [DataMember]
        public string ProfileCode { get; set; }
        #endregion
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string FName { get; set; }
        [DataMember]
        public string LName { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string Mobile { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string DeptCode { get; set; }
        [DataMember]
        public string DesignCode { get; set; }
        [DataMember]
        public string FOPGroup { get; set; }
        [DataMember]
        public string DOB { get; set; }
        [DataMember]
        public string TA { get; set; }
        [DataMember]
        public string PaxType { get; set; }
        [DataMember]
        public string TrvlType { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string SeatO { get; set; }
        [DataMember]
        public string MealO { get; set; }
        [DataMember]
        public string SeatR { get; set; }
        [DataMember]
        public string MealR { get; set; }
        [DataMember]
        public string BagO { get; set; }
        [DataMember]
        public string BagR { get; set; }
        //[DataMember]
        //public string FFO { get; set; }
        //[DataMember]
        //public string FFR { get; set; }
        #region FrequentFlyer
        [DataMember]
        public List<string> FFNO { get; set; }
        [DataMember]
        public List<string> FFNR { get; set; }
        [DataMember]
        public List<string> FFAO { get; set; }
        [DataMember]
        public List<string> FFAR { get; set; }
        #endregion
        [DataMember]
        public string PassportNo { get; set; }
        [DataMember]
        public string IssueCountry { get; set; }
        [DataMember]
        public string IssuePlace { get; set; }
        [DataMember]
        public string NameOnPassport { get; set; }
        [DataMember]
        public string ValidFrom { get; set; }
        [DataMember]
        public string ValidTo { get; set; }
        [DataMember]
        public string IssueDate { get; set; }
        [DataMember]
        public string ExpDate { get; set; }
        [DataMember]
        public string PNationality { get; set; }
        [DataMember]
        public string CostCenter { get; set; }
        [DataMember]
        public string ProjectCode { get; set; }
        [DataMember]
        public string EmployeeNo { get; set; }
        #region ICloneable Members

        public object Clone()
        {
            return this.MemberwiseClone();
        }


        #endregion

    }

    public class UAPIPrvtFareCode
    {
        public string AccountCode { get; set; }
        public string Provider { get; set; }
        public string SupplierCode { get; set; }
    }
    #endregion

}
