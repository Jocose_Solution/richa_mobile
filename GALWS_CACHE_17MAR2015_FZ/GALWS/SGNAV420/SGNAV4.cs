﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using navitaire.SG.sm.ver4.schemas.navitaire.com.WebServices.DataContracts.Session;
using navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Common;
using navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking;
using navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Common.Enumerations;
using STD.Shared;
using System.Collections;
using System.Data;
using STD.BAL;
using System.Globalization;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.Configuration;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using GALWS;
using System.Web;
using System.Xml.Linq;
using ITZERRORLOG;
using STD.DAL;
using BAL;
using OnlineCancellationSHARED;
namespace STD.BAL.SGNAV420
{
    public class SGNAV4
    {
        string username = "";
        string password = "";
        string domain = "";
        string Constr = "";
        string OrgCode = "";
        string AgentID = "";
        string Departure = "";
        string Arrival = "";
        //string PromoC = "";
        int ContractVers = 0;
        navitaire.SG.sm.ver4.ISessionManager sessionManager = null;
        navitaire.SG.ver4.IBookingManager bookingAPI = null;
        FlightCommonBAL objFltComm = null;
        string IDType = "";
        DataTable PCDt = new DataTable();        
        public SGNAV4(string uid, string pwd, string dom, string cstr, string OrganizationCode, string Dep, string Arr, string Agnt, string S_Binding, string B_Binding, string Surl, string Burl, int CV)
        {
            username = uid;
            password = pwd;
            domain = dom;
            Constr = cstr;
            OrgCode = OrganizationCode;
            AgentID = Agnt;
            Departure = Dep;
            Arrival = Arr;
            //PromoC = promocode;
            //Indigo
            ContractVers = 0;//CV;
            sessionManager = new navitaire.SG.sm.ver4.SessionManagerClient(S_Binding, Surl);
            bookingAPI = new navitaire.SG.ver4.BookingManagerClient(B_Binding, Burl);
            objFltComm = new FlightCommonBAL(cstr);
        }
        
        public SGNAV4(string uid, string pwd, string dom, string cstr, string OrganizationCode, string Dep, string Arr, string Agnt, string S_Binding, string B_Binding, string Surl, string Burl, int CV, string IdType)
        {           
            username = uid;
            password = pwd;
            domain = dom;
            Constr = cstr;
            OrgCode = OrganizationCode;
            AgentID = Agnt;
            Departure = Dep;
            Arrival = Arr;
            // Airline = AirlineName;
            //Indigo
            ContractVers = 0;//CV;
            sessionManager = new navitaire.SG.sm.ver4.SessionManagerClient(S_Binding, Surl);
            bookingAPI = new navitaire.SG.ver4.BookingManagerClient(B_Binding, Burl);
            IDType = IdType;
            objFltComm = new FlightCommonBAL(cstr);
        }        
        public SGNAV4(string uid, string pwd, string dom, string cstr, string OrganizationCode, string Dep, string Arr, string Agnt, string S_Binding, string B_Binding, string Surl, string Burl, int CV, string IdType, DataTable PFCodeDt)
        {            
            username = uid;
            password = pwd;
            domain = dom;
            Constr = cstr;
            OrgCode = OrganizationCode;
            AgentID = Agnt;
            Departure = Dep;
            Arrival = Arr;
            // Airline = AirlineName;
            //Indigo
            ContractVers = 0;//CV;
            sessionManager = new navitaire.SG.sm.ver4.SessionManagerClient(S_Binding, Surl);
            bookingAPI = new navitaire.SG.ver4.BookingManagerClient(B_Binding, Burl);
            IDType = IdType;
            PCDt = PFCodeDt;
            objFltComm = new FlightCommonBAL(cstr);
        }

        #region Availability
        navitaire.SG.sm.ver4.LogonRequest logonRequest = new navitaire.SG.sm.ver4.LogonRequest();
        public string Spice_Login()
        {
            string Signature = "FAILURE";
            navitaire.SG.sm.ver4.LogonResponse logonResponse = null;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                logonRequest.ContractVersion = ContractVers; //Indigo
                logonRequest.logonRequestData = new LogonRequestData();
                logonRequest.logonRequestData.DomainCode = domain;
                logonRequest.logonRequestData.AgentName = username;
                logonRequest.logonRequestData.Password = password;
                logonResponse = sessionManager.Logon(logonRequest);
                if (logonResponse != null)
                {
                    Signature = logonResponse.Signature;
                }
                try
                {
                    SerializeAnObject(logonRequest, "LogonReq");
                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_Login_SerializeAnObject)", "Error_001", ex, "SerializeAnObject");
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_Login)", "Error_001", ex, "Spice_Login");
                throw ex;
            }
            return Signature;
        }
        public void Spice_Logout(string Signature) //Indigo
        {
            navitaire.SG.sm.ver4.LogoutRequest logoutRequest = new navitaire.SG.sm.ver4.LogoutRequest();
            try
            {
                //Indigo
                logonRequest.ContractVersion = ContractVers; //Indigo
                logoutRequest.Signature = Signature;//logonResponse.Signature;
                sessionManager.Logout(logoutRequest);
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_Logout)", "Error_001", ex, "Spice_Logout");
                throw ex;
            }
        }

        public ArrayList Spice_GetAvailability_Dom(FlightSearch obj, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet markup, decimal InfantBFare, decimal InfTax, int schd, string IdType, List<MISCCharges> MiscList, string VC, string CrdType, List<FareTypeSettings> FareTypeSettingsList, bool Bag, bool SMEFare, HttpContext contx)
        {
            // public async Task<List<List<FlightSearchResults>>> Spice_GetAvailability_Dom(FlightSearch obj, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet markup, decimal InfantBFare, decimal InfTax, int schd, string VC, CORP_ENTY_CONFIG CpOrEtConfig, List<ClsServiceTax> ServiceTaxList, List<ClsTds> TdsList, List<FareTypeSettings> FareTypeSettingsList, int FltSecNum = 1)
            navitaire.SG.ver4.GetAvailabilityResponse resA = new navitaire.SG.ver4.GetAvailabilityResponse();
            HttpContext.Current = contx;
            string signature = "FAILURE";
            signature = Spice_Login();
            navitaire.SG.ver4.GetAvailabilityResponse response = null;
            navitaire.SG.ver4.PriceItineraryResponse ItRes;
            List<FlightSearchResults> resultO = null;
            List<FlightSearchResults> resultI = null;
            ArrayList final;
            #region NEW
            //Added new bool value
            navitaire.SG.ver4.PriceItineraryResponse[] ItRes_L = null;
            int Present = 0;
            string L_FNO = "";
            string[] FC = new string[1];//Fare Class
            string[] FT = null;// new string[1];//Fare Type
            string[] PC = null;//Product Class
            // FT[0] = ""; //SG
            FC[0] = ""; //SG

            #endregion
            //List<FareTypeSettings> objFareTypeSettingsList = FareTypeSettingsList.Where(x => x.AirCode == VC && x.Trip == obj.Trip.ToString() && x.IdType.ToUpper().Trim() == IDType.ToUpper().Trim()).ToList();
            string CabinType = "";
            CabinType = !string.IsNullOrEmpty(obj.Cabin) ? obj.Cabin : "ALL";
            //List<FareTypeSettings> objFareTypeSettingsList = FareTypeSettingsList.Where(x => x.AirCode == VC && x.Trip == obj.Trip.ToString() && x.IdType.ToUpper().Trim() == IDType.ToUpper().Trim() && x.Cabin.ToUpper().Trim() == CabinType.ToUpper().Trim()).ToList();
            //if (objFareTypeSettingsList.Count == 0)
            //{ objFareTypeSettingsList = FareTypeSettingsList.Where(x => x.AirCode == VC && x.Trip == obj.Trip.ToString() && x.IdType.ToUpper().Trim() == IDType.ToUpper().Trim() && x.Cabin.ToUpper().Trim() == "ALL").ToList(); }

            //DataRow[] PCRow = { };
            //if (PCDt.Rows.Count > 0)
            //    PCRow = PCDt.Select("IdType='" + IDType + "' and AirCode='" + VC + "'", "");


            #region Baggage           
            //List<FareTypeSettings> objFareTypeSettingsList = FareTypeSettingsList.Where(x => x.AirCode == VC && x.Trip == obj.Trip.ToString() && x.IdType.ToUpper().Trim() == CrdType.ToUpper().Trim() && x.IsBagFare == Bag && x.IsSMEFare == SMEFare).ToList();
            List<FareTypeSettings> objFareTypeSettingsList = FareTypeSettingsList.Where(x => x.AirCode == VC && x.Trip == obj.Trip.ToString() && x.IdType.ToUpper().Trim() == CrdType.ToUpper().Trim() && x.IsBagFare == Bag && x.IsSMEFare == SMEFare && x.Cabin == CabinType).ToList();
            #endregion

            DataRow[] PCRow = { };
            try
            {
                if (PCDt.Rows.Count > 0)
                {
                    PCRow = PCDt.Select("IdType='" + IDType + "' and AirCode='" + VC + "'", "");
                    if (PCRow.Count() <= 0)
                    {
                        //PCRow = PCDt.Select("IdType='" + IDType + "' and (AirCode='" + VC + "' or AirCode='ALL') ", "");
                        PCRow = PCDt.Select("IdType='" + IDType + "' and AirCode='ALL'", "");
                    }
                }
            }
            catch (Exception exDeal)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4_DOM(Spice_GetAvailability_Dom)", "Error_001", exDeal, "DEAL-CODE");
            }

            int TSeg1 = 0, TSeg2 = 0;
            int cnt = 0;
            //Indigo
            //IBookingManager bookingAPI = new BookingManagerClient();
            if (signature != "FAILURE")
            {
                //Indigo
                #region PaxCount
                short Paxcount = 0;
                if (VC == "6E" || VC == "G8")
                {
                    if (obj.Adult > 0) Paxcount++;
                    if (obj.Child > 0) Paxcount++;
                }
                else
                {
                    Paxcount = (short)(obj.Adult + obj.Child);
                }
                #endregion
                string Dep = Utility.Left(obj.HidTxtDepCity, 3);
                string Arr = Utility.Left(obj.HidTxtArrCity, 3);
                #region Product and FareTypes and FareClass

                if (VC == "6E")
                {
                    string[] columns =
                    // FT = new string[1];
                    // FT[0] = "R";

                    FT = objFareTypeSettingsList[0].FareType.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    //PC = new string[3];
                    //PC[0] = "R"; //For Retail Scenarios
                    //PC[1] = "S";
                    //PC[2] = "A";//For Retail Scenarios

                    PC = objFareTypeSettingsList[0].ProductClass_Req.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                }
                else if (VC == "SG")
                {
                    // FT = new string[3]; FT[0] = "R"; FT[1] = "IO"; FT[2] = "F";

                    FT = objFareTypeSettingsList[0].FareType.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    #region Baggage
                    if (Bag)
                        PC = objFareTypeSettingsList[0].ProductClass_Req.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    #endregion

                }
                else if (VC == "G8")
                {
                    // FT = new string[3]; FT[0] = "R"; FT[1] = "IO"; FT[2] = "F";

                    FT = objFareTypeSettingsList[0].FareType.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    PC = objFareTypeSettingsList[0].ProductClass_Req.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                }


                #endregion


                List<FlightSearchResults> OutBound = new List<FlightSearchResults>();

                #region Availability REQ
                // Create an availability request and populate request data
                navitaire.SG.ver4.GetAvailabilityRequest request = new navitaire.SG.ver4.GetAvailabilityRequest();
                request.Signature = signature;
                request.ContractVersion = ContractVers; //Indigo
                request.TripAvailabilityRequest = new TripAvailabilityRequest();
                if ((obj.RTF == true) || (obj.Trip == Trip.I))
                {
                    if (obj.TripType == STD.Shared.TripType.RoundTrip)
                        request.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                    else
                        request.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }
                else
                {
                    request.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];
                }


                #region pax
                PaxPriceType[] priceTypes = new PaxPriceType[Paxcount];

                if (VC == "6E")
                {
                    if (obj.Adult > 0)
                    {
                        int adt = 0;
                        priceTypes[adt] = new PaxPriceType();
                        priceTypes[adt].PaxType = "ADT";
                        priceTypes[adt].PaxDiscountCode = "true";// String.Empty;
                    }
                    if (obj.Child > 0)
                    {
                        int chd = 1;
                        if (obj.Adult == 0)
                            chd = 0;
                        priceTypes[chd] = new PaxPriceType();
                        priceTypes[chd].PaxType = "CHD";
                        priceTypes[chd].PaxDiscountCode = "true";//String.Empty;
                    }
                }
                else if (VC == "G8")
                {
                    if (obj.Adult > 0)
                    {
                        int adt = 0;
                        priceTypes[adt] = new PaxPriceType();
                        priceTypes[adt].PaxType = "ADT";
                        priceTypes[adt].PaxCount = Int16.Parse(obj.Adult.ToString());
                    }
                    if (obj.Child > 0)
                    {
                        int chd = 1;
                        if (obj.Adult == 0)
                            chd = 0;
                        priceTypes[chd] = new PaxPriceType();
                        priceTypes[chd].PaxType = "CHD";
                        priceTypes[chd].PaxCount = Int16.Parse(obj.Child.ToString());
                    }
                }
                else
                {
                    if (obj.Adult > 0)
                    {
                        for (int adt = 0; adt <= obj.Adult - 1; adt++)
                        {
                            priceTypes[adt] = new PaxPriceType();
                            priceTypes[adt].PaxType = "ADT";
                            priceTypes[adt].PaxDiscountCode = String.Empty;
                        }
                    }
                    if (obj.Child > 0)
                    {
                        for (int chd = obj.Adult; chd <= (obj.Adult + obj.Child - 1); chd++)
                        {
                            priceTypes[chd] = new PaxPriceType();
                            priceTypes[chd].PaxType = "CHD";
                            priceTypes[chd].PaxDiscountCode = String.Empty;
                        }
                    }
                }
                #endregion

                AvailabilityRequest availabilityRequest = new AvailabilityRequest();
                availabilityRequest.PaxPriceTypes = priceTypes;
                availabilityRequest.CarrierCode = VC;

                #region Deal Code 
                try
                {
                    if (PCRow.Count() > 0 && Convert.ToString(PCRow[0]["AppliedOn"]) == "BOTH")
                    {
                        availabilityRequest.PromotionCode = Convert.ToString(PCRow[0]["D_T_Code"]);
                    }
                }
                catch (Exception ex)
                { }
                #endregion

                availabilityRequest.AvailabilityType = AvailabilityType.Default;
                //If AvailabilityFilter is set to AvailabilityFilter.ExcludeUnavailable only journeys with fares are returned. Default returns all journeys.
                availabilityRequest.AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                if (schd == 0)
                {
                    availabilityRequest.BeginDate = DateTime.Parse(SetSearchDate(Utility.Right(obj.DepDate, 4) + "-" + Utility.Mid(obj.DepDate, 3, 2) + "-" + Utility.Left(obj.DepDate, 2), obj.Trip.ToString()));
                    availabilityRequest.EndDate = DateTime.Parse(Utility.Right(obj.DepDate, 4) + "-" + Utility.Mid(obj.DepDate, 3, 2) + "-" + Utility.Left(obj.DepDate, 2) + "T23:59:00");
                    availabilityRequest.DepartureStation = Dep;
                    availabilityRequest.ArrivalStation = Arr;
                }
                else
                {
                    availabilityRequest.BeginDate = DateTime.Parse(SetSearchDate(Utility.Right(obj.RetDate, 4) + "-" + Utility.Mid(obj.RetDate, 3, 2) + "-" + Utility.Left(obj.RetDate, 2), obj.Trip.ToString()));
                    availabilityRequest.EndDate = DateTime.Parse(Utility.Right(obj.RetDate, 4) + "-" + Utility.Mid(obj.RetDate, 3, 2) + "-" + Utility.Left(obj.RetDate, 2) + "T23:59:00");
                    availabilityRequest.DepartureStation = Arr;
                    availabilityRequest.ArrivalStation = Dep;
                }
                availabilityRequest.FlightType = FlightType.All;
                availabilityRequest.PaxCount = (short)(obj.Adult + obj.Child);
                availabilityRequest.CurrencyCode = "INR";
                availabilityRequest.Dow = DOW.Daily;
                //Indigo
                availabilityRequest.FareClassControl = FareClassControl.Default;
                availabilityRequest.FareTypes = FT;

                if (obj.RTF == true)
                {
                    if (VC.ToUpper().Trim() == "6E" || VC.ToUpper().Trim() == "G8")
                    {
                        availabilityRequest.FareClassControl = FareClassControl.LowestFareClass;
                        availabilityRequest.ProductClasses = PC;
                    }
                }
                else if (VC.ToUpper().Trim() == "6E")
                {
                    availabilityRequest.FareClassControl = FareClassControl.CompressByProductClass;
                    availabilityRequest.ProductClasses = PC;
                }
                else if (VC.ToUpper().Trim() == "G8")
                {
                    availabilityRequest.FareClassControl = FareClassControl.LowestFareClass;
                    availabilityRequest.ProductClasses = PC;
                }

                availabilityRequest.FareRuleFilter = FareRuleFilter.Default;
                availabilityRequest.IncludeTaxesAndFees = false;
                availabilityRequest.IncludeAllotments = false;
                availabilityRequest.MinimumFarePrice = 0;
                availabilityRequest.MaximumFarePrice = 0;
                if (obj.NStop == true)
                    availabilityRequest.MaximumConnectingFlights = 0;
                else
                    availabilityRequest.MaximumConnectingFlights = 10;
                request.TripAvailabilityRequest.AvailabilityRequests[0] = availabilityRequest;
                if ((obj.RTF == true) || (obj.Trip == Trip.I))
                {
                    if (obj.TripType == STD.Shared.TripType.RoundTrip)
                    {
                        AvailabilityRequest Request = new AvailabilityRequest();
                        Request.PaxPriceTypes = priceTypes;
                        Request.CarrierCode = VC;

                        #region DealCode 2 
                        try
                        {
                            if (PCRow.Count() > 0 && Convert.ToString(PCRow[0]["AppliedOn"]) == "BOTH")
                            {
                                Request.PromotionCode = PCRow[0]["D_T_Code"].ToString();
                            }
                        }
                        catch (Exception ex)
                        { }
                        #endregion

                        Request.AvailabilityType = AvailabilityType.Default;
                        //If AvailabilityFilter is set to AvailabilityFilter.ExcludeUnavailable only journeys with fares are returned. Default returns all journeys.
                        Request.AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                        Request.BeginDate = DateTime.Parse(SetSearchDate(Utility.Right(obj.RetDate, 4) + "-" + Utility.Mid(obj.RetDate, 3, 2) + "-" + Utility.Left(obj.RetDate, 2), obj.Trip.ToString()));
                        Request.EndDate = DateTime.Parse(Utility.Right(obj.RetDate, 4) + "-" + Utility.Mid(obj.RetDate, 3, 2) + "-" + Utility.Left(obj.RetDate, 2) + "T23:59:00");
                        Request.DepartureStation = Arr;
                        Request.ArrivalStation = Dep;
                        Request.FlightType = FlightType.All;
                        Request.PaxCount = (short)(obj.Adult + obj.Child);
                        Request.CurrencyCode = "INR";
                        Request.Dow = DOW.Daily;
                        //Indigo
                        Request.FareClassControl = FareClassControl.Default;
                        Request.FareTypes = FT;
                        if (obj.RTF == true)
                        {
                            if (VC.ToUpper().Trim() == "6E" || VC.ToUpper().Trim() == "G8")
                            {
                                Request.FareClassControl = FareClassControl.LowestFareClass;
                                Request.ProductClasses = PC;
                            }
                            //Request.FareClasses = FC;//for SG                            
                        }
                        else if (VC.ToUpper().Trim() == "6E")
                        {
                            Request.FareClassControl = FareClassControl.CompressByProductClass;
                            Request.ProductClasses = PC;
                        }
                        else if (VC.ToUpper().Trim() == "G8")
                        {
                            Request.FareClassControl = FareClassControl.LowestFareClass;
                            Request.ProductClasses = PC;
                        }
                        Request.FareRuleFilter = FareRuleFilter.Default;
                        Request.IncludeTaxesAndFees = false;
                        Request.IncludeAllotments = false;
                        //Request.MinimumFarePrice = 0;
                        //Request.MaximumFarePrice = 0;
                        if (obj.NStop == true)
                            Request.MaximumConnectingFlights = 0;
                        else
                            Request.MaximumConnectingFlights = 10;

                        //if (obj.RTF == true && VC.ToUpper().Trim() == "6E" && (Dep.Trim().ToUpper() != "IXA" || Arr.Trim().ToUpper() != "IXA" || Dep.Trim().ToUpper() != "GAU" || Arr.Trim().ToUpper() != "GAU" || Dep.Trim().ToUpper() != "DIB" || Arr.Trim().ToUpper() != "DIB" || Dep.Trim().ToUpper() != "DMU" || Arr.Trim().ToUpper() != "DMU" || Dep.Trim().ToUpper() != "GAU" || Arr.Trim().ToUpper() != "GAU" || Dep.Trim().ToUpper() != "PAT" || Arr.Trim().ToUpper() != "PAT" || Dep.Trim().ToUpper() != "IXR" || Arr.Trim().ToUpper() != "IXR"))
                        //{
                        //    Request.MaximumConnectingFlights = 0;
                        //}
                        //else
                        //{
                        //    Request.MaximumConnectingFlights = 100;
                        //}
                        request.TripAvailabilityRequest.AvailabilityRequests[1] = Request;

                    }
                }

                #endregion
                string RES = "";
                string REQ = SerializeAnObject(request, "AvailabilityReq");
                SaveResponse.SAVElOGFILE(REQ, "REQ", "XML", "LCC", VC, CrdType);
                try
                {
                    response = bookingAPI.GetAvailability(request);
                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetAvailability_Dom)", "Error_001", ex, "GetSearchResult-DOM,CrdType:"+ CrdType);
                    //throw ex;
                }
                try
                {
                    RES = SerializeAnObject(response.GetTripAvailabilityResponse, "AvailabilityRes");
                    SaveResponse.SAVElOGFILE(RES, "RES", "XML", "LCC", VC, CrdType);
                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetAvailability_Dom)", "Error_001", ex, "SerializeAnObject,CrdType:" + CrdType);
                    //throw ex;
                }

                #region Check Avilability Single Flight
                try
                {
                    // navitaire.indigo.bm.ver4.
                    navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.JourneyDateMarket[][] pp;
                    if (obj.CheckReprice == true && obj.JSK != null && obj.JSK.Count > 0)
                    {
                        navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.Journey[] aa = response.GetTripAvailabilityResponse.Schedules[0][0].Journeys.Where(x => x.JourneySellKey == obj.JSK[0]).ToArray(); //x.Where(y => y.Journeys.Where(z => z.JourneySellKey == obj.JSK[0]))).ToArray();//.Journeys.Where(x => x.JourneySellKey == "JourneySellKey").ToList(); //[0].JourneySellKey
                        //navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.JourneyDateMarket[,] jj = new JourneyDateMarket[1,100]();
                        //List<JourneyDateMarket[]> cc = new List<JourneyDateMarket>();
                        resA.GetTripAvailabilityResponse = new TripAvailabilityResponse();
                        resA.GetTripAvailabilityResponse.Schedules = new JourneyDateMarket[1][];
                        resA.GetTripAvailabilityResponse.Schedules[0] = new JourneyDateMarket[1]; //(new List<navitaire.bm.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.JourneyDateMarket>()).ToArray()[;
                        resA.GetTripAvailabilityResponse.Schedules[0][0] = new navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.JourneyDateMarket();
                        resA.GetTripAvailabilityResponse.Schedules[0][0].Journeys = aa;

                    }
                    else
                    {
                        resA = response;

                    }
                }
                catch (Exception ex)
                {
                    //throw ex;
                }
                #endregion

            }
            try
            {
                //final = new ArrayList(response.GetTripAvailabilityResponse.Schedules.Length);

                ////added by abhilash 11-dec-2013
                //bool flg = false;
                //if (obj.RTF == true)
                //{
                //    if ((response.GetTripAvailabilityResponse.Schedules[0].Length > 0) && (response.GetTripAvailabilityResponse.Schedules[1].Length > 0))
                //        flg = true;
                //}
                //else
                //{
                //    if (response.GetTripAvailabilityResponse.Schedules[0].Length > 0)
                //        flg = true;
                //}
                ////end

                //if ((response.GetTripAvailabilityResponse.Schedules.Length > 0) && (flg == true))
                //{
                //    ArrayList Pricing = new ArrayList();
                //    foreach (JourneyDateMarket[] jdmArray in response.GetTripAvailabilityResponse.Schedules)
                //    {

                final = new ArrayList(resA.GetTripAvailabilityResponse.Schedules.Length);

                //added by abhilash 11-dec-2013
                bool flg = false;
                if (obj.RTF == true)
                {
                    if ((resA.GetTripAvailabilityResponse.Schedules[0].Length > 0) && (resA.GetTripAvailabilityResponse.Schedules[1].Length > 0))
                        flg = true;
                }
                else
                {
                    if (resA.GetTripAvailabilityResponse.Schedules[0].Length > 0)
                        flg = true;
                }
                //end

                if ((resA.GetTripAvailabilityResponse.Schedules.Length > 0) && (flg == true))
                {
                    ArrayList Pricing = new ArrayList();
                    foreach (JourneyDateMarket[] jdmArray in resA.GetTripAvailabilityResponse.Schedules)
                    {
                        List<FlightSearchResults> result = new List<FlightSearchResults>();
                        List<FarePriceJourney_SGV4> a = new List<FarePriceJourney_SGV4>();
                        foreach (JourneyDateMarket jdm in jdmArray)
                        {
                            for (int i = 0; i < jdm.Journeys.Length; i++)
                            {
                                int lcnt = 0, segcnt = 0;  //Seg and Leg count For Each Journey
                                FarePriceJourney_SGV4 F = new FarePriceJourney_SGV4();
                                F.Departure = jdm.DepartureStation;
                                F.Arrival = jdm.ArrivalStation;
                                int ft1 = 0;
                                F.ASC = jdm.Journeys[i].Segments[0].ActionStatusCode;
                                F.JSK = jdm.Journeys[i].JourneySellKey;
                                if (VC == "SG")
                                {
                                    for (int ft = 0; ft <= jdm.Journeys[i].Segments[0].Fares.Length - 1; ft++)
                                    {
                                        if (jdm.Journeys[i].Segments[0].Fares[ft].ClassOfService.Contains("AP"))
                                        { ft1 = ft; break; }
                                    }
                                }
                                F.FSK = jdm.Journeys[i].Segments[0].Fares[ft1].FareSellKey;
                                F.FBC = jdm.Journeys[i].Segments[0].Fares[ft1].FareBasisCode;
                                F.CCD = jdm.Journeys[i].Segments[0].FlightDesignator.CarrierCode;
                                F.RNO = jdm.Journeys[i].Segments[0].Fares[ft1].RuleNumber;
                                F.FNO = jdm.Journeys[i].Segments[0].FlightDesignator.FlightNumber;
                                F.COS = jdm.Journeys[i].Segments[0].Fares[ft1].ClassOfService;
                                F.FCS = jdm.Journeys[i].Segments[0].Fares[ft1].FareClassOfService;
                                F.FSQ = jdm.Journeys[i].Segments[0].Fares[ft1].FareSequence;
                                F.PCS = jdm.Journeys[i].Segments[0].Fares[ft1].ProductClass;
                                navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.AvailableFare A = (navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.AvailableFare)(jdm.Journeys[i].Segments[0].Fares[ft1]);
                                F.AVLCNT = A.AvailableCount.ToString();
                                F.STA = jdm.Journeys[i].Segments[0].STA.ToString();
                                F.STD = jdm.Journeys[i].Segments[0].STD.ToString();
                                F.SegCnt = (short)jdm.Journeys[i].Segments.Length;
                                Dictionary<string, string>[] Seg = new Dictionary<string, string>[F.SegCnt];
                                //Spice Change 15 Nov - Merging of FSK(F1^F2) for MultiSegment
                                #region 15 Nov Changes
                                string SegFln = "";
                                string FSK_new = "";
                                string SG_FAT = "";
                                foreach (navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.Segment s in jdm.Journeys[i].Segments)
                                {
                                    FSK_new = FSK_new + s.Fares[ft1].FareSellKey + "^";
                                    SG_FAT = SG_FAT + s.Fares[ft1].FareApplicationType.ToString().ToLower() + " ";
                                    Dictionary<string, string> sg = new Dictionary<string, string>();
                                    sg.Add("DepS", s.DepartureStation);
                                    sg.Add("ArrS", s.ArrivalStation);
                                    sg.Add("FNO", s.FlightDesignator.FlightNumber);
                                    #region Find Overlapping Flights
                                    if (SegFln.Contains(s.FlightDesignator.FlightNumber)) { }
                                    else
                                    {
                                        SegFln = SegFln + s.FlightDesignator.FlightNumber;
                                        if (L_FNO.Contains(s.FlightDesignator.FlightNumber))
                                        {
                                            Present++;
                                        }
                                        L_FNO = L_FNO + s.FlightDesignator.FlightNumber;
                                    }
                                    #endregion
                                    sg.Add("STD", s.STD.ToString("yyyy-MM-ddTHH:mm:ss"));
                                    foreach (Leg l in s.Legs)
                                    {
                                        lcnt++;
                                    }
                                    Seg[segcnt] = sg;
                                    segcnt++;
                                }
                                F.Seg = Seg;
                                //if (VC == "SG" && (SG_FAT.Contains("governing") || SG_FAT.Contains("sector")))
                                if (((VC == "SG") || (VC == "6E") || (VC == "G8")) && (SG_FAT.Contains("governing") || SG_FAT.Contains("sector")))
                                {
                                    F.FSK = FSK_new.Substring(0, FSK_new.Length - 1);
                                }
                                Dictionary<string, string>[] leg = new Dictionary<string, string>[lcnt];
                                lcnt = 0;
                                for (int s = 0; s < jdm.Journeys[i].Segments.Length; s++)
                                {
                                    for (int k = 0; k < jdm.Journeys[i].Segments[s].Legs.Length; k++)
                                    {
                                        Dictionary<string, string> lg = new Dictionary<string, string>();
                                        lg.Add("DepartureStation", jdm.Journeys[i].Segments[s].Legs[k].DepartureStation);
                                        lg.Add("ArrivalStation", jdm.Journeys[i].Segments[s].Legs[k].ArrivalStation);
                                        lg.Add("DepartureTerminal", jdm.Journeys[i].Segments[s].Legs[k].LegInfo.DepartureTerminal);
                                        lg.Add("ArrivalTerminal", jdm.Journeys[i].Segments[s].Legs[k].LegInfo.ArrivalTerminal);
                                        lg.Add("FlightNumber", jdm.Journeys[i].Segments[s].Legs[k].FlightDesignator.FlightNumber);
                                        lg.Add("EQType", jdm.Journeys[i].Segments[s].Legs[k].LegInfo.EquipmentType);
                                        lg.Add("STD", jdm.Journeys[i].Segments[s].Legs[k].STD.ToString("yyyy-MM-ddTHH:mm:ss"));
                                        lg.Add("STA", jdm.Journeys[i].Segments[s].Legs[k].STA.ToString("yyyy-MM-ddTHH:mm:ss"));
                                        leg[lcnt] = lg;
                                        lcnt++;
                                    }
                                }
                                F.Leg = leg;
                                if (cnt == 0) { TSeg1 = TSeg1 + segcnt; }
                                else { TSeg2 = TSeg2 + segcnt; }
                                #endregion
                                a.Add(F);
                            }
                            Pricing.Add(a);
                            cnt++;
                        }
                    }
                    ItRes = null;
                    //ItRes = Spice_GetItneary(signature, obj.Adult, obj.Child, obj.Infant, Pricing, TSeg1, 0);                    
                    //if (Present == 0 && obj.RTF == false)
                    if (Present == -1 && obj.RTF == false)
                    {
                        //D11
                        ItRes_L = Spice_GetItneary(signature, obj.Adult, obj.Child, obj.Infant, Pricing, TSeg1, TSeg2, VC, PCRow, FT);
                        //ItRes_L = Spice_GetItneary(signature, obj.Adult, obj.Child, obj.Infant, Pricing, TSeg1, TSeg2, PCRow, FT); //New
                    }
                    //Logout After Getting Response
                    else
                    {
                        //ItRes_L = Spice_GetItneary_Combined(signature, obj.Adult, obj.Child, obj.Infant, Pricing, TSeg1, TSeg2, PCRow, FT);

                        if (VC.ToUpper().Trim() == "6E" && obj.RTF == false)
                        {
                            ArrayList DirectSegList = new ArrayList();
                            DirectSegList.Add(((List<FarePriceJourney_SGV4>)Pricing[0]).Where(x => x.SegCnt == 1).ToList());
                            if (Pricing.Count > 1)
                            {
                                DirectSegList.Add(((List<FarePriceJourney_SGV4>)Pricing[1]).Where(x => x.SegCnt == 1).ToList());

                            }


                            ArrayList InDirectSegList = new ArrayList();
                            InDirectSegList.Add(((List<FarePriceJourney_SGV4>)Pricing[0]).Where(x => x.SegCnt > 1).ToList());
                            if (Pricing.Count > 1)
                            {
                                InDirectSegList.Add(((List<FarePriceJourney_SGV4>)Pricing[1]).Where(x => x.SegCnt > 1).ToList());

                            }
                            List<navitaire.SG.ver4.PriceItineraryResponse> ItRes_L_List = new List<navitaire.SG.ver4.PriceItineraryResponse>();
                            if (DirectSegList.Count > 0)
                            {
                                //D22
                                navitaire.SG.ver4.PriceItineraryResponse[] ItRes_L_ListSub = Spice_GetItneary_Combined_WithDerectFlight(signature, obj.Adult, obj.Child, obj.Infant, DirectSegList, TSeg1, TSeg2, VC, PCRow, FT).Result;

                                try
                                {
                                    if ((ItRes_L_ListSub.Count() <= 0 || (ItRes_L_ListSub.Count() > 0 && ItRes_L_ListSub[0] == null)) && ((List<FarePriceJourney_SGV4>)DirectSegList[0]).Count > 0)
                                    {
                                        ItRes_L_ListSub = new navitaire.SG.ver4.PriceItineraryResponse[0];
                                        ItRes_L_List.AddRange(Spice_GetItneary_Combined1(signature, obj.Adult, obj.Child, obj.Infant, Pricing, TSeg1, TSeg2, VC, PCRow, FT));

                                    }
                                    else { ItRes_L_List.AddRange(ItRes_L_ListSub); }
                                }
                                catch { }

                            }
                            if (InDirectSegList.Count > 0 && ((List<FarePriceJourney_SGV4>)InDirectSegList[0]).Count > 0 || (InDirectSegList.Count > 1 && ((List<FarePriceJourney_SGV4>)InDirectSegList[1]).Count > 0))
                            {


                                ItRes_L_List.AddRange(Spice_GetItneary_Combined1(signature, obj.Adult, obj.Child, obj.Infant, InDirectSegList, TSeg1, TSeg2, VC, PCRow, FT));
                            }


                            ItRes_L = ItRes_L_List.ToArray();
                        }
                        else
                        {
                            if (VC.ToUpper().Trim() != "6E")
                            {
                                // D33
                                ItRes_L = Spice_GetItneary_Combined(signature, obj.Adult, obj.Child, obj.Infant, Pricing, TSeg1, TSeg2, VC, PCRow, FT);
                            }
                        }
                    }
                    //Logout After Getting Response
                    Spice_Logout(signature);
                    List<FarePriceJourney_SGV4> one = (List<FarePriceJourney_SGV4>)Pricing[0];
                    List<FarePriceJourney_SGV4> two = null;
                    LCCResult objlcc = new LCCResult(Constr);
                    #region 15/2
                    List<string> FNO = new List<string>();
                    if (obj.RTF == true)
                    {
                        foreach (FarePriceJourney_SGV4 f in (List<FarePriceJourney_SGV4>)Pricing[1])
                        {
                            FNO.Add(f.FNO);
                        }
                    }
                    else
                    {
                        foreach (FarePriceJourney_SGV4 f in (List<FarePriceJourney_SGV4>)Pricing[0])
                        {
                            FNO.Add(f.FNO);
                        }
                    }
                    #endregion                   

                    #region Only Normal Fare and SRF fare Result

                    if (!(VC.ToUpper().Trim() == "6E" && obj.RTF == true))
                    {
                        //D44
                        ItRes_L = ItRes_L.Where(x => x != null).Select(x => x).ToArray();
                        one = Spice_AddPrice_ToFareList(ItRes_L, one, obj.Infant, VC, FNO);
                    }

                    //if ((VC.ToUpper().Trim() == "6E" && obj.RTF == true))
                    //{
                    //    resultO = objlcc.Spice_GetFltResult(one, SrvchargeList, CityList, AirList, markup, obj, schd, InfantBFare, InfTax, IdType, MiscList, VC, CrdType, PCRow, username, objFareTypeSettingsList, true, false, SMEFare);
                    //}
                    //else
                    //{
                    //    resultO = objlcc.Spice_GetFltResult(one, SrvchargeList, CityList, AirList, markup, obj, schd, InfantBFare, InfTax, IdType, MiscList, VC, CrdType, PCRow, username, objFareTypeSettingsList, Bag, SMEFare);
                    //}
                    resultO = objlcc.Spice_GetFltResultSGNav4(one, SrvchargeList, CityList, AirList, markup, obj, schd, InfantBFare, InfTax, IdType, MiscList, VC, CrdType, PCRow, username, objFareTypeSettingsList, Bag, SMEFare);
                    final.Add(resultO);
                    if (Pricing.Count == 2)
                    {
                        two = (List<FarePriceJourney_SGV4>)Pricing[1];
                        #region changes for Rountrip fare in case of infant (Manish) 17-feb-2014
                        List<string> FNO2 = new List<string>();
                        foreach (FarePriceJourney_SGV4 f in (List<FarePriceJourney_SGV4>)Pricing[1])
                        {
                            FNO2.Add(f.FNO);
                        }
                        #endregion
                        if (!(VC.ToUpper().Trim() == "6E" && obj.RTF == true))
                        {
                            two = Spice_AddPrice_ToFareList(ItRes_L, two, obj.Infant, VC, FNO2);
                        }

                        //if ((VC.ToUpper().Trim() == "6E" && obj.RTF == true))
                        //{
                        //    resultI = objlcc.Spice_GetFltResult(two, SrvchargeList, CityList, AirList, markup, obj, 1, InfantBFare, InfTax, IdType, MiscList, VC, CrdType, PCRow, username, objFareTypeSettingsList, true, false, SMEFare);
                        //}
                        //else
                        //{
                        //    resultI = objlcc.Spice_GetFltResult(two, SrvchargeList, CityList, AirList, markup, obj, 1, InfantBFare, InfTax, IdType, MiscList, VC, CrdType, PCRow, username, objFareTypeSettingsList, Bag, SMEFare);
                        //}
                        resultI = objlcc.Spice_GetFltResultSGNav4(two, SrvchargeList, CityList, AirList, markup, obj, 1, InfantBFare, InfTax, IdType, MiscList, VC, CrdType, PCRow, username, objFareTypeSettingsList, Bag, SMEFare);

                        final.Add(resultI);
                    }
                    if (obj.RTF == true && resultI != null)
                    {
                        final = Spice_RoundTripFare(final, obj, SrvchargeList, CrdType);
                    }
                    else if (obj.RTF == true && resultI == null)
                    {
                        final.Clear();
                    }

                    #endregion

                }
            }
            catch (Exception ex)
            {
                final = new ArrayList(1);
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetAvailability_Dom)", "Error_001", ex, "CrdType:" + CrdType);
                throw ex;
            }
            //await Task.Delay(0);
            //List<List<FlightSearchResults>> resultList = (List<List<FlightSearchResults>>)final.ToArray().Cast<List<FlightSearchResults>>().ToList();
            // return resultList;

            try
            {
                if (final[0] != null)
                    objFltComm.AddFlightKey((List<FlightSearchResults>)final[0], obj.RTF);
            }
            catch(Exception ex) {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetAvailability_Dom)", "Error_001", ex, "CrdType:" + CrdType);
            }

            return final;

        }
        // public async Task<ArrayList> Spice_GetAvailability_Intl(FlightSearch obj, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet markup, decimal InfantBFare, decimal InfTax, string VC, CORP_ENTY_CONFIG CpOrEtConfig, List<ClsServiceTax> ServiceTaxList, List<ClsTds> TdsList, List<FareTypeSettings> FareTypeSettingsList, int fltSecNum = 1)// string IdType, float srvCharge,
        public ArrayList Spice_GetAvailability_Intl(FlightSearch obj, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet markup, decimal InfantBFare, decimal InfTax, string IdType, List<MISCCharges> MiscList, string VC, string CrdType, List<FareTypeSettings> FareTypeSettingsList, HttpContext contx)
        {
            HttpContext.Current = contx;
            string signature = "FAILURE";
            signature = Spice_Login();
            navitaire.SG.ver4.GetAvailabilityResponse response = null;
            navitaire.SG.ver4.PriceItineraryResponse[] piResponse;
            navitaire.SG.ver4.PriceItineraryResponse ItRes;
            List<FlightSearchResults> resultO = null;
            List<FlightSearchResults> resultI = null;
            ArrayList final;
            #region NEW
            //Added new bool value
            navitaire.SG.ver4.PriceItineraryResponse[] ItRes_L = null;
            int Present = 0;
            string L_FNO = "";
            string[] FC = new string[1];//Fare Class
            string[] FT = null; //new string[1];//Fare Type
            string[] PC = null;//Product Class
            //FT[0] = ""; //SG
            FC[0] = ""; //SG

            #endregion
            string CabinType = "";
            CabinType = !string.IsNullOrEmpty(obj.Cabin) ? obj.Cabin : "ALL";
            List<FareTypeSettings> objFareTypeSettingsList = FareTypeSettingsList.Where(x => x.AirCode == VC && x.Trip == obj.Trip.ToString() && x.IdType.ToUpper().Trim() == CrdType.ToUpper().Trim() && x.Cabin == CabinType).ToList();            
            // List<FareTypeSettings> objFareTypeSettingsList = FareTypeSettingsList.Where(x => x.AirCode == VC && x.Trip == obj.Trip.ToString() && x.IdType.ToUpper().Trim() == CrdType.ToUpper().Trim()).ToList();
            DataRow[] PCRow = { };
            try
            {
                if (PCDt.Rows.Count > 0)
                {
                    PCRow = PCDt.Select("IdType='" + IDType + "' and AirCode='" + VC + "'", "");
                    if (PCRow.Count() <= 0)
                    {
                        //PCRow = PCDt.Select("IdType='" + IDType + "' and (AirCode='" + VC + "' or AirCode='ALL') ", "");
                        PCRow = PCDt.Select("IdType='" + IDType + "' and AirCode='ALL'", "");
                    }
                }
            }
            catch (Exception exx)
            {
            }
            int TSeg1 = 0, TSeg2 = 0;
            int schd = 0;
            //Indigo
            //IBookingManager bookingAPI = new BookingManagerClient();
            if (signature != "FAILURE")
            {
                #region PaxCount
                short Paxcount = 0;
                if (VC == "6E")
                {
                    if (obj.Adult > 0) Paxcount++;
                    if (obj.Child > 0) Paxcount++;
                }
                else
                {
                    Paxcount = (short)(obj.Adult + obj.Child);
                }
                #endregion
                string Dep = Utility.Left(obj.HidTxtDepCity, 3);
                string Arr = Utility.Left(obj.HidTxtArrCity, 3);

                //Indigo
                #region Product and FareTypes and FareClass


                if (VC == "6E")
                {
                    string[] columns =
                    // FT = new string[1];
                    // FT[0] = "R";

                    FT = objFareTypeSettingsList[0].FareType.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    //PC = new string[3];
                    //PC[0] = "R"; //For Retail Scenarios
                    //PC[1] = "S";
                    //PC[2] = "A";//For Retail Scenarios

                    PC = objFareTypeSettingsList[0].ProductClass_Req.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                }
                else if (VC == "SG")
                {
                    // FT = new string[3]; FT[0] = "R"; FT[1] = "IO"; FT[2] = "F";

                    FT = objFareTypeSettingsList[0].FareType.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                }
                #endregion



                List<FlightSearchResults> OutBound = new List<FlightSearchResults>();

                #region Availability REQ
                // Create an availability request and populate request data
                navitaire.SG.ver4.GetAvailabilityRequest request = new navitaire.SG.ver4.GetAvailabilityRequest();
                request.Signature = signature;
                request.ContractVersion = ContractVers;
                request.TripAvailabilityRequest = new TripAvailabilityRequest();
                if (obj.TripType == STD.Shared.TripType.RoundTrip)
                    request.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[2];
                else
                    request.TripAvailabilityRequest.AvailabilityRequests = new AvailabilityRequest[1];

                #region pax
                PaxPriceType[] priceTypes = new PaxPriceType[Paxcount];

                if (VC == "6E")
                {
                    if (obj.Adult > 0)
                    {
                        int adt = 0;
                        priceTypes[adt] = new PaxPriceType();
                        priceTypes[adt].PaxType = "ADT";
                        priceTypes[adt].PaxDiscountCode = "true";// String.Empty;
                    }
                    if (obj.Child > 0)
                    {
                        int chd = 1;
                        if (obj.Adult == 0)
                            chd = 0;
                        priceTypes[chd] = new PaxPriceType();
                        priceTypes[chd].PaxType = "CHD";
                        priceTypes[chd].PaxDiscountCode = "true";//String.Empty;
                    }
                }
                else
                {
                    if (obj.Adult > 0)
                    {
                        for (int adt = 0; adt <= obj.Adult - 1; adt++)
                        {
                            priceTypes[adt] = new PaxPriceType();
                            priceTypes[adt].PaxType = "ADT";
                            priceTypes[adt].PaxDiscountCode = String.Empty;
                        }
                    }
                    if (obj.Child > 0)
                    {
                        for (int chd = obj.Adult; chd <= (obj.Adult + obj.Child - 1); chd++)
                        {
                            priceTypes[chd] = new PaxPriceType();
                            priceTypes[chd].PaxType = "CHD";
                            priceTypes[chd].PaxDiscountCode = String.Empty;
                        }
                    }
                }
                #endregion
                AvailabilityRequest availabilityRequest = new AvailabilityRequest();
                availabilityRequest.PaxPriceTypes = priceTypes;
                availabilityRequest.CarrierCode = VC;
                #region Deal Code
                try
                {
                    if (PCRow.Count() > 0 && Convert.ToString(PCRow[0]["AppliedOn"]) == "BOTH")
                    {
                        availabilityRequest.PromotionCode = PCRow[0]["D_T_Code"].ToString();
                    }
                }
                catch (Exception ex)
                { }
                #endregion

                availabilityRequest.AvailabilityType = AvailabilityType.Default;
                //If AvailabilityFilter is set to AvailabilityFilter.ExcludeUnavailable only journeys with fares are returned. Default returns all journeys.
                availabilityRequest.AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                availabilityRequest.BeginDate = DateTime.Parse(SetSearchDate(Utility.Right(obj.DepDate, 4) + "-" + Utility.Mid(obj.DepDate, 3, 2) + "-" + Utility.Left(obj.DepDate, 2), obj.Trip.ToString()));
                availabilityRequest.EndDate = DateTime.Parse(Utility.Right(obj.DepDate, 4) + "-" + Utility.Mid(obj.DepDate, 3, 2) + "-" + Utility.Left(obj.DepDate, 2) + "T23:59:00");
                availabilityRequest.DepartureStation = Dep;
                availabilityRequest.ArrivalStation = Arr;
                availabilityRequest.FlightType = FlightType.All;
                availabilityRequest.FareClassControl = FareClassControl.LowestFareClass;
                availabilityRequest.PaxCount = (short)(obj.Adult + obj.Child);
                availabilityRequest.CurrencyCode = "INR";
                availabilityRequest.Dow = DOW.Daily;
                availabilityRequest.MaximumConnectingFlights = 100;
                //Indigo
                availabilityRequest.FareRuleFilter = FareRuleFilter.Default;
                availabilityRequest.IncludeTaxesAndFees = false;
                availabilityRequest.IncludeAllotments = false;
                //availabilityRequest.MinimumFarePrice = 0;
                //availabilityRequest.MaximumFarePrice = 0;
                availabilityRequest.FareTypes = FT;
                if (obj.RTF == true)
                {
                    if (VC.ToUpper().Trim() == "6E")
                    {
                        availabilityRequest.ProductClasses = PC;
                    }
                }
                else if (VC.ToUpper().Trim() == "6E")
                {
                    availabilityRequest.FareClassControl = FareClassControl.CompressByProductClass;
                    availabilityRequest.ProductClasses = PC;
                }

                // availabilityRequest.FareClasses = FC; // for SG

                request.TripAvailabilityRequest.AvailabilityRequests[0] = availabilityRequest;

                if (obj.TripType == STD.Shared.TripType.RoundTrip)
                {

                    AvailabilityRequest Request = new AvailabilityRequest();

                    Request.PaxPriceTypes = priceTypes;
                    Request.CarrierCode = VC;
                    Request.AvailabilityType = AvailabilityType.Default;
                    //If AvailabilityFilter is set to AvailabilityFilter.ExcludeUnavailable only journeys with fares are returned. Default returns all journeys.
                    Request.AvailabilityFilter = AvailabilityFilter.ExcludeUnavailable;
                    Request.BeginDate = DateTime.Parse(SetSearchDate(Utility.Right(obj.RetDate, 4) + "-" + Utility.Mid(obj.RetDate, 3, 2) + "-" + Utility.Left(obj.RetDate, 2), obj.Trip.ToString()));
                    Request.EndDate = DateTime.Parse(Utility.Right(obj.RetDate, 4) + "-" + Utility.Mid(obj.RetDate, 3, 2) + "-" + Utility.Left(obj.RetDate, 2) + "T23:59:00");
                    Request.DepartureStation = Arr;
                    Request.ArrivalStation = Dep;
                    Request.FlightType = FlightType.All;
                    Request.FareClassControl = FareClassControl.LowestFareClass;
                    Request.PaxCount = (short)(obj.Adult + obj.Child);
                    Request.CurrencyCode = "INR";
                    Request.Dow = DOW.Daily;
                    Request.MaximumConnectingFlights = 100;
                    //Indigo
                    Request.FareRuleFilter = FareRuleFilter.Default;
                    Request.IncludeTaxesAndFees = false;
                    Request.IncludeAllotments = false;
                    //Request.MinimumFarePrice = 0;
                    //Request.MaximumFarePrice = 0;
                    Request.FareTypes = FT;
                    if (obj.RTF == true)
                    {

                        if (VC.ToUpper().Trim() == "6E")
                        {
                            Request.ProductClasses = PC;
                        }
                        //else
                        //{
                        //    Request.FareClasses = FC;
                        //}
                    }
                    else if (VC.ToUpper().Trim() == "6E")
                    {
                        Request.FareClassControl = FareClassControl.CompressByProductClass;
                        Request.ProductClasses = PC;
                    }

                    // availabilityRequest.FareClasses = FC;//for SG
                    request.TripAvailabilityRequest.AvailabilityRequests[1] = Request;

                }

                #endregion

                string REQ = SerializeAnObject(request, "AvailabilityReq");
                SaveResponse.SAVElOGFILE(REQ, "REQ", "XML", "LCC", VC, CrdType);
                try
                {
                    response = bookingAPI.GetAvailability(request);

                    string RES = SerializeAnObject(response.GetTripAvailabilityResponse, "AvailabilityRes");
                    SaveResponse.SAVElOGFILE(RES, "REQ", "XML", "LCC", VC, CrdType);
                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetAvailability_Intl)", "Error_001", ex, "GetSearchResult-INT,CrdType:" + CrdType);
                    // throw ex;
                }                
            }
            try
            {
                #region List Preparation
                final = new ArrayList(response.GetTripAvailabilityResponse.Schedules.Length);
                //added by abhilash 11-dec-2013
                bool flg = false;
                if (obj.RTF == true)
                {
                    if ((response.GetTripAvailabilityResponse.Schedules[0].Length > 0) && (response.GetTripAvailabilityResponse.Schedules[1].Length > 0))
                        flg = true;
                }
                else
                {
                    if (response.GetTripAvailabilityResponse.Schedules[0].Length > 0)
                        flg = true;
                }
                //end
                if ((response.GetTripAvailabilityResponse.Schedules.Length > 0) && (flg == true))//&& response.GetTripAvailabilityResponse.Schedules[0].Length > 0
                {
                    ArrayList Pricing = new ArrayList();
                    foreach (JourneyDateMarket[] jdmArray in response.GetTripAvailabilityResponse.Schedules)
                    {
                        List<FarePriceJourney_SGV4> a = new List<FarePriceJourney_SGV4>();
                        foreach (JourneyDateMarket jdm in jdmArray)
                        {
                            for (int i = 0; i < jdm.Journeys.Length; i++)
                            {
                                int lcnt = 0, segcnt = 0;//Seg and Leg count For Each Journey
                                FarePriceJourney_SGV4 F = new FarePriceJourney_SGV4();
                                F.Departure = jdm.DepartureStation;
                                F.Arrival = jdm.ArrivalStation;
                                F.ASC = jdm.Journeys[i].Segments[0].ActionStatusCode;
                                F.JSK = jdm.Journeys[i].JourneySellKey;
                                F.FSK = jdm.Journeys[i].Segments[0].Fares[0].FareSellKey;
                                F.FBC = jdm.Journeys[i].Segments[0].Fares[0].FareBasisCode;
                                F.CCD = jdm.Journeys[i].Segments[0].FlightDesignator.CarrierCode;
                                F.RNO = jdm.Journeys[i].Segments[0].Fares[0].RuleNumber;
                                F.FNO = jdm.Journeys[i].Segments[0].FlightDesignator.FlightNumber;
                                F.COS = jdm.Journeys[i].Segments[0].Fares[0].ClassOfService;
                                F.FCS = jdm.Journeys[i].Segments[0].Fares[0].FareClassOfService;
                                F.FSQ = jdm.Journeys[i].Segments[0].Fares[0].FareSequence;
                                F.PCS = jdm.Journeys[i].Segments[0].Fares[0].ProductClass;
                                F.STA = jdm.Journeys[i].Segments[0].STA.ToString();
                                F.STD = jdm.Journeys[i].Segments[0].STD.ToString();
                                F.SegCnt = (short)jdm.Journeys[i].Segments.Length;
                                Dictionary<string, string>[] Seg = new Dictionary<string, string>[F.SegCnt];
                                //Spice Change 15 Nov - Merging of FSK(F1^F2) for MultiSegment
                                #region 15 Nov Changes
                                string SegFln = "";
                                string FSK_new = "";//Spice Change 15 Nov
                                string SG_FAT = "";
                                foreach (navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.Segment s in jdm.Journeys[i].Segments)
                                {
                                    FSK_new = FSK_new + s.Fares[0].FareSellKey + "^";
                                    SG_FAT = SG_FAT + s.Fares[0].FareApplicationType.ToString().ToLower() + " ";
                                    Dictionary<string, string> sg = new Dictionary<string, string>();
                                    sg.Add("DepS", s.DepartureStation);
                                    sg.Add("ArrS", s.ArrivalStation);
                                    sg.Add("FNO", s.FlightDesignator.FlightNumber);
                                    #region Find Overlapping Flights
                                    if (SegFln.Contains(s.FlightDesignator.FlightNumber)) { }
                                    else
                                    {
                                        SegFln = SegFln + s.FlightDesignator.FlightNumber;
                                        if (L_FNO.Contains(s.FlightDesignator.FlightNumber))
                                        {
                                            Present++;
                                        }
                                        L_FNO = L_FNO + s.FlightDesignator.FlightNumber;
                                    }
                                    #endregion
                                    sg.Add("STD", s.STD.ToString("yyyy-MM-ddTHH:mm:ss"));
                                    foreach (Leg l in s.Legs)
                                    {
                                        lcnt++;
                                    }
                                    Seg[segcnt] = sg;
                                    segcnt++;
                                }
                                F.Seg = Seg;
                                //if (VC == "SG" && (SG_FAT.Contains("governing") || SG_FAT.Contains("sector")))
                                if (((VC == "SG") || (VC == "6E") || (VC == "G8")) && (SG_FAT.Contains("governing") || SG_FAT.Contains("sector")))
                                {
                                    F.FSK = FSK_new.Substring(0, FSK_new.Length - 1);
                                }
                                Dictionary<string, string>[] leg = new Dictionary<string, string>[lcnt];
                                lcnt = 0;
                                for (int s = 0; s < jdm.Journeys[i].Segments.Length; s++)
                                {
                                    for (int k = 0; k < jdm.Journeys[i].Segments[s].Legs.Length; k++)
                                    {
                                        Dictionary<string, string> lg = new Dictionary<string, string>();
                                        lg.Add("DepartureStation", jdm.Journeys[i].Segments[s].Legs[k].DepartureStation);
                                        lg.Add("ArrivalStation", jdm.Journeys[i].Segments[s].Legs[k].ArrivalStation);
                                        lg.Add("DepartureTerminal", jdm.Journeys[i].Segments[s].Legs[k].LegInfo.DepartureTerminal);
                                        lg.Add("ArrivalTerminal", jdm.Journeys[i].Segments[s].Legs[k].LegInfo.ArrivalTerminal);
                                        lg.Add("FlightNumber", jdm.Journeys[i].Segments[s].Legs[k].FlightDesignator.FlightNumber);
                                        lg.Add("EQType", jdm.Journeys[i].Segments[s].Legs[k].LegInfo.EquipmentType);
                                        lg.Add("STD", jdm.Journeys[i].Segments[s].Legs[k].STD.ToString("yyyy-MM-ddTHH:mm:ss"));
                                        lg.Add("STA", jdm.Journeys[i].Segments[s].Legs[k].STA.ToString("yyyy-MM-ddTHH:mm:ss"));
                                        leg[lcnt] = lg;
                                        lcnt++;
                                    }
                                }
                                F.Leg = leg;
                                if (schd == 0) { TSeg1 = TSeg1 + segcnt; }
                                else { TSeg2 = TSeg2 + segcnt; }
                                #endregion
                                a.Add(F);
                            }
                        }
                        Pricing.Add(a); //FarePrice Journey List
                        schd++;
                    }
                    //ItRes = Spice_GetItneary(signature, obj.Adult, obj.Child, obj.Infant, Pricing, TSeg1, TSeg2);
                    //ItRes = null;
                    //if (Present == 0 && obj.TripType.ToString() == TrpType.O.ToString())                    
                    //{
                    //    ItRes_L = Spice_GetItneary(signature, obj.Adult, obj.Child, obj.Infant, Pricing, TSeg1, TSeg2, PCRow, FT);
                    //}
                    //else
                    //{                        
                    //    ItRes_L = Spice_GetItneary_Combined(signature, obj.Adult, obj.Child, obj.Infant, Pricing, TSeg1, TSeg2, PCRow, FT);
                    //}
                    ItRes = null;
                    if (Present == 0 && obj.TripType.ToString() == STD.Shared.TripType.OneWay.ToString())
                    {
                        ItRes_L = Spice_GetItneary(signature, obj.Adult, obj.Child, obj.Infant, Pricing, TSeg1, TSeg2, VC, PCRow, FT);
                    }
                    else
                    {
                        //ItRes_L = Spice_GetItneary_List(signature, obj.Adult, obj.Child, obj.Infant, Pricing, TSeg1, 0);
                        ItRes_L = Spice_GetItneary_Combined(signature, obj.Adult, obj.Child, obj.Infant, Pricing, TSeg1, TSeg2, VC, PCRow, FT);
                    }

                    //Logout After Getting Response
                    Spice_Logout(signature);
                    ////Get Final List
                    LCCResult objlcc = new LCCResult(Constr);
                    List<FarePriceJourney_SGV4> one = (List<FarePriceJourney_SGV4>)Pricing[0];
                    List<FarePriceJourney_SGV4> two = null;
                    #region 15/2
                    List<string> FNO = new List<string>();
                    if (obj.TripType.ToString() == STD.Shared.TripType.RoundTrip.ToString())
                    {
                        foreach (FarePriceJourney_SGV4 f in (List<FarePriceJourney_SGV4>)Pricing[1])
                        {
                            FNO.Add(f.FNO);
                        }
                    }
                    else
                    {
                        foreach (FarePriceJourney_SGV4 f in (List<FarePriceJourney_SGV4>)Pricing[0])
                        {
                            FNO.Add(f.FNO);
                        }
                    }
                    #endregion
                    one = Spice_AddPrice_ToFareList(ItRes_L, one, obj.Infant, VC, FNO);
                    resultO = objlcc.Spice_GetFltResultSGNav4(one, SrvchargeList, CityList, AirList, markup, obj, 0, InfantBFare, InfTax, IdType, MiscList, VC, CrdType, PCRow, username, objFareTypeSettingsList);
                    final.Add(resultO);
                    if (Pricing.Count == 2)
                    {
                        two = (List<FarePriceJourney_SGV4>)Pricing[1];
                        #region changes for Rountrip fare in case of infant (Manish) 17-feb-2014
                        List<string> FNO2 = new List<string>();
                        foreach (FarePriceJourney_SGV4 f in (List<FarePriceJourney_SGV4>)Pricing[1])
                        {
                            FNO2.Add(f.FNO);
                        }
                        #endregion
                        two = Spice_AddPrice_ToFareList(ItRes_L, two, obj.Infant, VC, FNO2);
                        resultI = objlcc.Spice_GetFltResultSGNav4(two, SrvchargeList, CityList, AirList, markup, obj, 1, InfantBFare, InfTax, IdType, MiscList, VC, CrdType, PCRow, username, objFareTypeSettingsList);
                        final.Add(resultI);
                        //if (obj.TripType == STD.Shared.TripType.RoundTrip)
                        //{
                        //    final = Spice_RoundTripFare(final);
                        //}                       
                    }
                    if (obj.TripType == STD.Shared.TripType.RoundTrip && resultI != null)
                    {
                        final = Spice_RoundTripFare(final, obj, SrvchargeList, CrdType);
                    }
                    else if (obj.TripType == STD.Shared.TripType.RoundTrip && resultI == null)
                    {
                        final.Clear();
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                final = new ArrayList(1);
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetAvailability_Intl)", "Error_001", ex, "GetSearchResult-INT,CrdType:" + CrdType);
                //throw ex;
            }
            return final;

        }
        #endregion


        #region List
        public navitaire.SG.ver4.PriceItineraryResponse[] Spice_GetItneary(string signature, int Adult, int Child, int Infant, ArrayList Final, int TSeg1, int TSeg2, string vc, DataRow[] PCRow, string[] FT)
        {
            //Global Variables
            short PxCnt = (short)(Adult + Child);
            //Create an instance of BookingManagerClient
            navitaire.SG.ver4.PriceItineraryResponse piResponse = null;
            navitaire.SG.ver4.PriceItineraryResponse[] PiRes_list = new navitaire.SG.ver4.PriceItineraryResponse[1];
            //IBookingManager bookingAPI = new BookingManagerClient();
            int l1 = 0, l2 = 0, Loops = 0; //For Incrementing journey (For Price Iternary)
            short seg1 = 1, seg2 = 1;
            List<FarePriceJourney_SGV4> one = (List<FarePriceJourney_SGV4>)Final[0];
            l1 = Loops = one.Count;
            List<FarePriceJourney_SGV4> two = null;
            if (Final.Count == 2)
            {
                two = (List<FarePriceJourney_SGV4>)Final[1];
                l2 = two.Count;
                Loops = Loops + two.Count;
            }
            navitaire.SG.ver4.PriceItineraryResponse Result = new navitaire.SG.ver4.PriceItineraryResponse();
            try
            {
                #region <PriceItineraryRequest>
                navitaire.SG.ver4.PriceItineraryRequest priceItinRequest = new navitaire.SG.ver4.PriceItineraryRequest();
                priceItinRequest.Signature = signature;
                priceItinRequest.ContractVersion = ContractVers;
                //L1-First Inner Node
                #region <ItineraryPriceRequest>
                priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;
                #region <a:TypeOfSale>
                if ((PCRow.Count()) > 0 && Convert.ToString(PCRow[0]["AppliedOn"]) == "BOTH")
                {
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = PCRow[0]["D_T_Code"].ToString();
                    //string[] ft = new string[1];
                    //ft[0] = "C";
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes = FT;
                }
                #endregion
                //L2-Second Node
                #region <a:SSRRequest>

                SSRRequest SQ = new SSRRequest();
                #region <a:SegmentSSRRequests>
                if (Infant > 0)
                {

                    try
                    {
                        SQ.SegmentSSRRequests = new SegmentSSRRequest[TSeg1 + TSeg2]; //[l1 + l2]
                        #region <a:SegmentSSRRequest>
                        int tseg = 0;
                        #region OutBound

                        for (int l = 0; l < l1; l++) //For each journey
                        {
                            for (int s = 0; s < one[l].SegCnt; s++)
                            {
                                SegmentSSRRequest A = new SegmentSSRRequest();
                                A.FlightDesignator = new FlightDesignator();
                                A.FlightDesignator.CarrierCode = one[l].CCD;
                                A.FlightDesignator.FlightNumber = one[l].Seg[s]["FNO"];
                                A.STD = Convert.ToDateTime(one[l].Seg[s]["STD"].Trim());  //A.STD = Convert.ToDateTime(a["STD"]);
                                A.DepartureStation = one[l].Seg[s]["DepS"].Trim();
                                A.ArrivalStation = one[l].Seg[s]["ArrS"].Trim();
                                A.PaxSSRs = new PaxSSR[Infant];
                                for (short i = 0; i < Infant; i++)
                                {
                                    A.PaxSSRs[i] = new PaxSSR();
                                    A.PaxSSRs[i].ActionStatusCode = "NN";
                                    A.PaxSSRs[i].ArrivalStation = one[l].Seg[s]["ArrS"].Trim();
                                    A.PaxSSRs[i].DepartureStation = one[l].Seg[s]["DepS"].Trim();
                                    A.PaxSSRs[i].PassengerNumber = i;
                                    A.PaxSSRs[i].SSRCode = "INFT";
                                    A.PaxSSRs[i].SSRNumber = 0;
                                    A.PaxSSRs[i].SSRValue = 0;
                                }
                                SQ.SegmentSSRRequests[tseg] = A;
                                tseg++;
                            }
                        }
                        #endregion
                        #region Inbound
                        if (l2 > 0)
                        {
                            tseg = 0;
                            for (int l = 0; l < l2; l++)
                            {
                                for (int s = 0; s < two[l].SegCnt; s++)
                                {
                                    SegmentSSRRequest A = new SegmentSSRRequest();
                                    A.FlightDesignator = new FlightDesignator();
                                    A.FlightDesignator.CarrierCode = two[l].CCD;
                                    A.FlightDesignator.FlightNumber = two[l].Seg[s]["FNO"];
                                    A.STD = Convert.ToDateTime(two[l].Seg[s]["STD"].Trim());
                                    A.DepartureStation = two[l].Seg[s]["DepS"].Trim();
                                    A.ArrivalStation = two[l].Seg[s]["ArrS"].Trim();
                                    A.PaxSSRs = new PaxSSR[Infant];
                                    for (short i = 0; i < Infant; i++)
                                    {
                                        A.PaxSSRs[i] = new PaxSSR();
                                        A.PaxSSRs[i].ActionStatusCode = "NN";
                                        A.PaxSSRs[i].ArrivalStation = two[l].Seg[s]["ArrS"].Trim();
                                        A.PaxSSRs[i].DepartureStation = two[l].Seg[s]["DepS"].Trim();
                                        A.PaxSSRs[i].PassengerNumber = i;
                                        A.PaxSSRs[i].SSRCode = "INFT";
                                        A.PaxSSRs[i].SSRNumber = 0;
                                        A.PaxSSRs[i].SSRValue = 0;
                                    }
                                    SQ.SegmentSSRRequests[TSeg1 + tseg] = A;
                                    tseg++;
                                }
                            }

                        }
                        #endregion
                        #endregion

                    }
                    catch (Exception ex)
                    {
                        ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetItneary)", "Error_001", ex, "Spice_GetItneary");
                    }
                }
                #endregion
                priceItinRequest.ItineraryPriceRequest.SSRRequest = SQ;
                priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = "INR";
                priceItinRequest.ItineraryPriceRequest.SSRRequest.CancelFirstSSR = false;
                priceItinRequest.ItineraryPriceRequest.SSRRequest.SSRFeeForceWaiveOnSell = false;

                #endregion
                //L2-Third Node
                #region <a:SellByKeyRequest>
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[Loops];
                #region <a:JourneySellKeys>
                for (int l = 0; l < one.Count; l++)
                {
                    SellKeyList A = new SellKeyList();
                    A.JourneySellKey = one[l].JSK;
                    A.FareSellKey = one[l].FSK;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[l] = A;
                }
                if (l2 > 0)
                {
                    two = (List<FarePriceJourney_SGV4>)Final[1];
                    for (int l = 0; l < l2; l++)
                    {
                        SellKeyList A = new SellKeyList();
                        A.JourneySellKey = two[l].JSK;
                        A.FareSellKey = two[l].FSK;
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[l1 + l] = A;
                    }
                }
                #endregion

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[PxCnt];
                #region <a:PaxPriceType>
                if (Adult > 0)
                {
                    for (int i = 0; i < Adult; i++)
                    {
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "ADT";
                    }
                }
                if (Child > 0)
                {
                    for (int i = Adult; i < (Adult + Child); i++)
                    {
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "CHD";
                    }
                }
                #endregion

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = "INR";
                #region <a:SourcePOS>
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS = new PointOfSale();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.State = MessageState.New;
                if (username == "OTI032")
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = "AG";
                else
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = username;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.OrganizationCode = OrgCode;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.DomainCode = domain;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.LocationCode = domain;
                #endregion
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = PxCnt;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = navitaire.SG.ver4.Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations.LoyaltyFilter.MonetaryOnly;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;

                #endregion

                #endregion

                #endregion

                try
                {
                    SerializeAnObject(priceItinRequest, "PriceItinReq");
                    piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);
                    SerializeAnObject(piResponse, "PriceItinRes");
                }
                catch (Exception ex)
                {
                    piResponse = null;
                    ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetItneary)", "Error_001", ex, "PriceItinReq-PriceItinRes");
                    //throw ex;
                }
                //Result = piResponse;
                PiRes_list[0] = piResponse;
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetItneary)", "Error_001", ex, "PriceItinReq-PriceItinRes");
                //throw ex;
            }
            return PiRes_list;
        }


        public async Task priceitenRequest_WithDirectFlight(string signature, int Adult, int Child, int Infant, ArrayList Final, int TSeg1, int TSeg2, string vc, DataRow[] PCRow, string[] FT, int l1, int l2, int Loops, int n, List<FarePriceJourney_SGV4> one, List<FarePriceJourney_SGV4> two, navitaire.SG.ver4.PriceItineraryResponse[] PiRes_list, int PxCnt, int startIndex, int maxJrnyCombined)
        {

            await Task.Delay(0);
            navitaire.SG.ver4.PriceItineraryResponse piResponse = null;
            navitaire.SG.ver4.PriceItineraryResponse[] PiRes_list_New = new navitaire.SG.ver4.PriceItineraryResponse[maxJrnyCombined];
            #region <PriceItineraryRequest>
            navitaire.SG.ver4.PriceItineraryRequest priceItinRequest = new navitaire.SG.ver4.PriceItineraryRequest();

            try
            {


                priceItinRequest.Signature = signature;
                priceItinRequest.ContractVersion = ContractVers;
                //L1-First Inner Node
                #region <ItineraryPriceRequest>
                priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;
                #region <a:TypeOfSale>
                if ((PCRow.Count()) > 0 && Convert.ToString(PCRow[0]["AppliedOn"]) == "BOTH")
                {
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = Convert.ToString(PCRow[0]["D_T_Code"]);
                    //string[] ft = new string[1];
                    //ft[0] = "C";
                    priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes = FT;
                }
                #endregion
                //L2-Second Node
                #region <a:SSRRequest>

                SSRRequest SQ = new SSRRequest();



                #endregion
                //L2-Third Node
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";


                int sellkeyCount = PiRes_list.Count() - startIndex >= maxJrnyCombined ? maxJrnyCombined : (PiRes_list.Count() - startIndex);
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[sellkeyCount];
                #region <a:SellByKeyRequest>
                int fstloop = startIndex;
                int loopsec = startIndex;
                int kk = -1;
                for (int nn = 0; nn < sellkeyCount; nn++)
                {

                    #region <a:JourneySellKeys>
                    //12th December 
                    //12th December 

                    //if (fstloop == l1) { fstloop = 0; } else if (loopsec == l2) { loopsec = 0; }
                    if (fstloop < one.Count)
                    {
                        SellKeyList A = new SellKeyList();
                        A.JourneySellKey = one[fstloop].JSK;
                        A.FareSellKey = one[fstloop].FSK;
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[nn] = A;




                        #region <a:SegmentSSRRequests> For Infant
                        if (Infant > 0 && fstloop == startIndex)
                        {

                            try
                            {

                                #region <a:SegmentSSRRequest>
                                //12th December 


                                #region OutBound
                                if (n < Loops)
                                {   //For each journey Create Segment Requests for that Single Journey
                                    SQ.SegmentSSRRequests = new SegmentSSRRequest[one[fstloop].SegCnt]; //[l1 + l2]
                                    for (int s = 0; s < one[fstloop].SegCnt; s++)//Change l to n
                                    {
                                        SegmentSSRRequest AA = new SegmentSSRRequest();
                                        AA.FlightDesignator = new FlightDesignator();
                                        AA.FlightDesignator.CarrierCode = one[fstloop].CCD;
                                        AA.FlightDesignator.FlightNumber = one[fstloop].Seg[s]["FNO"];
                                        AA.STD = Convert.ToDateTime(one[fstloop].Seg[s]["STD"].Trim());  //A.STD = Convert.ToDateTime(a["STD"]);
                                        AA.DepartureStation = one[fstloop].Seg[s]["DepS"].Trim();
                                        AA.ArrivalStation = one[fstloop].Seg[s]["ArrS"].Trim();
                                        AA.PaxSSRs = new PaxSSR[Infant];
                                        for (short i = 0; i < Infant; i++)
                                        {
                                            AA.PaxSSRs[i] = new PaxSSR();
                                            AA.PaxSSRs[i].ActionStatusCode = "NN";
                                            AA.PaxSSRs[i].ArrivalStation = one[fstloop].Seg[s]["ArrS"].Trim();
                                            AA.PaxSSRs[i].DepartureStation = one[fstloop].Seg[s]["DepS"].Trim();
                                            AA.PaxSSRs[i].PassengerNumber = i;
                                            AA.PaxSSRs[i].SSRCode = "INFT";
                                            AA.PaxSSRs[i].SSRNumber = 0;
                                            AA.PaxSSRs[i].SSRValue = 0;
                                        }
                                        SQ.SegmentSSRRequests[s] = AA;
                                    }

                                }
                                #endregion

                                #endregion

                            }
                            catch (Exception ex)
                            {
                                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(priceitenRequest_WithDirectFlight)", "Error_001", ex, "priceItinRequest");
                            }
                        }
                        #endregion

                        fstloop++;


                    }


                    if (loopsec < (l2 + l1) && fstloop >= l1 && two != null)
                    {

                        int Rloopcnt = 0;


                        if (fstloop == l1 && startIndex <= l1)
                        {
                            Rloopcnt = 0;
                            if ((sellkeyCount - (nn + 1)) != 0)
                            {
                                kk = sellkeyCount - (nn + 1);
                                Rloopcnt = kk;

                                kk--;
                            }
                        }
                        else if (startIndex > l1 || loopsec > l1)
                        {
                            Rloopcnt = loopsec - l1;

                        }



                        //else if (loopsec >= l1 )
                        //{
                        //    Rloopcnt = loopsec - l1;

                        //}
                        //else if( (loopsec > startIndex) && fstloop> loopsec)
                        //{

                        //    Rloopcnt = (fstloop - loopsec)-1;
                        //}

                        SellKeyList A = new SellKeyList();
                        A.JourneySellKey = two[Rloopcnt].JSK;
                        A.FareSellKey = two[Rloopcnt].FSK;
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[nn] = A;


                        #region Inbound
                        if (fstloop >= l1 && loopsec == startIndex)
                        {
                            SQ.SegmentSSRRequests = new SegmentSSRRequest[two[Rloopcnt].SegCnt];
                            for (int s = 0; s < two[Rloopcnt].SegCnt; s++)//Change l to n
                            {
                                SegmentSSRRequest B = new SegmentSSRRequest();
                                B.FlightDesignator = new FlightDesignator();
                                B.FlightDesignator.CarrierCode = two[Rloopcnt].CCD;
                                B.FlightDesignator.FlightNumber = two[Rloopcnt].Seg[s]["FNO"];
                                B.STD = Convert.ToDateTime(two[Rloopcnt].Seg[s]["STD"].Trim());
                                B.DepartureStation = two[Rloopcnt].Seg[s]["DepS"].Trim();
                                B.ArrivalStation = two[Rloopcnt].Seg[s]["ArrS"].Trim();
                                B.PaxSSRs = new PaxSSR[Infant];
                                for (short i = 0; i < Infant; i++)
                                {
                                    B.PaxSSRs[i] = new PaxSSR();
                                    B.PaxSSRs[i].ActionStatusCode = "NN";
                                    B.PaxSSRs[i].ArrivalStation = two[Rloopcnt].Seg[s]["ArrS"].Trim();
                                    B.PaxSSRs[i].DepartureStation = two[Rloopcnt].Seg[s]["DepS"].Trim();
                                    B.PaxSSRs[i].PassengerNumber = i;
                                    B.PaxSSRs[i].SSRCode = "INFT";
                                    B.PaxSSRs[i].SSRNumber = 0;
                                    B.PaxSSRs[i].SSRValue = 0;
                                }
                                SQ.SegmentSSRRequests[s] = B;
                            }

                        }
                        #endregion

                        loopsec++;
                    }
                    #endregion
                }

                priceItinRequest.ItineraryPriceRequest.SSRRequest = SQ;

                priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = "INR";
                priceItinRequest.ItineraryPriceRequest.SSRRequest.CancelFirstSSR = false;
                priceItinRequest.ItineraryPriceRequest.SSRRequest.SSRFeeForceWaiveOnSell = false;

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[PxCnt];
                #region <a:PaxPriceType>
                if (Adult > 0)
                {
                    for (int i = 0; i < Adult; i++)
                    {
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "ADT";
                    }
                }
                if (Child > 0)
                {
                    for (int i = Adult; i < (Adult + Child); i++)
                    {
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "CHD";
                    }
                }
                #endregion

                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = "INR";
                #region <a:SourcePOS>
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS = new PointOfSale();
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.State = MessageState.New;
                // priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = username;
                if (username == "OTI032")
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = "AG";
                else
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = username;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.OrganizationCode = OrgCode;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.DomainCode = domain;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.LocationCode = domain;
                #endregion
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = Convert.ToInt16(PxCnt.ToString());
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = navitaire.SG.ver4.Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations.LoyaltyFilter.MonetaryOnly;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;

                #endregion


                #endregion

                #endregion

                try
                {
                    SerializeAnObject(priceItinRequest, "PriceItinReq");
                    piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);
                    SerializeAnObject(piResponse, "PriceItinRes");
                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(priceitenRequest_WithDirectFlight)", "Error_001", ex, "PriceItinReq-PriceItinRes");
                    piResponse = null;
                    // throw ex;
                }


                for (int j = 0; j < sellkeyCount; j++)
                {
                    navitaire.SG.ver4.PriceItineraryResponse piResponse1 = new navitaire.SG.ver4.PriceItineraryResponse();

                    Journey jj = Newtonsoft.Json.JsonConvert.DeserializeObject<Journey>(Newtonsoft.Json.JsonConvert.SerializeObject(piResponse.Booking.Journeys[j]));
                    Journey[] objJlist = { jj };
                    // string objJ = Newtonsoft.Json.JsonConvert.SerializeObject(objJlist);
                    // objJlist[0] = Newtonsoft.Json.JsonConvert.DeserializeObject<Journey>(objJ);// piResponse.Booking.Journeys[j];
                    piResponse1 = (navitaire.SG.ver4.PriceItineraryResponse)piResponse.CloneObject<navitaire.SG.ver4.PriceItineraryResponse>();
                    //piResponse1 = (PriceItineraryResponse)piResponse;
                    string objstr = Newtonsoft.Json.JsonConvert.SerializeObject(piResponse);

                    piResponse1 = Newtonsoft.Json.JsonConvert.DeserializeObject<navitaire.SG.ver4.PriceItineraryResponse>(objstr);

                    piResponse1.Booking.Journeys = objJlist; //priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[Loops];

                    if (PiRes_list.Count() > (startIndex))
                        PiRes_list[startIndex + j] = piResponse1;
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(priceitenRequest_WithDirectFlight)", "Error_001", ex, "PriceItinReq-PriceItinRes");

            }
            // PiRes_list[n] = piResponse;


        }

        public async Task<navitaire.SG.ver4.PriceItineraryResponse[]> Spice_GetItneary_Combined_WithDerectFlight(string signature, int Adult, int Child, int Infant, ArrayList Final, int TSeg1, int TSeg2, string vc, DataRow[] PCRow, string[] FT)
        {
            //Global Variables
            short PxCnt = (short)(Adult + Child);
            //Create an instance of BookingManagerClient

            navitaire.SG.ver4.PriceItineraryResponse[] PiRes_list = null;
            //IBookingManager bookingAPI = new BookingManagerClient();
            int l1 = 0, l2 = 0, Loops = 0; //For Incrementing journey (For Price Iternary)
            short seg1 = 1, seg2 = 1;
            List<FarePriceJourney_SGV4> one = (List<FarePriceJourney_SGV4>)Final[0];
            l1 = Loops = one.Count;
            List<FarePriceJourney_SGV4> two = null;
            if (Final.Count == 2)
            {
                two = (List<FarePriceJourney_SGV4>)Final[1];
                l2 = two.Count;
                //Loops = Loops + two.Count; 12 December
                //if (l1 >= l2)
                //{
                //    Loops = l1;
                //}
                //else
                //{
                //    Loops = l2;
                //}

                if (l2 > 0) { Loops = Loops + l2; }
            }
            navitaire.SG.ver4.PriceItineraryResponse Result = new navitaire.SG.ver4.PriceItineraryResponse();
            PiRes_list = new navitaire.SG.ver4.PriceItineraryResponse[Loops]; //Total PriceItinerary Responses
            try
            {
                //one.Where(x=>x.SegCnt==1).()to



                //int ssrcntr_o = 0, ssrcntr_i = 0; //For SegmentSSRRequest Looping
                ////12th December
                //int fstloop = 0, loopsec = 0;//For outbound loop //For  Inbound Loop

                int taskCount = int.Parse(System.Configuration.ConfigurationManager.AppSettings["TaskCnt"].ToString());
                int Count = 1;
                //List<Task> taskList = new List<Task>();
                ConcurrentBag<Task> bag = new ConcurrentBag<Task>();


                int maxJrnyCombined = 5;

                int mainLoop = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal((Loops / maxJrnyCombined)))) + ((Loops % maxJrnyCombined) > 0 ? 1 : 0);

                for (int n = 0; n < mainLoop; n++)
                {

                    int strtIndex = 0;
                    if (n > 0)
                    {
                        strtIndex = n * maxJrnyCombined;

                    }

                    bag.Add(Task.Run(() => priceitenRequest_WithDirectFlight(signature, Adult, Child, Infant, Final, TSeg1, TSeg2, vc, PCRow, FT, l1, l2, Loops, n, one, two, PiRes_list, PxCnt, strtIndex, maxJrnyCombined)));
                    Thread.Sleep(200);
                    if (taskCount == Count || (taskCount != Count && n == mainLoop - 1))
                    {
                        //Task.WhenAll(taskList).Wait();
                        try
                        {
                            var continuation = Task.WhenAll(bag.ToArray());
                            try
                            {
                                continuation.ConfigureAwait(true);
                                //continuation.RunSynchronously();
                                continuation.Wait();
                            }
                            catch (AggregateException)
                            { }
                            // Thread.Sleep(200);
                            Count = 1;
                            //SerkoLog.SerkoFileHandling("Thread Completed", "GALProfileSync", new Exception(i.ToString() + " PAR Thread Completed On " + DateTime.Now.ToString()), "GDS", "THRDMSG");
                            //taskList = new List<Task>();
                            bag = new ConcurrentBag<Task>();
                            Thread.Sleep(200);
                        }
                        catch(Exception ex) {
                            ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetItneary_Combined_WithDerectFlight)", "Error_001", ex, "priceitenRequest_WithDirectFlight");
                        }
                    }
                    else
                    {
                        Count++;
                    }



                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetItneary_Combined_WithDerectFlight)", "Error_001", ex, "priceitenRequest_WithDirectFlight");
                // throw ex;
            }
            //return Result;
            Task.Delay(0);
            return PiRes_list;
        }

        


        public navitaire.SG.ver4.PriceItineraryResponse[] Spice_GetItneary_List(string signature, int Adult, int Child, int Infant, ArrayList Final, int TSeg1, int TSeg2)
        {
            //Global Variables
            short PxCnt = (short)(Adult + Child);
            //Spice_Logout(signature);
            //signature = Spice_Login();
            //Create an instance of BookingManagerClient
            navitaire.SG.ver4.PriceItineraryResponse piResponse = null;
            navitaire.SG.ver4.PriceItineraryResponse[] PiRes_list = null;
            //IBookingManager bookingAPI = new BookingManagerClient();
            int l1 = 0, l2 = 0, Loops = 0; //For Incrementing journey (For Price Iternary)
            short seg1 = 1, seg2 = 1;
            List<FarePriceJourney_SGV4> one = (List<FarePriceJourney_SGV4>)Final[0];
            l1 = Loops = one.Count;
            List<FarePriceJourney_SGV4> two = null;
            if (Final.Count == 2)
            {
                two = (List<FarePriceJourney_SGV4>)Final[1];
                l2 = two.Count;
                Loops = Loops + two.Count;
            }
            navitaire.SG.ver4.PriceItineraryResponse Result = new navitaire.SG.ver4.PriceItineraryResponse();
            PiRes_list = new navitaire.SG.ver4.PriceItineraryResponse[Loops];
            try
            {
                int secloop = 0, loopsec = 0;//For  Inbound Loop
                for (int n = 0; n < Loops; n++)
                {
                    #region <PriceItineraryRequest>
                    navitaire.SG.ver4.PriceItineraryRequest priceItinRequest = new navitaire.SG.ver4.PriceItineraryRequest();
                    priceItinRequest.Signature = signature;
                    priceItinRequest.ContractVersion = ContractVers;
                    //L1-First Inner Node
                    #region <ItineraryPriceRequest>
                    priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                    priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;
                    //L2-Second Node
                    #region <a:SSRRequest>

                    SSRRequest SQ = new SSRRequest();
                    //#region <a:SegmentSSRRequests>
                    //if (Infant > 0)
                    //{

                    //    try
                    //    {

                    //        #region <a:SegmentSSRRequest>

                    //        #region OutBound
                    //        if (n < one.Count)
                    //        { ////for (int l = 0; l < l1; l++) //For each journey
                    //            SQ.SegmentSSRRequests = new SegmentSSRRequest[one[n].SegCnt]; //[l1 + l2]
                    //            for (int s = 0; s < one[n].SegCnt; s++)//Change l to n
                    //            {
                    //                SegmentSSRRequest A = new SegmentSSRRequest();
                    //                A.FlightDesignator = new FlightDesignator();
                    //                A.FlightDesignator.CarrierCode = one[n].CCD;
                    //                A.FlightDesignator.FlightNumber = one[n].Seg[s]["FNO"];
                    //                A.STD = Convert.ToDateTime(one[n].Seg[s]["STD"].Trim());  //A.STD = Convert.ToDateTime(a["STD"]);
                    //                A.DepartureStation = one[n].Seg[s]["DepS"].Trim();
                    //                A.ArrivalStation = one[n].Seg[s]["ArrS"].Trim();
                    //                A.PaxSSRs = new PaxSSR[Infant];
                    //                for (short i = 0; i < Infant; i++)
                    //                {
                    //                    A.PaxSSRs[i] = new PaxSSR();
                    //                    A.PaxSSRs[i].ActionStatusCode = "NN";
                    //                    A.PaxSSRs[i].ArrivalStation = one[n].Seg[s]["ArrS"].Trim();
                    //                    A.PaxSSRs[i].DepartureStation = one[n].Seg[s]["DepS"].Trim();
                    //                    A.PaxSSRs[i].PassengerNumber = i;
                    //                    A.PaxSSRs[i].SSRCode = "INFT";
                    //                    A.PaxSSRs[i].SSRNumber = 0;
                    //                    A.PaxSSRs[i].SSRValue = 0;
                    //                }
                    //                SQ.SegmentSSRRequests[s] = A;
                    //            }
                    //        }
                    //        #endregion
                    //        #region Inbound
                    //        else if (l2 > 0 && n == one.Count)
                    //        {
                    //            SQ.SegmentSSRRequests = new SegmentSSRRequest[two[secloop].SegCnt];
                    //            for (int s = 0; s < two[secloop].SegCnt; s++)//Change l to n
                    //            {
                    //                SegmentSSRRequest A = new SegmentSSRRequest();
                    //                A.FlightDesignator = new FlightDesignator();
                    //                A.FlightDesignator.CarrierCode = two[secloop].CCD;
                    //                A.FlightDesignator.FlightNumber = two[secloop].Seg[s]["FNO"];
                    //                A.STD = Convert.ToDateTime(two[secloop].Seg[s]["STD"].Trim());
                    //                A.DepartureStation = two[secloop].Seg[s]["DepS"].Trim();
                    //                A.ArrivalStation = two[secloop].Seg[s]["ArrS"].Trim();
                    //                A.PaxSSRs = new PaxSSR[Infant];
                    //                for (short i = 0; i < Infant; i++)
                    //                {
                    //                    A.PaxSSRs[i] = new PaxSSR();
                    //                    A.PaxSSRs[i].ActionStatusCode = "NN";
                    //                    A.PaxSSRs[i].ArrivalStation = two[secloop].Seg[s]["ArrS"].Trim();
                    //                    A.PaxSSRs[i].DepartureStation = two[secloop].Seg[s]["DepS"].Trim();
                    //                    A.PaxSSRs[i].PassengerNumber = i;
                    //                    A.PaxSSRs[i].SSRCode = "INFT";
                    //                    A.PaxSSRs[i].SSRNumber = 0;
                    //                    A.PaxSSRs[i].SSRValue = 0;
                    //                }
                    //                SQ.SegmentSSRRequests[s] = A;
                    //            }
                    //            secloop++;
                    //        }
                    //        #endregion
                    //        #endregion

                    //    }
                    //    catch (Exception ex)
                    //    {
                    //    }
                    //}
                    //#endregion
                    priceItinRequest.ItineraryPriceRequest.SSRRequest = null;// SQ;
                    //priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = "INR";
                    //priceItinRequest.ItineraryPriceRequest.SSRRequest.CancelFirstSSR = false;
                    //priceItinRequest.ItineraryPriceRequest.SSRRequest.SSRFeeForceWaiveOnSell = false;

                    #endregion
                    //L2-Third Node
                    #region <a:SellByKeyRequest>
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[1];
                    #region <a:JourneySellKeys>

                    if (n < one.Count) //for (int l = 0; l < l1; l++)
                    {
                        SellKeyList A = new SellKeyList();
                        A.JourneySellKey = one[n].JSK;
                        A.FareSellKey = one[n].FSK;
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = A;
                    }
                    else if (l2 > 0)
                    {
                        SellKeyList A = new SellKeyList();
                        A.JourneySellKey = two[loopsec].JSK;
                        A.FareSellKey = two[loopsec].FSK;
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = A;
                        loopsec++;
                    }
                    #endregion

                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[PxCnt];
                    #region <a:PaxPriceType>
                    if (Adult > 0)
                    {
                        for (int i = 0; i < Adult; i++)
                        {
                            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "ADT";
                        }
                    }
                    if (Child > 0)
                    {
                        for (int i = Adult; i < (Adult + Child); i++)
                        {
                            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "CHD";
                        }
                    }
                    #endregion

                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = "INR";
                    #region <a:SourcePOS>
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS = new PointOfSale();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.State = MessageState.New;
                    // priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = username;
                    //if (Airline.ToUpper().Trim() == "INDIGO")
                    //    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = "AG";
                    //else
                    //    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = username;
                    if (username == "OTI032")
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = "AG";
                    else
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = username;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.OrganizationCode = OrgCode;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.DomainCode = domain;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.LocationCode = domain;
                    #endregion
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = PxCnt;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = navitaire.SG.ver4.Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations.LoyaltyFilter.MonetaryOnly;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;

                    #endregion

                    #endregion

                    #endregion

                    try
                    {
                        SerializeAnObject(priceItinRequest, "PriceItinReq");
                        piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);
                        SerializeAnObject(piResponse, "PriceItinRes");
                    }
                    catch (Exception ex)
                    {
                        piResponse = null;
                        ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetItneary_List)", "Error_001", ex, "PriceItinReq-PriceItinRes");
                        throw ex;
                    }
                    PiRes_list[n] = piResponse;
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetItneary_List)", "Error_001", ex, "PriceItinReq-PriceItinRes");
                throw ex;
            }
            //return Result;
            return PiRes_list;
        }
        
        //Sell Combined Journey 
        public navitaire.SG.ver4.PriceItineraryResponse[] Spice_GetItneary_Combined(string signature, int Adult, int Child, int Infant, ArrayList Final, int TSeg1, int TSeg2, string vc, DataRow[] PCRow, string[] FT)
        {
            //Global Variables
            short PxCnt = (short)(Adult + Child);
            //Create an instance of BookingManagerClient
            navitaire.SG.ver4.PriceItineraryResponse piResponse = null;
            navitaire.SG.ver4.PriceItineraryResponse[] PiRes_list = null;
            //IBookingManager bookingAPI = new BookingManagerClient();
            int l1 = 0, l2 = 0, Loops = 0; //For Incrementing journey (For Price Iternary)
            short seg1 = 1, seg2 = 1;
            List<FarePriceJourney_SGV4> one = (List<FarePriceJourney_SGV4>)Final[0];
            l1 = Loops = one.Count;
            List<FarePriceJourney_SGV4> two = null;
            if (Final.Count == 2)
            {
                two = (List<FarePriceJourney_SGV4>)Final[1];
                l2 = two.Count;
                //Loops = Loops + two.Count; 12 December
                if (l1 >= l2)
                {
                    Loops = l1;
                }
                else
                {
                    Loops = l2;
                }
            }
            navitaire.SG.ver4.PriceItineraryResponse Result = new navitaire.SG.ver4.PriceItineraryResponse();
            PiRes_list = new navitaire.SG.ver4.PriceItineraryResponse[Loops]; //Total PriceItinerary Responses
            try
            {
                //one.Where(x=>x.SegCnt==1).()to



                int ssrcntr_o = 0, ssrcntr_i = 0; //For SegmentSSRRequest Looping
                //12th December
                int fstloop = 0, loopsec = 0;//For outbound loop //For  Inbound Loop
                for (int n = 0; n < Loops; n++)
                {
                    #region <PriceItineraryRequest>
                    navitaire.SG.ver4.PriceItineraryRequest priceItinRequest = new navitaire.SG.ver4.PriceItineraryRequest();
                    priceItinRequest.Signature = signature;
                    priceItinRequest.ContractVersion = ContractVers;
                    //L1-First Inner Node
                    #region <ItineraryPriceRequest>
                    priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
                    priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;
                    #region <a:TypeOfSale>
                    if ((PCRow.Count()) > 0 && Convert.ToString(PCRow[0]["AppliedOn"]) == "BOTH")
                    {
                        priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                        priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = Convert.ToString(PCRow[0]["D_T_Code"]);
                        //string[] ft = new string[1];
                        //ft[0] = "C";
                        priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes = FT;
                    }
                    #endregion
                    //L2-Second Node
                    #region <a:SSRRequest>

                    SSRRequest SQ = new SSRRequest();
                    #region <a:SegmentSSRRequests> For Infant
                    if (Infant > 0)
                    {

                        try
                        {

                            #region <a:SegmentSSRRequest>
                            //12th December 
                            if (ssrcntr_o == l1) { ssrcntr_o = 0; } else if (ssrcntr_i == l2) { ssrcntr_i = 0; }
                            #region OutBound
                            if (n < Loops)
                            {   //For each journey Create Segment Requests for that Single Journey
                                SQ.SegmentSSRRequests = new SegmentSSRRequest[one[ssrcntr_o].SegCnt]; //[l1 + l2]
                                for (int s = 0; s < one[ssrcntr_o].SegCnt; s++)//Change l to n
                                {
                                    SegmentSSRRequest A = new SegmentSSRRequest();
                                    A.FlightDesignator = new FlightDesignator();
                                    A.FlightDesignator.CarrierCode = one[ssrcntr_o].CCD;
                                    A.FlightDesignator.FlightNumber = one[ssrcntr_o].Seg[s]["FNO"];
                                    A.STD = Convert.ToDateTime(one[ssrcntr_o].Seg[s]["STD"].Trim());  //A.STD = Convert.ToDateTime(a["STD"]);
                                    A.DepartureStation = one[ssrcntr_o].Seg[s]["DepS"].Trim();
                                    A.ArrivalStation = one[ssrcntr_o].Seg[s]["ArrS"].Trim();
                                    A.PaxSSRs = new PaxSSR[Infant];
                                    for (short i = 0; i < Infant; i++)
                                    {
                                        A.PaxSSRs[i] = new PaxSSR();
                                        A.PaxSSRs[i].ActionStatusCode = "NN";
                                        A.PaxSSRs[i].ArrivalStation = one[ssrcntr_o].Seg[s]["ArrS"].Trim();
                                        A.PaxSSRs[i].DepartureStation = one[ssrcntr_o].Seg[s]["DepS"].Trim();
                                        A.PaxSSRs[i].PassengerNumber = i;
                                        A.PaxSSRs[i].SSRCode = "INFT";
                                        A.PaxSSRs[i].SSRNumber = 0;
                                        A.PaxSSRs[i].SSRValue = 0;
                                    }
                                    SQ.SegmentSSRRequests[s] = A;
                                }
                                ssrcntr_o++;
                            }
                            #endregion
                            #region Inbound
                            if (l2 > 0)
                            {
                                SQ.SegmentSSRRequests = new SegmentSSRRequest[two[ssrcntr_i].SegCnt];
                                for (int s = 0; s < two[ssrcntr_i].SegCnt; s++)//Change l to n
                                {
                                    SegmentSSRRequest A = new SegmentSSRRequest();
                                    A.FlightDesignator = new FlightDesignator();
                                    A.FlightDesignator.CarrierCode = two[ssrcntr_i].CCD;
                                    A.FlightDesignator.FlightNumber = two[ssrcntr_i].Seg[s]["FNO"];
                                    A.STD = Convert.ToDateTime(two[ssrcntr_i].Seg[s]["STD"].Trim());
                                    A.DepartureStation = two[ssrcntr_i].Seg[s]["DepS"].Trim();
                                    A.ArrivalStation = two[ssrcntr_i].Seg[s]["ArrS"].Trim();
                                    A.PaxSSRs = new PaxSSR[Infant];
                                    for (short i = 0; i < Infant; i++)
                                    {
                                        A.PaxSSRs[i] = new PaxSSR();
                                        A.PaxSSRs[i].ActionStatusCode = "NN";
                                        A.PaxSSRs[i].ArrivalStation = two[ssrcntr_i].Seg[s]["ArrS"].Trim();
                                        A.PaxSSRs[i].DepartureStation = two[ssrcntr_i].Seg[s]["DepS"].Trim();
                                        A.PaxSSRs[i].PassengerNumber = i;
                                        A.PaxSSRs[i].SSRCode = "INFT";
                                        A.PaxSSRs[i].SSRNumber = 0;
                                        A.PaxSSRs[i].SSRValue = 0;
                                    }
                                    SQ.SegmentSSRRequests[s] = A;
                                }
                                ssrcntr_i++;
                            }
                            #endregion
                            #endregion

                        }
                        catch (Exception ex)
                        {
                            ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetItneary_Combined)", "Error_001", ex, "SegmentSSRRequest");
                        }
                    }
                    #endregion
                    priceItinRequest.ItineraryPriceRequest.SSRRequest = SQ;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = "INR";
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.CancelFirstSSR = false;
                    priceItinRequest.ItineraryPriceRequest.SSRRequest.SSRFeeForceWaiveOnSell = false;

                    #endregion
                    //L2-Third Node
                    #region <a:SellByKeyRequest>
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
                    //12th December
                    if (Final.Count == 2)
                    {
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[2];
                    }
                    else
                    {
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[1];
                    }
                    #region <a:JourneySellKeys>
                    //12th December 
                    if (fstloop == l1) { fstloop = 0; } else if (loopsec == l2) { loopsec = 0; }
                    if (fstloop < one.Count)
                    {
                        SellKeyList A = new SellKeyList();
                        A.JourneySellKey = one[fstloop].JSK;
                        A.FareSellKey = one[fstloop].FSK;
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = A;
                        fstloop++;
                    }
                    if (loopsec < l2)
                    {
                        SellKeyList A = new SellKeyList();
                        A.JourneySellKey = two[loopsec].JSK;
                        A.FareSellKey = two[loopsec].FSK;
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1] = A;
                        loopsec++;
                    }
                    #endregion

                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[PxCnt];
                    #region <a:PaxPriceType>
                    if (Adult > 0)
                    {
                        for (int i = 0; i < Adult; i++)
                        {
                            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "ADT";
                        }
                    }
                    if (Child > 0)
                    {
                        for (int i = Adult; i < (Adult + Child); i++)
                        {
                            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "CHD";
                        }
                    }
                    #endregion

                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = "INR";
                    #region <a:SourcePOS>
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS = new PointOfSale();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.State = MessageState.New;
                    // priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = username;
                    if (username == "OTI032")
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = "AG";
                    else
                        priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = username;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.OrganizationCode = OrgCode;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.DomainCode = domain;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.LocationCode = domain;
                    #endregion
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = PxCnt;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = navitaire.SG.ver4.Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations.LoyaltyFilter.MonetaryOnly;
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;

                    #endregion

                    #endregion

                    #endregion

                    try
                    {
                        SerializeAnObject(priceItinRequest, "PriceItinReq");
                        piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);
                        SerializeAnObject(piResponse, "PriceItinRes");
                    }
                    catch (Exception ex)
                    {
                        piResponse = null;
                        ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetItneary_Combined)", "Error_001", ex, "PriceItinReq-PriceItinRes");
                        // throw ex;
                    }
                    PiRes_list[n] = piResponse;
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetItneary_Combined)", "Error_001", ex, "SegmentSSRRequest");
                // throw ex;
            }
            //return Result;
            return PiRes_list;
        }
        
        public navitaire.SG.ver4.PriceItineraryResponse[] Spice_GetItneary_Combined1(string signature, int Adult, int Child, int Infant, ArrayList Final, int TSeg1, int TSeg2, string vc, DataRow[] PCRow, string[] FT)
        {
            //Global Variables
            short PxCnt = (short)(Adult + Child);
            //Create an instance of BookingManagerClient

            navitaire.SG.ver4.PriceItineraryResponse[] PiRes_list = null;
            //IBookingManager bookingAPI = new BookingManagerClient();
            int l1 = 0, l2 = 0, Loops = 0; //For Incrementing journey (For Price Iternary)
            short seg1 = 1, seg2 = 1;
            List<FarePriceJourney_SGV4> one = (List<FarePriceJourney_SGV4>)Final[0];
            l1 = Loops = one.Count;
            List<FarePriceJourney_SGV4> two = null;
            if (Final.Count == 2)
            {
                two = (List<FarePriceJourney_SGV4>)Final[1];
                l2 = two.Count;
                //Loops = Loops + two.Count; 12 December
                if (l1 >= l2)
                {
                    Loops = l1;
                }
                else
                {
                    Loops = l2;
                }
            }
            navitaire.SG.ver4.PriceItineraryResponse Result = new navitaire.SG.ver4.PriceItineraryResponse();
            PiRes_list = new navitaire.SG.ver4.PriceItineraryResponse[Loops]; //Total PriceItinerary Responses
            try
            {
                //one.Where(x=>x.SegCnt==1).()to



                //int ssrcntr_o = 0, ssrcntr_i = 0; //For SegmentSSRRequest Looping
                ////12th December
                //int fstloop = 0, loopsec = 0;//For outbound loop //For  Inbound Loop

                int taskCount = int.Parse(System.Configuration.ConfigurationManager.AppSettings["TaskCnt"].ToString());
                int Count = 1;
                //List<Task> taskList = new List<Task>();
                ConcurrentBag<Task> bag = new ConcurrentBag<Task>();

                for (int n = 0; n < Loops; n++)
                {


                    bag.Add(Task.Factory.StartNew(async () => await priceitenRequest(signature, Adult, Child, Infant, Final, TSeg1, TSeg2, vc, PCRow, FT, l1, l2, Loops, n, one, two, PiRes_list, PxCnt)));
                    Thread.Sleep(250);
                    if (taskCount == Count || (taskCount != Count && n == Loops - 1))
                    {
                        //Task.WhenAll(taskList).Wait();
                        try
                        {
                            var continuation = Task.WhenAll(bag.ToArray());
                            try
                            {
                                continuation.ConfigureAwait(true);
                                //continuation.RunSynchronously();
                                continuation.Wait();
                            }
                            catch (AggregateException)
                            { }
                            // Thread.Sleep(200);
                            Count = 1;
                            //SerkoLog.SerkoFileHandling("Thread Completed", "GALProfileSync", new Exception(i.ToString() + " PAR Thread Completed On " + DateTime.Now.ToString()), "GDS", "THRDMSG");
                            //taskList = new List<Task>();
                            bag = new ConcurrentBag<Task>();
                            Thread.Sleep(200);
                        }
                        catch(Exception ex)
                        {
                            ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetItneary_Combined1)", "Error_001", ex, "priceitenRequest");
                        }
                    }
                    else
                    {
                        Count++;
                    }



                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetItneary_Combined1)", "Error_001", ex, "priceitenRequest");
                // throw ex;
            }
            //return Result;
            try
            {
                File.AppendAllText("C:\\\\CPN_SP\\\\6E_FWS_RES_INDIRECT_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "6E_Null_Result:" + Convert.ToString(PiRes_list.Where(x => x == null).ToList().Count()) + " ,6E_Toatl_Result:" + Convert.ToString(PiRes_list.Count()) + Environment.NewLine);
            }
            catch (Exception ex)
            {

            }

            return PiRes_list;
        }
       
        public async Task priceitenRequest(string signature, int Adult, int Child, int Infant, ArrayList Final, int TSeg1, int TSeg2, string vc, DataRow[] PCRow, string[] FT, int l1, int l2, int Loops, int n, List<FarePriceJourney_SGV4> one, List<FarePriceJourney_SGV4> two, navitaire.SG.ver4.PriceItineraryResponse[] PiRes_list, int PxCnt)
        {

            await Task.Delay(0);
            navitaire.SG.ver4.PriceItineraryResponse piResponse = null;
            #region <PriceItineraryRequest>
            navitaire.SG.ver4.PriceItineraryRequest priceItinRequest = new navitaire.SG.ver4.PriceItineraryRequest();
            priceItinRequest.Signature = signature;
            priceItinRequest.ContractVersion = ContractVers;
            //L1-First Inner Node
            #region <ItineraryPriceRequest>
            priceItinRequest.ItineraryPriceRequest = new ItineraryPriceRequest();
            priceItinRequest.ItineraryPriceRequest.PriceItineraryBy = PriceItineraryBy.JourneyBySellKey;
            #region <a:TypeOfSale>
            if ((PCRow.Count()) > 0 && Convert.ToString(PCRow[0]["AppliedOn"]) == "BOTH")
            {
                priceItinRequest.ItineraryPriceRequest.TypeOfSale = new TypeOfSale();
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.PromotionCode = Convert.ToString(PCRow[0]["D_T_Code"]);
                //string[] ft = new string[1];
                //ft[0] = "C";
                priceItinRequest.ItineraryPriceRequest.TypeOfSale.FareTypes = FT;
            }
            #endregion
            //L2-Second Node
            #region <a:SSRRequest>

            SSRRequest SQ = new SSRRequest();
            #region <a:SegmentSSRRequests> For Infant
            if (Infant > 0)
            {

                try
                {

                    #region <a:SegmentSSRRequest>
                    //12th December 

                    int ssrcntr_o = n;
                    int ssrcntr_i = n;
                    if (ssrcntr_o == l1) { ssrcntr_o = 0; } else if (ssrcntr_i == l2) { ssrcntr_i = 0; }
                    #region OutBound
                    if (n < Loops)
                    {   //For each journey Create Segment Requests for that Single Journey

                        if (one != null && one.Count > ssrcntr_o)
                        {
                            SQ.SegmentSSRRequests = new SegmentSSRRequest[one[ssrcntr_o].SegCnt]; //[l1 + l2]
                            for (int s = 0; s < one[ssrcntr_o].SegCnt; s++)//Change l to n
                            {
                                SegmentSSRRequest A = new SegmentSSRRequest();
                                A.FlightDesignator = new FlightDesignator();
                                A.FlightDesignator.CarrierCode = one[ssrcntr_o].CCD;
                                A.FlightDesignator.FlightNumber = one[ssrcntr_o].Seg[s]["FNO"];
                                A.STD = Convert.ToDateTime(one[ssrcntr_o].Seg[s]["STD"].Trim());  //A.STD = Convert.ToDateTime(a["STD"]);
                                A.DepartureStation = one[ssrcntr_o].Seg[s]["DepS"].Trim();
                                A.ArrivalStation = one[ssrcntr_o].Seg[s]["ArrS"].Trim();
                                A.PaxSSRs = new PaxSSR[Infant];
                                for (short i = 0; i < Infant; i++)
                                {
                                    A.PaxSSRs[i] = new PaxSSR();
                                    A.PaxSSRs[i].ActionStatusCode = "NN";
                                    A.PaxSSRs[i].ArrivalStation = one[ssrcntr_o].Seg[s]["ArrS"].Trim();
                                    A.PaxSSRs[i].DepartureStation = one[ssrcntr_o].Seg[s]["DepS"].Trim();
                                    A.PaxSSRs[i].PassengerNumber = i;
                                    A.PaxSSRs[i].SSRCode = "INFT";
                                    A.PaxSSRs[i].SSRNumber = 0;
                                    A.PaxSSRs[i].SSRValue = 0;
                                }
                                SQ.SegmentSSRRequests[s] = A;
                            }
                        }


                        ssrcntr_o++;
                    }
                    #endregion
                    #region Inbound
                    if (l2 > 0)
                    {
                        if (one != null && one.Count > ssrcntr_o)
                        {
                            SQ.SegmentSSRRequests = new SegmentSSRRequest[two[ssrcntr_i].SegCnt];
                            for (int s = 0; s < two[ssrcntr_i].SegCnt; s++)//Change l to n
                            {
                                SegmentSSRRequest A = new SegmentSSRRequest();
                                A.FlightDesignator = new FlightDesignator();
                                A.FlightDesignator.CarrierCode = two[ssrcntr_i].CCD;
                                A.FlightDesignator.FlightNumber = two[ssrcntr_i].Seg[s]["FNO"];
                                A.STD = Convert.ToDateTime(two[ssrcntr_i].Seg[s]["STD"].Trim());
                                A.DepartureStation = two[ssrcntr_i].Seg[s]["DepS"].Trim();
                                A.ArrivalStation = two[ssrcntr_i].Seg[s]["ArrS"].Trim();
                                A.PaxSSRs = new PaxSSR[Infant];
                                for (short i = 0; i < Infant; i++)
                                {
                                    A.PaxSSRs[i] = new PaxSSR();
                                    A.PaxSSRs[i].ActionStatusCode = "NN";
                                    A.PaxSSRs[i].ArrivalStation = two[ssrcntr_i].Seg[s]["ArrS"].Trim();
                                    A.PaxSSRs[i].DepartureStation = two[ssrcntr_i].Seg[s]["DepS"].Trim();
                                    A.PaxSSRs[i].PassengerNumber = i;
                                    A.PaxSSRs[i].SSRCode = "INFT";
                                    A.PaxSSRs[i].SSRNumber = 0;
                                    A.PaxSSRs[i].SSRValue = 0;
                                }
                                SQ.SegmentSSRRequests[s] = A;
                            }
                        }
                        ssrcntr_i++;
                    }
                    #endregion
                    #endregion

                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(priceitenRequest)", "Error_001", ex, "SegmentSSRRequest");
                }
            }
            #endregion
            priceItinRequest.ItineraryPriceRequest.SSRRequest = SQ;
            priceItinRequest.ItineraryPriceRequest.SSRRequest.CurrencyCode = "INR";
            priceItinRequest.ItineraryPriceRequest.SSRRequest.CancelFirstSSR = false;
            priceItinRequest.ItineraryPriceRequest.SSRRequest.SSRFeeForceWaiveOnSell = false;

            #endregion
            //L2-Third Node
            #region <a:SellByKeyRequest>
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest = new SellJourneyByKeyRequestData();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.ActionStatusCode = "NN";
            //12th December
            if (Final.Count == 2)
            {
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[2];
            }
            else
            {
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys = new SellKeyList[1];
            }
            #region <a:JourneySellKeys>
            //12th December 
            int fstloop = n;
            int loopsec = n;
            if (fstloop == l1) { fstloop = 0; } else if (loopsec == l2) { loopsec = 0; }
            if (fstloop < one.Count)
            {
                SellKeyList A = new SellKeyList();
                A.JourneySellKey = one[fstloop].JSK;
                A.FareSellKey = one[fstloop].FSK;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[0] = A;
                fstloop++;
            }
            if (loopsec < l2)
            {
                SellKeyList A = new SellKeyList();
                A.JourneySellKey = two[loopsec].JSK;
                A.FareSellKey = two[loopsec].FSK;
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.JourneySellKeys[1] = A;
                loopsec++;
            }
            #endregion

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType = new PaxPriceType[PxCnt];
            #region <a:PaxPriceType>
            if (Adult > 0)
            {
                for (int i = 0; i < Adult; i++)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "ADT";
                }
            }
            if (Child > 0)
            {
                for (int i = Adult; i < (Adult + Child); i++)
                {
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i] = new PaxPriceType();
                    priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxPriceType[i].PaxType = "CHD";
                }
            }
            #endregion

            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.CurrencyCode = "INR";
            #region <a:SourcePOS>
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS = new PointOfSale();
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.State = MessageState.New;
            // priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = username;
            if (username == "OTI032")
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = "AG";
            else
                priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.AgentCode = username;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.OrganizationCode = OrgCode;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.DomainCode = domain;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.SourcePOS.LocationCode = domain;
            #endregion
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.PaxCount = (short)PxCnt;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.LoyaltyFilter = navitaire.SG.ver4.Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations.LoyaltyFilter.MonetaryOnly;
            priceItinRequest.ItineraryPriceRequest.SellByKeyRequest.IsAllotmentMarketFare = false;

            #endregion

            #endregion

            #endregion

            try
            {
                SerializeAnObject(priceItinRequest, "PriceItinReq");
                piResponse = bookingAPI.GetItineraryPrice(priceItinRequest);
                SerializeAnObject(piResponse, "PriceItinRes");
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(priceitenRequest)", "Error_001", ex, "priceitenRequest");
                piResponse = null;
                // throw ex;
            }
            PiRes_list[n] = piResponse;


        }
        
        public List<FarePriceJourney_SGV4> Spice_AddPrice_ToFareList(navitaire.SG.ver4.PriceItineraryResponse[] piResponse_L, List<FarePriceJourney_SGV4> a, int infant, string VC, List<string> FNO)
        {
            #region FareBreakUp
            int rec = 0;
            piResponse_L = piResponse_L.Where(x => x != null).ToArray();
            List<FarePriceJourney_SGV4> ab = new List<FarePriceJourney_SGV4>();

            while (rec < a.Count)
            {
                bool chkmatch = false;
                for (int i = 0; i < piResponse_L.Length; i++)//Added For Loop
                {
                    navitaire.SG.ver4.PriceItineraryResponse piResponse = piResponse_L[i];
                    if ((piResponse != null) && (piResponse.Booking.Journeys.Length > 0))
                    {
                        for (int j = 0; j < piResponse.Booking.Journeys.Length; j++)
                        {
                            if (rec == a.Count)
                                break;
                            if (piResponse.Booking.Journeys[j].JourneySellKey == a[rec].JSK)
                            {
                                chkmatch = true;
                                a[rec].PaxFare = piResponse.Booking.Journeys[j].Segments[0].Fares[0].PaxFares;
                                #region Changes 15 NOV
                                //if (VC == "SG")
                                if (VC == "SG" || VC == "6E")
                                {
                                    string SG_FAT = "";
                                    foreach (navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.Segment sg_check in piResponse.Booking.Journeys[j].Segments)
                                    {
                                        SG_FAT = SG_FAT + sg_check.Fares[0].FareApplicationType.ToString().ToLower() + " ";
                                    }
                                    if (piResponse.Booking.Journeys[j].Segments.Count() > 1 && (SG_FAT.Contains("governing") || SG_FAT.Contains("sector")))
                                    {

                                        for (int sg = 1; sg < piResponse.Booking.Journeys[j].Segments.Count(); sg++)
                                        {
                                            int pxt = 0;
                                            foreach (PaxFare px in piResponse.Booking.Journeys[j].Segments.ElementAt(sg).Fares[0].PaxFares)
                                            {
                                                decimal aa = 0, bb = 0, cc = 0;
                                                bool flgaa = false;
                                                bool flgbb = false;
                                                bool flgcc = false;

                                                foreach (BookingServiceCharge sv in piResponse.Booking.Journeys[j].Segments.ElementAt(sg).Fares[0].PaxFares.ElementAt(pxt).ServiceCharges) //for (int sv = 0; sv < piResponse.Booking.Journeys[j].Segments.ElementAt(sg).Fares[0].PaxFares.ElementAt(0).ServiceCharges.Count(); sv++)
                                                {
                                                    //if (sv.ChargeCode.ToString() == String.Empty)
                                                    if (sv.ChargeCode.ToString() == String.Empty && sv.ChargeType.ToString() == "FarePrice")
                                                        aa += sv.Amount;
                                                    else if (sv.ChargeCode.ToString() == "YQ")
                                                        bb += sv.Amount;
                                                    else
                                                        cc += sv.Amount;
                                                }
                                                foreach (BookingServiceCharge sv in a[rec].PaxFare.ElementAt(pxt).ServiceCharges)
                                                {
                                                    //if ((sv.ChargeCode.ToString() == String.Empty) && (aa > 0))
                                                    if ((sv.ChargeCode.ToString() == String.Empty) && (aa > 0) && flgaa == false)
                                                    { sv.Amount += aa; flgaa = true; }
                                                    else if ((sv.ChargeCode.ToString() == "YQ") && (bb > 0) && flgbb == false)
                                                    { sv.Amount += bb; flgbb = true; }
                                                    else
                                                    {
                                                        if (cc > 0 && flgcc == false)
                                                        {
                                                            sv.Amount += cc; flgcc = true;
                                                        }
                                                        cc = 0;

                                                    }
                                                }

                                                pxt++;
                                            }
                                        }
                                    }

                                }
                                #endregion


                                ab.Add(a[rec]);
                                try
                                {
                                    a.RemoveAt(rec);
                                }
                                catch { }
                            }

                        }
                    }
                    else if (piResponse == null)
                    {
                        //try
                        //{
                        //    a.RemoveAt(rec); //rec++;
                        //}
                        //catch (Exception exx)
                        //{ }
                    }
                    else if ((piResponse != null) && (piResponse.Booking.Journeys.Length == 0))
                    {
                        // a.RemoveAt(rec);
                    }


                }


                if (chkmatch == false)
                {

                    try
                    {
                        // a.RemoveAt(rec); //rec++;
                    }
                    catch (Exception exx)
                    { }
                }
                rec++;
            }
            #endregion

            #region Infant
            // for (int i = 0; i < piResponse_L.Length; i++)
            for (int i = 0; i < ab.Count; i++)
            {
                navitaire.SG.ver4.PriceItineraryResponse piResponse = piResponse_L[0];
                if ((piResponse != null) && (infant > 0) && (piResponse.Booking.Passengers[0].PassengerFees.Length > 0))
                {
                    #region Lift Infant Fare according to First Flight OLd
                    //for (int j = 0; j < a.Count; j++)
                    //{
                    //    try
                    //    {
                    //        var srv = (from pfee in piResponse.Booking.Passengers[0].PassengerFees
                    //                   where pfee.FlightReference.Contains("SG" + a[j].FNO)
                    //                   where pfee.SSRCode == "INFT"
                    //                   select pfee.ServiceCharges);
                    //        decimal infare = 0;
                    //        decimal inftax = 0;
                    //        foreach (var b in srv)
                    //        {
                    //            foreach (BookingServiceCharge bs in b)
                    //            {
                    //                if (bs.ChargeCode == "INFT")
                    //                {
                    //                    infare = infare + bs.Amount;
                    //                }
                    //                else
                    //                {
                    //                    if (bs.ChargeType == ChargeType.IncludedTax)
                    //                    { inftax = inftax + bs.Amount; }
                    //                    else
                    //                    {
                    //                        infare = infare + bs.Amount;
                    //                        inftax = inftax + bs.Amount;
                    //                    }
                    //                }
                    //            }
                    //        }
                    //        a[j].InfFare = infare;
                    //        a[j].InfTax = inftax;
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        a[j].InfFare = 0;
                    //        a[j].InfTax = 0;
                    //    }
                    //}
                    #endregion

                    #region Lift Infant Fare according to First Flight New
                    try
                    {
                        var srv = (from pfee in piResponse.Booking.Passengers[0].PassengerFees
                                   where pfee.FlightReference.Contains(VC.Trim() + FNO[0]) //where pfee.FlightReference.Contains("SG" + a[j].FNO)
                                   where pfee.SSRCode == "INFT"
                                   select pfee.ServiceCharges);
                        decimal infare = 0;
                        decimal inftax = 0;
                        foreach (var b in srv)
                        {
                            foreach (BookingServiceCharge bs in b)
                            {
                                if (bs.ChargeCode == "INFT")
                                {
                                    infare = infare + bs.Amount;
                                }
                                else
                                {
                                    if (bs.ChargeType == ChargeType.IncludedTax)
                                    { inftax = inftax + bs.Amount; }
                                    else
                                    {
                                        infare = infare + bs.Amount;
                                        inftax = inftax + bs.Amount;
                                    }
                                }
                            }
                        }


                        ab[i].InfFare = infare;
                        ab[i].InfTax = inftax;

                    }
                    catch (Exception ex)
                    {

                        ab[i].InfFare = 0;
                        ab[i].InfTax = 0;

                    }

                    #endregion
                }
            }

            #endregion


            #region Filter Only Price Results
            List<FarePriceJourney_SGV4> Final = new List<FarePriceJourney_SGV4>();
            for (int j = 0; j < ab.Count; j++)
            {
                if (ab[j].PaxFare != null)
                {
                    Final.Add(ab[j]);
                }
            }
            #endregion
            return Final;
        }


        //public List<FarePriceJourney_SGV4> Spice_AddPrice_ToFareList(navitaire.SG.ver4.PriceItineraryResponse[] piResponse_L, List<FarePriceJourney_SGV4> a, int infant, string VC, List<string> FNO)
        //{
        //    #region FareBreakUp
        //    int rec = 0;

        //    while (rec < a.Count)
        //    {
        //        for (int i = 0; i < piResponse_L.Length; i++)//Added For Loop
        //        {
        //            navitaire.SG.ver4.PriceItineraryResponse piResponse = piResponse_L[i];
        //            if ((piResponse != null) && (piResponse.Booking.Journeys.Length > 0))
        //            {
        //                for (int j = 0; j < piResponse.Booking.Journeys.Length; j++)
        //                {
        //                    if (rec == a.Count)
        //                        break;
        //                    if (piResponse.Booking.Journeys[j].JourneySellKey == a[rec].JSK)
        //                    {
        //                        a[rec].PaxFare = piResponse.Booking.Journeys[j].Segments[0].Fares[0].PaxFares;
        //                        #region Changes 15 NOV
        //                        //if (VC == "SG")
        //                        if (VC == "SG" || VC == "6E" || VC == "G8")
        //                        {
        //                            string SG_FAT = "";
        //                            foreach (navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.Segment sg_check in piResponse.Booking.Journeys[j].Segments)
        //                            {
        //                                SG_FAT = SG_FAT + sg_check.Fares[0].FareApplicationType.ToString().ToLower() + " ";
        //                            }
        //                            if (piResponse.Booking.Journeys[j].Segments.Count() > 1 && (SG_FAT.Contains("governing") || SG_FAT.Contains("sector")))
        //                            {

        //                                for (int sg = 1; sg < piResponse.Booking.Journeys[j].Segments.Count(); sg++)
        //                                {
        //                                    int pxt = 0;
        //                                    foreach (PaxFare px in piResponse.Booking.Journeys[j].Segments.ElementAt(sg).Fares[0].PaxFares)
        //                                    {
        //                                        decimal aa = 0, bb = 0, cc = 0;
        //                                        foreach (BookingServiceCharge sv in piResponse.Booking.Journeys[j].Segments.ElementAt(sg).Fares[0].PaxFares.ElementAt(pxt).ServiceCharges) //for (int sv = 0; sv < piResponse.Booking.Journeys[j].Segments.ElementAt(sg).Fares[0].PaxFares.ElementAt(0).ServiceCharges.Count(); sv++)
        //                                        {
        //                                            if (sv.ChargeCode.ToString() == String.Empty)
        //                                                aa += sv.Amount;
        //                                            else if (sv.ChargeCode.ToString() == "YQ")
        //                                                bb += sv.Amount;
        //                                            else
        //                                                cc += sv.Amount;
        //                                        }
        //                                        foreach (BookingServiceCharge sv in a[rec].PaxFare.ElementAt(pxt).ServiceCharges)
        //                                        {
        //                                            if ((sv.ChargeCode.ToString() == String.Empty) && (aa > 0))
        //                                                sv.Amount += aa;
        //                                            else if ((sv.ChargeCode.ToString() == "YQ") && (bb > 0))
        //                                                sv.Amount += bb;
        //                                            else
        //                                            {
        //                                                if (cc > 0)
        //                                                    sv.Amount += cc;
        //                                                cc = 0;
        //                                            }
        //                                        }

        //                                        pxt++;
        //                                    }
        //                                }
        //                            }

        //                        }
        //                        #endregion


        //                        rec++;
        //                    }
        //                }
        //            }
        //            else if (piResponse == null)
        //            {
        //                a.RemoveAt(rec); //rec++;
        //            }
        //            else if ((piResponse != null) && (piResponse.Booking.Journeys.Length == 0))
        //            {
        //                a.RemoveAt(rec);
        //            }

        //        }
        //    }
        //    #endregion

        //    #region Infant
        //    for (int i = 0; i < piResponse_L.Length; i++)
        //    {
        //        navitaire.SG.ver4.PriceItineraryResponse piResponse = piResponse_L[i];
        //        if ((piResponse != null) && (infant > 0) && (piResponse.Booking.Passengers[0].PassengerFees.Length > 0))
        //        {
        //            #region Lift Infant Fare according to First Flight
        //            //for (int j = 0; j < a.Count; j++)
        //            //{
        //            //    try
        //            //    {
        //            //        var srv = (from pfee in piResponse.Booking.Passengers[0].PassengerFees
        //            //                   where pfee.FlightReference.Contains(VC.Trim() + a[j].FNO) //where pfee.FlightReference.Contains("SG" + a[j].FNO)
        //            //                   where pfee.SSRCode == "INFT"
        //            //                   select pfee.ServiceCharges);
        //            //        decimal infare = 0;
        //            //        decimal inftax = 0;
        //            //        foreach (var b in srv)
        //            //        {
        //            //            foreach (BookingServiceCharge bs in b)
        //            //            {
        //            //                if (bs.ChargeCode == "INFT")
        //            //                {
        //            //                    infare = infare + bs.Amount;
        //            //                }
        //            //                else
        //            //                {
        //            //                    if (bs.ChargeType == ChargeType.IncludedTax)
        //            //                    { inftax = inftax + bs.Amount; }
        //            //                    else
        //            //                    {
        //            //                        infare = infare + bs.Amount;
        //            //                        inftax = inftax + bs.Amount;
        //            //                    }
        //            //                }
        //            //            }
        //            //        }
        //            //        a[j].InfFare = infare;
        //            //        a[j].InfTax = inftax;
        //            //    }
        //            //    catch (Exception ex)
        //            //    {
        //            //        a[j].InfFare = 0;
        //            //        a[j].InfTax = 0;
        //            //    }
        //            //}
        //            #endregion
        //            #region New
        //            //for (int j = 0; j < a.Count; j++)
        //            //{
        //            try
        //            {
        //                var srv = (from pfee in piResponse.Booking.Passengers[0].PassengerFees
        //                           where pfee.FlightReference.Contains(VC.Trim() + FNO[i]) //where pfee.FlightReference.Contains("SG" + a[j].FNO)
        //                           where pfee.SSRCode == "INFT"
        //                           select pfee.ServiceCharges);
        //                decimal infare = 0;
        //                decimal inftax = 0;
        //                foreach (var b in srv)
        //                {
        //                    foreach (BookingServiceCharge bs in b)
        //                    {
        //                        if (bs.ChargeCode == "INFT")
        //                        {
        //                            infare = infare + bs.Amount;
        //                        }
        //                        else
        //                        {
        //                            if (bs.ChargeType == ChargeType.IncludedTax)
        //                            { inftax = inftax + bs.Amount; }
        //                            else
        //                            {
        //                                infare = infare + bs.Amount;
        //                                inftax = inftax + bs.Amount;
        //                            }
        //                        }
        //                    }
        //                }
        //                a[i].InfFare = infare;
        //                a[i].InfTax = inftax;
        //            }
        //            catch (Exception ex)
        //            {
        //                a[i].InfFare = 0;
        //                a[i].InfTax = 0;
        //            }
        //            //}

        //            #endregion

        //        }
        //    }

        //    #endregion


        //    #region Filter Only Price Results
        //    List<FarePriceJourney_SGV4> Final = new List<FarePriceJourney_SGV4>();
        //    for (int j = 0; j < a.Count; j++)
        //    {
        //        if (a[j].PaxFare != null)
        //        {
        //            Final.Add(a[j]);
        //        }
        //    }
        //    #endregion
        //    return Final;
        //}

        public ArrayList Spice_RoundTripFare(ArrayList List, FlightSearch obj, List<FltSrvChargeList> SrvchargeList, string CrdType)
        {
            List<FlightSearchResults> objO = (List<FlightSearchResults>)List[0];
            List<FlightSearchResults> objR = (List<FlightSearchResults>)List[1];
            int ln = 1;//For Total Line No.
            int k = 1;
            int LnOb = objO[objO.Count - 1].LineNumber;
            int LnIb = objR[objR.Count - 1].LineNumber;
            ArrayList Comb = new ArrayList();
            List<FlightSearchResults> Final = new List<FlightSearchResults>();
            while (k <= LnOb)
            {
                var OB = (from ct in objO where ct.LineNumber == k select ct).ToList();
                int l = 1;
                while (l <= LnIb)
                {
                    var IB = (from c in objR where c.LineNumber == l select c).ToList();
                    List<FlightSearchResults> st;
                    st = Merge(OB, IB, ln, obj, SrvchargeList, CrdType);
                    foreach (FlightSearchResults item in st)
                    {
                        Final.Add(item);
                    }
                    ln++;///Increment Total Ln
                    l++;//Increment IB Ln
                }
                k++; //Increment OW Ln
            }
            Comb.Add(Final);
            return Comb;
        }

        public List<FlightSearchResults> Merge(List<FlightSearchResults> OB, List<FlightSearchResults> IB, int Ln, FlightSearch searchInputs, List<FltSrvChargeList> SrvchargeList, string CrdType)
        {
            List<FlightSearchResults> Final = new List<FlightSearchResults>();

            float AdtFSur = 0, AdtWO = 0, AdtIN = 0, AdtJN = 0, AdtYR = 0, AdtBfare = 0, AdtOT = 0, AdtFare = 0, AdtTax = 0,
                ADTAdminMrk = 0, ADTAgentMrk = 0, AdtDiscount = 0, AdtDiscount1 = 0, AdtCB = 0, AdtSrvTax = 0, AdtSrvTax1 = 0, AdtTF = 0, AdtTds = 0, IATAComm = 0, AdtMfee = 0;
            float ChdFSur = 0, ChdWO = 0, ChdIN = 0, ChdJN = 0, ChdYR = 0, ChdBFare = 0, ChdOT = 0, ChdFare = 0, ChdTax = 0,
                CHDAdminMrk = 0, CHDAgentMrk = 0, ChdDiscount = 0, ChdDiscount1 = 0, ChdCB = 0, ChdSrvTax = 0, ChdSrvTax1 = 0, ChdTF = 0, ChdTds = 0, ChdMfee = 0;
            float InfFSur = 0, InfIN = 0, InfJN = 0, InfOT = 0, InfQ = 0, InfFare = 0, InfBfare = 0, InfTax = 0,
             InfSrvTax = 0, InfTF = 0, InfMfee = 0;

            var item = (FlightSearchResults)OB[0].Clone();
            var itemib = (FlightSearchResults)IB[0].Clone();
            #region ADULT
            int Adult = item.Adult;
            AdtFSur = AdtFSur + item.AdtFSur + itemib.AdtFSur;
            AdtWO = AdtWO + item.AdtWO + itemib.AdtWO;
            AdtIN = AdtIN + item.AdtIN + itemib.AdtIN;
            AdtJN = AdtJN + item.AdtJN + itemib.AdtJN;
            AdtYR = AdtYR + item.AdtYR + itemib.AdtYR;
            AdtBfare = AdtBfare + item.AdtBfare + itemib.AdtBfare;
            AdtOT = AdtOT + item.AdtOT + itemib.AdtOT;
            AdtFare = AdtFare + item.AdtFare + itemib.AdtFare;
            AdtTax = AdtTax + item.AdtTax + itemib.AdtTax;
            ADTAdminMrk = ADTAdminMrk + item.ADTAdminMrk + itemib.ADTAdminMrk;
            ADTAgentMrk = ADTAgentMrk + item.ADTAgentMrk + itemib.ADTAgentMrk;
            AdtDiscount = AdtDiscount + item.AdtDiscount + itemib.AdtDiscount;
            AdtDiscount1 = AdtDiscount1 + item.AdtDiscount1 + itemib.AdtDiscount1;
            AdtCB = AdtCB + item.AdtCB + itemib.AdtCB;
            AdtSrvTax = AdtSrvTax + item.AdtSrvTax + itemib.AdtSrvTax;
            AdtSrvTax1 = AdtSrvTax1 + item.AdtSrvTax1 + itemib.AdtSrvTax1;
            AdtTF = AdtTF + item.AdtTF + itemib.AdtTF;
            AdtTds = AdtTds + item.AdtTds + itemib.AdtTds;
            AdtMfee = AdtMfee + item.AdtMgtFee + itemib.AdtMgtFee;
            IATAComm = IATAComm + item.IATAComm + itemib.IATAComm;

            //if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF == true))
            //{
            //    //DataTable CommDt = new DataTable();
            //    //Hashtable STTFTDS = new Hashtable();
            //    //CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, item.ValiDatingCarrier, decimal.Parse(AdtBfare.ToString()), decimal.Parse(AdtFSur.ToString()), 1, "", item.AdtCabin, searchInputs.DepDate, item.OrgDestFrom + "-" + item.OrgDestTo, searchInputs.RetDate, item.AdtFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), item.FlightIdentification, item.OperatingCarrier, item.MarketingCarrier, CrdType, "");
            //    //AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
            //    //AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
            //    //STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, item.ValiDatingCarrier, AdtDiscount1, AdtBfare, AdtFSur, searchInputs.TDS);
            //    //AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
            //    //AdtDiscount = AdtDiscount1 - AdtSrvTax1;
            //    //AdtSrvTax = 0;
            //    //AdtTF = float.Parse(STTFTDS["TFee"].ToString());
            //    //AdtTds = float.Parse(STTFTDS["Tds"].ToString());
            //    //IATAComm = float.Parse(STTFTDS["IATAComm"].ToString());
            //}
            //if ((searchInputs.IsCorp == true) && (searchInputs.RTF == true))
            //{
            //    try
            //    {
            //        DataTable MGDT = new DataTable();
            //        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, item.ValiDatingCarrier, decimal.Parse(AdtBfare.ToString()), decimal.Parse(AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(AdtFare.ToString()));
            //        AdtMfee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
            //        AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
            //    }
            //    catch { }
            //}
            #endregion



            #region CHILD
            int Child = item.Child;
            if (searchInputs.Child > 0)
            {

                ChdFSur = ChdFSur + item.ChdFSur + itemib.ChdFSur;
                ChdWO = ChdWO + item.ChdWO + itemib.ChdWO;
                ChdIN = ChdIN + item.ChdIN + itemib.ChdIN;
                ChdJN = ChdJN + item.ChdJN + itemib.ChdJN;
                ChdYR = ChdYR + item.ChdYR + itemib.ChdYR;
                ChdBFare = ChdBFare + item.ChdBFare + itemib.ChdBFare;
                ChdOT = ChdOT + item.ChdOT + itemib.ChdOT;
                ChdFare = ChdFare + item.ChdFare + itemib.ChdFare;
                ChdTax = ChdTax + item.ChdTax + itemib.ChdTax;
                CHDAdminMrk = CHDAdminMrk + item.CHDAdminMrk + itemib.CHDAdminMrk;
                CHDAgentMrk = CHDAgentMrk + item.CHDAgentMrk + itemib.CHDAgentMrk;
                ChdDiscount = ChdDiscount + item.ChdDiscount + itemib.ChdDiscount;
                ChdCB = ChdCB + item.ChdCB + itemib.ChdCB;
                ChdSrvTax = ChdSrvTax + item.ChdSrvTax + itemib.ChdSrvTax;
                ChdTF = ChdTF + item.ChdTF + itemib.ChdTF;
                ChdTds = ChdTds + item.ChdTds + itemib.ChdTds;
                ChdMfee = ChdMfee + item.ChdMgtFee + itemib.ChdMgtFee;


                //if ((searchInputs.Trip.ToString() == JourneyType.D.ToString()) && (searchInputs.RTF == true))
                //{
                //    //DataTable CommDt = new DataTable();
                //    //Hashtable STTFTDS = new Hashtable();
                //    //CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, item.ValiDatingCarrier, decimal.Parse(ChdBFare.ToString()), decimal.Parse(ChdFSur.ToString()), 1, "", item.ChdCabin, searchInputs.DepDate, item.OrgDestFrom + "-" + item.OrgDestTo, searchInputs.RetDate, item.ChdFarebasis, searchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInputs.HidTxtArrCity.Split(',')[1].ToString().Trim(), item.FlightIdentification, item.OperatingCarrier, item.MarketingCarrier, CrdType, "");
                //    //ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                //    //ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                //    //STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, item.ValiDatingCarrier, ChdDiscount1, ChdBFare, ChdFSur, searchInputs.TDS);
                //    //ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                //    //ChdDiscount = ChdDiscount1 - ChdSrvTax1;
                //    //ChdSrvTax = 0;
                //    //ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                //    //ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                //}
                //if ((searchInputs.IsCorp == true) && (searchInputs.RTF == true))
                //{
                //    try
                //    {
                //        DataTable MGDT = new DataTable();
                //        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, item.ValiDatingCarrier, decimal.Parse(ChdBFare.ToString()), decimal.Parse(ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(ChdFare.ToString()));
                //        ChdMfee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                //        ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                //    }
                //    catch { }
                //}
            }
            #endregion


            #region INFANT
            int Infant = item.Infant;
            InfFare = InfFare + item.InfFare + itemib.InfFare;
            InfBfare = InfBfare + item.InfBfare + itemib.InfBfare;
            InfFSur = InfFSur + item.InfFSur + itemib.InfFSur;
            InfIN = InfIN + item.InfIN + itemib.InfIN;
            InfJN = InfJN + item.InfJN + itemib.InfJN;
            InfOT = InfOT + item.InfOT + itemib.InfOT;
            InfQ = InfQ + item.InfQ + itemib.InfQ;
            InfTax = InfTax + item.InfTax + itemib.InfTax;
            InfMfee = InfMfee + item.InfMgtFee + itemib.InfMgtFee;
            #endregion

            #region TOTAL
            float OriginalTF = item.OriginalTF + itemib.OriginalTF;
            float TotBfare = (AdtBfare * Adult) + (ChdBFare * Child) + (InfBfare * Infant);
            float TotalTax = (AdtTax * Adult) + (ChdTax * Child) + (InfTax * Infant);
            float TotalFuelSur = (AdtFSur * Adult) + (ChdFSur * Child);
            float TotalFare = (AdtFare * Adult) + (ChdFare * Child) + (InfFare * Infant);
            float STax = (AdtSrvTax * Adult) + (ChdSrvTax * Child) + (InfSrvTax * Infant);
            float TFee = (AdtTF * Adult) + (ChdTF * Child) + (InfTF * Infant);
            float TotDis = (AdtDiscount * Adult) + (ChdDiscount * Child);
            float TotCB = (AdtCB * Adult) + (ChdCB * Child);
            float TotTds = (AdtTds * Adult) + (ChdTds * Child);// +InfTds;
            float TotMgtFee = (AdtMfee * Adult) + (ChdMfee * Child) + (InfMfee * Infant);
            float TotMrkUp = (ADTAdminMrk * Adult) + (ADTAgentMrk * Adult) + (CHDAdminMrk * Child) + (CHDAgentMrk * Child);
            if ((item.Trip.ToString() == JourneyType.I.ToString()) && (item.IsCorp == true))
                TotalFare = (TotalFare + TotMrkUp + STax + TFee + TotMgtFee) - ((ADTAdminMrk * Adult) + (CHDAdminMrk * Child));
            else
                TotalFare = TotalFare + TotMrkUp + STax + TFee + TotMgtFee;
            float NetFare = (TotalFare + TotTds) - (TotDis + TotCB + (ADTAgentMrk * Adult) + (CHDAgentMrk * Child));

            #endregion


            foreach (FlightSearchResults a in OB)
            {
                var PrcF = (FlightSearchResults)a.Clone();
                PrcF.LineNumber = Ln;
                #region Adult
                PrcF.AdtFSur = AdtFSur;
                PrcF.AdtIN = AdtIN;
                PrcF.AdtJN = AdtJN;
                PrcF.AdtYR = AdtYR;
                PrcF.AdtBfare = AdtBfare;
                PrcF.AdtOT = AdtOT;
                PrcF.AdtFare = AdtFare;
                PrcF.AdtTax = AdtTax;
                PrcF.ADTAdminMrk = ADTAdminMrk;
                PrcF.ADTAgentMrk = ADTAgentMrk;
                PrcF.AdtDiscount = AdtDiscount;
                PrcF.AdtDiscount1 = AdtDiscount1;
                PrcF.AdtCB = AdtCB;
                PrcF.AdtSrvTax = AdtSrvTax;
                PrcF.AdtSrvTax1 = AdtSrvTax1;
                PrcF.AdtTF = AdtTF;
                PrcF.AdtTds = AdtTds;
                PrcF.IATAComm = IATAComm;
                PrcF.AdtMgtFee = AdtMfee;
                #endregion

                #region Child
                PrcF.ChdFSur = ChdFSur;
                PrcF.ChdWO = ChdWO;
                PrcF.ChdIN = ChdIN;
                PrcF.ChdJN = ChdJN;
                PrcF.ChdYR = ChdYR;
                PrcF.ChdBFare = ChdBFare;
                PrcF.ChdOT = ChdOT;
                PrcF.ChdFare = ChdFare;
                PrcF.ChdTax = ChdTax;
                PrcF.CHDAdminMrk = CHDAdminMrk;
                PrcF.CHDAgentMrk = CHDAgentMrk;
                PrcF.ChdDiscount = ChdDiscount;
                PrcF.ChdDiscount1 = ChdDiscount1;
                PrcF.ChdCB = ChdCB;
                PrcF.ChdSrvTax = ChdSrvTax;
                PrcF.ChdSrvTax1 = ChdSrvTax1;
                PrcF.ChdTF = ChdTF;
                PrcF.ChdTds = ChdTds;
                PrcF.ChdMgtFee = ChdMfee;
                #endregion

                #region Infant
                PrcF.InfFare = InfFare;
                PrcF.InfBfare = InfBfare;
                PrcF.InfFSur = InfFSur;
                PrcF.InfIN = InfIN;
                PrcF.InfJN = InfJN;
                PrcF.InfOT = InfOT;
                PrcF.InfQ = InfQ;
                PrcF.InfMgtFee = InfMfee;
                PrcF.InfTax = InfTax;
                #endregion

                #region Total
                PrcF.TotBfare = TotBfare;
                PrcF.TotalTax = TotalTax;
                PrcF.TotalFuelSur = TotalFuelSur;
                PrcF.TotalFare = TotalFare;
                PrcF.STax = STax;
                PrcF.TFee = TFee;
                PrcF.TotDis = TotDis;
                PrcF.TotTds = TotTds;
                PrcF.TotMrkUp = TotMrkUp;
                PrcF.TotMgtFee = TotMgtFee;
                PrcF.OriginalTF = OriginalTF;
                PrcF.NetFare = NetFare;
                #endregion
                Final.Add(PrcF);

            }

            foreach (FlightSearchResults b in IB)
            {
                var PrcF = (FlightSearchResults)b.Clone();
                PrcF.LineNumber = Ln;

                #region Adult
                PrcF.AdtFSur = AdtFSur;
                PrcF.AdtIN = AdtIN;
                PrcF.AdtJN = AdtJN;
                PrcF.AdtYR = AdtYR;
                PrcF.AdtBfare = AdtBfare;
                PrcF.AdtOT = AdtOT;
                PrcF.AdtFare = AdtFare;
                PrcF.AdtTax = AdtTax;
                PrcF.ADTAdminMrk = ADTAdminMrk;
                PrcF.ADTAgentMrk = ADTAgentMrk;
                PrcF.AdtDiscount = AdtDiscount;
                PrcF.AdtDiscount1 = AdtDiscount1;
                PrcF.AdtCB = AdtCB;
                PrcF.AdtSrvTax = AdtSrvTax;
                PrcF.AdtSrvTax1 = AdtSrvTax1;
                PrcF.AdtTF = AdtTF;
                PrcF.AdtTds = AdtTds;
                PrcF.IATAComm = IATAComm;
                PrcF.AdtMgtFee = AdtMfee;
                #endregion

                #region Child
                PrcF.ChdFSur = ChdFSur;
                PrcF.ChdWO = ChdWO;
                PrcF.ChdIN = ChdIN;
                PrcF.ChdJN = ChdJN;
                PrcF.ChdYR = ChdYR;
                PrcF.ChdBFare = ChdBFare;
                PrcF.ChdOT = ChdOT;
                PrcF.ChdFare = ChdFare;
                PrcF.ChdTax = ChdTax;
                PrcF.CHDAdminMrk = CHDAdminMrk;
                PrcF.CHDAgentMrk = CHDAgentMrk;
                PrcF.ChdDiscount = ChdDiscount;
                PrcF.ChdDiscount1 = ChdDiscount1;
                PrcF.ChdCB = ChdCB;
                PrcF.ChdSrvTax = ChdSrvTax;
                PrcF.ChdSrvTax1 = ChdSrvTax1;
                PrcF.ChdTF = ChdTF;
                PrcF.ChdTds = ChdTds;
                PrcF.ChdMgtFee = ChdMfee;
                #endregion

                #region Infant
                PrcF.InfFare = InfFare;
                PrcF.InfBfare = InfBfare;
                PrcF.InfFSur = InfFSur;
                PrcF.InfIN = InfIN;
                PrcF.InfJN = InfJN;
                PrcF.InfOT = InfOT;
                PrcF.InfQ = InfQ;
                PrcF.InfMgtFee = InfMfee;
                PrcF.InfTax = InfTax;
                #endregion

                #region Total
                PrcF.TotBfare = TotBfare;
                PrcF.TotalTax = TotalTax;
                PrcF.TotalFuelSur = TotalFuelSur;
                PrcF.TotalFare = TotalFare;
                PrcF.STax = STax;
                PrcF.TFee = TFee;
                PrcF.TotDis = TotDis;
                PrcF.TotTds = TotTds;
                PrcF.TotMrkUp = TotMrkUp;
                PrcF.TotMgtFee = TotMgtFee;
                PrcF.OriginalTF = OriginalTF;
                PrcF.NetFare = NetFare;
                #endregion

                Final.Add(PrcF);
            }

            return Final;
        }

        #endregion
        #region Booking


        
        //public string Spice_GetPnr(FlightSearch obj, string[] JSK, string[] FSK, ArrayList Seg, DataTable Pax, decimal TotFare, decimal InfFare, BookingResources objBkgRes, out Dictionary<string, string> xml, DataTable MealBagDT, string[] ViaArr, string[] FT, string PROMOCODE, List<Seat> SeatList, string Orderid, bool Reissue, string RID, string RefID)
        public string Spice_GetPnr(FlightSearch obj, string[] JSK, string[] FSK, ArrayList Seg, DataTable Pax, decimal TotFare, decimal InfFare, Hashtable Cust, out Dictionary<string, string> xml, DataTable MealBagDT, string[] ViaArr, string[] FT, string PROMOCODE, string PromoAppliedOn, bool Bag, string SSRCode,List<STD.Shared.Seat> SeatList)
        {
            string Reqxml = "";
            string Resxml = "";
            string signature = "FAIL-TK";
            string sjkAmount = "FAILURE";
            string SSRAmount = "FAILURE";
            string UPXAmount = "FAILURE";
            string APBAmount = "FAILURE";
            string PaxAmt = "FAILURE";
            #region Seat
            string SBAmount = "FAILURE";
            #endregion
            #region Re-issue
            // ReissueDetails RDs = new ReissueDetails();
            string TotalAmountAfterCan = "0";
            string BalanceDueAfterCan = "0";
            string RecordLocator = "";
            string RefundAmount = "";
            string vc = "";
            string TAID = "";
            string TAUSERID = "";
            #endregion
            string PNR = "-FQ";
            decimal diff = 0;
            Dictionary<string, string> xmlo = new Dictionary<string, string>();
            try
            {
                #region SIGNATURE
                //if (Reissue)
                //{
                //    LNBCORPDAL.ReissuePnrDAL ObjDAL = new ReissuePnrDAL(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                //    RDs = new ReissueDetails();
                //    RDs.RID = RID;
                //    RDs.RefNo = RefID;
                //    RDs.Action = "GET";
                //    RDs = ObjDAL.GetSignature(RDs);
                //    signature = RDs.Signature;
                //    UPXAmount = RDs.BalanceDue;
                //}
                //else
                //{
                //    signature = Spice_Login();
                //}
                signature = Spice_Login();
                #endregion
                //if (!Reissue)
                //{
                #region GST for updateContactifo
                if (!string.IsNullOrEmpty(Convert.ToString(Pax.Rows[0]["GSTNumber"])))
                {
                    UpdateContactInfo(signature, Pax, ref xmlo);
                }
                #endregion

                string ServiceBundleCode = "";
                #region SJK
                if (signature != "FAIL-TK")
                {

                    //if (SeatList.Count() > 0)
                    //{
                    //FlightCommonBAL objFleSBal = new FlightCommonBAL(Constr);
                    //ServiceBundleCode = objFleSBal.GetServiceBundleCode(Pax.Rows[0]["OrderId"].ToString().Trim());
                    //}
                    sjkAmount = Spice_SellJourneyByKey(signature, obj, JSK, FSK, ref xmlo, FT, PROMOCODE, PromoAppliedOn);
                    // sjkAmount = Spice_SellJourneyByKey(signature, obj, JSK, FSK, ref xmlo, FT, PROMOCODE, ServiceBundleCode);
                }
                else
                {
                    PNR = PNR + "TK";
                    xmlo.Add("SSR", "");
                    xmlo.Add("UPPAXREQ", "");
                    xmlo.Add("UPPAXRES", "");
                    xmlo.Add("APBREQ", "");
                    xmlo.Add("APBRES", "");
                    xmlo.Add("BC-REQ", "");
                    xmlo.Add("BC-RES", "");
                    xmlo.Add("SJKREQ", "");
                    xmlo.Add("SJKRES", "");
                    xmlo.Add("OTHER", signature);
                    goto LABPNR;
                }
                #endregion

                #region SSR -OPTIONAL AND UPDATE PAX
                double UpdTotFare = 0, DiffGst = 0;

                bool exists = false;
                if ((sjkAmount != "FAILURE") && ServiceBundleCode != "")
                {
                    if (obj.Infant > 0 || (MealBagDT.Rows.Count > 0 && Convert.ToInt32(MealBagDT.Rows[0]["BagCount"].ToString()) > 0) || obj.Cabin == "BUSINESS")
                    {
                        exists = true;
                    }
                }
                else if ((sjkAmount != "FAILURE") && ServiceBundleCode == "")
                {
                    if ((sjkAmount != "FAILURE") && ((obj.Infant > 0) || MealBagDT.Rows.Count > 0) || obj.Cabin == "BUSINESS")
                        exists = true;
                }

                //if ((sjkAmount != "FAILURE") && ((obj.Infant > 0) || MealBagDT.Rows.Count > 0))
                if (exists == true)
                {


                    SSRAmount = Spice_Sell_SSR(signature, obj, Seg, ref xmlo, Pax, MealBagDT, ViaArr, Bag, SSRCode);
                    // SSRAmount = Spice_Sell_SSR(signature, obj, Seg, ref xmlo, Pax, MealBagDT, ViaArr, ServiceBundleCode);
                    // UpdTotFare = (Convert.ToDouble(sjkAmount)) - Convert.ToDouble(TotFare);
                    UpdTotFare = (Convert.ToDouble(SSRAmount)) - (Convert.ToDouble(TotFare) + (Convert.ToDouble(InfFare) * obj.Infant));
                    DiffGst = UpdTotFare / (obj.Adult + obj.Child);

                    if (SSRAmount != "FAILURE")
                    {
                        if (obj.Infant > 0)
                        {
                            diff = Math.Abs(Math.Round(((Convert.ToDecimal(SSRAmount) - Convert.ToDecimal(UpdTotFare.ToString())) - TotFare) / obj.Infant) - InfFare);
                        }
                        else if ((Convert.ToDecimal(SSRAmount) - TotFare) > 0 && DiffGst > 9)
                        {
                            goto LABPNR;
                        }
                        //if (DiffGst > 0)
                        if (DiffGst != 0)
                        {
                            try
                            {

                                //FlightCommonBAL FltBal = new FlightCommonBAL(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                //int GST = FltBal.UpdateGSTTax(Pax.Rows[0]["OrderId"].ToString().Trim(), Convert.ToDouble(UpdTotFare));
                                FlightCommonBAL FltBal = new FlightCommonBAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                                int GST = FltBal.UpdateGSTTax(Pax.Rows[0]["OrderId"].ToString().Trim(), Convert.ToDouble(UpdTotFare));
                                if (GST < 1)
                                    xmlo.Add("OTHER", signature + " GST-" + "GST NOT UPDATED");
                                goto LABPNR;
                            }
                            catch (Exception ex)
                            {
                                ExecptionLogger.FileHandling("SGNAV4(GSTUpdate)", "Error_003", ex, "GST");
                                goto LABPNR;
                            }
                        }

                        if (!string.IsNullOrEmpty(Convert.ToString(Pax.Rows[0]["GSTNumber"])))
                        {
                            #region GST for GetBookingfromstate                            
                            GetBookingFromState(signature, ref xmlo);
                            #endregion
                        }

                        #region UpdatePax
                        if (DiffGst < 10)
                        {
                            UPXAmount = Spice_UpdatePassenger(signature, obj, Pax, ref xmlo);
                            if ((UPXAmount != "FAILURE") && (diff * obj.Infant <= obj.Infant))
                            {
                                #region Seat
                                if (SeatList.Count > 0)
                                {
                                    SBAmount = SeatBookingFinal(signature, obj, Pax, SeatList, ref xmlo);
                                    if (SBAmount != "FAILURE")
                                    {
                                        decimal SeatAmount = Convert.ToDecimal(SeatList.Sum(x => x.Amount));
                                        decimal SAmount = Convert.ToDecimal(SBAmount) - (TotFare + (InfFare * obj.Infant) + SeatAmount + Convert.ToDecimal(UpdTotFare));
                                        if (SAmount <= 0)
                                        {
                                         //   FlightCommonBAL objFleSBal = new FlightCommonBAL(Constr);
                                           // objFleSBal.Update_NET_TOT_Fare_Seat(Pax.Rows[0]["OrderId"].ToString().Trim(), SeatAmount.ToString());
                                            APBAmount = Spice_AddPaymentToBooking(signature, SBAmount, ref xmlo);
                                        }
                                        else
                                        {
                                            PNR = PNR + "UP";
                                            xmlo.Add("OTHER", "Fare has been changed and updated fare is " + SBAmount.ToString());
                                            goto LABPNR;
                                        }
                                    }
                                    else
                                    {
                                        PNR = PNR + "UP";
                                        goto LABPNR;
                                    }
                                }
                                else
                                {
                                    APBAmount = Spice_AddPaymentToBooking(signature, UPXAmount, ref xmlo);
                                }
                                #endregion
                            }
                            else
                            {
                                PNR = PNR + "UP";
                                goto LABPNR;
                            }
                        }
                        else
                        {
                            PNR = PNR + "UP";
                            goto LABPNR;
                        }
                        #endregion

                    }
                    else
                    {
                        PNR = PNR + "SSR";
                        goto LABPNR;
                    }
                }
                else if (((sjkAmount != "FAILURE") && (obj.Infant == 0)) || sjkAmount == "SELLWARNING")
                {
                    //else if (((sjkAmount != "FAILURE") && (obj.Infant == 0)) || sjkAmount== "SELLWARNING")
                    //else if ((sjkAmount != "FAILURE") && (obj.Infant == 0))
                    xmlo.Add("SSR", "");

                    //#region GST for GetBookingfromstate
                    //if (Convert.ToBoolean(Pax.Rows[0]["IsGST"]) == true)
                    //{
                    //    GetBookingFromState(signature, ref xmlo);
                    //}
                    //#endregion

                    #region GST for GetBookingfromstate
                    if (!string.IsNullOrEmpty(Convert.ToString(Pax.Rows[0]["GSTNumber"])))
                    {
                        GetBookingFromState(signature, ref xmlo);
                    }
                    #endregion

                    UPXAmount = Spice_UpdatePassenger(signature, obj, Pax, ref xmlo);

                    #region UpdatePax

                    if ((UPXAmount != "FAILURE"))
                    {
                        UpdTotFare = (Convert.ToDouble(UPXAmount)) - Convert.ToDouble(TotFare);
                        DiffGst = UpdTotFare / (obj.Adult + obj.Child);
                        if (((Convert.ToDecimal(UPXAmount) == TotFare) || DiffGst < 10))
                        {
                            //if (DiffGst > 0)
                            if (DiffGst != 0)
                            {
                                try
                                {
                                    //FlightCommonBAL FltBal1 = new FlightCommonBAL(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                                    //int K = FltBal1.UpdateGSTTax(Pax.Rows[0]["OrderId"].ToString().Trim(), Convert.ToDouble(UpdTotFare));

                                    FlightCommonBAL FltBal1 = new FlightCommonBAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                                    int K = FltBal1.UpdateGSTTax(Pax.Rows[0]["OrderId"].ToString().Trim(), Convert.ToDouble(UpdTotFare));
                                    if (K < 1)
                                        goto LABPNR;
                                }
                                catch (Exception ex)
                                {
                                    ExecptionLogger.FileHandling("SGNAV4(GSTUpdate)", "Error_003", ex, "GST");
                                    //xmlo.Add("APBREQ", "");
                                    //xmlo.Add("APBRES", "");
                                    //xmlo.Add("BC-REQ", "");
                                    //xmlo.Add("BC-RES", "");
                                    xmlo.Add("OTHER", UPXAmount);
                                    PNR = PNR + "UP";
                                    goto LABPNR;

                                }


                            }
                            // APBAmount = Spice_AddPaymentToBooking(signature, UPXAmount, ref xmlo);
                            #region Seat

                            //decimal SeatAmount = Convert.ToDecimal(SeatList.Sum(x => x.Amount));
                            //decimal SAmount = Convert.ToDecimal(SBAmount) - (TotFare + InfFare + SeatAmount);

                            //if (SAmount <= 0)
                            //{
                            //    FlightCommonBAL objFleSBal = new FlightCommonBAL(Constr);
                            //    objFleSBal.Update_NET_TOT_Fare(Pax.Rows[0]["OrderId"].ToString().Trim(), SeatAmount.ToString());
                            //    APBAmount = Spice_AddPaymentToBooking(signature, SBAmount, ref xmlo);
                            //}
                            //else
                            //{
                            //    PNR = PNR + "UP";
                            //    goto LABPNR;
                            //}
                            if (SeatList.Count > 0)
                            {

                                SBAmount = SeatBookingFinal(signature, obj, Pax, SeatList, ref xmlo);
                                if (SBAmount != "FAILURE")
                                {
                                    decimal SeatAmount = Convert.ToDecimal(SeatList.Sum(x => x.Amount));
                                    decimal SAmount = Convert.ToDecimal(SBAmount) - (TotFare + SeatAmount + Convert.ToDecimal(UpdTotFare));
                                    if (SAmount <= 0)
                                    {
                                        //FlightCommonBAL objFleSBal = new FlightCommonBAL(Constr);
                                        //objFleSBal.Update_NET_TOT_Fare_Seat(Pax.Rows[0]["OrderId"].ToString().Trim(), SeatAmount.ToString());
                                        APBAmount = Spice_AddPaymentToBooking(signature, SBAmount, ref xmlo);
                                    }
                                    else
                                    {
                                        PNR = PNR + "UP";
                                        xmlo.Add("OTHER", "Fare has been changed and updated fare is " + SBAmount.ToString());
                                        goto LABPNR;
                                    }
                                }
                                else
                                {
                                    PNR = PNR + "UP";
                                    goto LABPNR;
                                }
                            }
                            else
                            {
                                APBAmount = Spice_AddPaymentToBooking(signature, UPXAmount, ref xmlo);
                            }
                            #endregion
                        }
                        else
                        {
                            xmlo.Add("APBREQ", "");
                            xmlo.Add("APBRES", "");
                            xmlo.Add("BC-REQ", "");
                            xmlo.Add("BC-RES", "");
                            xmlo.Add("OTHER", UPXAmount);
                            PNR = PNR + "UP";
                            goto LABPNR;
                        }
                    }
                    else
                    {
                        xmlo.Add("APBREQ", "");
                        xmlo.Add("APBRES", "");
                        xmlo.Add("BC-REQ", "");
                        xmlo.Add("BC-RES", "");
                        xmlo.Add("OTHER", UPXAmount);
                        PNR = PNR + "UP";
                        goto LABPNR;
                    }
                    #endregion

                }
                else
                {
                    PNR = PNR + "SJ";
                    goto LABPNR;
                }
                #endregion
                //}

                #region BookingCommit
                if (APBAmount != "FAILURE")
                {
                    //PNR = Spice_BookingCommit(signature, obj, Pax, objBkgRes, ref xmlo, RecordLocator);
                    //goto LABPNR;
                    PNR = Spice_BookingCommit(signature, obj, Pax, Cust, ref xmlo);
                    goto LABPNR;
                }
                #endregion
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_GetPnr)", "Error_003", ex, "Spice_BookingCommit");
                //ExecptionLogger.FileHandling("_6ENAV(SeatBookingFinal)", "Error_008", ex, "Seat");
            }
            finally
            {

            }
            LABPNR:
            xml = xmlo;
            if (PNR == "FAILURE")
                PNR = "-FQ";
            return PNR;
        }

        public string Spice_SellJourneyByKey(string signature, FlightSearch obj, string[] JSK, string[] FSK, ref Dictionary<string, string> xml, string[] FT, string PROMOCODE, string PromoAppliedOn)//string ServiceBundleCode)
        {
            string Reqxml = "";
            string Resxml = "";
            string Retval = "FAILURE";
            try
            {
                // IBookingManager bookingAPI = new BookingManagerClient();
                int Trip = 1;
                navitaire.SG.ver4.SellResponse sellres = new navitaire.SG.ver4.SellResponse();

                int PaxCount = obj.Adult + obj.Child;
                //if (obj.Trip == LNBShared.Trip.I && obj.TripType == LNBShared.TripType.RoundTrip)
                //    Trip = 2;
                //else if (obj.Trip == LNBShared.Trip.D && obj.RTF == true)
                //    Trip = 2;
                //else
                //    Trip = 1;
                if (obj.TripType == STD.Shared.TripType.RoundTrip)
                    Trip = 2;
                else
                    Trip = 1;

                #region SELL REQUEST

                //Indicate that the we are selling a journey
                navitaire.SG.ver4.SellRequest sellrequest = new navitaire.SG.ver4.SellRequest();

                sellrequest.Signature = signature;
                sellrequest.ContractVersion = ContractVers;
                #region <SellRequestData>
                sellrequest.SellRequestData = new SellRequestData();
                sellrequest.SellRequestData.SellBy = SellBy.JourneyBySellKey;
                #region  <a:SellJourneyByKeyRequest>

                sellrequest.SellRequestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                #region <SellJourneyByKeyRequestData>
                SellJourneyByKeyRequestData sjrd = new SellJourneyByKeyRequestData();
                sjrd.ActionStatusCode = "NN";
                sjrd.JourneySellKeys = new SellKeyList[Trip];
                for (int i = 0; i < Trip; i++)
                {
                    sjrd.JourneySellKeys[i] = new SellKeyList();
                    sjrd.JourneySellKeys[i].JourneySellKey = JSK[i];
                    sjrd.JourneySellKeys[i].FareSellKey = FSK[i];
                    sjrd.JourneySellKeys[i].StandbyPriorityCode = "";
                }


                sjrd.PaxPriceType = new PaxPriceType[PaxCount];
                if (obj.Adult > 0)
                {
                    for (int adt = 0; adt <= obj.Adult - 1; adt++)
                    {
                        sjrd.PaxPriceType[adt] = new PaxPriceType();
                        sjrd.PaxPriceType[adt].PaxType = "ADT";
                        sjrd.PaxPriceType[adt].PaxDiscountCode = String.Empty;
                    }
                }
                if (obj.Child > 0)
                {
                    for (int chd = obj.Adult; chd <= (obj.Adult + obj.Child - 1); chd++)
                    {
                        sjrd.PaxPriceType[chd] = new PaxPriceType();
                        sjrd.PaxPriceType[chd].PaxType = "CHD";
                        sjrd.PaxPriceType[chd].PaxDiscountCode = String.Empty;
                    }
                }

                sjrd.CurrencyCode = "INR";
                sjrd.SourcePOS = new PointOfSale();
                sjrd.SourcePOS.State = MessageState.New;
                //sjrd.SourcePOS.AgentCode = username;
                if (obj.HidTxtAirLine.ToUpper().Trim() == "6E") sjrd.SourcePOS.AgentCode = "AG";
                else sjrd.SourcePOS.AgentCode = username;
                sjrd.SourcePOS.OrganizationCode = OrgCode;
                sjrd.SourcePOS.DomainCode = domain;
                sjrd.SourcePOS.LocationCode = domain;
                sjrd.PaxCount = (short)(PaxCount);

                #region Seat
                //if (String.IsNullOrEmpty(ServiceBundleCode) == false)
                //{
                //    sjrd.ApplyServiceBundle = ApplyServiceBundle.Yes;
                //    string[] bundle = new string[1];
                //    bundle[0] = (string)(ServiceBundleCode);
                //    sjrd.ServiceBundleList = bundle;
                //}
                #endregion

                if (PROMOCODE != "" && PromoAppliedOn == "BOTH")
                {
                    sjrd.TypeOfSale = new TypeOfSale();
                    sjrd.TypeOfSale.State = MessageState.New;
                    sjrd.TypeOfSale.PaxResidentCountry = "";
                    sjrd.TypeOfSale.PromotionCode = PROMOCODE;
                    sjrd.TypeOfSale.FareTypes = FT;
                }
                //else
                //{
                //    sjrd.TypeOfSale.PromotionCode = "";
                //}


                sjrd.LoyaltyFilter = navitaire.SG.ver4.Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations.LoyaltyFilter.MonetaryOnly;
                sjrd.IsAllotmentMarketFare = false;
                #endregion
                sellrequest.SellRequestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = sjrd;
                #endregion

                #endregion

                #endregion
                Reqxml = SerializeAnObject(sellrequest, "SellJrnyByKey-Req");
                sellres = bookingAPI.Sell(sellrequest);
                Resxml = SerializeAnObject(sellres, "SellRes-Res");
                if (sellres.BookingUpdateResponseData.Success != null)
                    Retval = sellres.BookingUpdateResponseData.Success.PNRAmount.TotalCost.ToString();
                else
                {
                    try
                    {
                        //                        Resxml = @"<?xml version='1.0'?>
                        //<SellResponse xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
                        //  <BookingUpdateResponseData>
                        //    <ExtensionData />
                        //    <Warning>
                        //      <ExtensionData />
                        //      <WarningText>sell warning</WarningText>
                        //    </Warning>
                        //    <OtherServiceInformations>
                        //      <OtherServiceInformation>
                        //        <ExtensionData />
                        //        <Text>Promotion W202GLEN is not valid for organization: DELDII6763.</Text>
                        //        <OsiSeverity>Warning</OsiSeverity>
                        //        <OSITypeCode>PromotionNotApplied</OSITypeCode>
                        //        <SubType />
                        //      </OtherServiceInformation>
                        //    </OtherServiceInformations>
                        //  </BookingUpdateResponseData>
                        //</SellResponse>";

                        //XDocument xd1 = XDocument.Parse(Resxml);
                        //string Des = "";//xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Descendants("Text").First().Value;
                        //bool Status = xd1.Descendants("BookingUpdateResponseData").Any() == true ? xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Any() == true ? xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Any() == true ? xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Descendants("Text").Any() == true ? true : false : false : false : false;
                        //if (Status == true)
                        //{
                        //    Des = xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Descendants("Text").First().Value;
                        //    xml.Add("OTHER", Des);
                        //}

                        XDocument xd1 = XDocument.Parse(Resxml);
                        string Des = "", SellWarning = "", SellWarningMsg = "";//xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Descendants("Text").First().Value;
                        bool Status = xd1.Descendants("BookingUpdateResponseData").Any() == true ? xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Any() == true ? xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Any() == true ? xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Descendants("Text").Any() == true ? true : false : false : false : false;
                        SellWarning = xd1.Descendants("BookingUpdateResponseData").Any() == true && xd1.Descendants("BookingUpdateResponseData").Descendants("Warning").Any() && xd1.Descendants("BookingUpdateResponseData").Descendants("Warning").Descendants("WarningText").Any() ? xd1.Descendants("BookingUpdateResponseData").Descendants("Warning").Descendants("WarningText").First().Value : "";
                        if (Status == true)
                        {
                            Des = xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Descendants("Text").First().Value;
                            SellWarningMsg = xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Descendants("OSITypeCode").Any() ? xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Descendants("OSITypeCode").First().Value : "";
                            //xml.Add("OTHER", SellWarning.Trim() + " " + Des);
                            xml["OTHER"] = xml.ContainsKey("OTHER") ? xml["OTHER"] : SellWarning.Trim() + " " + Des;
                            Retval = SellWarning.Trim().ToLower() == "sell warning" && SellWarningMsg.ToUpper() == "PROMOTIONNOTAPPLIED" && Des.Trim().ToLower().Contains("not applied on journey") ? "SELLWARNING" : Retval;
                        }
                    }
                    catch (Exception ex)
                    {
                        ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_SellJourneyByKey)", "Error_003", ex, "Spice_SellJourneyByKey");
                        //xml.Add("SSR", "");
                        //xml.Add("UPPAXREQ", "");
                        //xml.Add("UPPAXRES", "");
                        //xml.Add("APBREQ", "");
                        //xml.Add("APBRES", "");
                        //xml.Add("BC-REQ", "");
                        //xml.Add("BC-RES", "");
                        xml.Add("OTHER", ex.Message);
                        Spice_Logout(signature);
                    }
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_SellJourneyByKey)", "Error_003", ex, "Spice_SellJourneyByKey");
                //xml.Add("SSR", "");
                //xml.Add("UPPAXREQ", "");
                //xml.Add("UPPAXRES", "");
                //xml.Add("APBREQ", "");
                //xml.Add("APBRES", "");
                //xml.Add("BC-REQ", "");
                //xml.Add("BC-RES", "");
                xml.Add("OTHER", ex.Message);
                Spice_Logout(signature);
            }
            finally
            {
                //xml.Add("SJKREQ", Reqxml);
                //xml.Add("SJKRES", Resxml);
                xml.Add("SJKREQ", Reqxml);
                if (String.IsNullOrEmpty(Resxml) == true)
                {
                    xml.Add("SJKRES", "No Response");
                    if (xml.ContainsKey("OTHER") == false)
                        xml.Add("OTHER", "Sell request failed.Please try again.");
                }
                else
                    xml.Add("SJKRES", Resxml);
            }
            return Retval;
        }
        public string Spice_SellJourneyByKeyOut(string signature, FlightSearch obj, string[] JSK, string[] FSK, ref Dictionary<string, string> xml, string[] FT, string PROMOCODE, string ServiceBundleCode, ref string BalanceDue)
        {
            string Reqxml = "";
            string Resxml = "";
            string Retval = "FAILURE";
            try
            {
                // IBookingManager bookingAPI = new BookingManagerClient();
                int Trip = 1;
                navitaire.SG.ver4.SellResponse sellres = new navitaire.SG.ver4.SellResponse();

                int PaxCount = obj.Adult + obj.Child;
                //if (obj.Trip == LNBShared.Trip.I && obj.TripType == LNBShared.TripType.RoundTrip)
                //    Trip = 2;
                //else if (obj.Trip == LNBShared.Trip.D && obj.RTF == true)
                //    Trip = 2;
                //else
                //    Trip = 1;
                if (obj.TripType == STD.Shared.TripType.RoundTrip)
                    Trip = 2;
                else
                    Trip = 1;

                #region SELL REQUEST

                //Indicate that the we are selling a journey
                navitaire.SG.ver4.SellRequest sellrequest = new navitaire.SG.ver4.SellRequest();

                sellrequest.Signature = signature;
                sellrequest.ContractVersion = ContractVers;
                #region <SellRequestData>
                sellrequest.SellRequestData = new SellRequestData();
                sellrequest.SellRequestData.SellBy = SellBy.JourneyBySellKey;
                #region  <a:SellJourneyByKeyRequest>

                sellrequest.SellRequestData.SellJourneyByKeyRequest = new SellJourneyByKeyRequest();
                #region <SellJourneyByKeyRequestData>
                SellJourneyByKeyRequestData sjrd = new SellJourneyByKeyRequestData();
                sjrd.ActionStatusCode = "NN";
                sjrd.JourneySellKeys = new SellKeyList[Trip];
                for (int i = 0; i < Trip; i++)
                {
                    sjrd.JourneySellKeys[i] = new SellKeyList();
                    sjrd.JourneySellKeys[i].JourneySellKey = JSK[i];
                    sjrd.JourneySellKeys[i].FareSellKey = FSK[i];
                    sjrd.JourneySellKeys[i].StandbyPriorityCode = "";
                }


                sjrd.PaxPriceType = new PaxPriceType[PaxCount];
                if (obj.Adult > 0)
                {
                    for (int adt = 0; adt <= obj.Adult - 1; adt++)
                    {
                        sjrd.PaxPriceType[adt] = new PaxPriceType();
                        sjrd.PaxPriceType[adt].PaxType = "ADT";
                        sjrd.PaxPriceType[adt].PaxDiscountCode = String.Empty;
                    }
                }
                if (obj.Child > 0)
                {
                    for (int chd = obj.Adult; chd <= (obj.Adult + obj.Child - 1); chd++)
                    {
                        sjrd.PaxPriceType[chd] = new PaxPriceType();
                        sjrd.PaxPriceType[chd].PaxType = "CHD";
                        sjrd.PaxPriceType[chd].PaxDiscountCode = String.Empty;
                    }
                }

                sjrd.CurrencyCode = "INR";
                sjrd.SourcePOS = new PointOfSale();
                sjrd.SourcePOS.State = MessageState.New;
                //sjrd.SourcePOS.AgentCode = username;
                if (obj.HidTxtAirLine.ToUpper().Trim() == "6E") sjrd.SourcePOS.AgentCode = "AG";
                else sjrd.SourcePOS.AgentCode = username;
                sjrd.SourcePOS.OrganizationCode = OrgCode;
                sjrd.SourcePOS.DomainCode = domain;
                sjrd.SourcePOS.LocationCode = domain;
                sjrd.PaxCount = (short)(PaxCount);

                #region Seat
                if (String.IsNullOrEmpty(ServiceBundleCode) == false)
                {
                    sjrd.ApplyServiceBundle = ApplyServiceBundle.Yes;
                    string[] bundle = new string[1];
                    bundle[0] = (string)(ServiceBundleCode);
                    sjrd.ServiceBundleList = bundle;
                }
                #endregion

                if (PROMOCODE != "")
                {
                    sjrd.TypeOfSale = new TypeOfSale();
                    sjrd.TypeOfSale.State = MessageState.New;
                    sjrd.TypeOfSale.PaxResidentCountry = "";
                    sjrd.TypeOfSale.PromotionCode = PROMOCODE;
                    sjrd.TypeOfSale.FareTypes = FT;
                }
                //else
                //{
                //    sjrd.TypeOfSale.PromotionCode = "";
                //}


                sjrd.LoyaltyFilter = navitaire.SG.ver4.Navitaire.NewSkies.WebServices.DataContracts.Common.Enumerations.LoyaltyFilter.MonetaryOnly;
                sjrd.IsAllotmentMarketFare = false;
                #endregion
                sellrequest.SellRequestData.SellJourneyByKeyRequest.SellJourneyByKeyRequestData = sjrd;
                #endregion

                #endregion

                #endregion
                Reqxml = SerializeAnObject(sellrequest, "SellJrnyByKey-Req");
                sellres = bookingAPI.Sell(sellrequest);
                Resxml = SerializeAnObject(sellres, "SellRes-Res");
                if (sellres.BookingUpdateResponseData.Success != null)
                {
                    Retval = sellres.BookingUpdateResponseData.Success.PNRAmount.TotalCost.ToString();
                    BalanceDue = sellres.BookingUpdateResponseData.Success.PNRAmount.BalanceDue.ToString();
                }
                else
                {
                    try
                    {
                        //                        Resxml = @"<?xml version='1.0'?>
                        //<SellResponse xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
                        //  <BookingUpdateResponseData>
                        //    <ExtensionData />
                        //    <Warning>
                        //      <ExtensionData />
                        //      <WarningText>sell warning</WarningText>
                        //    </Warning>
                        //    <OtherServiceInformations>
                        //      <OtherServiceInformation>
                        //        <ExtensionData />
                        //        <Text>Promotion W202GLEN is not valid for organization: DELDII6763.</Text>
                        //        <OsiSeverity>Warning</OsiSeverity>
                        //        <OSITypeCode>PromotionNotApplied</OSITypeCode>
                        //        <SubType />
                        //      </OtherServiceInformation>
                        //    </OtherServiceInformations>
                        //  </BookingUpdateResponseData>
                        //</SellResponse>";
                        XDocument xd1 = XDocument.Parse(Resxml);
                        string Des = "";//xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Descendants("Text").First().Value;
                        bool Status = xd1.Descendants("BookingUpdateResponseData").Any() == true ? xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Any() == true ? xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Any() == true ? xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Descendants("Text").Any() == true ? true : false : false : false : false;
                        if (Status == true)
                        {
                            Des = xd1.Descendants("BookingUpdateResponseData").Descendants("OtherServiceInformations").Descendants("OtherServiceInformation").Descendants("Text").First().Value;
                            xml.Add("OTHER", Des);
                        }
                    }
                    catch (Exception ex)
                    {
                        ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_SellJourneyByKeyOut)", "Error_003", ex, "Spice_SellJourneyByKeyOut");
                        //xml.Add("SSR", "");
                        //xml.Add("UPPAXREQ", "");
                        //xml.Add("UPPAXRES", "");
                        //xml.Add("APBREQ", "");
                        //xml.Add("APBRES", "");
                        //xml.Add("BC-REQ", "");
                        //xml.Add("BC-RES", "");
                        xml.Add("OTHER", ex.Message);
                        Spice_Logout(signature);
                    }
                }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_SellJourneyByKeyOut)", "Error_003", ex, "Spice_SellJourneyByKeyOut");
                //xml.Add("SSR", "");
                //xml.Add("UPPAXREQ", "");
                //xml.Add("UPPAXRES", "");
                //xml.Add("APBREQ", "");
                //xml.Add("APBRES", "");
                //xml.Add("BC-REQ", "");
                //xml.Add("BC-RES", "");
                xml.Add("OTHER", ex.Message);
                Spice_Logout(signature);
            }
            finally
            {
                //xml.Add("SJKREQ", Reqxml);
                //xml.Add("SJKRES", Resxml);
                xml.Add("SJKREQ", Reqxml);
                if (String.IsNullOrEmpty(Resxml) == true)
                {
                    xml.Add("SJKRES", "No Response");
                    if (xml.ContainsKey("OTHER") == false)
                        xml.Add("OTHER", "Sell request failed.Please try again.");
                }
                else
                    xml.Add("SJKRES", Resxml);
            }
            return Retval;
        }
        #region GST
        public string UpdateContactInfo(string signature, DataTable Pax, ref Dictionary<string, string> xml)
        {
            string Reqxml = "";
            string Resxml = "";
            string Retval = "FAILURE";

            try
            {

                navitaire.SG.ver4.UpdateContactsResponse sellres = new navitaire.SG.ver4.UpdateContactsResponse();
                //Indicate that the we are selling a journey
                navitaire.SG.ver4.UpdateContactsRequest sellrequest = new navitaire.SG.ver4.UpdateContactsRequest();
                sellrequest.Signature = signature;
                sellrequest.ContractVersion = ContractVers;
                sellrequest.updateContactsRequestData = new UpdateContactsRequestData();
                sellrequest.updateContactsRequestData.BookingContactList = new BookingContact[1];
                BookingContact BContact = new BookingContact();
                BContact.State = MessageState.New;
                if (Pax.Rows[0]["GSTVC"].ToString().Trim().ToUpper() == "6E")
                    BContact.TypeCode = "I";
                else
                    BContact.TypeCode = "G";
                BContact.EmailAddress = Pax.Rows[0]["GSTEmail"].ToString();
                BContact.CompanyName = Pax.Rows[0]["GSTName"].ToString();
                BContact.CustomerNumber = Pax.Rows[0]["GSTNumber"].ToString();
                BContact.NotificationPreference = NotificationPreference.None;
                sellrequest.updateContactsRequestData.BookingContactList[0] = BContact;
                Reqxml = SerializeAnObject(sellrequest, "Update-Req");
                sellres = bookingAPI.UpdateContacts(sellrequest);
                Resxml = SerializeAnObject(sellres, "Update-Res");
                if (sellres.BookingUpdateResponseData.Success != null)
                    Retval = "SUCCESS";
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("SGNAV4(UpdateContactInfo)", "Error_003", ex, "GST");
                //xml.Add("SSR", "");
                //xml.Add("UPPAXREQ", "");
                //xml.Add("UPPAXRES", "");
                //xml.Add("APBREQ", "");
                //xml.Add("APBRES", "");
                //xml.Add("BC-REQ", "");
                //xml.Add("BC-RES", "");
                xml.Add("OTHER", ex.Message);
                Spice_Logout(signature);
            }
            finally
            {
                xml.Add("UCCONREQ", Reqxml);
                xml.Add("UCCONRES", Resxml);
            }

            return Retval;
        }
        public string GetBookingFromState(string signature, ref Dictionary<string, string> xml)
        {
            string Reqxml = "";
            string Resxml = "";
            string Retval = "FAILURE";

            try
            {
                navitaire.SG.ver4.GetBookingFromStateResponse sellres = new navitaire.SG.ver4.GetBookingFromStateResponse();
                //Indicate that the we are selling a journey
                navitaire.SG.ver4.GetBookingFromStateRequest sellrequest = new navitaire.SG.ver4.GetBookingFromStateRequest();
                sellrequest.Signature = signature;
                sellrequest.ContractVersion = ContractVers;
                //sellrequest.updateContactsRequestData = new UpdateContactsRequestData();
                //sellrequest.updateContactsRequestData.BookingContactList = new BookingContact[1];
                //BookingContact BContact = new BookingContact();
                //BContact.State = MessageState.New;
                //BContact.TypeCode = "G";
                //BContact.EmailAddress = Pax.Rows[0]["GSTEmail"].ToString();
                //BContact.CompanyName = Pax.Rows[0]["GSTName"].ToString();
                //BContact.CustomerNumber = Pax.Rows[0]["GSTNumber"].ToString();
                //BContact.NotificationPreference = NotificationPreference.None;
                //sellrequest.updateContactsRequestData.BookingContactList[0] = BContact;
                Reqxml = SerializeAnObject(sellrequest, "Update-Req");
                sellres = bookingAPI.GetBookingFromState(sellrequest);
                Resxml = SerializeAnObject(sellres, "Update-Res");
                //if (sellres.BookingData. != null)
                Retval = "SUCCESS";
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("SGNAV4(GetBookingFromState)", "Error_003", ex, "GST");
                //xml.Add("SSR", "");
                //xml.Add("UCCONREQ", "");
                //xml.Add("UCCONRES", "");
                //xml.Add("UPPAXREQ", "");
                //xml.Add("UPPAXRES", "");
                //xml.Add("APBREQ", "");
                //xml.Add("APBRES", "");
                //xml.Add("BC-REQ", "");
                //xml.Add("BC-RES", "");
                xml.Add("OTHER", ex.Message);
                Spice_Logout(signature);
            }
            finally
            {
                xml.Add("STATEREQ", Reqxml);
                xml.Add("STATERES", Resxml);
            }
            return Retval;
        }
        #endregion

       // public string Spice_Sell_SSR(string signature, FlightSearch obj, ArrayList SegInfo, ref Dictionary<string, string> xml, DataTable Pax, DataTable MealBagDT, string[] ViaArrv, string ServiceBundleCode)
       public string Spice_Sell_SSR_OLD(string signature, FlightSearch obj, ArrayList SegInfo, ref Dictionary<string, string> xml, DataTable Pax, DataTable MealBagDT, string[] ViaArrv, bool Bag, string SSRCode)
        {
            string Reqxml = "";
            string Resxml = "";
            string Retval = "FAILURE";

            try
            {
                //Spice_SellJourneyByKey = new BookingManagerClient();
                int Trip = SegInfo.Count;
                navitaire.SG.ver4.SellResponse sellres = new navitaire.SG.ver4.SellResponse();
                DataRow[] PxDt = new DataRow[obj.Adult + obj.Child];
                PxDt = Pax.Select("PaxType = 'ADT' OR PaxType ='CHD'", "PaxId ASC");

                //foreach (DataRow Px in PxDt)
                //{
                //    if (Px["MealType"] != null && Px["MealType"].ToString().Trim().Length == 4) TotSSR++;
                //    if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) TotSSR++;
                //    if (Px["Baggage"] != null && Px["Baggage"].ToString().Trim().Length == 4) TotSSR++;
                //}



                #region SELL REQUEST

                //Indicate that the we are selling a journey
                navitaire.SG.ver4.SellRequest sellrequest = new navitaire.SG.ver4.SellRequest();

                sellrequest.Signature = signature;
                sellrequest.ContractVersion = ContractVers;
                #region <SellRequestData>
                sellrequest.SellRequestData = new SellRequestData();
                sellrequest.SellRequestData.SellBy = SellBy.SSR;
                sellrequest.SellRequestData.SellSSR = new SellSSR();
                #region <SellSSR>

                sellrequest.SellRequestData.SellSSR.SSRRequest = new SSRRequest();
                //Confirm for Oway RT
                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[Trip];
                #region <SegmentSSRRequest>
                int MCntO = 0, MCntR = 0;
                for (int i = 0; i < Trip; i++)
                {
                    Dictionary<string, string> Seg = new Dictionary<string, string>();
                    Seg = (Dictionary<string, string>)SegInfo[i];
                    string VC = Seg["VC"].ToString().Trim().ToUpper();
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = Seg["VC"];
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = Seg["FNO"];
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = DateTime.Parse(Seg["STD"]);
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = Seg["Departure"];
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = Seg["Arrival"];

                    #region <PaxSSRS>
                    ////PaxSSRS NOde

                    int TotSSR = obj.Infant;
                    DataRow[] MBlist;
                    if (Seg["Flight"].ToString() == "1")
                    {
                        MBlist = MealBagDT.Select("TripType='O'");
                        MCntO++;
                    }
                    else
                    {
                        MBlist = MealBagDT.Select("TripType='R'");
                        MCntR++;
                    }

                    foreach (DataRow Px in MBlist)
                    {
                        if (VC == "SG" && Seg["Flight"].ToString() == "1" && MCntO > 1)
                        { }
                        else if (VC == "SG" && Seg["Flight"].ToString() == "2" && MCntR > 1)
                        { }
                        else
                        {
                            //if (String.IsNullOrEmpty(ServiceBundleCode) == true)
                            //    if (Px["MealCode"] != null && Px["MealCode"].ToString().Trim().Length == 4) TotSSR++;
                            if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) TotSSR++;
                        }
                        //if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) TotSSR++;
                        //if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) TotSSR++;
                    }
                    #region SG BUSINESS  
                    if (obj.Cabin.ToUpper() == "BUSINESS")
                        TotSSR += PxDt.Length;
                    #endregion
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = new PaxSSR[TotSSR];
                    //for (short inf = 0; inf < obj.Infant; inf++)
                    short inf = 0;
                    int Counter = 0;
                    short paxno = 0;
                    short ssrno = 0;


                    #region SSR for Infant
                    if (obj.Infant > 0)
                    {
                        for (int ii = 0; ii < obj.Infant; ii++)
                        {
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = "INFT";
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                            inf++;
                            Counter++;
                            ssrno++;
                            paxno++;
                        }
                    }
                    #endregion
                    paxno = 0; // Set Pax 0 again 
                    foreach (DataRow Px in MBlist)
                    {

                        #region For Counting Other Conditions
                        string meal = ""; //VGML
                        string ocode = "";//FFWD -Fast Forward Chekin
                        string bag_code = "";//XBPA,XBPB

                        //if (Px["InfAssociatePaxName"] != null && Px["InfAssociatePaxName"].ToString().Length == 4) fact++;
                        if (Px["MealCode"] != null && Px["MealCode"].ToString().Trim().Length == 4) { meal = Px["MealCode"].ToString().Trim(); }
                        // if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) { ocode = Px["MordifyStatus"].ToString().Trim(); }
                        if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) { bag_code = Px["BaggageCode"].ToString().Trim(); }
                        #endregion
                        //#region Seat
                        //if (String.IsNullOrEmpty(ServiceBundleCode) == true)
                        //{
                        //    if (meal.Length == 4)
                        //    {
                        //        if (VC == "SG" && Seg["Flight"].ToString() == "1" && MCntO > 1)
                        //        { }
                        //        else if (VC == "SG" && Seg["Flight"].ToString() == "2" && MCntR > 1)
                        //        { }
                        //        else
                        //        {
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                        //            //sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                        //            if (VC == "SG" && ViaArrv.Count() > 0)
                        //            {
                        //                try
                        //                {
                        //                    if (ViaArrv[i] != null && ViaArrv[i] != "")
                        //                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = ViaArrv[i];
                        //                    else
                        //                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                        //                }
                        //                catch (Exception ex)
                        //                {
                        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                        //                }
                        //            }
                        //            else
                        //            {
                        //                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                        //            }
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = meal;
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                        //            Counter++;
                        //            ssrno++;
                        //        }
                        //    }
                        //    if (ocode.Length == 4)
                        //    {
                        //        if (VC == "SG" && Seg["Flight"].ToString() == "1" && MCntO > 1)
                        //        { }
                        //        else if (VC == "SG" && Seg["Flight"].ToString() == "2" && MCntR > 1)
                        //        { }
                        //        else
                        //        {
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = ocode;
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                        //            Counter++;
                        //            ssrno++;
                        //        }
                        //    }
                        //}
                        //#endregion
                        #region Bag
                        if (bag_code.Length == 4)
                        {
                            if (VC == "SG" && Seg["Flight"].ToString() == "1" && MCntO > 1)
                            { }
                            else if (VC == "SG" && Seg["Flight"].ToString() == "2" && MCntR > 1)
                            { }
                            else
                            {
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = bag_code;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                                Counter++;
                                ssrno++;
                            }
                        }
                        #endregion
                        paxno++;

                    }


                    ////PaxSSRS Node End
                    #endregion
                    #region SG BUSINESS       
                    if (obj.Cabin.ToUpper() == "BUSINESS")
                    {
                        paxno = 0;
                        foreach (var pax in PxDt)
                        {
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = "BIZZ";
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                            Counter++;
                            ssrno++;
                            paxno++;
                        }
                    }
                    #endregion

                }
                #endregion

                sellrequest.SellRequestData.SellSSR.SSRRequest.CurrencyCode = "INR";
                sellrequest.SellRequestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                sellrequest.SellRequestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;

                #endregion

                #endregion

                #endregion

                Reqxml = SerializeAnObject(sellrequest, "SellSSR-Req");
                sellres = bookingAPI.Sell(sellrequest);
                Resxml = SerializeAnObject(sellres, "SellSSR-Res");
                if (sellres.BookingUpdateResponseData.Success != null)
                    Retval = sellres.BookingUpdateResponseData.Success.PNRAmount.TotalCost.ToString();//sellres.BookingUpdateResponseData.Success.PNRAmount.BalanceDue
            }
            catch (Exception ex)
            {
                Spice_Logout(signature);
                //xml.Add("UPPAXREQ", "");
                //xml.Add("UPPAXRES", "");
                //xml.Add("APBREQ", "");
                //xml.Add("APBRES", "");
                //xml.Add("BC-REQ", "");
                //xml.Add("BC-RES", "");
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_Sell_SSR)", "Error_003", ex, "Spice_Sell_SSR");
                xml.Add("OTHER", ex.Message);

            }
            finally
            {
                xml.Add("SSR", Reqxml + "<br>" + Resxml);
            }
            return Retval;
        }
        public string Spice_Sell_SSR(string signature, FlightSearch obj, ArrayList SegInfo, ref Dictionary<string, string> xml, DataTable Pax, DataTable MealBagDT, string[] ViaArrv, bool Bag, string SSRCode)
        {
            //Spice_Sell_SSR_OLD(string signature, FlightSearch obj, ArrayList SegInfo, ref Dictionary < string, string > xml, DataTable Pax, DataTable MealBagDT, string[] ViaArrv, bool Bag, string SSRCode)
            //Spice_Sell_SSR(string signature, FlightSearch obj, ArrayList SegInfo, ref Dictionary < string, string > xml, DataTable Pax, DataTable MealBagDT, string[] ViaArrv, string ServiceBundleCode, List < Seat > SeatList)
            string Reqxml = "";
            string Resxml = "";
            string Retval = "FAILURE";
            bool isMaxSeat = false;
            try
            {
                //Spice_SellJourneyByKey = new BookingManagerClient();
                int Trip = SegInfo.Count;
                navitaire.SG.ver4.SellResponse sellres = new navitaire.SG.ver4.SellResponse();
                DataRow[] PxDt = new DataRow[obj.Adult + obj.Child];
                PxDt = Pax.Select("PaxType = 'ADT' OR PaxType ='CHD'", "PaxId ASC");

                //foreach (DataRow Px in PxDt)
                //{
                //    if (Px["MealType"] != null && Px["MealType"].ToString().Trim().Length == 4) TotSSR++;
                //    if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) TotSSR++;
                //    if (Px["Baggage"] != null && Px["Baggage"].ToString().Trim().Length == 4) TotSSR++;
                //}



                #region SELL REQUEST

                //Indicate that the we are selling a journey
                navitaire.SG.ver4.SellRequest sellrequest = new navitaire.SG.ver4.SellRequest();

                sellrequest.Signature = signature;
                sellrequest.ContractVersion = ContractVers;
                #region <SellRequestData>
                sellrequest.SellRequestData = new SellRequestData();
                sellrequest.SellRequestData.SellBy = SellBy.SSR;
                sellrequest.SellRequestData.SellSSR = new SellSSR();
                #region <SellSSR>

                sellrequest.SellRequestData.SellSSR.SSRRequest = new SSRRequest();
                //Confirm for Oway RT
                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[Trip];
                #region <SegmentSSRRequest>
                int MCntO = 0, MCntR = 0, MCntT = 0, MCntF = 0;
                for (int i = 0; i < Trip; i++)
                {
                    Dictionary<string, string> Seg = new Dictionary<string, string>();
                    Seg = (Dictionary<string, string>)SegInfo[i];
                    string VC = Seg["VC"].ToString().Trim().ToUpper();
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = Seg["VC"];
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = Seg["FNO"];
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = DateTime.Parse(Seg["STD"]);
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = Seg["Departure"];
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = Seg["Arrival"];


                    #region <PaxSSRS>
                    ////PaxSSRS NOde

                    int TotSSR = obj.Infant;


                    #region SpiceMax
                    //string maxSeatSSRCode = SeatList.Where(x => x.Origin == Seg["Departure"] && x.Destination == Seg["Arrival"] && x.isMaxSeat).Select(x => x.MaxSeatSSRCode).FirstOrDefault();
                    //int maxSeatSSR = 0;
                    //if (!string.IsNullOrEmpty(maxSeatSSRCode))
                    //{
                    //    foreach (var MSSSRCode in maxSeatSSRCode.Split(','))
                    //    {
                    //        maxSeatSSR = string.IsNullOrEmpty(MSSSRCode) ? maxSeatSSR : (maxSeatSSR + 1);
                    //    }
                    //}
                    //TotSSR = TotSSR + maxSeatSSR;
                    #endregion SpiceMax
                    DataRow[] MBlist;
                    if (Seg["Flight"].ToString() == "1")
                    {
                        MBlist = MealBagDT.Select("TripType='O'");
                        MCntO++;
                    }
                    else if (Seg["Flight"].ToString() == "2")
                    {
                        MBlist = MealBagDT.Select("TripType='R'");
                        MCntR++;
                    }
                    else if (Seg["Flight"].ToString() == "3")
                    {
                        MBlist = MealBagDT.Select("TripType='T'");
                        MCntT++;
                    }
                    else
                    {
                        MBlist = MealBagDT.Select("TripType='F'");
                        MCntF++;
                    }

                    foreach (DataRow Px in MBlist)
                    {
                        if (VC == "SG" && Seg["Flight"].ToString() == "1" && MCntO > 1)
                        { }
                        else if (VC == "SG" && Seg["Flight"].ToString() == "2" && MCntR > 1)
                        { }
                        else if (VC == "SG" && Seg["Flight"].ToString() == "3" && MCntT > 1)
                        { }
                        else if (VC == "SG" && Seg["Flight"].ToString() == "4" && MCntF > 1)
                        { }
                        else
                        {
                            //if (String.IsNullOrEmpty(ServiceBundleCode) == true)
                            if (Px["MealCode"] != null && Px["MealCode"].ToString().Trim().Length == 4) TotSSR++;
                            if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) TotSSR++;
                        }
                        //if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) TotSSR++;
                        //if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) TotSSR++;
                    }
                    #region SG BUSINESS  
                    if (obj.Cabin.ToUpper() == "BUSINESS")
                        TotSSR += PxDt.Length;
                    #endregion
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = new PaxSSR[TotSSR];
                    //for (short inf = 0; inf < obj.Infant; inf++)
                    short inf = 0;
                    int Counter = 0;
                    short paxno = 0;
                    short ssrno = 0;


                    #region SSR for Infant
                    if (obj.Infant > 0)
                    {
                        for (int ii = 0; ii < obj.Infant; ii++)
                        {
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = "INFT";
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                            inf++;
                            Counter++;
                            ssrno++;
                            paxno++;
                        }
                    }
                    #endregion

                    #region SSR for MaxSeat
                    //paxno = 0;
                    //if (!string.IsNullOrEmpty(maxSeatSSRCode))
                    //{
                    //    foreach (var SM_SSRCode in maxSeatSSRCode.Split(','))
                    //    {
                    //        if (!string.IsNullOrEmpty(SM_SSRCode))
                    //        {
                    //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                    //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                    //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                    //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                    //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                    //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                    //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = SM_SSRCode.Trim();
                    //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                    //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                    //            Counter++;
                    //            ssrno++;
                    //        }
                    //    }
                    //}
                    #endregion

                    paxno = 0; // Set Pax 0 again 
                    foreach (DataRow Px in MBlist)
                    {

                        #region For Counting Other Conditions
                        string meal = ""; //VGML
                        string ocode = "";//FFWD -Fast Forward Chekin
                        string bag_code = "";//XBPA,XBPB

                        //if (Px["InfAssociatePaxName"] != null && Px["InfAssociatePaxName"].ToString().Length == 4) fact++;
                        if (Px["MealCode"] != null && Px["MealCode"].ToString().Trim().Length == 4) { meal = Px["MealCode"].ToString().Trim(); }
                        // if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) { ocode = Px["MordifyStatus"].ToString().Trim(); }
                        if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) { bag_code = Px["BaggageCode"].ToString().Trim(); }
                        #endregion
                        #region Seat
                        // if (String.IsNullOrEmpty(ServiceBundleCode) == true)
                        // {
                        if (meal.Length == 4)
                        {
                            if (VC == "SG" && Seg["Flight"].ToString() == "1" && MCntO > 1)
                            { }
                            else if (VC == "SG" && Seg["Flight"].ToString() == "2" && MCntR > 1)
                            { }
                            else if (VC == "SG" && Seg["Flight"].ToString() == "3" && MCntT > 1)
                            { }
                            else if (VC == "SG" && Seg["Flight"].ToString() == "4" && MCntF > 1)
                            { }
                            else
                            {
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                                //sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                                if (VC == "SG" && ViaArrv.Count() > 0)
                                {
                                    try
                                    {
                                        if (ViaArrv[i] != null && ViaArrv[i] != "")
                                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = ViaArrv[i];
                                        else
                                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                                    }
                                    catch (Exception ex)
                                    {
                                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                                    }
                                }
                                else
                                {
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                                }
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = meal;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                                Counter++;
                                ssrno++;
                            }
                        }
                        if (ocode.Length == 4)
                        {
                            if (VC == "SG" && Seg["Flight"].ToString() == "1" && MCntO > 1)
                            { }
                            else if (VC == "SG" && Seg["Flight"].ToString() == "2" && MCntR > 1)
                            { }
                            else if (VC == "SG" && Seg["Flight"].ToString() == "3" && MCntT > 1)
                            { }
                            else if (VC == "SG" && Seg["Flight"].ToString() == "4" && MCntF > 1)
                            { }
                            else
                            {
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = ocode;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                                Counter++;
                                ssrno++;
                            }
                        }
                        //}
                        #endregion
                        #region Bag
                        if (bag_code.Length == 4)
                        {
                            if (VC == "SG" && Seg["Flight"].ToString() == "1" && MCntO > 1)
                            { }
                            else if (VC == "SG" && Seg["Flight"].ToString() == "2" && MCntR > 1)
                            { }
                            else if (VC == "SG" && Seg["Flight"].ToString() == "3" && MCntT > 1)
                            { }
                            else if (VC == "SG" && Seg["Flight"].ToString() == "4" && MCntF > 1)
                            { }
                            else
                            {
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = bag_code;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                                Counter++;
                                ssrno++;
                            }
                        }
                        #endregion
                        paxno++;

                    }

                    #region HBAG
                    if (Bag && SSRCode != "")
                    {
                        short paxnu = 0;
                        foreach (DataRow Px in PxDt)
                        {

                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxnu;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = SSRCode;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                            Counter++;
                            paxnu++;
                        }
                    }
                    #endregion
                    ////PaxSSRS Node End
                    #endregion
                    #region SG BUSINESS       
                    if (obj.Cabin.ToUpper() == "BUSINESS")
                    {
                        paxno = 0;
                        foreach (var pax in PxDt)
                        {
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = "BIZZ";
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                            Counter++;
                            ssrno++;
                            paxno++;
                        }
                    }
                    #endregion

                }
                #endregion

                sellrequest.SellRequestData.SellSSR.SSRRequest.CurrencyCode = "INR";
                sellrequest.SellRequestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                sellrequest.SellRequestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;

                #endregion

                #endregion

                #endregion

                Reqxml = SerializeAnObject(sellrequest, "SellSSR-Req");
                sellres = bookingAPI.Sell(sellrequest);
                Resxml = SerializeAnObject(sellres, "SellSSR-Res");
                if (sellres.BookingUpdateResponseData.Success != null)
                    Retval = sellres.BookingUpdateResponseData.Success.PNRAmount.TotalCost.ToString();//sellres.BookingUpdateResponseData.Success.PNRAmount.BalanceDue
            }
            catch (Exception ex)
            {
                Spice_Logout(signature);
                //xml.Add("UPPAXREQ", "");
                //xml.Add("UPPAXRES", "");
                //xml.Add("APBREQ", "");
                //xml.Add("APBRES", "");
                //xml.Add("BC-REQ", "");
                //xml.Add("BC-RES", "");
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_Sell_SSR)", "Error_003", ex, "Spice_Sell_SSR");
                xml.Add("OTHER", ex.Message);

            }
            finally
            {
                xml.Add("SSR", Reqxml + "<br>" + Resxml);
            }
            return Retval;
        }


        //public string Spice_Sell_SSR(string signature, FlightSearch obj, ArrayList SegInfo, ref Dictionary<string, string> xml, DataTable Pax, DataTable MealBagDT, string[] ViaArrv, string ServiceBundleCode)
        //{
        //    string Reqxml = "";
        //    string Resxml = "";
        //    string Retval = "FAILURE";

        //    try
        //    {
        //        //Spice_SellJourneyByKey = new BookingManagerClient();
        //        int Trip = SegInfo.Count;
        //        navitaire.SG.ver4.SellResponse sellres = new navitaire.SG.ver4.SellResponse();
        //        DataRow[] PxDt = new DataRow[obj.Adult + obj.Child];
        //        PxDt = Pax.Select("PaxType = 'ADT' OR PaxType ='CHD'", "PaxId ASC");

        //        //foreach (DataRow Px in PxDt)
        //        //{
        //        //    if (Px["MealType"] != null && Px["MealType"].ToString().Trim().Length == 4) TotSSR++;
        //        //    if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) TotSSR++;
        //        //    if (Px["Baggage"] != null && Px["Baggage"].ToString().Trim().Length == 4) TotSSR++;
        //        //}



        //        #region SELL REQUEST

        //        //Indicate that the we are selling a journey
        //        navitaire.SG.ver4.SellRequest sellrequest = new navitaire.SG.ver4.SellRequest();

        //        sellrequest.Signature = signature;
        //        sellrequest.ContractVersion = ContractVers;
        //        #region <SellRequestData>
        //        sellrequest.SellRequestData = new SellRequestData();
        //        sellrequest.SellRequestData.SellBy = SellBy.SSR;
        //        sellrequest.SellRequestData.SellSSR = new SellSSR();
        //        #region <SellSSR>

        //        sellrequest.SellRequestData.SellSSR.SSRRequest = new SSRRequest();
        //        //Confirm for Oway RT
        //        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[Trip];
        //        #region <SegmentSSRRequest>
        //        int MCntO = 0, MCntR = 0;
        //        for (int i = 0; i < Trip; i++)
        //        {
        //            Dictionary<string, string> Seg = new Dictionary<string, string>();
        //            Seg = (Dictionary<string, string>)SegInfo[i];
        //            string VC = Seg["VC"].ToString().Trim().ToUpper();
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = Seg["VC"];
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = Seg["FNO"];
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = DateTime.Parse(Seg["STD"]);
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = Seg["Departure"];
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = Seg["Arrival"];

        //            #region <PaxSSRS>
        //            ////PaxSSRS NOde

        //            int TotSSR = obj.Infant;
        //            DataRow[] MBlist;
        //            if (Seg["Flight"].ToString() == "1")
        //            {
        //                MBlist = MealBagDT.Select("TripType='O'");
        //                MCntO++;
        //            }
        //            else
        //            {
        //                MBlist = MealBagDT.Select("TripType='R'");
        //                MCntR++;
        //            }

        //            foreach (DataRow Px in MBlist)
        //            {
        //                if (VC == "6E" && Seg["Flight"].ToString() == "1" && MCntO > 1)
        //                { }
        //                else if (VC == "6E" && Seg["Flight"].ToString() == "2" && MCntR > 1)
        //                { }
        //                else
        //                {
        //                    if (String.IsNullOrEmpty(ServiceBundleCode) == true)
        //                        if (Px["MealCode"] != null && Px["MealCode"].ToString().Trim().Length == 4) TotSSR++;
        //                    if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) TotSSR++;
        //                }
        //                //if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) TotSSR++;
        //                //if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) TotSSR++;
        //            }
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = new PaxSSR[TotSSR];
        //            //for (short inf = 0; inf < obj.Infant; inf++)
        //            short inf = 0;
        //            int Counter = 0;
        //            short paxno = 0;
        //            short ssrno = 0;


        //            #region SSR for Infant
        //            if (obj.Infant > 0)
        //            {
        //                for (int ii = 0; ii < obj.Infant; ii++)
        //                {
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = "INFT";
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
        //                    inf++;
        //                    Counter++;
        //                    ssrno++;
        //                    paxno++;
        //                }
        //            }
        //            #endregion
        //            paxno = 0; // Set Pax 0 again 
        //            foreach (DataRow Px in MBlist)
        //            {

        //                #region For Counting Other Conditions
        //                string meal = ""; //VGML
        //                string ocode = "";//FFWD -Fast Forward Chekin
        //                string bag_code = "";//XBPA,XBPB

        //                //if (Px["InfAssociatePaxName"] != null && Px["InfAssociatePaxName"].ToString().Length == 4) fact++;
        //                if (Px["MealCode"] != null && Px["MealCode"].ToString().Trim().Length == 4) { meal = Px["MealCode"].ToString().Trim(); }
        //                // if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) { ocode = Px["MordifyStatus"].ToString().Trim(); }
        //                if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) { bag_code = Px["BaggageCode"].ToString().Trim(); }
        //                #endregion
        //                #region Seat
        //                if (String.IsNullOrEmpty(ServiceBundleCode) == true)
        //                {
        //                    if (meal.Length == 4)
        //                    {
        //                        if (VC == "6E" && Seg["Flight"].ToString() == "1" && MCntO > 1)
        //                        { }
        //                        else if (VC == "6E" && Seg["Flight"].ToString() == "2" && MCntR > 1)
        //                        { }
        //                        else
        //                        {
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
        //                            //sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
        //                            if (VC == "6E" && ViaArrv.Count() > 0)
        //                            {
        //                                try
        //                                {
        //                                    if (ViaArrv[i] != null && ViaArrv[i] != "")
        //                                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = ViaArrv[i];
        //                                    else
        //                                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
        //                                }
        //                                catch (Exception ex)
        //                                {
        //                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
        //                                }
        //                            }
        //                            else
        //                            {
        //                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
        //                            }
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = meal;
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
        //                            Counter++;
        //                            ssrno++;
        //                        }
        //                    }
        //                    if (ocode.Length == 4)
        //                    {
        //                        if (VC == "6E" && Seg["Flight"].ToString() == "1" && MCntO > 1)
        //                        { }
        //                        else if (VC == "6E" && Seg["Flight"].ToString() == "2" && MCntR > 1)
        //                        { }
        //                        else
        //                        {
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = ocode;
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
        //                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
        //                            Counter++;
        //                            ssrno++;
        //                        }
        //                    }
        //                }
        //                #endregion
        //                #region Bag
        //                if (bag_code.Length == 4)
        //                {
        //                    if (VC == "6E" && Seg["Flight"].ToString() == "1" && MCntO > 1)
        //                    { }
        //                    else if (VC == "6E" && Seg["Flight"].ToString() == "2" && MCntR > 1)
        //                    { }
        //                    else
        //                    {
        //                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
        //                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
        //                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
        //                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
        //                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
        //                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
        //                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = bag_code;
        //                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
        //                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
        //                        Counter++;
        //                        ssrno++;
        //                    }
        //                }
        //                #endregion
        //                paxno++;

        //            }
        //            ////PaxSSRS Node End
        //            #endregion

        //        }
        //        #endregion

        //        sellrequest.SellRequestData.SellSSR.SSRRequest.CurrencyCode = "INR";
        //        sellrequest.SellRequestData.SellSSR.SSRRequest.CancelFirstSSR = false;
        //        sellrequest.SellRequestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;

        //        #endregion

        //        #endregion

        //        #endregion

        //        Reqxml = SerializeAnObject(sellrequest, "SellSSR-Req");
        //        sellres = bookingAPI.Sell(sellrequest);
        //        Resxml = SerializeAnObject(sellres, "SellSSR-Res");
        //        if (sellres.BookingUpdateResponseData.Success != null)
        //            Retval = sellres.BookingUpdateResponseData.Success.PNRAmount.TotalCost.ToString();//sellres.BookingUpdateResponseData.Success.PNRAmount.BalanceDue
        //    }
        //    catch (Exception ex)
        //    {
        //        Spice_Logout(signature);
        //        //xml.Add("UPPAXREQ", "");
        //        //xml.Add("UPPAXRES", "");
        //        //xml.Add("APBREQ", "");
        //        //xml.Add("APBRES", "");
        //        //xml.Add("BC-REQ", "");
        //        //xml.Add("BC-RES", "");
        //        xml.Add("OTHER", ex.Message);

        //    }
        //    finally
        //    {
        //        xml.Add("SSR", Reqxml + "<br>" + Resxml);
        //    }
        //    return Retval;
        //}
        public string Spice_Sell_SSR_Reissue(string signature, FlightSearch obj, ArrayList SegInfo, ref Dictionary<string, string> xml, DataTable Pax, DataTable MealBagDT, string[] ViaArrv, string ServiceBundleCode, ref string BalanceDueAfterCan)
        {
            string Reqxml = "";
            string Resxml = "";
            string Retval = "FAILURE";

            try
            {
                //Spice_SellJourneyByKey = new BookingManagerClient();
                int Trip = SegInfo.Count;
                navitaire.SG.ver4.SellResponse sellres = new navitaire.SG.ver4.SellResponse();
                DataRow[] PxDt = new DataRow[obj.Adult + obj.Child];
                PxDt = Pax.Select("PaxType = 'ADT' OR PaxType ='CHD'", "PaxId ASC");

                //foreach (DataRow Px in PxDt)
                //{
                //    if (Px["MealType"] != null && Px["MealType"].ToString().Trim().Length == 4) TotSSR++;
                //    if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) TotSSR++;
                //    if (Px["Baggage"] != null && Px["Baggage"].ToString().Trim().Length == 4) TotSSR++;
                //}



                #region SELL REQUEST

                //Indicate that the we are selling a journey
                navitaire.SG.ver4.SellRequest sellrequest = new navitaire.SG.ver4.SellRequest();

                sellrequest.Signature = signature;
                sellrequest.ContractVersion = ContractVers;
                #region <SellRequestData>
                sellrequest.SellRequestData = new SellRequestData();
                sellrequest.SellRequestData.SellBy = SellBy.SSR;
                sellrequest.SellRequestData.SellSSR = new SellSSR();
                #region <SellSSR>

                sellrequest.SellRequestData.SellSSR.SSRRequest = new SSRRequest();
                //Confirm for Oway RT
                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[Trip];
                #region <SegmentSSRRequest>
                int MCntO = 0, MCntR = 0;
                for (int i = 0; i < Trip; i++)
                {
                    Dictionary<string, string> Seg = new Dictionary<string, string>();
                    Seg = (Dictionary<string, string>)SegInfo[i];
                    string VC = Seg["VC"].ToString().Trim().ToUpper();
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = Seg["VC"];
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = Seg["FNO"];
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = DateTime.Parse(Seg["STD"]);
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = Seg["Departure"];
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = Seg["Arrival"];

                    #region <PaxSSRS>
                    ////PaxSSRS NOde

                    int TotSSR = obj.Infant;
                    DataRow[] MBlist;
                    if (Seg["Flight"].ToString() == "1")
                    {
                        MBlist = MealBagDT.Select("TripType='O'");
                        MCntO++;
                    }
                    else
                    {
                        MBlist = MealBagDT.Select("TripType='R'");
                        MCntR++;
                    }

                    foreach (DataRow Px in MBlist)
                    {
                        //if (VC == "6E" && Seg["Flight"].ToString() == "1" && MCntO > 1)
                        //{ }
                        //else if (VC == "6E" && Seg["Flight"].ToString() == "2" && MCntR > 1)
                        //{ }
                        //else
                        //{
                        if (String.IsNullOrEmpty(ServiceBundleCode) == true)
                            if (Px["MealCode"] != null && Px["MealCode"].ToString().Trim().Length == 4) TotSSR++;
                        if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) TotSSR++;
                        //  }
                        //if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) TotSSR++;
                        //if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) TotSSR++;
                    }
                    #region SG BUSINESS  
                    if (obj.Cabin.ToUpper() == "BUSINESS")
                        TotSSR += PxDt.Length;
                    #endregion
                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = new PaxSSR[TotSSR];
                    //for (short inf = 0; inf < obj.Infant; inf++)
                    short inf = 0;
                    int Counter = 0;
                    short paxno = 0;
                    short ssrno = 0;


                    #region SSR for Infant
                    if (obj.Infant > 0)
                    {
                        for (int ii = 0; ii < obj.Infant; ii++)
                        {
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = "INFT";
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                            inf++;
                            Counter++;
                            ssrno++;
                            paxno++;
                        }
                    }
                    #endregion
                    paxno = 0; // Set Pax 0 again 
                    foreach (DataRow Px in MBlist)
                    {

                        #region For Counting Other Conditions
                        string meal = ""; //VGML
                        string ocode = "";//FFWD -Fast Forward Chekin
                        string bag_code = "";//XBPA,XBPB

                        //if (Px["InfAssociatePaxName"] != null && Px["InfAssociatePaxName"].ToString().Length == 4) fact++;
                        if (Px["MealCode"] != null && Px["MealCode"].ToString().Trim().Length == 4) { meal = Px["MealCode"].ToString().Trim(); }
                        // if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) { ocode = Px["MordifyStatus"].ToString().Trim(); }
                        if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) { bag_code = Px["BaggageCode"].ToString().Trim(); }
                        #endregion
                        #region Seat
                        if (String.IsNullOrEmpty(ServiceBundleCode) == true)
                        {
                            if (meal.Length == 4)
                            {
                                if (VC == "6E" && Seg["Flight"].ToString() == "1" && MCntO > 1)
                                { }
                                else if (VC == "6E" && Seg["Flight"].ToString() == "2" && MCntR > 1)
                                { }
                                else
                                {
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                                    //sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                                    if (VC == "6E" && ViaArrv.Count() > 0)
                                    {
                                        try
                                        {
                                            if (ViaArrv[i] != null && ViaArrv[i] != "")
                                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = ViaArrv[i];
                                            else
                                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                                        }
                                        catch (Exception ex)
                                        {
                                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                                        }
                                    }
                                    else
                                    {
                                        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                                    }
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = meal;
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                                    Counter++;
                                    ssrno++;
                                }
                            }
                            if (ocode.Length == 4)
                            {
                                if (VC == "6E" && Seg["Flight"].ToString() == "1" && MCntO > 1)
                                { }
                                else if (VC == "6E" && Seg["Flight"].ToString() == "2" && MCntR > 1)
                                { }
                                else
                                {
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = ocode;
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                                    Counter++;
                                    ssrno++;
                                }
                            }
                        }
                        #endregion
                        #region Bag
                        if (bag_code.Length == 4)
                        {
                            if (VC == "6E" && Seg["Flight"].ToString() == "1" && MCntO > 1)
                            { }
                            else if (VC == "6E" && Seg["Flight"].ToString() == "2" && MCntR > 1)
                            { }
                            else
                            {
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = bag_code;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                                sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                                Counter++;
                                ssrno++;
                            }
                        }
                        #endregion
                        paxno++;

                    }
                    ////PaxSSRS Node End
                    #endregion
                    #region SG BUSINESS       
                    if (obj.Cabin.ToUpper() == "BUSINESS")
                    {
                        paxno = 0;
                        foreach (var pax in PxDt)
                        {
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = "BIZZ";
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = 0;// ssrno;
                            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
                            Counter++;
                            ssrno++;
                            paxno++;
                        }
                    }
                    #endregion
                }
                #endregion

                sellrequest.SellRequestData.SellSSR.SSRRequest.CurrencyCode = "INR";
                sellrequest.SellRequestData.SellSSR.SSRRequest.CancelFirstSSR = false;
                sellrequest.SellRequestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;

                #endregion

                #endregion

                #endregion

                Reqxml = SerializeAnObject(sellrequest, "SellSSR-Req");
                sellres = bookingAPI.Sell(sellrequest);
                Resxml = SerializeAnObject(sellres, "SellSSR-Res");
                if (sellres.BookingUpdateResponseData.Success != null)
                {
                    Retval = sellres.BookingUpdateResponseData.Success.PNRAmount.TotalCost.ToString();//sellres.BookingUpdateResponseData.Success.PNRAmount.BalanceDue
                    BalanceDueAfterCan = sellres.BookingUpdateResponseData.Success.PNRAmount.BalanceDue.ToString();
                }
            }
            catch (Exception ex)
            {
                Spice_Logout(signature);
                //xml.Add("UPPAXREQ", "");
                //xml.Add("UPPAXRES", "");
                //xml.Add("APBREQ", "");
                //xml.Add("APBRES", "");
                //xml.Add("BC-REQ", "");
                //xml.Add("BC-RES", "");
                xml.Add("OTHER", ex.Message);

            }
            finally
            {
                xml.Add("SSR", Reqxml + "<br>" + Resxml);
            }
            return Retval;
        }

        //public string Spice_Sell_SSR(string signature, FlightSearch obj, ArrayList SegInfo, ref Dictionary<string, string> xml, DataTable Pax, DataTable MealBagDT)
        //{
        //    string Reqxml = "";
        //    string Resxml = "";
        //    string Retval = "FAILURE";

        //    try
        //    {
        //        //Spice_SellJourneyByKey = new BookingManagerClient();
        //        int Trip = SegInfo.Count;
        //        SellResponse sellres = new SellResponse();
        //        DataRow[] PxDt = new DataRow[obj.Adult + obj.Child];
        //        PxDt = Pax.Select("PaxType = 'ADT' OR PaxType ='CHD'", "PaxId ASC");

        //        //foreach (DataRow Px in PxDt)
        //        //{
        //        //    if (Px["MealType"] != null && Px["MealType"].ToString().Trim().Length == 4) TotSSR++;
        //        //    if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) TotSSR++;
        //        //    if (Px["Baggage"] != null && Px["Baggage"].ToString().Trim().Length == 4) TotSSR++;
        //        //}



        //        #region SELL REQUEST

        //        //Indicate that the we are selling a journey
        //        SellRequest sellrequest = new SellRequest();

        //        sellrequest.Signature = signature;
        //        sellrequest.ContractVersion = ContractVers;
        //        #region <SellRequestData>
        //        sellrequest.SellRequestData = new SellRequestData();
        //        sellrequest.SellRequestData.SellBy = SellBy.SSR;
        //        sellrequest.SellRequestData.SellSSR = new SellSSR();
        //        #region <SellSSR>

        //        sellrequest.SellRequestData.SellSSR.SSRRequest = new SSRRequest();
        //        //Confirm for Oway RT
        //        sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests = new SegmentSSRRequest[Trip];
        //        #region <SegmentSSRRequest>
        //        for (int i = 0; i < Trip; i++)
        //        {
        //            Dictionary<string, string> Seg = new Dictionary<string, string>();
        //            Seg = (Dictionary<string, string>)SegInfo[i];
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i] = new SegmentSSRRequest();
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator = new FlightDesignator();
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.CarrierCode = Seg["VC"];
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].FlightDesignator.FlightNumber = Seg["FNO"];
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].STD = DateTime.Parse(Seg["STD"]);
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].DepartureStation = Seg["Departure"];
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].ArrivalStation = Seg["Arrival"];

        //            #region <PaxSSRS>
        //            ////PaxSSRS NOde
        //            int TotSSR = obj.Infant;
        //            DataRow[] MBlist;
        //            if (Seg["Flight"].ToString() == "1")
        //            {
        //                MBlist = MealBagDT.Select("TripType='O'");
        //            }
        //            else
        //            {
        //                MBlist = MealBagDT.Select("TripType='R'");
        //            }

        //            foreach (DataRow Px in MBlist)
        //            {
        //                if (Px["MealCode"] != null && Px["MealCode"].ToString().Trim().Length == 4) TotSSR++;
        //                //if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) TotSSR++;
        //                if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) TotSSR++;
        //            }
        //            sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs = new PaxSSR[TotSSR];
        //            //for (short inf = 0; inf < obj.Infant; inf++)
        //            short inf = 0;
        //            int Counter = 0;
        //            short paxno = 0;
        //            short ssrno = 0;


        //            #region SSR for Infant
        //            if (obj.Infant > 0)
        //            {
        //                for (int ii = 0; ii < obj.Infant; ii++)
        //                {
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = "INFT";
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = ssrno;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
        //                    inf++;
        //                    Counter++;
        //                    ssrno++;
        //                }
        //            }
        //            #endregion

        //            foreach (DataRow Px in MBlist)
        //            {

        //                #region For Counting Other Conditions
        //                string meal = ""; //VGML
        //                string ocode = "";//FFWD -Fast Forward Chekin
        //                string bag_code = "";//XBPA,XBPB

        //                //if (Px["InfAssociatePaxName"] != null && Px["InfAssociatePaxName"].ToString().Length == 4) fact++;
        //                if (Px["MealCode"] != null && Px["MealCode"].ToString().Trim().Length == 4) { meal = Px["MealCode"].ToString().Trim(); }
        //                // if (Px["MordifyStatus"] != null && Px["MordifyStatus"].ToString().Trim().Length == 4) { ocode = Px["MordifyStatus"].ToString().Trim(); }
        //                if (Px["BaggageCode"] != null && Px["BaggageCode"].ToString().Trim().Length == 4) { bag_code = Px["BaggageCode"].ToString().Trim(); }
        //                #endregion
        //                //#region SSR for Infant
        //                //if (obj.Infant > 0 && inf < obj.Infant)
        //                //{
        //                //    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
        //                //    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
        //                //    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
        //                //    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
        //                //    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
        //                //    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
        //                //    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = "INFT";
        //                //    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = ssrno;
        //                //    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
        //                //    inf++;
        //                //    Counter++;
        //                //    ssrno++;
        //                //}
        //                //#endregion
        //                #region SSR for Other Codes
        //                if (meal.Length == 4)
        //                {
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = meal;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = ssrno;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
        //                    Counter++;
        //                    ssrno++;
        //                }
        //                if (ocode.Length == 4)
        //                {
        //                    //sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
        //                    //sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
        //                    //sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
        //                    //sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
        //                    //sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
        //                    //sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
        //                    //sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = ocode;
        //                    //sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = ssrno;
        //                    //sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
        //                    //Counter++;
        //                    //ssrno++;
        //                }
        //                if (bag_code.Length == 4)
        //                {
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter] = new PaxSSR();
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].State = MessageState.New;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ActionStatusCode = "NN";
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].ArrivalStation = Seg["Arrival"];
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].DepartureStation = Seg["Departure"];
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].PassengerNumber = paxno;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRCode = bag_code;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRNumber = ssrno;
        //                    sellrequest.SellRequestData.SellSSR.SSRRequest.SegmentSSRRequests[i].PaxSSRs[Counter].SSRValue = 0;
        //                    Counter++;
        //                    ssrno++;
        //                }
        //                paxno++;
        //                #endregion
        //            }
        //            ////PaxSSRS Node End
        //            #endregion

        //        }
        //        #endregion

        //        sellrequest.SellRequestData.SellSSR.SSRRequest.CurrencyCode = "INR";
        //        sellrequest.SellRequestData.SellSSR.SSRRequest.CancelFirstSSR = false;
        //        sellrequest.SellRequestData.SellSSR.SSRRequest.SSRFeeForceWaiveOnSell = false;

        //        #endregion

        //        #endregion

        //        #endregion

        //        Reqxml = SerializeAnObject(sellrequest, "SellSSR-Req");
        //        sellres = bookingAPI.Sell(sellrequest);
        //        Resxml = SerializeAnObject(sellres, "SellSSR-Res");
        //        if (sellres.BookingUpdateResponseData.Success != null)
        //            Retval = sellres.BookingUpdateResponseData.Success.PNRAmount.TotalCost.ToString();//sellres.BookingUpdateResponseData.Success.PNRAmount.BalanceDue
        //    }
        //    catch (Exception ex)
        //    {
        //        Spice_Logout(signature);
        //        xml.Add("UPPAXREQ", "");
        //        xml.Add("UPPAXRES", "");
        //        xml.Add("APBREQ", "");
        //        xml.Add("APBRES", "");
        //        xml.Add("BC-REQ", "");
        //        xml.Add("BC-RES", "");
        //        xml.Add("OTHER", ex.Message);
        //    }
        //    finally
        //    {
        //        xml.Add("SSR", Reqxml + "<br>" + Resxml);
        //    }
        //    return Retval;
        //}



        public string Spice_UpdatePassenger(string signature, FlightSearch obj, DataTable dt, ref Dictionary<string, string> xml)
        {
            string Reqxml = "";
            string Resxml = "";

            string ret = "FAILURE";
            try
            {
                int m = (obj.Adult + obj.Child);
                //IBookingManager bookingAPI = new BookingManagerClient();
                navitaire.SG.ver4.UpdatePassengersResponse updateres = new navitaire.SG.ver4.UpdatePassengersResponse();
                navitaire.SG.ver4.UpdatePassengersRequest upsr = new navitaire.SG.ver4.UpdatePassengersRequest();
                upsr.Signature = signature;
                upsr.ContractVersion = ContractVers;
                UpdatePassengersRequestData upsd = new UpdatePassengersRequestData();
                upsd.Passengers = new Passenger[m];
                var adt = obj.Adult;
                var inf = obj.Infant;

                DataRow[] dr = dt.Select("PaxType = 'ADT'");
                DataRow[] dinf = new DataRow[obj.Infant];
                DataRow[] dchd = new DataRow[obj.Child];
                if (obj.Infant > 0)
                {
                    dinf = dt.Select("PaxType = 'INF'");
                }
                if (obj.Child > 0)
                {
                    dchd = dt.Select("PaxType = 'CHD'");
                }

                #region Adult
                for (int i = 0; i < obj.Adult; i++)
                {
                    string MNAME = dr[i]["MName"].ToString().ToUpper();
                    string LNAME = dr[i]["LName"].ToString().ToUpper();
                    string Title = Convert.ToString(dr[i]["Title"]).ToUpper();
                    if (Title.ToUpper() == "MISS")
                    {
                        Title = "MS"; // Passenger Title Miss is invalid; 
                    }
                    upsd.Passengers[i] = new Passenger();
                    upsd.Passengers[i].State = MessageState.New;
                    #region PassengerPrograms
                    upsd.Passengers[i].PassengerPrograms = new PassengerProgram[1];
                    upsd.Passengers[i].PassengerPrograms[0] = new PassengerProgram();
                    upsd.Passengers[i].PassengerPrograms[0].State = MessageState.New;

                    #endregion

                    upsd.Passengers[i].CustomerNumber = "";
                    upsd.Passengers[i].PassengerNumber = (short)i;
                    upsd.Passengers[i].FamilyNumber = 0;
                    upsd.Passengers[i].PaxDiscountCode = "";

                    upsd.Passengers[i].Names = new BookingName[1];
                    upsd.Passengers[i].Names[0] = new BookingName();
                    //BOTH MIDDLE NAME AND LAST NAME NOT THERE
                    if ((MNAME == "" || MNAME == null) && (LNAME == "" || LNAME == null))
                    {
                        upsd.Passengers[i].Names[0].FirstName = Title.ToUpper();//dr[i]["Title"].ToString(); //Passenger Title Miss is invalid; 
                        upsd.Passengers[i].Names[0].MiddleName = "";
                        upsd.Passengers[i].Names[0].LastName = dr[i]["FName"].ToString().ToUpper();
                        upsd.Passengers[i].Names[0].Suffix = "";
                        upsd.Passengers[i].Names[0].Title = Title.ToUpper();//dr[i]["Title"].ToString();//"";
                    }
                    //ONLY MIDDLE NAME THERE
                    else if ((MNAME != "" && MNAME != null) && (LNAME == "" || LNAME == null))
                    {
                        upsd.Passengers[i].Names[0].FirstName = Title;//dr[i]["Title"].ToString();
                        upsd.Passengers[i].Names[0].MiddleName = dr[i]["FName"].ToString().ToUpper();
                        upsd.Passengers[i].Names[0].LastName = dr[i]["MName"].ToString().ToUpper();
                        upsd.Passengers[i].Names[0].Suffix = "";
                        upsd.Passengers[i].Names[0].Title = Title.ToUpper();//dr[i]["Title"].ToString();//"";
                    }
                    //ONLY LAST NAME THERE
                    else
                    {
                        upsd.Passengers[i].Names[0].FirstName = dr[i]["FName"].ToString().ToUpper();
                        upsd.Passengers[i].Names[0].MiddleName = dr[i]["MName"].ToString().ToUpper();
                        upsd.Passengers[i].Names[0].LastName = dr[i]["LName"].ToString().ToUpper();
                        upsd.Passengers[i].Names[0].Suffix = "";
                        upsd.Passengers[i].Names[0].Title = Title.ToUpper(); //dr[i]["Title"].ToString();
                    }
                    upsd.Passengers[i].PassengerID = 0;
                    upsd.Passengers[i].PassengerInfo = new PassengerInfo();
                    string AdtTit = dr[i]["Title"].ToString().Trim().ToUpper();
                    //if (AdtTit == "MR" || AdtTit == "MSTR")
                    //    upsd.Passengers[i].PassengerInfo.Gender = schemas.navitaire.com.WebServices.DataContracts.Common.Enumerations.Gender.Male;
                    //else
                    //    upsd.Passengers[i].PassengerInfo.Gender = schemas.navitaire.com.WebServices.DataContracts.Common.Enumerations.Gender.Female;

                    string AdtGnd = dr[i]["GENDER"].ToString().Trim().ToUpper();
                    if (AdtGnd == "M")
                        upsd.Passengers[i].PassengerInfo.Gender = navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Common.Enumerations.Gender.Male;
                    else
                        upsd.Passengers[i].PassengerInfo.Gender = navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Common.Enumerations.Gender.Female;

                    #region <PassengerTypeInfos>
                    upsd.Passengers[i].PassengerTypeInfos = new PassengerTypeInfo[1];
                    upsd.Passengers[i].PassengerTypeInfos[0] = new PassengerTypeInfo();
                    upsd.Passengers[i].PassengerTypeInfos[0].State = MessageState.New;
                    //upsd.Passengers[i].PassengerTypeInfos[0].DOB = DateTime.Parse(dr[i]["DOB"].ToString());
                    upsd.Passengers[i].PassengerTypeInfos[0].PaxType = "ADT";

                    #endregion

                    if (inf > 0)
                    {
                        upsd.Passengers[i].Infant = new PassengerInfant();
                        upsd.Passengers[i].Infant.State = MessageState.New;
                        upsd.Passengers[i].Infant.DOB = DateTime.Parse(Utility.Right(dinf[i]["DOB"].ToString(), 4) + "-" + Utility.Mid(dinf[i]["DOB"].ToString(), 3, 2) + "-" + Utility.Left(dinf[i]["DOB"].ToString(), 2));

                        string InfGnd = dinf[i]["GENDER"].ToString().Trim().ToUpper();
                        if (InfGnd == "M")
                            upsd.Passengers[i].Infant.Gender = navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Common.Enumerations.Gender.Male;
                        else
                            upsd.Passengers[i].Infant.Gender = navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Common.Enumerations.Gender.Female;

                        upsd.Passengers[i].Infant.Names = new BookingName[1];
                        upsd.Passengers[i].Infant.Names[0] = new BookingName();
                        upsd.Passengers[i].Infant.Names[0].State = MessageState.New;
                        upsd.Passengers[i].Infant.Names[0].FirstName = dinf[i]["FName"].ToString().ToUpper();
                        upsd.Passengers[i].Infant.Names[0].MiddleName = dinf[i]["MName"].ToString().ToUpper();
                        upsd.Passengers[i].Infant.Names[0].LastName = dinf[i]["LName"].ToString().ToUpper();
                        upsd.Passengers[i].Infant.Names[0].Title = "CHD";// dinf[i]["Title"].ToString();

                        //upsd.Passengers[i].PassengerInfants = new PassengerInfant[1];
                        //upsd.Passengers[i].PassengerInfants[0] = new PassengerInfant();
                        //upsd.Passengers[i].PassengerInfants[0].State = MessageState.New;
                        //upsd.Passengers[i].PassengerInfants[0].DOB = DateTime.Parse(Utility.Right(dinf[i]["DOB"].ToString(), 4) + "-" + Utility.Mid(dinf[i]["DOB"].ToString(), 3, 2) + "-" + Utility.Left(dinf[i]["DOB"].ToString(), 2));
                        //upsd.Passengers[i].PassengerInfants[0].Names = new BookingName[1];
                        //upsd.Passengers[i].PassengerInfants[0].Names[0] = new BookingName();
                        //upsd.Passengers[i].PassengerInfants[0].Names[0].State = MessageState.New;
                        //upsd.Passengers[i].PassengerInfants[0].Names[0].FirstName = dinf[i]["FName"].ToString();
                        //upsd.Passengers[i].PassengerInfants[0].Names[0].MiddleName = dinf[i]["MName"].ToString();
                        //upsd.Passengers[i].PassengerInfants[0].Names[0].LastName = dinf[i]["LName"].ToString();
                        //upsd.Passengers[i].PassengerInfants[0].Names[0].Title = dinf[i]["Title"].ToString();
                        inf--;
                    }
                    upsd.Passengers[i].PseudoPassenger = false;
                }
                #endregion

                #region Child
                for (int k = 0; k < obj.Child; k++)
                {
                    string MNAME = dchd[k]["MName"].ToString().ToUpper();
                    string LNAME = dchd[k]["LName"].ToString().ToUpper();

                    upsd.Passengers[adt + k] = new Passenger();
                    upsd.Passengers[adt + k].State = MessageState.New;
                    #region PassengerPrograms
                    upsd.Passengers[adt + k].PassengerPrograms = new PassengerProgram[1];
                    upsd.Passengers[adt + k].PassengerPrograms[0] = new PassengerProgram();
                    upsd.Passengers[adt + k].PassengerPrograms[0].State = MessageState.New;

                    #endregion

                    upsd.Passengers[adt + k].CustomerNumber = "";
                    upsd.Passengers[adt + k].PassengerNumber = (short)(obj.Adult + k);
                    upsd.Passengers[adt + k].FamilyNumber = 0;
                    upsd.Passengers[adt + k].PaxDiscountCode = "";

                    upsd.Passengers[adt + k].Names = new BookingName[1];
                    upsd.Passengers[adt + k].Names[0] = new BookingName();

                    //BOTH MIDDLE NAME AND LAST NAME NOT THERE
                    if ((MNAME == "" || MNAME == null) && (LNAME == "" || LNAME == null))
                    {
                        upsd.Passengers[adt + k].Names[0].FirstName = dchd[k]["Title"].ToString().ToUpper();
                        upsd.Passengers[adt + k].Names[0].MiddleName = "";
                        upsd.Passengers[adt + k].Names[0].LastName = dchd[k]["FName"].ToString().ToUpper();
                        upsd.Passengers[adt + k].Names[0].Suffix = "";
                        upsd.Passengers[adt + k].Names[0].Title = "CHD";//dchd[k]["Title"].ToString(); ;
                    }
                    //ONLY MIDDLE NAME THERE
                    else if ((MNAME != "" && MNAME != null) && (LNAME == "" || LNAME == null))
                    {
                        upsd.Passengers[adt + k].Names[0].FirstName = dchd[k]["Title"].ToString().ToUpper();
                        upsd.Passengers[adt + k].Names[0].MiddleName = dchd[k]["FName"].ToString().ToUpper();
                        upsd.Passengers[adt + k].Names[0].LastName = dchd[k]["MName"].ToString().ToUpper();
                        upsd.Passengers[adt + k].Names[0].Suffix = "";
                        upsd.Passengers[adt + k].Names[0].Title = "CHD";//dchd[k]["Title"].ToString();
                    }
                    //LAST NAME THERE
                    else
                    {
                        upsd.Passengers[adt + k].Names[0].FirstName = dchd[k]["FName"].ToString().ToUpper();
                        upsd.Passengers[adt + k].Names[0].MiddleName = dchd[k]["MName"].ToString().ToUpper();
                        upsd.Passengers[adt + k].Names[0].LastName = dchd[k]["LName"].ToString().ToUpper();
                        upsd.Passengers[adt + k].Names[0].Suffix = "";
                        upsd.Passengers[adt + k].Names[0].Title = "CHD";//dchd[k]["Title"].ToString();
                    }
                    upsd.Passengers[adt + k].PassengerID = 0;
                    upsd.Passengers[adt + k].PassengerInfo = new PassengerInfo();
                    string chdTit = dchd[k]["Title"].ToString().Trim().ToUpper();
                    if (chdTit == "MR" || chdTit == "MSTR")
                        upsd.Passengers[adt + k].PassengerInfo.Gender = navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Common.Enumerations.Gender.Male;
                    else
                        upsd.Passengers[adt + k].PassengerInfo.Gender = navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Common.Enumerations.Gender.Female;

                    #region <PassengerTypeInfos>
                    upsd.Passengers[adt + k].PassengerTypeInfos = new PassengerTypeInfo[1];
                    upsd.Passengers[adt + k].PassengerTypeInfos[0] = new PassengerTypeInfo();
                    upsd.Passengers[adt + k].PassengerTypeInfos[0].State = MessageState.New;
                    //upsd.Passengers[adt + k].PassengerTypeInfos[0].DOB = DateTime.Parse("2003-01-01");
                    upsd.Passengers[adt + k].PassengerTypeInfos[0].PaxType = "CHD";
                    #endregion

                    upsd.Passengers[adt + k].PseudoPassenger = false;
                }
                #endregion


                upsr.updatePassengersRequestData = upsd;

                Reqxml = SerializeAnObject(upsr, "UpdatePassenger-Req");
                updateres = bookingAPI.UpdatePassengers(upsr);
                Resxml = SerializeAnObject(updateres, "UpdatePassenger-Res");
                if (updateres.BookingUpdateResponseData.Success != null)
                    ret = updateres.BookingUpdateResponseData.Success.PNRAmount.TotalCost.ToString();
            }
            catch (Exception ex)
            {

                // 
                //xml.Add("APBREQ", "");
                //xml.Add("APBRES", "");
                //xml.Add("BC-REQ", "");
                //xml.Add("BC-RES", "");
                xml.Add("OTHER", ex.Message);
                Spice_Logout(signature);
            }
            finally
            {
                xml.Add("UPPAXREQ", Reqxml);
                xml.Add("UPPAXRES", Resxml);
            }
            return ret;
        }

        public string Spice_AddPaymentToBooking(string signature, string Amount, ref Dictionary<string, string> xml)
        {
            string Reqxml = "";
            string Resxml = "";

            string ret = "FAILURE";
            try
            {
                //IBookingManager bookingAPI = new BookingManagerClient();
                navitaire.SG.ver4.AddPaymentToBookingResponse res = new navitaire.SG.ver4.AddPaymentToBookingResponse();
                navitaire.SG.ver4.AddPaymentToBookingRequest apb = new navitaire.SG.ver4.AddPaymentToBookingRequest();

                apb.ContractVersion = ContractVers;
                apb.Signature = signature;
                apb.addPaymentToBookingReqData = new AddPaymentToBookingRequestData();
                apb.addPaymentToBookingReqData.MessageState = MessageState.New;
                apb.addPaymentToBookingReqData.WaiveFee = false;
                apb.addPaymentToBookingReqData.ReferenceType = PaymentReferenceType.Default;
                apb.addPaymentToBookingReqData.PaymentMethodType = RequestPaymentMethodType.AgencyAccount;
                apb.addPaymentToBookingReqData.PaymentMethodCode = "AG";
                apb.addPaymentToBookingReqData.QuotedCurrencyCode = "INR";
                apb.addPaymentToBookingReqData.QuotedAmount = Convert.ToDecimal(Amount);
                apb.addPaymentToBookingReqData.Status = BookingPaymentStatus.New;
                apb.addPaymentToBookingReqData.AccountNumberID = 0;
                apb.addPaymentToBookingReqData.AccountNumber = OrgCode;
                apb.addPaymentToBookingReqData.Expiration = DateTime.Parse("0001-01-01T00:00:00");
                apb.addPaymentToBookingReqData.ParentPaymentID = 0;
                //apb.addPaymentToBookingReqData.Installments = 0;
                if (ContractVers == 420)
                    apb.addPaymentToBookingReqData.Installments = 1;
                else
                    apb.addPaymentToBookingReqData.Installments = 0;

                apb.addPaymentToBookingReqData.Deposit = false;
                Reqxml = SerializeAnObject(apb, "AddPaymentToBooking-Req");
                res = bookingAPI.AddPaymentToBooking(apb);
                Resxml = SerializeAnObject(res, "AddPaymentToBooking-Res");
                //if (res.BookingPaymentResponse.ValidationPayment.Payment != null)
                //    ret = res.BookingPaymentResponse.ValidationPayment.Payment.PaymentAmount.ToString();
                if (res.BookingPaymentResponse.ValidationPayment.Payment != null)
                {
                    if (res.BookingPaymentResponse.ValidationPayment.PaymentValidationErrors.Any() == false)
                    {
                        ret = res.BookingPaymentResponse.ValidationPayment.Payment.PaymentAmount.ToString();
                    }
                    else
                    {

                        xml.Add("OTHER", res.BookingPaymentResponse.ValidationPayment.PaymentValidationErrors[0].ErrorDescription.ToString());
                        xml.Add("BC-REQ", "");
                        xml.Add("BC-RES", "");
                    }
                }

            }
            catch (Exception ex)
            {
                Spice_Logout(signature);
                //xml.Add("BC-REQ", "");
                //xml.Add("BC-RES", "");
                xml.Add("OTHER", ex.Message);
            }
            finally
            {
                xml.Add("APBREQ", Reqxml);
                xml.Add("APBRES", Resxml);
            }
            return ret;
        }

        //public string Spice_BookingCommit(string signature, FlightSearch obj, DataTable dt, BookingResources objBkgRes, ref Dictionary<string, string> xml, string RecordLocator = "")
        public string Spice_BookingCommit(string signature, FlightSearch obj, DataTable dt, Hashtable Cust, ref Dictionary<string, string> xml, string RecordLocator = "")
        {
            string Reqxml = "";
            string Resxml = "";
            string Other = "";
            string retval = "FAILURE";
            try
            {
                //IBookingManager bookingAPI = new BookingManagerClient();
                navitaire.SG.ver4.BookingCommitRequest request = new navitaire.SG.ver4.BookingCommitRequest();
                request.ContractVersion = ContractVers;
                request.Signature = signature;
                //DataRow[] dr = dt.Select("PaxType = 'ADT'");
                DataRow[] dr;
                if (obj.Adult == 0 && obj.Child > 0)
                    dr = dt.Select("PaxType = 'CHD'");
                else
                    dr = dt.Select("PaxType = 'ADT'");
                string MNAME = dr[0]["MName"].ToString().ToUpper();
                string LNAME = dr[0]["LName"].ToString().ToUpper();
                #region <BookingCommitRequestData>
                BookingCommitRequestData requestData = new BookingCommitRequestData();
                Booking booking = new Booking();
                requestData.State = MessageState.New;
                if (!String.IsNullOrEmpty(RecordLocator))
                {
                    requestData.RecordLocator = RecordLocator.Trim();
                }
                requestData.CurrencyCode = "INR";
                requestData.PaxCount = (short)(obj.Adult + obj.Child);
                requestData.BookingID = 0;
                requestData.DistributeToContacts = false;
                requestData.BookingParentID = 0;
                requestData.SourcePOS = new PointOfSale();
                requestData.SourcePOS.State = MessageState.New;
                //requestData.SourcePOS.AgentCode = username;
                if (obj.HidTxtAirLine.ToUpper().Trim() == "6E") requestData.SourcePOS.AgentCode = "AG";
                else requestData.SourcePOS.AgentCode = username;
                requestData.SourcePOS.OrganizationCode = OrgCode;
                requestData.SourcePOS.DomainCode = domain;
                requestData.SourcePOS.LocationCode = domain;

                requestData.BookingContacts = new BookingContact[1];
                requestData.BookingContacts[0] = new BookingContact();
                requestData.BookingContacts[0].CompanyName = Cust["sAddName"].ToString();//objBkgRes.sAddName;// Cust["sAddName"].ToString();//
                requestData.BookingContacts[0].Names = new BookingName[1];
                requestData.BookingContacts[0].Names[0] = new BookingName();

                string Title = Convert.ToString(dr[0]["Title"]).ToUpper();
                if (Title == "MISS")
                {
                    Title = "Ms"; // Passenger Title Miss is invalid; 
                }
                //BOTH MIDDLE NAME AND LAST NAME NOT THERE
                if ((MNAME == "" || MNAME == null) && (LNAME == "" || LNAME == null))
                {
                    requestData.BookingContacts[0].Names[0].FirstName = Title.ToUpper();//dr[0]["Title"].ToString();
                    requestData.BookingContacts[0].Names[0].LastName = dr[0]["FName"].ToString().ToUpper();
                    requestData.BookingContacts[0].Names[0].Title = Title.ToUpper();//dr[0]["Title"].ToString();
                }
                //ONLY LAST NAME NOT THERE
                else if ((MNAME != "" && MNAME != null) && (LNAME == "" || LNAME == null))
                {
                    requestData.BookingContacts[0].Names[0].FirstName = dr[0]["FName"].ToString().ToUpper();
                    requestData.BookingContacts[0].Names[0].LastName = dr[0]["MName"].ToString().ToUpper();
                    requestData.BookingContacts[0].Names[0].Title = Title.ToUpper();//dr[0]["Title"].ToString();
                }
                //LAST NAME THERE
                else
                {
                    requestData.BookingContacts[0].Names[0].FirstName = dr[0]["FName"].ToString().ToUpper();
                    requestData.BookingContacts[0].Names[0].LastName = dr[0]["LName"].ToString().ToUpper();
                    requestData.BookingContacts[0].Names[0].Title = Title.ToUpper();//dr[0]["Title"].ToString();
                }
                requestData.BookingContacts[0].AddressLine1 = Cust["sLine1"].ToString(); //objBkgRes.sAddLine1;// Cust["sLine1"].ToString() + " " + Cust["sLine2"].ToString();
                requestData.BookingContacts[0].AddressLine2 = Cust["sLine2"].ToString();// Cust["sLine2"].ToString();
                requestData.BookingContacts[0].City = Cust["sCity"].ToString();//objBkgRes.sCity;//  Cust["sCity"].ToString();// "Salt Lake City";
                requestData.BookingContacts[0].ProvinceState = Cust["sState"].ToString().Substring(0, 3).ToUpper();//objBkgRes.sState.Substring(0, 3).ToUpper();//  Cust["sState"].ToString().Substring(0, 3).ToUpper();//"UT";
                requestData.BookingContacts[0].CountryCode = Cust["sCountry"].ToString().Substring(0, 2).ToUpper();//objBkgRes.sCountry.Substring(0, 2).ToUpper();// Cust["sCountry"].ToString().Substring(0, 2).ToUpper();//"US";
                requestData.BookingContacts[0].EmailAddress = Cust["Customeremail"].ToString();//"07seaz@gmail.com";//dr[0]["Email"].ToString();//Cust["Customeremail"].ToString();//"manish.netbug@gmail.com";
                requestData.BookingContacts[0].DistributionOption = DistributionOption.Email;
                if (obj.HidTxtAirLine.ToUpper().Trim() == "SG")
                    requestData.BookingContacts[0].HomePhone = "+91" + Cust["sHomePhn"].ToString();//dr[0]["Mobile"].ToString(); //Cust["sHomePhn"].ToString();//"888777666";
                else
                    requestData.BookingContacts[0].HomePhone = Cust["sHomePhn"].ToString();//dr[0]["Mobile"].ToString();
                requestData.BookingContacts[0].PostalCode = Cust["sZip"].ToString();//objBkgRes.sZip;//Cust["sZip"].ToString();//"84844";             

                requestData.BookingContacts[0].NotificationPreference = NotificationPreference.None;
                requestData.BookingContacts[0].TypeCode = "P";
                requestData.BookingContacts[0].CultureCode = "en-GB";
                requestData.BookingContacts[0].SourceOrganization = OrgCode;

                #endregion

                ///*
                //* Payments are not supported using booking commit the recommended
                //* practice is to add payments using the AddPaymentToBooking method.
                //* */
                request.BookingCommitRequestData = requestData;
                navitaire.SG.ver4.BookingCommitResponse response = null;
                Reqxml = SerializeAnObject(request, "BookingUpdate-Req");
                response = bookingAPI.BookingCommit(request);
                Resxml = SerializeAnObject(response, "BookingUpdate-Res");
                if (response.BookingUpdateResponseData != null)
                    retval = response.BookingUpdateResponseData.Success.RecordLocator.ToString();
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_BookingCommit)", "Error_003", ex, "Spice_BookingCommit");
                Other = ex.Message;
            }
            finally
            {                
                try
                {
                    xml.Add("BC-REQ", Reqxml);
                    xml.Add("BC-RES", Resxml);
                    xml.Add("OTHER", Other);
                    Spice_Logout(signature);
                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("SGNAV4(Spice_BookingCommit)", "Error_004", ex, "Spice_BookingCommit");
                }
            }
            return retval;
        }

        #endregion



        #region Utility

        public string SerializeAnObject(object obj, string Res)
        {
            string newFileName = Res + "- " + Departure + "- " + Arrival + "-" + AgentID + "-" + DateTime.Now.ToString().Trim().Replace("/", "-") + "-" + username;//
            string xml = "";


            //string activeDir = @"C:\Req_Res_Spice\" + DateTime.Now.ToString("dd-MMMM-yyyy") + "/";//
            //DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);//
            //if (!Directory.Exists(objDirectoryInfo.FullName))//
            //{
            //    Directory.CreateDirectory(activeDir);//
            //}//



            XmlDocument doc = new XmlDocument();
            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            try
            {
                serializer.Serialize(stream, obj);
                stream.Position = 0;
                doc.Load(stream);
                //if (AgentID != "")//
                // doc.Save(activeDir + newFileName.Replace(":", "") + ".xml");//


                xml = doc.InnerXml.ToString();
            }
            catch (Exception ex)
            {
                // File.AppendAllText("D:\\\\CCPG\\\\Error" + System.DateTime.Now.Date.ToString("ddMMyyyy") + ".txt", ex.StackTrace.ToString() + ex.Message + Environment.NewLine);
            }

            finally
            {
                stream.Close();
                stream.Dispose();
            }
            return xml;
        }


        private string SetSearchDate(string travelDate, string trip)
        {
            try
            {
                if (Convert.ToDateTime(travelDate) == DateTime.Now.Date)
                {
                    string hr = "00";
                    string mm = "00";
                    int chktime = 0;
                    if (trip == "D")
                        chktime = 22;
                    else
                        chktime = 20;
                    if (DateTime.Now.Hour >= chktime & DateTime.Now.Minute >= 0)
                    {
                        hr = "23";
                        mm = "59";
                    }
                    else
                    {
                        if (trip == "D")
                        {
                            if ((DateTime.Now.Hour + 2).ToString().Length > 1)
                                hr = (DateTime.Now.Hour + 2).ToString();
                            else
                                hr = "0" + (DateTime.Now.Hour + 2).ToString();
                        }
                        else
                        {
                            if ((DateTime.Now.Hour + 4).ToString().Length > 1)
                                hr = (DateTime.Now.Hour + 4).ToString();
                            else
                                hr = "0" + (DateTime.Now.Hour + 4).ToString();
                        }
                        if (DateTime.Now.Minute.ToString().Length > 1)
                            mm = DateTime.Now.Minute.ToString();
                        else
                            mm = "0" + DateTime.Now.Minute.ToString();
                    }
                    travelDate = travelDate + "T" + hr + ":" + mm + ":00";
                }
                else
                {
                    travelDate = travelDate + "T00:00:00";
                }
            }
            catch (Exception ex)
            {
                travelDate = travelDate + "T00:00:00";
            }
            return travelDate;
        }

        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;            
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                //STHT.Add("Tds", Math.Round(((originalDis * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }
        #endregion

        
        private static string Check_Via_Connecting1(DataTable Dt, string Flight, string VC)
        {
            // Check if Multiple FlightID's Exsists for Particualr Flight (1,2)
            string FTYPE = "";
            string Arrival = "";

            try
            {
                dynamic dt1 = Dt.Select("Flight='" + Flight + "'", "");
                string FID = dt1[0]["FlightIdentification"].ToString();

                if (dt1.Length > 1)
                {
                    for (int jj = 1; jj <= dt1.Length - 1; jj++)
                    {
                        if ((FID == dt1[jj]["FlightIdentification"].ToString()))
                        {
                            FTYPE = "Via";
                        }
                        else
                        {
                            FTYPE = "Con";
                        }

                    }

                }
                else
                {
                    Arrival = dt1[0]["ArrivalLocation"].ToString();
                }
                if ((!string.IsNullOrEmpty((FTYPE))))
                {
                    if (FTYPE == "Via" && VC == "6E")
                    {
                        Arrival = dt1[0]["ArrivalLocation"].ToString();
                    }
                    else
                    {
                        //Arrival = dt1(dt1.Length - 1)("ArrivalLocation").ToString()
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return Arrival;
        }
        #region Seat
        public static List<SeatMapFinal> SeatBooking(string orderId, string TCCode, string[] FT, string PROMOCODE)
        {
            List<SeatMapFinal> SeatMapFinal_ALL = new List<SeatMapFinal>();

            string ConStr = System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            List<CredentialList> CrdList;
            DataTable FltDT;
            DataTable PaxDT;
            DataTable MealBagDT;
            FlightCommonBAL objFleSBal = new FlightCommonBAL(ConStr);
            // FlightSearchBAL objFleSBal = new FlightSearchBAL();
            Credentials objCrd = new Credentials(ConStr);
            BookingResources objBkgRes = new BookingResources(TCCode);
            Dictionary<string, string> Xml = new Dictionary<string, string>();
            string SSRPRICE = "";
            string Signature = "";
            int Trip = 1;
            decimal Diff = 0;
            //decimal totMBPrice = 0;

            try
            {

                DataSet ds = objFleSBal.GetAllFlightDetailsByOrderId(orderId);
                FltDT = ds.Tables[0];
                PaxDT = ds.Tables[1];
                MealBagDT = ds.Tables.Count > 2 ? ds.Tables[2] : new DataTable();

                //if (MealBagDT.Rows.Count > 0)
                //{

                string vc = Convert.ToString(FltDT.Rows[0]["ValiDatingCarrier"]);
                decimal OriginalTF = Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"].ToString());
                decimal InfFare = Convert.ToDecimal(FltDT.Rows[0]["InfFare"].ToString());
                string strTrip = Convert.ToString(FltDT.Rows[0]["Trip"]);
                string idType = Convert.ToString(FltDT.Rows[0]["TripCnt"]);
                string Provider = Convert.ToString(FltDT.Rows[0]["Provider"]);
                string CrdType = Convert.ToString(FltDT.Rows[0]["CrdType"]);
                CrdList = objCrd.GetGALBookingCredentialsSeat(Provider, strTrip, vc, CrdType);


                string Org = "";
                string Dest = "";
                FlightSearch objInputs = new FlightSearch();
                if (FltDT.Rows[FltDT.Rows.Count - 1]["TripType"].ToString() == "R")
                    objInputs.TripType =Shared.TripType.R;
                else
                    objInputs.TripType = Shared.TripType.O;
                if (FltDT.Rows[0]["Trip"].ToString() == "D")
                    objInputs.Trip = Shared.Trip.D;
                else
                    objInputs.Trip = Shared.Trip.I;
                objInputs.Adult = Convert.ToInt16(FltDT.Rows[0]["Adult"].ToString());
                objInputs.Child = Convert.ToInt16(FltDT.Rows[0]["Child"].ToString());
                objInputs.Infant = Convert.ToInt16(FltDT.Rows[0]["Infant"].ToString());
                objInputs.HidTxtAirLine = vc;
                int inx = 0;
                if ((objInputs.TripType == Shared.TripType.R))
                {
                    inx = 1;
                    Trip = 2;
                }
                ArrayList seginfo = new ArrayList();

                string FNO = "";
                string[] JSK = new string[inx + 1];
                string[] FSK = new string[inx + 1];
                string[] ViaArr = new string[inx + 1];
                //CC(inx), FNO(inx), DD(inx) 

                dynamic dt = FltDT.DefaultView.ToTable(true, "FlightIdentification");
                //Sorted By FNo
                for (int jj = 0; jj <= dt.Rows.Count - 1; jj++)
                {
                    dynamic dt1 = FltDT.Select("FlightIdentification='" + dt.Rows[jj]["FlightIdentification"] + "'", "");
                    FNO = dt1[0]["FlightIdentification"].Trim();
                    Dictionary<string, string> Seg = new Dictionary<string, string>();
                    Seg.Add("FNO", FNO);
                    Seg.Add("STD", dt1[0]["depdatelcc"]);
                    Seg.Add("Departure", dt1[0]["DepartureLocation"]);
                    Seg.Add("Arrival", dt1[dt1.Length - 1]["ArrivalLocation"]);
                    Seg.Add("Flight", dt1[0]["Flight"]);
                    Seg.Add("VC", vc.ToUpper());
                    seginfo.Add(Seg);
                }


                for (int ii = 0; ii <= FltDT.Rows.Count - 1; ii++)
                {
                    if ((ii == 0))
                    {
                        Dictionary<string, string> Seg = new Dictionary<string, string>();
                        Org = FltDT.Rows[ii]["OrgDestFrom"].ToString();
                        Dest = FltDT.Rows[ii]["OrgDestTo"].ToString();
                        OriginalTF = Convert.ToDecimal(FltDT.Rows[ii]["OriginalTF"].ToString());
                    }
                    if ((Org == FltDT.Rows[ii]["OrgDestFrom"].ToString()))
                    {
                        JSK[0] = FltDT.Rows[ii]["sno"].ToString();
                        FSK[0] = FltDT.Rows[ii]["Searchvalue"].ToString();
                        ViaArr[0] = Check_Via_Connecting1(FltDT, FltDT.Rows[ii]["Flight"].ToString(), vc);
                    }
                    else if ((Org == FltDT.Rows[ii]["OrgDestTo"].ToString()))
                    {
                        JSK[1] = FltDT.Rows[ii]["sno"].ToString();
                        FSK[1] = FltDT.Rows[ii]["Searchvalue"].ToString();
                        ViaArr[1] = Check_Via_Connecting1(FltDT, FltDT.Rows[ii]["Flight"].ToString(), vc);
                    }
                }


                if ((objInputs.Infant > 0))
                {
                    objInputs.Infant = 0;
                    // Set Infant to 0
                }

                if ((vc == "SG"))
                {
                    string ServiceBundleCode = "";
                    ServiceBundleCode = objFleSBal.GetServiceBundleCode(orderId);
                    SGNAV4 objSG = new SGNAV4(CrdList[0].UserID, CrdList[0].Password, CrdList[0].AvailabilityURL, ConStr, CrdList[0].CorporateID, objInputs.HidTxtDepCity ?? "", objInputs.HidTxtArrCity ?? "", TCCode, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager",
                 CrdList[0].CarrierAcc, "0", int.Parse("0"), "SpiceJet");
                    Signature = objSG.Spice_Login();
                    objSG.Spice_SellJourneyByKey(Signature, objInputs, JSK, FSK, ref Xml, FT, PROMOCODE, ServiceBundleCode);
                    for (int jj = 0; jj <= dt.Rows.Count - 1; jj++)
                    {
                        List<SeatMap> SeatMaping = new List<SeatMap>();
                        dynamic dt1 = FltDT.Select("FlightIdentification='" + dt.Rows[jj]["FlightIdentification"] + "'", "");
                        SeatMaping = objSG.GetSeatAvailability_Response_SG(Signature, dt1[0]["depdatelcc"], dt1[0]["DepartureLocation"], dt1[dt1.Length - 1]["ArrivalLocation"], dt1[0]["FlightIdentification"].Trim(), vc.ToUpper());
                        SeatMapFinal SMFinal = new SeatMapFinal();
                        SMFinal.SeatMapDetails_Final = SeatMaping;
                        SMFinal.Rows = SeatMaping[0].Rows;
                        SMFinal.Columns = SeatMaping[0].Columns;
                        SeatMapFinal_ALL.Add(SMFinal);
                    }
                    objSG.Spice_Logout(Signature);

                }


            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("SGNAV4(SELL_SSR)", "Error_008", ex, "Seat");

            }
            return SeatMapFinal_ALL;

        }
        public string SeatBookingFinal(string signature, FlightSearch obj, DataTable Pax, List<Seat> SeatList, ref Dictionary<string, string> xml)
        {
            string MealVal = "FAILURE", Reqxml = "", Resxml = "";

            try
            {
                navitaire.SG.ver4.AssignSeatsRequest ASRequest = new navitaire.SG.ver4.AssignSeatsRequest();
                navitaire.SG.ver4.AssignSeatsResponse ASResponse = new navitaire.SG.ver4.AssignSeatsResponse();

                ASRequest.Signature = signature;
                ASRequest.ContractVersion = ContractVers;

                DataRow[] PxDt = new DataRow[obj.Adult + obj.Child];
                PxDt = Pax.Select("PaxType = 'ADT' OR PaxType ='CHD'", "PaxId ASC");
                //SegmentSeatRequest SSR = new SegmentSeatRequest();
                SeatSellRequest SSR = new SeatSellRequest();
                SSR.SegmentSeatRequests = new SegmentSeatRequest[SeatList.Count];
                int paxno = 0;
                int Seatno = 0;
                foreach (DataRow Px in PxDt)
                {
                    List<Seat> SeatL = new List<Seat>();
                    SeatL = SeatList.Where(x => x.PaxId == Convert.ToInt32(Px["PaxId"].ToString())).ToList();
                    for (int i = 0; i < SeatL.Count; i++)
                    {
                        SSR.SegmentSeatRequests[Seatno] = new SegmentSeatRequest();
                        SSR.SegmentSeatRequests[Seatno].FlightDesignator = new FlightDesignator();
                        SSR.SegmentSeatRequests[Seatno].FlightDesignator.CarrierCode = Pax.Rows[0]["GSTVC"].ToString().Trim().ToUpper();
                        SSR.SegmentSeatRequests[Seatno].FlightDesignator.FlightNumber = SeatL[i].FlightNumber;
                        SSR.SegmentSeatRequests[Seatno].FlightDesignator.OpSuffix = null;
                        SSR.SegmentSeatRequests[Seatno].STD = Convert.ToDateTime(SeatL[i].FlightTime);
                        SSR.SegmentSeatRequests[Seatno].DepartureStation = SeatL[i].Origin;
                        SSR.SegmentSeatRequests[Seatno].ArrivalStation = SeatL[i].Destination;
                        SSR.SegmentSeatRequests[Seatno].UnitDesignator = SeatL[i].SeatDesignator;
                        //short[]  SSR.SegmentSeatRequests[i].PassengerNumbers 
                        short[] abc = new short[1];
                        abc[0] = (short)(paxno);
                        SSR.SegmentSeatRequests[Seatno].PassengerNumbers = abc;
                        SSR.SegmentSeatRequests[Seatno].CompartmentDesignator = null;
                        SSR.SegmentSeatRequests[Seatno].PassengerSeatPreferences = null;
                        long[] id = new long[1];
                        id[0] = paxno;
                        SSR.SegmentSeatRequests[Seatno].PassengerIDs = id;
                        Seatno = Seatno + 1;
                    }
                    paxno = paxno + 1;
                }
                ASRequest.SellSeatRequest = SSR;
                Reqxml = SerializeAnObject(ASRequest, "UpdateSeat-Req");
                ASResponse = bookingAPI.AssignSeats(ASRequest);
                Resxml = SerializeAnObject(ASResponse, "UpdateSeat-Res");
                if (ASResponse.BookingUpdateResponseData.Success != null)
                {
                    MealVal = ASResponse.BookingUpdateResponseData.Success.PNRAmount.TotalCost.ToString();
                }


            }
            catch (Exception ex)
            {
                //xml.Add("SSR", "");
                //xml.Add("UPPAXREQ", "");
                //xml.Add("UPPAXRES", "");
                //xml.Add("STATEREQ", "");
                //xml.Add("STATERES", "");
                //xml.Add("APBREQ", "");
                //xml.Add("APBRES", "");
                //xml.Add("BC-REQ", "");
                //xml.Add("BC-RES", "");
                xml.Add("OTHER", ex.Message);
                Spice_Logout(signature);
                ExecptionLogger.FileHandling("SPICE_NEWSKIES(SeatBookingFinal)", "Error_003", ex, "Seat");
            }
            finally
            {
                xml.Add("SBREQ", Reqxml);
                xml.Add("SBRES", Resxml);
            }

            return MealVal;
        }
        public List<SeatMap> GetSeatAvailability_Response_SG(string signature, string STD, string DepartureStation, string ArrivalStation, string FlightNumber, string CarrierCode)
        {
            string Reqxml = "";
            string Resxml = "";
            List<SeatMap> SeatMapFinal = new List<SeatMap>();
            try
            {
                navitaire.SG.ver4.GetSeatAvailabilityResponse seatavlres = new navitaire.SG.ver4.GetSeatAvailabilityResponse();
                //Indicate that the we are selling a journey
                navitaire.SG.ver4.GetSeatAvailabilityRequest seatavlrequest = new navitaire.SG.ver4.GetSeatAvailabilityRequest();
                seatavlrequest.Signature = signature;
                seatavlrequest.ContractVersion = ContractVers;
                //sellrequest.updateContactsRequestData = new UpdateContactsRequestData();
                //sellrequest.updateContactsRequestData.BookingContactList = new BookingContact[1];
                SeatAvailabilityRequest SAR = new SeatAvailabilityRequest();
                SAR.STD = Convert.ToDateTime(STD);
                SAR.DepartureStation = DepartureStation;
                SAR.ArrivalStation = ArrivalStation;
                SAR.IncludeSeatFees = true;
                SAR.SeatAssignmentMode = SeatAssignmentMode.PreSeatAssignment;
                SAR.FlightNumber = FlightNumber.Length == 3 ? " " + FlightNumber : FlightNumber.Length == 2 ? "  " + FlightNumber : FlightNumber;
                SAR.CarrierCode = CarrierCode;
                SAR.CompressProperties = false;
                SAR.EnforceSeatGroupRestrictions = false;
                //sellrequest.updateContactsRequestData.BookingContactList[0] = BContact;
                seatavlrequest.SeatAvailabilityRequest = SAR;
                Reqxml = SerializeAnObject(seatavlrequest, "Seat-Req");
                seatavlres = bookingAPI.GetSeatAvailability(seatavlrequest);
                Resxml = SerializeAnObject(seatavlres, "Seat-Res");

                string FlightNo = FlightNumber.Length == 3 ? " " + FlightNumber : FlightNumber.Length == 2 ? "  " + FlightNumber : FlightNumber;
                SeatMap SeatMap = new SeatMap();
                // SeatMapDetails SeatMap = new SeatMapDetails();
                List<SeatMapDetails> SeatMapList = new List<SeatMapDetails>();
                //string Blocked = "1A#4C#5D#28A#28B#28E#28F#29A#29B#29E#29F#30A#30B#30C#30D#30E#30F";
                string Blocked = "";
                //Cabin = Cabin == "BUSINESS" ? "C" : "Y";
                foreach (var K in seatavlres.SeatAvailabilityResponse.EquipmentInfos)
                {
                    SeatMapDetails SMD = new SeatMapDetails();
                    int Rows = 0, Column = 0;
                    SeatMap.EquipmentType = K.EquipmentType;
                    SeatMap.EquipmentTypeSuffix = K.EquipmentTypeSuffix;
                    if (K.EquipmentType.ToUpper() == "737" && K.EquipmentTypeSuffix.ToUpper() == "186")
                    {
                        Rows = 32;
                        Column = 7;
                        SeatMap.Rows = 32;
                        SeatMap.Columns = 7;
                    }
                    else if (K.EquipmentType.ToUpper() == "737" && K.EquipmentTypeSuffix.ToUpper() == "189")
                    {
                        Rows = 32;
                        Column = 7;
                        SeatMap.Rows = 32;
                        SeatMap.Columns = 7;
                    }
                    else if (K.EquipmentType.ToUpper() == "737" && K.EquipmentTypeSuffix.ToUpper() == "212")
                    {
                        Rows = 36;
                        Column = 7;
                        SeatMap.Rows = 36;
                        SeatMap.Columns = 7;
                    }
                    else if (K.EquipmentType.ToUpper() == "737" && K.EquipmentTypeSuffix.ToUpper() == "700")
                    {
                        Rows = 26;
                        Column = 7;
                        SeatMap.Rows = 26;
                        SeatMap.Columns = 7;
                    }
                    else if (K.EquipmentType.ToUpper() == "737" && K.EquipmentTypeSuffix.ToUpper() == "800")
                    {
                        Rows = 31;
                        Column = 7;
                        SeatMap.Rows = 31;
                        SeatMap.Columns = 7;
                    }
                    else if (K.EquipmentType.ToUpper() == "TJO" && K.EquipmentTypeSuffix.ToUpper() == "800")
                    {
                        Rows = 33;
                        Column = 7;
                        SeatMap.Rows = 33;
                        SeatMap.Columns = 7;
                    }
                    else if (K.EquipmentType.ToUpper() == "SGL" && K.EquipmentTypeSuffix.ToUpper() == "189")
                    {
                        Rows = 32;
                        Column = 7;
                        SeatMap.Rows = 32;
                        SeatMap.Columns = 7;
                    }
                    else if (K.EquipmentType.ToUpper() == "DH8")
                    {
                        Rows = 20;
                        Column = 5;
                        SeatMap.Rows = 20;
                        SeatMap.Columns = 5;
                    }
                    else if (K.EquipmentType.ToUpper() == "SUQ")
                    {
                        Rows = 20;
                        Column = 5;
                        SeatMap.Rows = 20;
                        SeatMap.Columns = 5;
                        //  EmergencyRow = "1A1B1C1D2A2B2C2D";
                    }

                    foreach (var P in K.Compartments)
                    {
                        //P.
                        List<SeatList> SeatListDetails = new List<SeatList>();
                        var Seats = P.Seats.Where(x => x.SeatAvailability.ToString().ToUpper() == "OPEN");

                        for (int i = 1; i < Rows + 1; i++)
                        {
                            //var SeatAlignment = (dynamic)null;
                            var SeatAlignment = Seats.Where(a => Utility.RemoveAlpha(a.SeatDesignator) == i.ToString());
                            for (int k = 1; k < Column + 1; k++)
                            {
                                string SeatAlpha = "", Seatside = "";
                                if (Column == 5)
                                {
                                    SeatAlpha = k == 1 ? "A" : k == 2 ? "B" : k == 4 ? "C" : k == 5 ? "D" : "";
                                    Seatside = k == 1 ? "Window" : k == 2 ? "Aisle" : k == 4 ? "Aisle" : k == 5 ? "Window" : "";
                                }
                                else
                                {
                                    SeatAlpha = k == 1 ? "A" : k == 2 ? "B" : k == 3 ? "C" : k == 5 ? "D" : k == 6 ? "E" : k == 7 ? "F" : "";
                                    Seatside = k == 1 ? "Window" : k == 2 ? "Middle" : k == 3 ? "Aisle" : k == 5 ? "Aisle" : k == 6 ? "Middle" : k == 7 ? "Window" : "";
                                }
                                var SeatAlignmentA = SeatAlignment.Where(a => a.SeatDesignator == i.ToString() + SeatAlpha);
                                #region Seat List
                                if (SeatAlpha == "" && (k == 4 || k == 3))
                                {
                                    SeatList SeatList = new SeatList();
                                    SeatList.RowNo = i;
                                    SeatList.ColumnNo = k;
                                    SeatList.Assignable = false;
                                    SeatList.SeatSet = 0;
                                    SeatList.SeatAvailability = "NA";
                                    SeatList.SeatDesignator = i.ToString() + SeatAlpha;
                                    SeatList.SeatType = "NA";
                                    SeatList.TravelClassCode = "NA";
                                    SeatList.SeatGroup = 1000;
                                    SeatList.SeatAngle = 0;
                                    SeatList.PremiumSeatIndicator = false;
                                    SeatList.SeatStatus = "BLANK";
                                    SeatList.ExitSeats = "NA";
                                    SeatList.Message = "";//(i.ToString() == "11" || i.ToString() == "12" || i.ToString() == "30") ? "You have selected non redining seat" : "";
                                    SeatList.SeatAlignment = Seatside;
                                    SeatList.FlightNumber = FlightNo;
                                    SeatList.FlightTime = STD;
                                    SeatListDetails.Add(SeatList);
                                }
                                else if (SeatAlpha != "" && SeatAlignmentA.Count() > 0)
                                {

                                    foreach (var assign in SeatAlignmentA)
                                    {
                                        SeatList SeatList = new SeatList();
                                        SeatList.RowNo = i;
                                        SeatList.ColumnNo = k;
                                        SeatList.Assignable = assign.Assignable;
                                        SeatList.SeatSet = assign.SeatSet;
                                        SeatList.SeatAvailability = assign.SeatAvailability.ToString();
                                        SeatList.SeatDesignator = assign.SeatDesignator;
                                        SeatList.SeatType = assign.SeatType;
                                        SeatList.TravelClassCode = assign.TravelClassCode;
                                        SeatList.SeatGroup = assign.SeatGroup;
                                        SeatList.SeatAngle = assign.SeatAngle;
                                        SeatList.PremiumSeatIndicator = assign.PremiumSeatIndicator;
                                        if (Blocked.Contains(assign.SeatDesignator) == true)
                                            SeatList.SeatStatus = "OCCUPIED";
                                        else
                                            SeatList.SeatStatus = "OPEN";
                                        SeatList.ExitSeats = GetExitRow(SeatMap.EquipmentType, SeatMap.EquipmentTypeSuffix, assign.SeatDesignator);
                                        SeatList.Message = "";// (i.ToString() == "11" || i.ToString() == "12" || i.ToString() == "30") ? "You have selected non redining seat" : "";
                                        SeatList.SeatAlignment = Seatside;
                                        SeatList.FlightNumber = FlightNo;
                                        SeatList.FlightTime = STD;
                                        SeatListDetails.Add(SeatList);
                                    }

                                }
                                else
                                {
                                    SeatList SeatList = new SeatList();
                                    SeatList.RowNo = i;
                                    SeatList.ColumnNo = k;
                                    SeatList.Assignable = false;
                                    SeatList.SeatSet = 0;
                                    SeatList.SeatAvailability = SeatAvailability.Unknown.ToString();
                                    SeatList.SeatDesignator = i.ToString() + SeatAlpha;
                                    SeatList.SeatType = "NA";
                                    SeatList.TravelClassCode = "NA";
                                    SeatList.SeatGroup = 1000;
                                    SeatList.SeatAngle = 0;
                                    SeatList.PremiumSeatIndicator = false;
                                    SeatList.SeatStatus = "OCCUPIED";
                                    SeatList.Message = "";// (i.ToString() == "11" || i.ToString() == "12" || i.ToString() == "30") ? "You have selected non redining seat" : "";
                                    SeatList.ExitSeats = GetExitRow(SeatMap.EquipmentType, SeatMap.EquipmentTypeSuffix, i.ToString() + SeatAlpha);// i.ToString() + SeatAlpha == "12A" ? "EXIT" : i.ToString() + SeatAlpha == "13A" ? "EXIT" : i.ToString() + SeatAlpha == "12F" ? "EXIT" : i.ToString() + SeatAlpha == "13F" ? "EXIT" : "NA";
                                    SeatList.SeatAlignment = Seatside;
                                    SeatList.FlightNumber = FlightNo;
                                    SeatList.FlightTime = STD;
                                    SeatListDetails.Add(SeatList);
                                }
                                #endregion

                            }
                        }

                        SMD.Deck = P.Deck;
                        SMD.AvailableUnits = P.AvailableUnits;
                        SMD.SeatListDetails = SeatListDetails;
                        SeatMapList.Add(SMD);
                    }
                    SeatMap.Aircraft = K.Name;
                    SeatMap.ArrivalStation = K.ArrivalStation;
                    SeatMap.DepartureStation = K.DepartureStation;
                    SeatMap.FlightNumber = FlightNo;
                    SeatMap.FlightTime = STD;
                    SeatMap.EquipmentType = K.EquipmentType;
                    SeatMap.EquipmentTypeSuffix = K.EquipmentTypeSuffix;
                    SeatMap.EquipmentCategory = K.EquipmentCategory.ToString();
                    SeatMap.SeatMapDetails = SeatMapList;
                    SeatMapFinal.Add(SeatMap);
                }
                #region Price
                foreach (var Prc in seatavlres.SeatAvailabilityResponse.SeatGroupPassengerFees)
                {
                    string FeeCode = "", FeeType = "", FlightReference = "";
                    short FeeGroup;
                    decimal SeatFee = 0, CGST = 0, SGST = 0, IGST = 0, UGST = 0;
                    FeeGroup = Prc.SeatGroup;
                    FeeCode = Prc.PassengerFee.FeeCode;
                    FeeType = Prc.PassengerFee.FeeType.ToString();
                    FlightReference = Prc.PassengerFee.FlightReference.ToString();
                    foreach (var FeePrc in Prc.PassengerFee.ServiceCharges)
                    {
                        if (FeePrc.ChargeCode.ToUpper() == FeeCode)
                        {
                            SeatFee = SeatFee + FeePrc.Amount;
                        }
                        else if (FeePrc.ChargeCode.ToUpper().Contains("CGST") == true)
                        {
                            CGST = CGST + FeePrc.Amount;
                        }
                        else if (FeePrc.ChargeCode.ToUpper().Contains("SGST") == true)
                        {
                            SGST = SGST + FeePrc.Amount;
                        }
                        else if (FeePrc.ChargeCode.ToUpper().Contains("IGST") == true)
                        {
                            IGST = IGST + FeePrc.Amount;
                        }
                        else if (FeePrc.ChargeCode.ToUpper().Contains("UGST") == true)
                        {
                            UGST = UGST + FeePrc.Amount;
                        }


                    }

                    foreach (var PP in SeatMap.SeatMapDetails)
                    {
                        PP.SeatListDetails.Where(x => x.SeatGroup == Prc.SeatGroup)
                   .Select(usr => { usr.SeatFee = SeatFee; usr.CGST = CGST; usr.SGST = SGST; usr.UGST = UGST; usr.IGST = IGST; return usr; })
                   .ToList();

                        PP.SeatListDetails.ToList().ForEach(p =>
                        {

                        });
                    }

                }
                #endregion
            }
            catch (Exception ex)
            {
                SeatMap SeatMapError = new SeatMap();
                SeatMapError.Error = "Error_Availability";
                SeatMapFinal.Add(SeatMapError);
                ExecptionLogger.FileHandling("SGNAV4(SeatBookingFinal)", "Error_003", ex, "SeatBooking");
                Spice_Logout(signature);

            }
            return SeatMapFinal;
        }
        private string GetExitRow(string EquipmentType, string EquipmentTypeSuffix, string SeatDesignator)
        {
            string Exit = "NA";
            if (EquipmentType.ToUpper() == "737" && EquipmentTypeSuffix.ToUpper() == "186")
            {
                Exit = (SeatDesignator == "15A" || SeatDesignator == "15B" || SeatDesignator == "15C" || SeatDesignator == "15D" || SeatDesignator == "15E" || SeatDesignator == "15F" || SeatDesignator == "16A" || SeatDesignator == "16B" || SeatDesignator == "16C" || SeatDesignator == "16D" || SeatDesignator == "16E" || SeatDesignator == "16F") ? "EXIT" : "NA";
            }
            else if (EquipmentType.ToUpper() == "737" && EquipmentTypeSuffix.ToUpper() == "189")
            {
                Exit = (SeatDesignator == "15A" || SeatDesignator == "15B" || SeatDesignator == "15C" || SeatDesignator == "15D" || SeatDesignator == "15E" || SeatDesignator == "15F" || SeatDesignator == "16A" || SeatDesignator == "16B" || SeatDesignator == "16C" || SeatDesignator == "16D" || SeatDesignator == "16E" || SeatDesignator == "16F") ? "EXIT" : "NA";
            }
            else if (EquipmentType.ToUpper() == "737" && EquipmentTypeSuffix.ToUpper() == "212")
            {
                Exit = (SeatDesignator == "17A" || SeatDesignator == "17B" || SeatDesignator == "17C" || SeatDesignator == "17D" || SeatDesignator == "17E" || SeatDesignator == "17F" || SeatDesignator == "18A" || SeatDesignator == "18B" || SeatDesignator == "18C" || SeatDesignator == "18D" || SeatDesignator == "18E" || SeatDesignator == "18F" || SeatDesignator == "28A" || SeatDesignator == "28B" || SeatDesignator == "28C" || SeatDesignator == "28D" || SeatDesignator == "28E" || SeatDesignator == "28F") ? "EXIT" : "NA";
            }
            else if (EquipmentType.ToUpper() == "737" && EquipmentTypeSuffix.ToUpper() == "700")
            {
                Exit = (SeatDesignator == "13A" || SeatDesignator == "13B" || SeatDesignator == "13C" || SeatDesignator == "13D" || SeatDesignator == "13E" || SeatDesignator == "13F") ? "EXIT" : "NA";
            }
            else if (EquipmentType.ToUpper() == "737" && EquipmentTypeSuffix.ToUpper() == "800")
            {
                Exit = (SeatDesignator == "15A" || SeatDesignator == "15B" || SeatDesignator == "15C" || SeatDesignator == "15D" || SeatDesignator == "15E" || SeatDesignator == "15F" || SeatDesignator == "16A" || SeatDesignator == "16B" || SeatDesignator == "16C" || SeatDesignator == "16D" || SeatDesignator == "16E" || SeatDesignator == "16F") ? "EXIT" : "NA";
            }
            else if (EquipmentType.ToUpper() == "TJO" && EquipmentTypeSuffix.ToUpper() == "800")
            {
                Exit = (SeatDesignator == "17A" || SeatDesignator == "17B" || SeatDesignator == "17C" || SeatDesignator == "17D" || SeatDesignator == "17E" || SeatDesignator == "17F" || SeatDesignator == "16A" || SeatDesignator == "16B" || SeatDesignator == "16C" || SeatDesignator == "16D" || SeatDesignator == "16E" || SeatDesignator == "16F") ? "EXIT" : "NA";
            }
            else if (EquipmentType.ToUpper() == "SGL" && EquipmentTypeSuffix.ToUpper() == "189")
            {
                Exit = (SeatDesignator == "15A" || SeatDesignator == "15B" || SeatDesignator == "15C" || SeatDesignator == "15D" || SeatDesignator == "15E" || SeatDesignator == "15F" || SeatDesignator == "16A" || SeatDesignator == "16B" || SeatDesignator == "16C" || SeatDesignator == "16D" || SeatDesignator == "16E" || SeatDesignator == "16F") ? "EXIT" : "NA";
            }
            else if (EquipmentType.ToUpper() == "DH8")
            {
                Exit = (SeatDesignator == "1A" || SeatDesignator == "1B" || SeatDesignator == "1C" || SeatDesignator == "1D" || SeatDesignator == "1E" || SeatDesignator == "1F" || SeatDesignator == "2A" || SeatDesignator == "2B" || SeatDesignator == "2C" || SeatDesignator == "2D" || SeatDesignator == "2E" || SeatDesignator == "2F") ? "EXIT" : "NA";
            }
            else if (EquipmentType.ToUpper() == "SUQ")
            {
                Exit = (SeatDesignator == "1A" || SeatDesignator == "1B" || SeatDesignator == "1C" || SeatDesignator == "1D" || SeatDesignator == "1E" || SeatDesignator == "1F" || SeatDesignator == "2A" || SeatDesignator == "2B" || SeatDesignator == "2C" || SeatDesignator == "2D" || SeatDesignator == "2E" || SeatDesignator == "2F") ? "EXIT" : "NA";
            }
            return Exit;
        }
        private bool BlockedSeat(string SeatDesignator)
        {
            bool Blocked = false;
            if (SeatDesignator == "1A" || SeatDesignator == "4C" || SeatDesignator == "5D" || SeatDesignator == "28A" || SeatDesignator == "28B" || SeatDesignator == "28E" || SeatDesignator == "28F" || SeatDesignator == "29A" || SeatDesignator == "29B" || SeatDesignator == "29E" || SeatDesignator == "29F" || SeatDesignator == "30A" || SeatDesignator == "30B" || SeatDesignator == "30C" || SeatDesignator == "30D" || SeatDesignator == "30E" || SeatDesignator == "30F")
                Blocked = true;
            return Blocked;
        }
        #endregion

        #region Cancellation
        public static navitaire.SG.ver4.GetBookingResponse RetriveBooking(string orderId, out string Msg)
        {
            string result = "";
            Msg = "";
            string TCCode = "";
            string ConStr = System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            List<CredentialList> CrdList;
            DataTable FltDT;
            DataTable PaxDT;
            DataTable FLTHdDT;
            FlightCommonBAL objFleSBal = new FlightCommonBAL(ConStr);
            Credentials objCrd = new Credentials(ConStr);
            string Signature = "";
            string vc = "";
            Dictionary<string, string> xmlo = new Dictionary<string, string>();
            navitaire.SG.ver4.GetBookingResponse BookingReposne = null;
            try
            {

                DataSet ds = objFleSBal.GetAllFlightDetailsByOrderId(orderId);
                FltDT = ds.Tables[0];
                PaxDT = ds.Tables[1];
                FLTHdDT = ds.Tables.Count > 3 ? ds.Tables[3] : new DataTable();

                vc = Convert.ToString(FltDT.Rows[0]["ValiDatingCarrier"]);
                decimal OriginalTF = Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"].ToString());
                decimal InfFare = Convert.ToDecimal(FltDT.Rows[0]["InfFare"].ToString());
                string strTrip = Convert.ToString(FltDT.Rows[0]["Trip"]);
                string idType = Convert.ToString(FltDT.Rows[0]["TripCnt"]);
                //TCCode = Convert.ToString(FLTHdDT.Rows[0]["CORPID"]);
                //CrdList = objCrd.GetGALBookingCredentials("LCC",strTrip,  vc.ToUpper(),  idType );
                //CrdList = objCrd.GetGALBookingCredentials(strTrip, TCCode, vc.ToUpper(), vc.ToString(), idType, "LCC");
                OnlineCancellationDAL.PNRCancellationDAL objDA = new OnlineCancellationDAL.PNRCancellationDAL(ConStr);
                DataSet FltHdrDs = objDA.GetHdrDetails(orderId);
                CrdList = objCrd.GetServiceCredentials("");

                CrdList = CrdList.Where(X => X.AirlineCode == vc && X.CorporateID == Convert.ToString(FltHdrDs.Tables[0].Rows[0]["TicketId"]).ToUpper().Trim() && X.CrdType == Convert.ToString(FltHdrDs.Tables[1].Rows[0]["RESULTTYPE"]).ToUpper().Trim() && X.Trip == strTrip).ToList();
                string url = CrdList[0].AvailabilityURL, userid = CrdList[0].UserID, Pwd = CrdList[0].Password, OrgCode = CrdList[0].CorporateID, smUrl = CrdList[0].LoginID, bmUrl = CrdList[0].LoginPWD, promocode = CrdList[0].APISource;

                if (vc == "SG")
                {
                    //SGNAV4 objSG = new SGNAV4(CrdList[0].TAUSERID, CrdList[0].TAPASSWORD, CrdList[0].URL, ConStr, CrdList[0].TAID, "", "", "", "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", CrdList[0].Exprs1, CrdList[0].Exprs2, int.Parse(CrdList[0].Exprs5), "SpiceJet");
                    SGNAV4 objSG = new SGNAV4(CrdList[0].UserID, CrdList[0].Password, CrdList[0].BookingURL, ConStr, CrdList[0].UserID, "", "", "", "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0, CrdList[0].CrdType);
                    Signature = objSG.Spice_Login();
                    OnlineCancellationBAL.PNRCancellationBAL ObjBal = new OnlineCancellationBAL.PNRCancellationBAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                    BookingReposne = objSG.GetBookingRquest(FLTHdDT.Rows[0]["AIRLINEPNR"].ToString().Trim(), Signature, ref xmlo, objSG.ContractVers);
                    Msg = xmlo.ContainsKey("ERROR") ? xmlo["ERROR"] : "";
                    objSG.Spice_Logout(Signature);

                }
            }
            catch (Exception ex)
            {
                ExecptionLogger.FileHandling("SPICE_NEWSKIES(FinalCancellation)", "Error_008", ex, "Cancellation");
                Msg = ex.Message.ToString();
            }
            return BookingReposne;

        }
        public navitaire.SG.ver4.GetBookingResponse GetBookingRquest(string RecordLocator, string Signature, ref Dictionary<string, string> xml, int cver)
        {
            string Reqxml = "";
            string Resxml = "";
            navitaire.SG.ver4.GetBookingResponse getBookingResp = null;
            navitaire.SG.ver4.GetBookingRequest getBookingReq = new navitaire.SG.ver4.GetBookingRequest();
            try
            {
                getBookingReq.Signature = Signature;
                getBookingReq.ContractVersion = cver;
                getBookingReq.GetBookingReqData = new GetBookingRequestData();
                getBookingReq.GetBookingReqData.GetBookingBy = GetBookingBy.RecordLocator;
                getBookingReq.GetBookingReqData.GetByRecordLocator = new GetByRecordLocator();
                getBookingReq.GetBookingReqData.GetByRecordLocator.RecordLocator = RecordLocator;
                Reqxml = SerializeAnObject(getBookingReq, "getBookingReq");
                getBookingResp = bookingAPI.GetBooking(getBookingReq);

                if (getBookingResp != null)
                {

                }
                try
                {
                    Resxml = SerializeAnObject(getBookingResp, "GetBookingReq");
                }
                catch (Exception ex)
                {
                    if (!xml.ContainsKey("ERROR"))
                        xml.Add("ERROR", ex.Message.ToString());
                }
            }
            catch (Exception ex)
            {
                if (!xml.ContainsKey("ERROR"))
                    xml.Add("ERROR", ex.Message.ToString());
                throw ex;
            }
            finally
            {
                xml.Add("BKG_REQ", Reqxml);
                xml.Add("BKG_RES", Resxml);
            }
            return getBookingResp;
        }
        public static string FinalCancellation(string orderId, string RefNo)
        {
            string TAID = "";
            string TAUSERID = "";
            string result = "FAILURE";
            string TCCode = "";
            string ConStr = System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            List<CredentialList> CrdList;
            DataTable FltDT;
            DataTable PaxDT;
            DataTable FLTHdDT;
            DataTable MealBagDT;
            string RefundAmount = "";
            decimal TicketCost = 0;
            FlightCommonBAL objFleSBal = new FlightCommonBAL(ConStr);
            Credentials objCrd = new Credentials(ConStr);
            string Signature = "";
            string vc = "";
            Dictionary<string, string> xmlo = new Dictionary<string, string>();
            try
            {
                DataSet ds = objFleSBal.GetAllFlightDetailsByOrderId(orderId);
                FltDT = ds.Tables[0];
                PaxDT = ds.Tables[1];
                MealBagDT = ds.Tables.Count > 2 ? ds.Tables[2] : new DataTable();
                FLTHdDT = ds.Tables.Count > 3 ? ds.Tables[3] : new DataTable();

                vc = Convert.ToString(FltDT.Rows[0]["ValiDatingCarrier"]);
                decimal OriginalTF = Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"].ToString());
                decimal InfFare = Convert.ToDecimal(FltDT.Rows[0]["InfFare"].ToString());
                string strTrip = Convert.ToString(FltDT.Rows[0]["Trip"]);
                string idType = Convert.ToString(FltDT.Rows[0]["TripCnt"]);
                //TCCode = Convert.ToString(FLTHdDT.Rows[0]["CORPID"]);
                //CrdList = objCrd.GetGALBookingCredentials(strTrip, TCCode, vc.ToUpper(), vc.ToString(), idType, "LCC");
                //TAID = CrdList[0].TAID;
                //TAUSERID = CrdList[0].TAUSERID;
                OnlineCancellationDAL.PNRCancellationDAL objDA = new OnlineCancellationDAL.PNRCancellationDAL(ConStr);
                DataSet FltHdrDs = objDA.GetHdrDetails(orderId);
                CrdList = objCrd.GetServiceCredentials("");
                CrdList = CrdList.Where(X => X.AirlineCode == vc && X.CorporateID == Convert.ToString(FltHdrDs.Tables[0].Rows[0]["TicketId"]).ToUpper().Trim() && X.CrdType == Convert.ToString(FltHdrDs.Tables[1].Rows[0]["RESULTTYPE"]).ToUpper().Trim() && X.Trip == strTrip).ToList();
                string url = CrdList[0].AvailabilityURL, userid = CrdList[0].UserID, Pwd = CrdList[0].Password, OrgCode = CrdList[0].CorporateID, smUrl = CrdList[0].LoginID, bmUrl = CrdList[0].LoginPWD, promocode = CrdList[0].APISource;
                TAID = CrdList[0].UserID;
                TAUSERID = CrdList[0].UserID;
                List<string> MealCodeList = new List<string>();
                List<string> BaggageCodeList = new List<string>();
                for (int i = 0; i < MealBagDT.Rows.Count; i++)
                {
                    MealCodeList.Add(Convert.ToString(MealBagDT.Rows[i]["MealCode"]));
                    BaggageCodeList.Add(Convert.ToString(MealBagDT.Rows[i]["BaggageCode"]));
                }
                MealCodeList = MealCodeList.Distinct().ToList();
                BaggageCodeList = BaggageCodeList.Distinct().ToList();
                if (vc == "SG")
                {
                    //SGNAV4 objSG = new SGNAV4(CrdList[0].TAUSERID, CrdList[0].TAPASSWORD, CrdList[0].URL, ConStr, CrdList[0].TAID, "", "", "", "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", CrdList[0].Exprs1, CrdList[0].Exprs2, int.Parse(CrdList[0].Exprs5), "SpiceJet");
                    SGNAV4 objSG = new SGNAV4(CrdList[0].UserID, CrdList[0].Password, CrdList[0].BookingURL, ConStr, CrdList[0].UserID, "", "", "", "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0, CrdList[0].CrdType);
                    Signature = objSG.Spice_Login();
                    OnlineCancellationBAL.PNRCancellationBAL ObjBal = new OnlineCancellationBAL.PNRCancellationBAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                    navitaire.SG.ver4.GetBookingResponse BookingReposne = objSG.GetBookingRquest(FLTHdDT.Rows[0]["AIRLINEPNR"].ToString().Trim(), Signature, ref xmlo, objSG.ContractVers);
                    List<SegTotalCost> objSTC = objSG.getSegmentWiseTotalCost(BookingReposne);
                    List<MBSPrice> objMBS = objSG.getMealBagSeatPrice(BookingReposne, MealBagDT);
                    string JourneySellKey = (BookingReposne.Booking.Journeys.Count() > 0 && BookingReposne.Booking.Journeys[0].JourneySellKey != null) ? BookingReposne.Booking.Journeys[0].JourneySellKey : "";

                    if (JourneySellKey != "")
                    {
                        OnlineCancellationDAL.PNRCancellationDAL ObjDAL = new OnlineCancellationDAL.PNRCancellationDAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                        ObjDAL.UpdateCancelTicket(orderId, RefNo, "0", "InProcess", "", "");

                        List<SegmentDetail> obj = ObjBal.GetCancellationSegmentDetails(orderId, "", "GET");
                        navitaire.SG.ver4.CancelResponse webRes = objSG.CanReq(Signature, obj, vc, RefNo, JourneySellKey, BookingReposne, ref xmlo);
                        RefundAmount = Convert.ToString(webRes.BookingUpdateResponseData.Success.PNRAmount.BalanceDue);
                        List<string> Segment = obj.Where(y => y.RefNo == RefNo).Select(y => y.Segment).Distinct().ToList();
                        foreach (var s in Segment)
                        {
                            TicketCost += Convert.ToDecimal(objSTC.Where(x => x.Segment == s).Select(x => x.TotalCost).FirstOrDefault());
                        }
                        int TotalPax = 1;
                        try
                        {
                            TotalPax = obj.Select(x => x.PaxId).Distinct().Count();
                        }
                        catch (Exception ex) { }
                        TicketCost = TicketCost * TotalPax;
                        decimal MealPrice = 0;
                        List<string> Sector = obj.Where(y => y.RefNo == RefNo).Select(y => y.Sector).Distinct().ToList();
                        foreach (var s in Sector)
                        {
                            MealPrice += Convert.ToDecimal(objMBS.Where(x => x.Segment == s.Replace('-', ':') && MealCodeList.Contains(x.Type)).Sum(x => Convert.ToDecimal(x.Price)));
                        }
                        decimal BaggagePrice = 0;
                        foreach (var s in Segment)
                        {
                            BaggagePrice += Convert.ToDecimal(objMBS.Where(x => x.Segment == s && BaggageCodeList.Contains(x.Type)).Sum(x => Convert.ToDecimal(x.Price)));
                        }
                        decimal SeatPrice = 0;
                        foreach (var s in Segment)
                        {
                            SeatPrice += Convert.ToDecimal(objMBS.Where(x => x.Segment == s && x.Type == "SEAT").Sum(x => Convert.ToDecimal(x.Price)));
                        }
                        TicketCost += MealPrice + BaggagePrice + SeatPrice;
                        objSG.Spice_AddPaymentToBooking(Signature, RefundAmount, ref xmlo);
                        result = objSG.Spice_BookingCommit(Signature, FLTHdDT.Rows[0]["AIRLINEPNR"].ToString().Trim(), Convert.ToInt32(obj.Where(x => x.RefNo == RefNo).Distinct().Count()), ref xmlo);
                        if (result != "FAILURE") { result = "Success_" + RefundAmount + "_" + Convert.ToString(TicketCost); }
                        else { result = "FAILURE_" + (xmlo.ContainsKey("ERROR") ? xmlo["ERROR"] : xmlo.ContainsKey("OTHER") ? xmlo["OTHER"] : "Unable to cancel ticket"); }
                    }
                    else
                    {
                        result = "FAILURE_" + "JourneySellKey not found.";
                    }
                    objSG.Spice_Logout(Signature);
                }
            }
            catch (Exception ex)
            {
                result = "FAILURE_" + (xmlo.ContainsKey("ERROR") ? xmlo["ERROR"] : xmlo.ContainsKey("OTHER") ? xmlo["OTHER"] : ex.Message.ToString());
                ExecptionLogger.FileHandling("SGNAV4(FinalCancellation)", "Error_008", ex, "Cancellation");
            }
            finally
            {
                OnlineCancellationDAL.PNRCancellationDAL ObjDAL = new OnlineCancellationDAL.PNRCancellationDAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                ObjDAL.InsertCancellationlLogs(orderId, RefNo, RefundAmount, vc.ToUpper(), xmlo.ContainsKey("BKG_REQ") ? xmlo["BKG_REQ"] : "", xmlo.ContainsKey("BKG_RES") ? xmlo["BKG_RES"] : "", xmlo.ContainsKey("CAN_REQ") ? xmlo["CAN_REQ"] : "", xmlo.ContainsKey("CAN_RES") ? xmlo["CAN_RES"] : "", xmlo.ContainsKey("APBREQ") ? xmlo["APBREQ"] : "", xmlo.ContainsKey("APBRES") ? xmlo["APBRES"] : "", xmlo.ContainsKey("BKGCOMMIT_REQ") ? xmlo["BKGCOMMIT_REQ"] : "", xmlo.ContainsKey("BKGCOMMIT_RES") ? xmlo["BKGCOMMIT_RES"] : "", xmlo.ContainsKey("ERROR") ? xmlo["ERROR"] : xmlo.ContainsKey("OTHER") ? xmlo["OTHER"] : "");
                ObjDAL.UpdateCancelTicket(orderId, RefNo, RefundAmount, result.Split('_')[0], TAID, TAUSERID);
            }

            return result;

        }
        public static string CheckFinalCancellationAmount(string orderId, string RefNo) 
        {
            string TAID = "";
            string TAUSERID = "";
            string result = "FAILURE";
            string TCCode = "";
            string ConStr = System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            List<CredentialList> CrdList;
            DataTable FltDT;
            DataTable PaxDT;
            DataTable FLTHdDT;
            DataTable MealBagDT;
            string RefundAmount = "";
            decimal TicketCost = 0;
            FlightCommonBAL objFleSBal = new FlightCommonBAL(ConStr);
            Credentials objCrd = new Credentials(ConStr);
            string Signature = "";
            string vc = "";
            Dictionary<string, string> xmlo = new Dictionary<string, string>();
            try
            {
                DataSet ds = objFleSBal.GetAllFlightDetailsByOrderId(orderId);
                FltDT = ds.Tables[0];
                PaxDT = ds.Tables[1];
                MealBagDT = ds.Tables.Count > 2 ? ds.Tables[2] : new DataTable();
                FLTHdDT = ds.Tables.Count > 3 ? ds.Tables[3] : new DataTable();

                vc = Convert.ToString(FltDT.Rows[0]["ValiDatingCarrier"]);
                decimal OriginalTF = Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"].ToString());
                decimal InfFare = Convert.ToDecimal(FltDT.Rows[0]["InfFare"].ToString());
                string strTrip = Convert.ToString(FltDT.Rows[0]["Trip"]);
                string idType = Convert.ToString(FltDT.Rows[0]["TripCnt"]);
                //TCCode = Convert.ToString(FLTHdDT.Rows[0]["CORPID"]);
                //CrdList = objCrd.GetGALBookingCredentials(strTrip, TCCode, vc.ToUpper(), vc.ToString(), idType, "LCC");
                //TAID = CrdList[0].TAID;
                //TAUSERID = CrdList[0].TAUSERID;
                OnlineCancellationDAL.PNRCancellationDAL objDA = new OnlineCancellationDAL.PNRCancellationDAL(ConStr);
                DataSet FltHdrDs = objDA.GetHdrDetails(orderId);
                CrdList = objCrd.GetServiceCredentials("");
                CrdList = CrdList.Where(X => X.AirlineCode == vc && X.CorporateID == Convert.ToString(FltHdrDs.Tables[0].Rows[0]["TicketId"]).ToUpper().Trim() && X.CrdType == Convert.ToString(FltHdrDs.Tables[1].Rows[0]["RESULTTYPE"]).ToUpper().Trim() && X.Trip == strTrip).ToList();
                string url = CrdList[0].AvailabilityURL, userid = CrdList[0].UserID, Pwd = CrdList[0].Password, OrgCode = CrdList[0].CorporateID, smUrl = CrdList[0].LoginID, bmUrl = CrdList[0].LoginPWD, promocode = CrdList[0].APISource;
                TAID = CrdList[0].UserID;
                TAUSERID = CrdList[0].UserID;
                List<string> MealCodeList = new List<string>();
                List<string> BaggageCodeList = new List<string>();
                for (int i = 0; i < MealBagDT.Rows.Count; i++)
                {
                    MealCodeList.Add(Convert.ToString(MealBagDT.Rows[i]["MealCode"]));
                    BaggageCodeList.Add(Convert.ToString(MealBagDT.Rows[i]["BaggageCode"]));
                }
                MealCodeList = MealCodeList.Distinct().ToList();
                BaggageCodeList = BaggageCodeList.Distinct().ToList();
                if (vc == "SG")
                {
                    //SGNAV4 objSG = new SGNAV4(CrdList[0].TAUSERID, CrdList[0].TAPASSWORD, CrdList[0].URL, ConStr, CrdList[0].TAID, "", "", "", "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", CrdList[0].Exprs1, CrdList[0].Exprs2, int.Parse(CrdList[0].Exprs5), "SpiceJet");
                    SGNAV4 objSG = new SGNAV4(CrdList[0].UserID, CrdList[0].Password, CrdList[0].BookingURL, ConStr, CrdList[0].UserID, "", "", "", "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 0, CrdList[0].CrdType);
                    Signature = objSG.Spice_Login();
                    OnlineCancellationBAL.PNRCancellationBAL ObjBal = new OnlineCancellationBAL.PNRCancellationBAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                    navitaire.SG.ver4.GetBookingResponse BookingReposne = objSG.GetBookingRquest(FLTHdDT.Rows[0]["AIRLINEPNR"].ToString().Trim(), Signature, ref xmlo, objSG.ContractVers);
                    List<SegTotalCost> objSTC = objSG.getSegmentWiseTotalCost(BookingReposne);
                    List<MBSPrice> objMBS = objSG.getMealBagSeatPrice(BookingReposne, MealBagDT);
                    string JourneySellKey = (BookingReposne.Booking.Journeys.Count() > 0 && BookingReposne.Booking.Journeys[0].JourneySellKey != null) ? BookingReposne.Booking.Journeys[0].JourneySellKey : "";

                    if (JourneySellKey != "")
                    {
                        OnlineCancellationDAL.PNRCancellationDAL ObjDAL = new OnlineCancellationDAL.PNRCancellationDAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                        ObjDAL.UpdateCancelTicket(orderId, RefNo, "0", "InProcess", "", "");

                        List<SegmentDetail> obj = ObjBal.GetCancellationSegmentDetails(orderId, "", "GET");
                        navitaire.SG.ver4.CancelResponse webRes = objSG.CanReq(Signature, obj, vc, RefNo, JourneySellKey, BookingReposne, ref xmlo);
                        RefundAmount = Convert.ToString(webRes.BookingUpdateResponseData.Success.PNRAmount.BalanceDue);
                        List<string> Segment = obj.Where(y => y.RefNo == RefNo).Select(y => y.Segment).Distinct().ToList();
                        foreach (var s in Segment)
                        {
                            TicketCost += Convert.ToDecimal(objSTC.Where(x => x.Segment == s).Select(x => x.TotalCost).FirstOrDefault());
                        }
                        int TotalPax = 1;
                        try
                        {
                            TotalPax = obj.Select(x => x.PaxId).Distinct().Count();
                        }
                        catch (Exception ex) { }
                        TicketCost = TicketCost * TotalPax;
                        decimal MealPrice = 0;
                        List<string> Sector = obj.Where(y => y.RefNo == RefNo).Select(y => y.Sector).Distinct().ToList();
                        foreach (var s in Sector)
                        {
                            MealPrice += Convert.ToDecimal(objMBS.Where(x => x.Segment == s.Replace('-', ':') && MealCodeList.Contains(x.Type)).Sum(x => Convert.ToDecimal(x.Price)));
                        }
                        decimal BaggagePrice = 0;
                        foreach (var s in Segment)
                        {
                            BaggagePrice += Convert.ToDecimal(objMBS.Where(x => x.Segment == s && BaggageCodeList.Contains(x.Type)).Sum(x => Convert.ToDecimal(x.Price)));
                        }
                        decimal SeatPrice = 0;
                        foreach (var s in Segment)
                        {
                            SeatPrice += Convert.ToDecimal(objMBS.Where(x => x.Segment == s && x.Type == "SEAT").Sum(x => Convert.ToDecimal(x.Price)));
                        }
                        TicketCost += MealPrice + BaggagePrice + SeatPrice;
                        result = objSG.Spice_AddPaymentToBooking(Signature, RefundAmount, ref xmlo);
                        if (result != "FAILURE") { result = "Success_" + RefundAmount + "_" + Convert.ToString(TicketCost); }
                        else { result = "FAILURE_" + (xmlo.ContainsKey("ERROR") ? xmlo["ERROR"] : xmlo.ContainsKey("OTHER") ? xmlo["OTHER"] : "Unable to cancel ticket"); }
                    }
                    else
                    {
                        result = "FAILURE_" + "JourneySellKey not found.";
                    }
                    objSG.Spice_Logout(Signature);
                }
            }
            catch (Exception ex)
            {
                result = "FAILURE_" + (xmlo.ContainsKey("ERROR") ? xmlo["ERROR"] : xmlo.ContainsKey("OTHER") ? xmlo["OTHER"] : ex.Message.ToString());
                ExecptionLogger.FileHandling("SGNAV4(FinalCancellation)", "Error_008", ex, "Cancellation");
            }
            finally
            {
                OnlineCancellationDAL.PNRCancellationDAL ObjDAL = new OnlineCancellationDAL.PNRCancellationDAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                ObjDAL.InsertCancellationlLogs(orderId, RefNo, RefundAmount, vc.ToUpper(), xmlo.ContainsKey("BKG_REQ") ? xmlo["BKG_REQ"] : "", xmlo.ContainsKey("BKG_RES") ? xmlo["BKG_RES"] : "", xmlo.ContainsKey("CAN_REQ") ? xmlo["CAN_REQ"] : "", xmlo.ContainsKey("CAN_RES") ? xmlo["CAN_RES"] : "", xmlo.ContainsKey("APBREQ") ? xmlo["APBREQ"] : "", xmlo.ContainsKey("APBRES") ? xmlo["APBRES"] : "", xmlo.ContainsKey("BKGCOMMIT_REQ") ? xmlo["BKGCOMMIT_REQ"] : "", xmlo.ContainsKey("BKGCOMMIT_RES") ? xmlo["BKGCOMMIT_RES"] : "", xmlo.ContainsKey("ERROR") ? xmlo["ERROR"] : xmlo.ContainsKey("OTHER") ? xmlo["OTHER"] : "");
                //ObjDAL.UpdateCancelTicket(orderId, RefNo, RefundAmount, result.Split('_')[0], TAID, TAUSERID);
            }

            return result;

        }
        public List<SegTotalCost> getSegmentWiseTotalCost(navitaire.SG.ver4.GetBookingResponse BkgRes)
        {
            List<SegTotalCost> obj = new List<SegTotalCost>();
            SegTotalCost objSTC = null;
            decimal totalFare = 0;
            try
            {
                foreach (var j in BkgRes.Booking.Journeys)
                {

                    foreach (var seg in j.Segments)
                    {
                        totalFare = 0;
                        objSTC = new SegTotalCost();
                        objSTC.Departure = seg.DepartureStation;
                        objSTC.Arrival = seg.ArrivalStation;
                        objSTC.Segment = objSTC.Departure + ":" + objSTC.Arrival;
                        foreach (var f in seg.Fares)
                        {
                            foreach (var pf in f.PaxFares)
                            {
                                foreach (var sc in pf.ServiceCharges)
                                {
                                    if (sc.ChargeType.ToString().ToUpper() != "PROMOTIONDISCOUNT")
                                        totalFare += sc.Amount;
                                    else
                                        totalFare -= sc.Amount;

                                }
                                break;
                            }
                        }
                        objSTC.TotalCost = Convert.ToString(totalFare);
                        obj.Add(objSTC);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return obj;
        }
        public List<MBSPrice> getMealBagSeatPrice(navitaire.SG.ver4.GetBookingResponse BkgRes, DataTable MealBagDT)
        {
            List<MBSPrice> obj = new List<MBSPrice>();
            MBSPrice objMBS = null;
            decimal totalFare = 0;
            try
            {
                foreach (var p in BkgRes.Booking.Passengers)
                {

                    foreach (var fee in p.PassengerFees)
                    {
                        totalFare = 0;
                        objMBS = new MBSPrice();
                        objMBS.Type = fee.FeeCode == "SEAT" ? fee.FeeCode : fee.SSRCode;
                        string Segment = (fee.FlightReference.Split(' ')[fee.FlightReference.Split(' ').Length - 1]).Trim();
                        if (Segment.Length == 6)
                        {
                            string Dept = Segment.Substring(0, 3);
                            string Arr = Segment.Substring(3, 3);
                            objMBS.Segment = Dept + ":" + Arr;
                            foreach (var sc in fee.ServiceCharges)
                            {
                                if (sc.ChargeType.ToString().ToUpper() != "IncludedTax".ToUpper())
                                    totalFare += sc.Amount;
                            }
                            objMBS.Price = Convert.ToString(totalFare);
                            obj.Add(objMBS);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return obj;
        }
        public navitaire.SG.ver4.CancelResponse CanReq(string Signature, List<SegmentDetail> ObjSD, string VC, string RefNo, string JourneySellKey, navitaire.SG.ver4.GetBookingResponse BkgRes, ref Dictionary<string, string> xml)
        {
            string Reqxml = "";
            string Resxml = "";
            string res = "";
            navitaire.SG.ver4.CancelResponse getCancelResp = null;
            navitaire.SG.ver4.CancelRequest getCancelReq = new navitaire.SG.ver4.CancelRequest();
            try
            {
                getCancelReq.ContractVersion = ContractVers;
                getCancelReq.Signature = Signature;
                getCancelReq.CancelRequestData = new CancelRequestData();
                int legCount = 0;
                List<string> LstSeg = new List<string>();
                List<string> LstSegForCan = ObjSD.Where(y => y.RefNo == RefNo).Select(y => y.Segment).Distinct().ToList();
                foreach (var j in BkgRes.Booking.Journeys)
                {
                    foreach (var seg in j.Segments)
                    {
                        legCount += seg.Legs.Count();
                        LstSeg.Add(seg.DepartureStation + ":" + seg.ArrivalStation);
                    }
                }
                LstSeg = LstSeg.Distinct().ToList();
                if (ObjSD.Count == ObjSD.Where(x => x.RefNo == RefNo).ToList().Count && LstSegForCan.Count == LstSeg.Count && LstSeg.All(LstSegForCan.Contains))
                {
                    getCancelReq.CancelRequestData.CancelBy = CancelBy.All;
                }
                else
                {
                    List<string> Segment = ObjSD.Where(y => y.RefNo == RefNo).Select(y => y.Segment).Distinct().ToList();
                    getCancelReq.CancelRequestData.CancelBy = CancelBy.Journey;
                    getCancelReq.CancelRequestData.CancelJourney = new CancelJourney();
                    getCancelReq.CancelRequestData.CancelJourney.CancelJourneyRequest = new CancelJourneyRequest();
                    getCancelReq.CancelRequestData.CancelJourney.CancelJourneyRequest.Journeys = new Journey[1];
                    Journey ObjJ = new Journey();
                    ObjJ.State = MessageState.New;
                    ObjJ.NotForGeneralUse = false;
                    ObjJ.JourneySellKey = JourneySellKey;
                    getCancelReq.CancelRequestData.CancelJourney.CancelJourneyRequest.Journeys[0] = ObjJ;
                    getCancelReq.CancelRequestData.CancelJourney.CancelJourneyRequest.Journeys[0].Segments = new navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.Segment[Segment.Count];
                    for (int i = 0; i < Segment.Count; i++)
                    {
                        navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.Segment ObjSeg = new navitaire.SG.ver4.schemas.navitaire.com.WebServices.DataContracts.Booking.Segment();
                        int totalSector = ObjSD.Where(y => y.Segment == Segment[i]).Select(y => y.Sector).Count();
                        int totalSectorSelected = ObjSD.Where(y => y.RefNo == RefNo && y.Segment == Segment[i]).Select(y => y.Sector).Count();
                        if (totalSector == totalSectorSelected)
                        {
                            foreach (var j in BkgRes.Booking.Journeys)
                            {
                                foreach (var seg in j.Segments)
                                {
                                    if (seg.DepartureStation == Segment[i].Split(':')[0] && seg.ArrivalStation == Segment[i].Split(':')[1])
                                    {
                                        ObjSeg.STA = seg.STA;
                                        ObjSeg.STD = seg.STD;
                                        ObjSeg.FlightDesignator = seg.FlightDesignator;
                                        break;
                                    }
                                }
                            }
                            ObjSeg.ArrivalStation = Segment[i].Split(':')[1];
                            ObjSeg.DepartureStation = Segment[i].Split(':')[0];
                        }
                        else { }
                        ObjSeg.State = MessageState.New;
                        ObjSeg.ActionStatusCode = "NN";
                        ObjSeg.International = false;
                        ObjSeg.SalesDate = Convert.ToDateTime("0001-01-01T00:00:00");
                        ObjSeg.ChannelType = ChannelType.Default;
                        getCancelReq.CancelRequestData.CancelJourney.CancelJourneyRequest.Journeys[0].Segments[i] = ObjSeg;
                    }
                    getCancelReq.CancelRequestData.CancelJourney.CancelJourneyRequest.WaivePenaltyFee = false;
                    getCancelReq.CancelRequestData.CancelJourney.CancelJourneyRequest.WaiveSpoilageFee = false;
                    getCancelReq.CancelRequestData.CancelJourney.CancelJourneyRequest.PreventReprice = false;
                    getCancelReq.CancelRequestData.CancelJourney.CancelJourneyRequest.ForceRefareForItineraryIntegrity = false;
                    getCancelReq.CancelRequestData.CancelJourney.CancelJourneyRequest.WaiveSpoilageFee = false;
                }
                Reqxml = SerializeAnObject(getCancelReq, "getCancelReq");
                getCancelResp = bookingAPI.Cancel(getCancelReq);
                if (getCancelReq != null)
                {

                }
                try
                {
                    Resxml = SerializeAnObject(getCancelResp, "GetBookingReq");
                }
                catch (Exception ex)
                {
                    if (!xml.ContainsKey("ERROR"))
                        xml.Add("ERROR", ex.Message.ToString());
                }
            }
            catch (Exception ex)
            {
                if (!xml.ContainsKey("ERROR"))
                    xml.Add("ERROR", ex.Message.ToString());
                throw ex;
            }
            finally
            {
                xml.Add("CAN_REQ", Reqxml);
                xml.Add("CAN_RES", Resxml);
            }
            return getCancelResp;
        }
        public string Spice_BookingCommit(string signature, string RecordLocator, int PaxCount, ref Dictionary<string, string> xml)
        {
            string Reqxml = "";
            string Resxml = "";
            string Other = "";
            string retval = "FAILURE";
            try
            {
                //IBookingManager bookingAPI = new BookingManagerClient();
                navitaire.SG.ver4.BookingCommitRequest request = new navitaire.SG.ver4.BookingCommitRequest();
                request.ContractVersion = ContractVers;
                request.Signature = signature;
                #region <BookingCommitRequestData>
                BookingCommitRequestData requestData = new BookingCommitRequestData();
                requestData.State = MessageState.New;
                requestData.RecordLocator = RecordLocator;
                requestData.CurrencyCode = "INR";
                requestData.PaxCount = (short)PaxCount;
                requestData.BookingID = 0;
                requestData.BookingParentID = 0;
                requestData.RestrictionOverride = false;
                requestData.ChangeHoldDateTime = false;
                requestData.WaiveNameChangeFee = false;
                requestData.WaivePenaltyFee = false;
                requestData.WaiveSpoilageFee = false;
                requestData.DistributeToContacts = false;
                #endregion
                request.BookingCommitRequestData = requestData;
                navitaire.SG.ver4.BookingCommitResponse response = null;
                Reqxml = SerializeAnObject(request, "BookingUpdate-Req");
                response = bookingAPI.BookingCommit(request);
                Resxml = SerializeAnObject(response, "BookingUpdate-Res");
                if (response.BookingUpdateResponseData != null)
                    retval = response.BookingUpdateResponseData.Success.RecordLocator.ToString();
            }
            catch (Exception ex)
            {
                if (!xml.ContainsKey("ERROR"))
                    xml.Add("ERROR", ex.Message.ToString());
                Other = ex.Message;
            }
            finally
            {
                xml.Add("BKGCOMMIT_REQ", Reqxml);
                xml.Add("BKGCOMMIT_RES", Resxml);
                xml.Add("OTHER", Other);
            }
            return retval;
        }
        #endregion
    }
}
