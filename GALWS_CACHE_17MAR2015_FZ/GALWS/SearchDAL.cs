﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using STD.Shared;

namespace STD.DAL
{
    public class SearchDAL
    {
        SqlConnection conn;
        SqlCommand cmd; 

        public SearchDAL()
        {
           conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SPRDISTR"].ConnectionString);

        }

        //Gets a string value from text box and Fetch Data from table  //public List<Search> Fetch_Commission_ADMIN() -convert to list
        public List<Search> GetAirportList(string city)
        {
            List<Search> CityList = new List<Search>();
            try
            {
                var list = new List<Search>();
                conn.Open();
                cmd = new SqlCommand("SP_DIST_CITY_SRCH",conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@param1",city);
                
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Search p = new Search();
                    p.CityName = reader.GetString(reader.GetOrdinal("CityName"));
                    p.AirportCode = reader.GetString(reader.GetOrdinal("AirportCode"));
                    p.CountryCode = reader.GetString(reader.GetOrdinal("CountryCode"));
                    CityList.Add(p);
                }
                
            }
            catch
            {
                //throw;
            }
            finally
            {
                conn.Close();
                cmd.Dispose();
            }
            return CityList;
        }

        public List<Search> GetAirLinesList(string city)
        {
            List<Search> airlineList = new List<Search>(); 
            try
            {
                var list = new List<Search>();
                conn.Open();
                cmd = new SqlCommand("SP_DIST_AIRL_LIST", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@param1", city);
                
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Search p = new Search();                            
                    p.ALCode = reader.GetString(reader.GetOrdinal("AL_Code"));
                    p.ALName = reader.GetString(reader.GetOrdinal("AL_Name"));
                    airlineList.Add(p);
                }
               
            }
            catch
            {
               // throw;
            }
            finally
            {
                conn.Close();
                cmd.Dispose();
            }
            return airlineList;
        }

    }
}
