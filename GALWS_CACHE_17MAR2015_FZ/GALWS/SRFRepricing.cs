﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using STD.Shared;
using STD.DAL;
using System.Threading;
using System.Data;
using Microsoft.VisualBasic;
using System.Collections;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.XPath;
using LCCINTL;
using AirArabia;
using LCCRTFDISTR;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
using STD.BAL.TBO;
using System.IO;
using STD.BAL.G8NAV;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Configuration;
using GALWS;
using STD.BAL;
using Newtonsoft.Json;
using STD.BAL._6ENAV420;

namespace GALWS
{
   public class SRFRepricing
    {
        //string ConnStr = "";
        public string connectionString { get; set; }
        public SRFRepricing(string ConStr)
        {
            //ConnStr = ConStr;
            connectionString = ConStr;
        }
        
        //string ConStr = Convert.ToString(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        List<FareTypeSettings> FareTypeSettingsList;
        private List<CredentialList> CrdList;
        List<FltSrvChargeList> SrvChargeList;
        DataTable PFCodeDt;
        DataTable PFCodeDt_LCC;
        List<MISCCharges> MiscList = new List<MISCCharges>();
        DataSet MarkupDs = new DataSet();
        public ArrayList SpecailReturnFarePricing(ArrayList AirArray, string Trip ,string AgentType,string UID , string AirLine)
        {
            string fbStr = "";            
            ArrayList Air = new ArrayList();
            try
            {
                Dictionary<string, object> a = new Dictionary<string, object>();
                a = (Dictionary<string, object>)AirArray[0];
                string adtrbd = "", chdrbd = "", infrbd = "";
                int adt = 0, chd = 0, inf = 0;
                string AllFltNo = "";

                int Adult = int.Parse(a["Adult"].ToString());
                int Child = int.Parse(a["Child"].ToString());
                int Infant = int.Parse(a["Infant"].ToString());
                string ValiDatingCarrier = Convert.ToString(a["ValiDatingCarrier"]);
                string CrdType = Convert.ToString(a["AdtFar"]); //"NRM";
                //string Airline = Convert.ToString(a["Adult"]);
                //string sno = Convert.ToString(a["Adult"]);
                //string Searchvalue = Convert.ToString(a["Adult"]);
                Trip = Convert.ToString(a["Trip"]);
                string sno="";// OBFlight[l].sno;
                string Searchvalue="";                
                float srvCharge = 0;
                float srvChargeAdt = 0;
                float srvChargeChd = 0;
                float InfantBFare = 0;
                float InfTax = 0;
                string Sector = Convert.ToString(a["Sector"]).Replace(':', '-');

                for (int i = 0; i < AirArray.Count; i++)
                {
                    Dictionary<string, object> b = new Dictionary<string, object>();
                    b = (Dictionary<string, object>)AirArray[i];
                    adtrbd += b["AdtRbd"].ToString() + ":";
                    //chdrbd += b["ChdRbd"].ToString() + ":";
                    chdrbd += chd > 0 ? b["ChdRbd"].ToString() : "" + ":";
                    AllFltNo += b["FlightIdentification"].ToString() + ",";
                   
                     sno += Convert.ToString(a["sno"]) + "#"; ;
                     Searchvalue += Convert.ToString(a["Searchvalue"]) + "#";                    
                }
                List<string> uniquessno = sno.Split('#').Distinct().ToList();
                List<string> uniquesSearchvalue = Searchvalue.Split('#').Distinct().ToList();
                Credentials objCrd = new Credentials(connectionString);
                FlightCommonBAL objfltBal = new FlightCommonBAL(connectionString);
                FareTypeSettingsList = objfltBal.GetFareTypeSettings(Convert.ToString(a["ValiDatingCarrier"]), Convert.ToString(a["Trip"]), Convert.ToString(a["Provider"]));
                
                ArrayList objFltResultList = new ArrayList();
                CrdList = objCrd.GetServiceCredentials("");              
                SrvChargeList = Data.GetSrvChargeInfo(Trip, connectionString);// Get Data From DB or Cache
                FltDeal objDeal = new FltDeal(connectionString);
                PFCodeDt = objDeal.GetCodeforPForDCFromDB("PF", Convert.ToString(a["Trip"]), AgentType, UID, Convert.ToString(a["ValiDatingCarrier"]), "", Convert.ToString(a["OrgDestFrom"]), Convert.ToString(a["OrgDestTo"]), "IN", "IN");
                PFCodeDt_LCC = objDeal.GetCodeforPForDCFromDB("PC", Convert.ToString(a["Trip"]), AgentType, UID, Convert.ToString(a["ValiDatingCarrier"]), "", Convert.ToString(a["OrgDestFrom"]), Convert.ToString(a["OrgDestTo"]), "IN", "IN");
                //PFCodeDt = objDeal.GetCodeforPForDCFromDB("PF", Trip, AgentType, UID, AirLine, "", searchInputs.HidTxtDepCity.Split(',')[0], searchInputs.HidTxtArrCity.Split(',')[0], searchInputs.HidTxtDepCity.Split(',')[1], searchInputs.HidTxtArrCity.Split(',')[1]);
                //PFCodeDt_LCC = objDeal.GetCodeforPForDCFromDB("PC", searchInputs.Trip.ToString(), searchInputs.AgentType, searchInputs.UID, searchInputs.AirLine, "", searchInputs.HidTxtDepCity.Split(',')[0], searchInputs.HidTxtArrCity.Split(',')[0], searchInputs.HidTxtDepCity.Split(',')[1], searchInputs.HidTxtArrCity.Split(',')[1]);
                //FareTypeSettingsList = objfltBal.GetFareTypeSettings("", Trip, "");
               


                #region Markup
                string Provider = Convert.ToString(a["Provider"]);               
                DataTable dtAgentMarkup = new DataTable();
                DataTable dtAdminMarkup = new DataTable();
                //Calculation For AgentMarkUp
                string DISTRID = "";
                dtAgentMarkup = Data.GetMarkup(connectionString,UID, DISTRID,Trip, "TA");
                dtAgentMarkup.TableName = "AgentMarkUp";
                MarkupDs.Tables.Add(dtAgentMarkup);
                //Calculation For AdminMarkUp'
                dtAdminMarkup = Data.GetMarkup(connectionString, UID,DISTRID,Trip, "AD");
                dtAdminMarkup.TableName = "AdminMarkUp";
                MarkupDs.Tables.Add(dtAdminMarkup);
                #endregion
                #region Misc Charges
                FlightCommonBAL objFltComm = new FlightCommonBAL(connectionString);
                try
                {
                    MiscList = objFltComm.GetMiscCharges(Trip, Convert.ToString(a["ValiDatingCarrier"]), UID, AgentType, Convert.ToString(a["OrgDestFrom"]), Convert.ToString(a["OrgDestTo"]));
                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("SRFRepricing(SpecailReturnFarePricing)", "Error_008", ex, "objFltComm.GetMiscCharges");
                }
                #endregion
                DataRow[] PFRow = null;
                try
                {
                    PFRow = PFCodeDt_LCC.Select("AppliedOn <>'BOOK'");
                }
                catch(Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("SRFRepricing(SpecailReturnFarePricing)", "Error_008", ex, "PFCodeDt_LCC");
                }
                
                List<CredentialList> CrdListCPN = CrdList.Where(x => (x.CrdType == Convert.ToString(a["AdtFar"])) && x.Provider == AirLine && x.Status == true && x.Trip == Trip).ToList();
                ArrayList Pricing = new ArrayList();
                int TSeg1 = 0, TSeg2 = 0;
                PriceItineraryResponse[] ItRes_L =null;
                // PCDt = PFCodeDt;
                string PriceResXML = null;
                string url = CrdListCPN[0].AvailabilityURL, userid = CrdListCPN[0].UserID, Pwd = CrdListCPN[0].Password, OrgCode = CrdListCPN[0].CorporateID, smUrl = CrdListCPN[0].LoginID, bmUrl = CrdListCPN[0].LoginPWD, promocode = CrdListCPN[0].APISource;
                if(CrdListCPN[0].ServerIP =="V4")
                {
                    _6ENAV objLcc = new _6ENAV(userid, Pwd, url, connectionString, OrgCode, Convert.ToString(a["OrgDestFrom"]), Convert.ToString(a["OrgDestTo"]), UID, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, CrdListCPN[0].CrdType, PFCodeDt_LCC);//,promocode
                    PriceResXML = objLcc.SRF_Spice_GetItnearyIndigo(AirArray, FareTypeSettingsList);
                }
                else
                {
                    SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, Convert.ToString(a["OrgDestFrom"]), Convert.ToString(a["OrgDestTo"]), UID, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, CrdListCPN[0].CrdType, PFCodeDt_LCC);//,promocode
                    PriceResXML = objLcc.SRF_Spice_GetItnearyIndigo(AirArray, FareTypeSettingsList);

                }
                //SpiceAPI objLcc = new SpiceAPI(userid, Pwd, url, connectionString, OrgCode, searchInputs.HidTxtDepCity, searchInputs.HidTxtArrCity, searchInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", smUrl, bmUrl, 340, CrdListCPN[0].CrdType, PFCodeDt_LCC);//,promocode
                //ItRes_L = objLcc.SRF_Spice_GetItneary(AirArray, FareTypeSettingsList);


                string jsona = null;               
                #region Fare variable
                float adtFarePrice = 0;
                float adtPHF = 0;
                float adtRCF = 0;
                float adtYQ = 0;
                float adtPSF = 0;
                float adtUDF = 0;
                float adtCGST = 0;
                float adtSGST = 0;
                float adtOtherAmount = 0;                

                float chdFarePrice = 0;
                float chdPHF = 0;
                float chdRCF = 0;
                float chdYQ = 0;
                float chdPSF = 0;
                float chdUDF = 0;
                float chdCGST = 0;
                float chdSGST = 0;
                float chdOtherAmount = 0;
                #endregion Fare variable
                decimal AdtTF = 0, ChdTF = 0, InfTF = 0;
                string strcheckentry = "";
                try
                {                
                if (!string.IsNullOrEmpty(PriceResXML))
                {
                        FlightSearchResults objFS = new FlightSearchResults();
                        LCCResult objLCC = new LCCResult();

                        objFS.Adult = Adult;
                        objFS.Child = Child;
                        objFS.Infant = Infant;
                        // PriceResXML = (PriceResXML.Replace("<xml version=\"1.0\">", "")).Replace(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"","");
                        PriceResXML = (PriceResXML.Replace("<?xml version=\"1.0\"?>", "")).Replace(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "");
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(PriceResXML);
                    jsona = JsonConvert.SerializeXmlNode(doc);
                        //File.AppendAllText("C:\\\\CPN_SP\\\\RePrice6ERes1_" + Sector + "-" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "XML_RESPONSE:" + Environment.NewLine + PriceResXML + Environment.NewLine + Environment.NewLine + "JSON_RESPONSE:" + Environment.NewLine + Environment.NewLine + jsona + Environment.NewLine);
                        #region Parse Resprice Response
                        RootObject aa = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(jsona);
                        if (!string.IsNullOrEmpty(Convert.ToString(aa)))
                        {
                            if (aa.PriceItineraryResponse.Booking.Journeys.Journey.Count > 0)
                            {

                                for (int i = 0; i < aa.PriceItineraryResponse.Booking.Journeys.Journey.Count; i++)
                                {

                                    #region Change Segment in Object
                                    string strSegment = (aa.PriceItineraryResponse.Booking.Journeys.Journey[i]).Segments.Segment.ToString();
                                    string strSeg = strSegment.Substring(0, 3);
                                    if (!strSeg.Contains("["))
                                    {
                                        strSegment = "[" + strSegment + "]";
                                    }
                                    //((aa.PriceItineraryResponse.Booking.Journeys.Journey[i]).Segments.Segment) as List<Segment>;
                                    // List <Segment> SG = Newtonsoft.Json.JsonConvert.DeserializeObject<Segment>(strSegment);
                                    Segment[] seg = Newtonsoft.Json.JsonConvert.DeserializeObject<Segment[]>(strSegment);

                                    for (int j = 0; j < seg.Count(); j++)
                                    {
                                        if (!string.IsNullOrEmpty(Convert.ToString(seg[j].Fares.Fare.PaxFares)))
                                        {
                                        string strPaxFare = seg[j].Fares.Fare.PaxFares.PaxFare.ToString();
                                        string strPFare = strPaxFare.Substring(0, 3);
                                        if (!strPFare.Contains("["))
                                        {
                                            strPaxFare = "[" + strPaxFare + "]";
                                        }
                                        PaxFare[] pFare = Newtonsoft.Json.JsonConvert.DeserializeObject<PaxFare[]>(strPaxFare);

                                        //for (int m = 0; m < (seg[j].Fares.Fare.PaxFares).PaxFare.Count(); m++)  //Check
                                        for (int m = 0; m < pFare.Count(); m++)
                                        {
                                            #region Set Amount1                                            
                                            string strBookingServiceCharge = pFare[m].ServiceCharges.BookingServiceCharge.ToString();
                                            //Convert.ToString(Newtonsoft.Json.JsonConvert.DeserializeObject<BookingServiceChargeObject[]>(aa.PriceItineraryResponse.Booking.Journeys.Journey[i].Segments.Segment[j].Fares.Fare.PaxFares.PaxFare.ServiceCharges.BookingServiceCharge.ToString()));
                                            if (!strBookingServiceCharge.Contains("["))
                                            {
                                                strBookingServiceCharge = "[" + strBookingServiceCharge + "]";
                                            }
                                            //BookingServiceChargeObject[] pp = Newtonsoft.Json.JsonConvert.DeserializeObject<BookingServiceChargeObject[]>(aa.PriceItineraryResponse.Booking.Journeys.Journey[i].Segments.Segment[j].Fares.Fare.PaxFares.PaxFare.ServiceCharges.BookingServiceCharge.ToString());

                                            BookingServiceChargeObject[] pp = Newtonsoft.Json.JsonConvert.DeserializeObject<BookingServiceChargeObject[]>(strBookingServiceCharge);

                                            for (int k = 0; k < pp.Count(); k++)
                                            {
                                                strcheckentry = strcheckentry + Convert.ToString(pp[k].ChargeType) + "-" + Convert.ToString(pp[k].ChargeCode) + ":" + Convert.ToString(pp[k].Amount);

                                                if (pFare[m].PaxType.ToUpper() == "ADT")
                                                {
                                                    #region Adult Fare Find
                                                    if (Convert.ToString(pp[k].ChargeType) == "FarePrice" && pp[k].ChargeCode == null)
                                                    {
                                                        adtFarePrice = adtFarePrice + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //adtFarePrice = adtFarePrice + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "PHF" && Convert.ToString(pp[k].ChargeType) == "TravelFee")
                                                    {
                                                        adtPHF = adtPHF + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //adtPHF = adtPHF + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "RCF" && Convert.ToString(pp[k].ChargeType) == "TravelFee")
                                                    {
                                                        adtRCF = adtRCF + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //adtRCF = adtRCF + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "YQ" && Convert.ToString(pp[k].ChargeType) == "TravelFee")
                                                    {
                                                        adtYQ = adtYQ + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //adtYQ = adtYQ + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "PSF" && Convert.ToString(pp[k].ChargeType) == "TravelFee")
                                                    {
                                                        adtPSF = adtPSF + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //adtPSF = adtPSF + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "UDF" && Convert.ToString(pp[k].ChargeType) == "TravelFee")
                                                    {
                                                        adtUDF = adtUDF + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //adtUDF = adtUDF + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "CGT" && Convert.ToString(pp[k].ChargeType) == "Tax")
                                                    {
                                                        adtCGST = adtCGST + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //adtCGST = adtCGST + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "SGT" && Convert.ToString(pp[k].ChargeType) == "Tax")
                                                    {
                                                        adtSGST = adtSGST + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //adtSGST = adtSGST + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (!string.IsNullOrEmpty(Convert.ToString(pp[k].Amount)) && Convert.ToDecimal(pp[k].Amount) > 0)
                                                    {
                                                        adtOtherAmount = adtOtherAmount + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        // adtOtherAmount = adtOtherAmount + Convert.ToDecimal(pp[k].Amount);
                                                    }

                                                    if (!string.IsNullOrEmpty(Convert.ToString(pp[k].Amount)) && Convert.ToDecimal(pp[k].Amount) > 0)
                                                    {
                                                        objFS.AdtFare = float.Parse(Math.Round(decimal.Parse(objFS.AdtFare.ToString()) + decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        AdtTF = AdtTF + decimal.Parse(pp[k].Amount.ToString());
                                                    }
                                                    
                                                    #endregion Set Amount
                                                }
                                                if (pFare[m].PaxType.ToUpper() == "CHD")
                                                {
                                                    #region Child Fare Find

                                                    if (Convert.ToString(pp[k].ChargeType) == "FarePrice" && pp[k].ChargeCode == null)
                                                    {
                                                        chdFarePrice = chdFarePrice + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //chdFarePrice = chdFarePrice + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "PHF" && Convert.ToString(pp[k].ChargeType) == "TravelFee")
                                                    {
                                                        chdPHF = chdPHF + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //chdPHF = chdPHF + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "RCF" && Convert.ToString(pp[k].ChargeType) == "TravelFee")
                                                    {
                                                        chdRCF = chdRCF + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //chdRCF = chdRCF + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "YQ" && Convert.ToString(pp[k].ChargeType) == "TravelFee")
                                                    {
                                                        chdYQ = chdYQ + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //chdYQ = chdYQ + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "PSF" && Convert.ToString(pp[k].ChargeType) == "TravelFee")
                                                    {
                                                        chdPSF = chdPSF + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //chdPSF = chdPSF + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "UDF" && Convert.ToString(pp[k].ChargeType) == "TravelFee")
                                                    {
                                                        chdUDF = chdUDF + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //chdUDF = chdUDF + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "CGT" && Convert.ToString(pp[k].ChargeType) == "Tax")
                                                    {
                                                        chdCGST = chdCGST + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //chdCGST = chdCGST + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (Convert.ToString(pp[k].TicketCode) == "SGT" && Convert.ToString(pp[k].ChargeType) == "Tax")
                                                    {
                                                        chdSGST = chdSGST + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        //chdSGST = chdSGST + Convert.ToDecimal(pp[k].Amount);
                                                    }
                                                    else if (!string.IsNullOrEmpty(Convert.ToString(pp[k].Amount)) && Convert.ToDecimal(pp[k].Amount) > 0)
                                                    {
                                                        chdOtherAmount = chdOtherAmount + float.Parse(Math.Round(decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                       // chdOtherAmount = chdOtherAmount + Convert.ToDecimal(pp[k].Amount);
                                                    }

                                                    if (!string.IsNullOrEmpty(Convert.ToString(pp[k].Amount)) && Convert.ToDecimal(pp[k].Amount) > 0)
                                                    {
                                                        objFS.ChdFare = float.Parse(Math.Round(decimal.Parse(objFS.ChdFare.ToString()) + decimal.Parse(pp[k].Amount.ToString()), 0).ToString());
                                                        ChdTF = ChdTF + decimal.Parse(pp[k].Amount.ToString());                                                        
                                                    }
                                                    #endregion Child Fare Find
                                                }
                                            }
                                            #endregion Set Amount1
                                        }
                                      }
                                    }
                                    #endregion
                                }
                                #region Set Fare

                                #region Adult Fare Calculate
                                //adtTotalFare = adtFarePrice + adtPHF + adtRCF + adtYQ + adtPSF + adtUDF + adtCGST + adtSGST + adtOtherAmount;

                                objFS.AdtFSur = adtYQ;
                                objFS.AdtWO = objFS.AdtWO;
                                objFS.AdtIN = objFS.AdtIN;
                                objFS.AdtJN = adtCGST + adtSGST;
                                objFS.AdtYR = objFS.AdtYR;
                                objFS.AdtBfare = adtFarePrice;
                                objFS.AdtOT = adtPHF + adtRCF + adtPSF + adtUDF + adtOtherAmount; //float.Parse(Math.Round(decimal.Parse(objFS.AdtOT.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                objFS.AdtTax = objFS.AdtFare - objFS.AdtBfare;
                                // objFS.AdtFare = float.Parse(Math.Round(decimal.Parse(objFS.AdtFare.ToString()) + decimal.Parse(adtd.Amount.ToString()), 0).ToString());
                                //AdtTF = AdtTF + decimal.Parse(adtd.Amount.ToString());
                                objFS.AdtTax = objFS.AdtFare - objFS.AdtBfare;

                                #region Get MISC Markup Charges Date 06-03-2018
                                try
                                {
                                    //MiscList = objFltComm.GetMiscCharges(Trip, Convert.ToString(a["ValiDatingCarrier"]), UID, AgentType, Convert.ToString(a["OrgDestFrom"]), Convert.ToString(a["OrgDestTo"]));
                                    srvChargeAdt = objFltComm.MISCServiceFee(MiscList, ValiDatingCarrier, CrdType, Convert.ToString(objFS.AdtBfare), Convert.ToString(objFS.AdtFSur));
                                }
                                catch { srvChargeAdt = 0; }
                                #endregion
                                //SMS charge add 
                                objFS.AdtOT = objFS.AdtOT + srvChargeAdt;
                                objFS.AdtTax = objFS.AdtTax + srvChargeAdt;
                                objFS.AdtFare = objFS.AdtFare + srvChargeAdt;
                                //SMS charge add end
                                objFS.ADTAdminMrk = 0;
                                try
                                {
                                    objFS.ADTAgentMrk = objLCC.CalcMarkup(MarkupDs.Tables["AgentMarkUp"], ValiDatingCarrier, objFS.AdtFare, "D");
                                }
                                catch { objFS.ADTAdminMrk = 0; }
                                #endregion Adult Fare Calculate

                                #region Child Fare Calculate
                                if (Child > 0)
                                {
                                    //chdTotalFare = chdFarePrice + chdPHF + chdRCF + chdYQ + chdPSF + chdUDF + chdCGST + chdSGST + chdOtherAmount;                                
                                    objFS.ChdFSur = chdYQ;
                                    objFS.ChdWO = objFS.ChdWO;
                                    objFS.ChdIN = objFS.ChdIN;
                                    objFS.ChdJN = chdCGST + chdSGST;
                                    objFS.ChdYR = objFS.ChdYR;
                                    objFS.ChdBFare = chdFarePrice;
                                    objFS.ChdOT = chdPHF + chdRCF + chdPSF + chdUDF + chdOtherAmount;//float.Parse(Math.Round(decimal.Parse(objFS.ChdOT.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());

                                    //objFS.ChdFare = float.Parse(Math.Round(decimal.Parse(objFS.ChdFare.ToString()) + decimal.Parse(chdd.Amount.ToString()), 0).ToString());
                                    //ChdTF = ChdTF + decimal.Parse(chdd.Amount.ToString());
                                    objFS.ChdTax = objFS.ChdFare - objFS.ChdBFare;
                                    #region Get MISC Markup Charges Date 06-03-2018
                                    try
                                    {
                                        srvChargeChd = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier, CrdType, Convert.ToString(objFS.ChdBFare), Convert.ToString(objFS.ChdFSur));//GetMiscServiceCharge(searchInputs.Trip.ToString(), objFS.ValiDatingCarrier, searchInputs.UID, searchInputs.AgentType, Utility.Left(searchInputs.HidTxtDepCity, 3), Utility.Left(searchInputs.HidTxtArrCity, 3));
                                    }
                                    catch { srvChargeChd = 0; }
                                    #endregion


                                    //SMS charge add 
                                    objFS.ChdOT = objFS.ChdOT + srvChargeChd;
                                    objFS.ChdTax = objFS.ChdTax + srvChargeChd;
                                    objFS.ChdFare = objFS.ChdFare + srvChargeChd;
                                    objFS.CHDAdminMrk = 0;
                                    try
                                    {
                                        objFS.CHDAgentMrk = objLCC.CalcMarkup(MarkupDs.Tables["AgentMarkUp"], ValiDatingCarrier, objFS.ChdFare, "D");
                                    }
                                    catch { objFS.CHDAdminMrk = 0; }
                                    
                                }
                                #endregion Child Fare Calculate

                                #region Infant Fare Calculate
                                
                                    #region Infant
                                    if (Infant > 0)
                                    {
                                    InfantBFare = 2380;//1190;
                                    InfTax = 120;//60;
                                    objFS.InfFare = float.Parse(Math.Round(InfantBFare + InfTax).ToString());
                                    objFS.InfBfare = float.Parse(Math.Round(InfantBFare).ToString());
                                    objFS.InfTax = float.Parse(Math.Round(InfTax).ToString());
                                    objFS.InfOT = objFS.InfTax;
                                    InfTF = decimal.Parse(Math.Round(objFS.InfFare).ToString());
                                    objFS.InfFSur = 0;
                                        objFS.InfIN = 0;
                                        objFS.InfJN = 0;
                                        //objFS.InfOT = 0;
                                        objFS.InfQ = 0;                                        
                                    }
                                    else
                                    {
                                        objFS.InfFare = 0;
                                        objFS.InfBfare = 0;
                                        objFS.InfFSur = 0;
                                        objFS.InfIN = 0;
                                        objFS.InfJN = 0;
                                        objFS.InfOT = 0;
                                        objFS.InfQ = 0;
                                    }
                                #endregion Infant Fare Calculate End

                                #endregion
                                    
                                #region Set All Fare
                                objFS.OriginalTF = float.Parse((AdtTF * objFS.Adult + ChdTF * objFS.Child).ToString());//Without Infant Fare
                                objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                                objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                                objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                                objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                                objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                                objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                                objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                                objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                                objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                                objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                                objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);                                
                                objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                                objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                                //objFS.OperatingCarrier = p.opreatingcarrier;
                                objFS.NetFareSRTF = objFS.NetFare;

                                srvCharge = srvChargeAdt + srvChargeChd;  //06-03-2018  Add MISC CHARGES PAX WISE
                                objFS.OriginalTT = srvCharge;
                                #endregion
                                if (adtFarePrice>0 && objFS.TotalFare > 0 && objFS.NetFareSRTF>0)
                                {
                                    Air= UpadteSRFFareArrayList(AirArray, objFS);
                                }

                                #region CheckFare
                                float adtTotalFare = 0;
                                float chdTotalFare = 0;
                                decimal TotalCost = Convert.ToDecimal(aa.PriceItineraryResponse.Booking.BookingSum.TotalCost);
                                if (Child > 0)
                                {
                                    //TotalFare = (FarePrice + PHF + RCF + YQ + PSF + UDF + CGST + SGST + OtherAmount)/2;
                                    //TotalFare1 = ((FarePrice / 2) + (PHF / 2) + (RCF / 2) + (YQ / 2) + (PSF / 2) + (UDF / 2) + (CGST / 2) + (SGST / 2) + (OtherAmount / 2));
                                    adtTotalFare = adtFarePrice + adtPHF + adtRCF + adtYQ + adtPSF + adtUDF + adtCGST + adtSGST + adtOtherAmount;
                                    chdTotalFare = chdFarePrice + chdPHF + chdRCF + chdYQ + chdPSF + chdUDF + chdCGST + chdSGST + chdOtherAmount;
                                }
                                else
                                {
                                    adtTotalFare = adtFarePrice + adtPHF + adtRCF + adtYQ + adtPSF + adtUDF + adtCGST + adtSGST + adtOtherAmount;
                                }
                                #endregion

                                #endregion Set Fare End
                                decimal TotalBookingAmount = Convert.ToDecimal(aa.PriceItineraryResponse.Booking.BookingSum.TotalCost);
                            }
                        }                        
                       // Infant Fare 1190 Infant Tax 60
                        string ttt = strcheckentry;

                        #endregion Parse Resprice Response

                    }
                }
                catch (Exception ex)
                {
                    File.AppendAllText("C:\\\\CPN_SP\\\\RePrice6E1_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "Error:" + ex.StackTrace.ToString() + ex.Message + Environment.NewLine);
                    File.AppendAllText("C:\\\\CPN_SP\\\\RePrice6ERes1_" + Sector + "-" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + Environment.NewLine + "XML_RESPONSE:" + Environment.NewLine + Environment.NewLine + PriceResXML + Environment.NewLine + Environment.NewLine + "JSON_RESPONSE:" + Environment.NewLine + Environment.NewLine + jsona + Environment.NewLine);
                }        
                

            }
            catch (Exception ex)
            {
                File.AppendAllText("C:\\\\CPN_SP\\\\RePrice6E2_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "Error:" + ex.StackTrace.ToString() + ex.Message + Environment.NewLine);                
            }           
            finally
            {
            }
            return Air;
        }

        public ArrayList UpadteSRFFareArrayList (ArrayList AirArray, FlightSearchResults objFS)
        {
            ArrayList Final = new ArrayList();
            for (int i = 0; i < AirArray.Count; i++)
            {
                Dictionary<string, object> Rec = new Dictionary<string, object>();
                Rec = (Dictionary<string, object>)AirArray[i];                
                Rec["AdtFare"]= objFS.AdtFare;
                Rec["AdtBfare"] = objFS.AdtBfare;
                Rec["AdtTax"] = objFS.AdtTax;//, 631]}
                Rec["AdtFSur"] = objFS.AdtFSur;//, 200]}
                Rec["AdtYR"] = objFS.AdtYR;//, 0]}
                Rec["AdtWO"] = objFS.AdtWO;//, 0]}
                Rec["AdtIN"] = objFS.AdtIN;//, 0]}
                //Rec["AdtQ"] = objFS.AdtQ;//, 0 //Not Clare
                Rec["AdtJN"] = objFS.AdtJN;//, 116]}
                Rec["AdtOT"] = objFS.AdtOT;// 315]}
                //Rec["AdtSrvTax"] = objFS.AdtSrvTax;// 0]}
                //Rec["AdtSrvTax1"] = objFS.AdtSrvTax1;// 0]}
                Rec["AdtEduCess"] = objFS.AdtEduCess;// 0]}
                Rec["AdtHighEduCess"] = objFS.AdtHighEduCess;// 0]}
                Rec["AdtTF"] = objFS.AdtTF;// 0]}
                Rec["ADTAdminMrk"] = objFS.ADTAdminMrk;// 0]}
                Rec["ADTAgentMrk"] = objFS.ADTAgentMrk;// 100]}
                Rec["ADTDistMrk"] = objFS.ADTDistMrk;// 0]}
                //Rec["AdtDiscount"] = objFS.AdtDiscount;// 32]}
                //Rec["AdtDiscount1"] = objFS.AdtDiscount1;// 32]}
                //Rec["AdtCB"] = objFS.AdtCB;// 0]}
                //Rec["AdtTds"] = objFS.AdtTds;// 2]}
                Rec["AdtMgtFee"] = objFS.AdtMgtFee;// 0]}
                Rec["ChdFare"] = objFS.ChdFare;// 2593]}
                Rec["ChdBFare"] = objFS.ChdBFare;// 1962]}
                Rec["ChdTax"] = objFS.ChdTax;// 631]}
                Rec["ChdFSur"] = objFS.ChdFSur;// 200]}
                Rec["ChdYR"] = objFS.ChdYR;// 0]}
                Rec["ChdWO"] = objFS.ChdWO;// 0]}
                Rec["ChdIN"] = objFS.ChdIN;// 0]}
                Rec["ChdQ"] = objFS.ChdQ;// 0]}
                Rec["ChdJN"] = objFS.ChdJN;// 116]}
                Rec["ChdOT"] = objFS.ChdOT;// 315]}
                //Rec["ChdSrvTax"] = objFS.ChdSrvTax;// 0]}
                //Rec["ChdSrvTax1"] = objFS.ChdSrvTax1;// 0]}
                Rec["ChdEduCess"] = objFS.ChdEduCess;// 0]}
                Rec["ChdHighEduCess"] = objFS.ChdHighEduCess;// 0]}
                Rec["ChdTF"] = objFS.ChdTF;// 0]}
                Rec["CHDAdminMrk"] = objFS.CHDAdminMrk;// 0]}
                Rec["CHDAgentMrk"] = objFS.CHDAgentMrk;// 100]}
                Rec["CHDDistMrk"] = objFS.CHDDistMrk;// 0]}
                //Rec["ChdTds"] = objFS.ChdTds;// 2]}
                //Rec["ChdDiscount"] = objFS.ChdDiscount;// 32]}
                //Rec["ChdDiscount1"] = objFS.ChdDiscount1;// 32]}
                //Rec["ChdCB"] = objFS.ChdCB;// 0]}
                Rec["ChdMgtFee"] = objFS.ChdMgtFee;// 0]}
                Rec["InfFare"] = objFS.InfFare;// 1250]}
                Rec["InfBfare"] = objFS.InfBfare;// 1190]}
                Rec["InfTax"] = objFS.InfTax;// 60]}
                Rec["InfFSur"] = objFS.InfFSur;// 0]}
                Rec["InfYR"] = objFS.InfYR;// 0]}
                Rec["InfWO"] = objFS.InfWO;// 0]}
                Rec["InfIN"] = objFS.InfIN;// 0]}
                Rec["InfQ"] = objFS.InfQ;// 0]}
                Rec["InfJN"] = objFS.InfJN;// 0]}
                Rec["InfOT"] = objFS.InfOT;// 60]}
                Rec["InfSrvTax"] = objFS.InfSrvTax;// 0]}
                Rec["InfSrvTax1"] = objFS.AdtFare;// 0]}
                Rec["InfEduCess"] = objFS.InfEduCess;// 0]}
                Rec["InfHighEduCess"] = objFS.InfHighEduCess;// 0]}
                Rec["InfTF"] = objFS.InfTF;// 0]}
                Rec["InfAdminMrk"] = objFS.InfAdminMrk;// 0]}
                Rec["InfAgentMrk"] = objFS.InfAgentMrk;// 0]}
                Rec["InfDistMrk"] = objFS.InfDistMrk;// 0]}
                Rec["InfTds"] = objFS.InfTds;// 0]}
                Rec["InfDiscount"] = objFS.InfDiscount;// 0]}
                Rec["InfDiscount1"] = objFS.InfDiscount1;// 0]}
                Rec["InfCB"] = objFS.InfCB;// 0]}
                Rec["InfMgtFee"] = objFS.InfMgtFee;// 0]}
                Rec["NetFareSRTF"] = objFS.NetFareSRTF;// 12872]}
                Rec["TotBfare"] = objFS.TotBfare;// 10228]}
                Rec["TotalFare"] = objFS.TotalFare;// 13272]}
                Rec["TotalTax"] = objFS.TotalTax;// 2644]}
                Rec["TotalFuelSur"] = objFS.TotalFuelSur;// 800]}
                Rec["NetFare"] = objFS.NetFare;// 12752]}
                Rec["TotMrkUp"] = objFS.TotMrkUp;// 400]}
                //Rec["STax"] = objFS.STax;// 0]}
                //Rec["TFee"] = objFS.TFee;// 0]}
                //Rec["TotDis"] = objFS.TotDis;// 128]}
                //Rec["TotCB"] = objFS.TotCB;// 0]}
                //Rec["TotTds"] = objFS.TotTds;// 8]}
                Rec["TotMgtFee"] = objFS.TotMgtFee;//objFS.AdtFare;// 0]}
                //Rec["IATAComm"] = objFS.IATAComm;// 0]}
                Rec["OriginalTF"] = objFS.OriginalTF;//objFS.AdtFare;// 10372]}
                Rec["OriginalTT"] = objFS.OriginalTT;//objFS.AdtFare;// 0]}                
                Final.Add(Rec);
            }
            return Final;
        }

        #region Reprice Class
        public class BookingInfo
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public string BookingStatus { get; set; }
            public object BookingType { get; set; }
            public string ChannelType { get; set; }
            public DateTime CreatedDate { get; set; }
            public DateTime ExpiredDate { get; set; }
            public DateTime ModifiedDate { get; set; }
            public string PriceStatus { get; set; }
            public string ProfileStatus { get; set; }
            public string ChangeAllowed { get; set; }
            public string CreatedAgentID { get; set; }
            public string ModifiedAgentID { get; set; }
            public DateTime BookingDate { get; set; }
            public object OwningCarrierCode { get; set; }
            public string PaidStatus { get; set; }
        }

        public class POS
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public string AgentCode { get; set; }
            public string OrganizationCode { get; set; }
            public string DomainCode { get; set; }
            public string LocationCode { get; set; }
        }

        public class SourcePOS
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public string AgentCode { get; set; }
            public string OrganizationCode { get; set; }
            public string DomainCode { get; set; }
            public string LocationCode { get; set; }
        }

        public class TypeOfSale
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public string PaxResidentCountry { get; set; }
            public object PromotionCode { get; set; }
            public object FareTypes { get; set; }
        }

        public class BookingSum
        {
            public object ExtensionData { get; set; }
            public string BalanceDue { get; set; }
            public string AuthorizedBalanceDue { get; set; }
            public string SegmentCount { get; set; }
            public string PassiveSegmentCount { get; set; }
            public string TotalCost { get; set; }
            public string PointsBalanceDue { get; set; }
            public string TotalPointCost { get; set; }
            public object AlternateCurrencyCode { get; set; }
            public string AlternateCurrencyBalanceDue { get; set; }
        }

        public class ReceivedBy1
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public object ReceivedBy { get; set; }
            public object ReceivedReference { get; set; }
            public object ReferralCode { get; set; }
            public object LatestReceivedBy { get; set; }
            public object LatestReceivedReference { get; set; }
        }

        public class PassengerInfo
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public string BalanceDue { get; set; }
            public string Gender { get; set; }
            public object Nationality { get; set; }
            public object ResidentCountry { get; set; }
            public string TotalCost { get; set; }
            public string WeightCategory { get; set; }
        }

        public class PassengerTypeInfo
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public DateTime DOB { get; set; }
            public string PaxType { get; set; }
        }

        public class PassengerTypeInfos
        {
            public PassengerTypeInfo PassengerTypeInfo { get; set; }
        }

        public class PassengerInfo2
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public string BalanceDue { get; set; }
            public string Gender { get; set; }
            public object Nationality { get; set; }
            public object ResidentCountry { get; set; }
            public string TotalCost { get; set; }
            public string WeightCategory { get; set; }
        }

        public class PassengerInfos
        {
            public PassengerInfo2 PassengerInfo { get; set; }
        }

        public class Passenger
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public object CustomerNumber { get; set; }
            public string PassengerNumber { get; set; }
            public string FamilyNumber { get; set; }
            public object PaxDiscountCode { get; set; }
            public object Names { get; set; }
            public PassengerInfo PassengerInfo { get; set; }
            public object PassengerFees { get; set; }
            public object PassengerAddresses { get; set; }
            public object PassengerTravelDocuments { get; set; }
            public object PassengerBags { get; set; }
            public string PassengerID { get; set; }
            public PassengerTypeInfos PassengerTypeInfos { get; set; }
            public PassengerInfos PassengerInfos { get; set; }
            public object PassengerInfants { get; set; }
            public string PseudoPassenger { get; set; }
        }

        public class Passengers
        {
            //public Passenger Passenger { get; set; }
            // public List<Passenger> Passenger { get; set; } //Change
            public object Passenger { get; set; }
        }

        public class FlightDesignator
        {
            public object ExtensionData { get; set; }
            public string CarrierCode { get; set; }
            public string FlightNumber { get; set; }
            public object OpSuffix { get; set; }
        }

        public class ServiceCharges
        {

            public object BookingServiceCharge { get; set; }
        }


        [JsonObject]
        public class BookingServiceChargeObject
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public string ChargeType { get; set; }
            public string CollectType { get; set; }
            public object ChargeCode { get; set; }
            public object TicketCode { get; set; }
            public string CurrencyCode { get; set; }
            public string Amount { get; set; }
            public object ChargeDetail { get; set; }
            public string ForeignCurrencyCode { get; set; }
            public string ForeignAmount { get; set; }
        }

        public class PaxFare
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public string PaxType { get; set; }
            public object PaxDiscountCode { get; set; }
            public object FareDiscountCode { get; set; }
            public ServiceCharges ServiceCharges { get; set; }
        }

        public class PaxFares
        {
            //public PaxFare PaxFare { get; set; }
            // public List<PaxFare> PaxFare { get; set; }  //Change
            public object PaxFare { get; set; } //11
        }

        public class Fare
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public string ClassOfService { get; set; }
            public object ClassType { get; set; }
            public object RuleTariff { get; set; }
            public string CarrierCode { get; set; }
            public string RuleNumber { get; set; }
            public string FareBasisCode { get; set; }
            public string FareSequence { get; set; }
            public string FareClassOfService { get; set; }
            public string FareStatus { get; set; }
            public string FareApplicationType { get; set; }
            public string OriginalClassOfService { get; set; }
            public object XrefClassOfService { get; set; }
            public PaxFares PaxFares { get; set; }
            public string ProductClass { get; set; }
            public string IsAllotmentMarketFare { get; set; }
            public object TravelClassCode { get; set; }
            public string FareSellKey { get; set; }
            public string InboundOutbound { get; set; }
        }

        public class Fares
        {
            public Fare Fare { get; set; }
        }

        public class FlightDesignator2
        {
            public object ExtensionData { get; set; }
            public string CarrierCode { get; set; }
            public string FlightNumber { get; set; }
            public object OpSuffix { get; set; }
        }

        public class LegInfo
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public string AdjustedCapacity { get; set; }
            public object EquipmentType { get; set; }
            public object EquipmentTypeSuffix { get; set; }
            public object ArrivalTerminal { get; set; }
            public string ArrvLTV { get; set; }
            public string Capacity { get; set; }
            public object CodeShareIndicator { get; set; }
            public object DepartureTerminal { get; set; }
            public string DeptLTV { get; set; }
            public string ETicket { get; set; }
            public string FlifoUpdated { get; set; }
            public string IROP { get; set; }
            public string Status { get; set; }
            public string Lid { get; set; }
            public object OnTime { get; set; }
            public DateTime PaxSTA { get; set; }
            public DateTime PaxSTD { get; set; }
            public object PRBCCode { get; set; }
            public object ScheduleServiceType { get; set; }
            public string Sold { get; set; }
            public string OutMoveDays { get; set; }
            public string BackMoveDays { get; set; }
            public object LegNests { get; set; }
            public object LegSSRs { get; set; }
            public object OperatingFlightNumber { get; set; }
            public object OperatedByText { get; set; }
            public object OperatingCarrier { get; set; }
            public object OperatingOpSuffix { get; set; }
            public string SubjectToGovtApproval { get; set; }
            public object MarketingCode { get; set; }
            public string ChangeOfDirection { get; set; }
            public string MarketingOverride { get; set; }
        }

        public class Leg
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public string ArrivalStation { get; set; }
            public string DepartureStation { get; set; }
            public DateTime STA { get; set; }
            public DateTime STD { get; set; }
            public FlightDesignator2 FlightDesignator { get; set; }
            public LegInfo LegInfo { get; set; }
            public string InventoryLegID { get; set; }
        }

        public class Legs
        {
            public Leg Leg { get; set; }
        }

        public class PaxSegment
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public object BoardingSequence { get; set; }
            public DateTime CreatedDate { get; set; }
            public string LiftStatus { get; set; }
            public object OverBookIndicator { get; set; }
            public string PassengerNumber { get; set; }
            public DateTime PriorityDate { get; set; }
            public string TripType { get; set; }
            public string TimeChanged { get; set; }
            public object VerifiedTravelDocs { get; set; }
            public DateTime ModifiedDate { get; set; }
            public DateTime ActivityDate { get; set; }
            public string BaggageAllowanceWeight { get; set; }
            public string BaggageAllowanceWeightType { get; set; }
            public string BaggageAllowanceUsed { get; set; }
        }

        public class PaxSegments
        {
            // public PaxSegment PaxSegment { get; set; }
            public object PaxSegment { get; set; } //Change
        }

        public class Segment
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public string ActionStatusCode { get; set; }
            public string ArrivalStation { get; set; }
            public object CabinOfService { get; set; }
            public object ChangeReasonCode { get; set; }
            public string DepartureStation { get; set; }
            public object PriorityCode { get; set; }
            public object SegmentType { get; set; }
            public DateTime STA { get; set; }
            public DateTime STD { get; set; }
            public string International { get; set; }
            public FlightDesignator FlightDesignator { get; set; }
            public Fares Fares { get; set; }
            //public Legs Legs { get; set; }
            public object Legs { get; set; }
            public object PaxBags { get; set; }
            public object PaxSeats { get; set; }
            public object PaxSSRs { get; set; }
            public PaxSegments PaxSegments { get; set; }
            public object PaxTickets { get; set; }
            public DateTime SalesDate { get; set; }
            public string SegmentSellKey { get; set; }
            public object PaxScores { get; set; }
            public string ChannelType { get; set; }
        }

        public class Segments
        {
            //public List<Segment> Segment { get; set; }
            // public Segment Segment { get; set; }
            // public Segment[] Segment { get; set; }
            public object Segment { get; set; }
        }

        public class Journey
        {
            public object ExtensionData { get; set; }
            public string State { get; set; }
            public string NotForGeneralUse { get; set; }
            public Segments Segments { get; set; }
            public string JourneySellKey { get; set; }
        }

        public class Journeys
        {
            public List<Journey> Journey { get; set; }
        }

        public class Booking
        {
            public object ExtensionData { get; set; }
            public object OtherServiceInfoList { get; set; }
            public string State { get; set; }
            public object RecordLocator { get; set; }
            public string CurrencyCode { get; set; }
            public string PaxCount { get; set; }
            public object SystemCode { get; set; }
            public string BookingID { get; set; }
            public string BookingParentID { get; set; }
            public object ParentRecordLocator { get; set; }
            public object BookingChangeCode { get; set; }
            public object GroupName { get; set; }
            public BookingInfo BookingInfo { get; set; }
            public POS POS { get; set; }
            public SourcePOS SourcePOS { get; set; }
            public TypeOfSale TypeOfSale { get; set; }
            public BookingSum BookingSum { get; set; }
            public ReceivedBy1 ReceivedBy { get; set; }
            public object RecordLocators { get; set; }
            public Passengers Passengers { get; set; }
            public Journeys Journeys { get; set; }
            public object BookingComments { get; set; }
            public object BookingQueueInfos { get; set; }
            public object BookingContacts { get; set; }
            public object Payments { get; set; }
            public object BookingComponents { get; set; }
            public object NumericRecordLocator { get; set; }
        }

        public class PriceItineraryResponse
        {
            public Booking Booking { get; set; }
        }

        public class RootObject
        {
            public PriceItineraryResponse PriceItineraryResponse { get; set; }
        }

        #endregion Reprice Class

    }



}
