﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Xml.Linq;
using System.Xml;
using System.Collections;
using System.Data;
using System.Reflection;

namespace PNRImport
{
    public class IAPNRImport
    {


        public static string PostXml(String XMLIn, String Module_Version)
        {
            string rawResponse = string.Empty;
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(XMLIn);
                 //HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://test.webservices.amadeus.com");
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://production.webservices.amadeus.com");
                request.Headers.Add(String.Format("SOAPAction: \"{0}\"", Module_Version));
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = @"text/xml;charset=utf-8";
                request.Credentials = CredentialCache.DefaultCredentials;
                request.ProtocolVersion = HttpVersion.Version11;
                request.Timeout = 3000000;
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }
                    rawResponse = GetWebResponseString(response);
                    return rawResponse;
                }
            }
            catch (WebException wex)
            {
                throw wex;
                // We issued an HttpWebRequest, so the response will be an HttpWebResponse.
                //HttpWebResponse httpResponse = wex.Response as HttpWebResponse;

                //// Grab the response stream.           
                //using (Stream responseStream = httpResponse.GetResponseStream())
                //{
                //    using (StreamReader reader = new StreamReader(responseStream))
                //    {
                //        // Grab the body of the response as a string for parsing.
                //        rawResponse = GetWebResponseString(httpResponse);
                //        //IBE.Shared.LogUtility.LogInfo(rawResponse, "SoapFaults", IBE.Shared.Utilities.GetCurrentTimestamp());[commented HS 11-10-2011]
                //        //ExceptionLogger objExceptionLogger = new ExceptionLogger(ExceptionLogger.ERROR_SOURCE.Services, wex, "698", rawResponse, string.Empty, "");
                //        throw wex;
                //    }
                //}
            }

            catch (Exception ex)
            {
                //ExceptionLogger objExceptionLogger = new ExceptionLogger(ExceptionLogger.ERROR_SOURCE.Services, ex, "699", rawResponse, string.Empty, "");
                throw ex;
            }
            return rawResponse;
        }
        private static string GetWebResponseString(HttpWebResponse myHttpWebResponse)
        {
            StringBuilder rawResponse = new StringBuilder();
            Stream streamResponse;
            using (streamResponse = myHttpWebResponse.GetResponseStream())
            {
                if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    streamResponse = new GZipStream(streamResponse, CompressionMode.Decompress);
                else if (myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    streamResponse = new DeflateStream(streamResponse, CompressionMode.Decompress);
                using (StreamReader streamRead = new StreamReader(streamResponse))
                {
                    Char[] readBuffer = new Char[256];
                    int count = streamRead.Read(readBuffer, 0, 256);

                    while (count > 0)
                    {
                        String resultData = new String(readBuffer, 0, count);
                        rawResponse.Append(resultData);
                        count = streamRead.Read(readBuffer, 0, 256);
                    }
                }
            }
            return rawResponse.ToString();
        }
        public string GetSessionXML(string SessionId, string PNR)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:pnr='http://xml.amadeus.com/PNRADD_11_1_1A'>");
            XmlBuilder.Append("<soapenv:Header>");
            XmlBuilder.Append(GetSessionHeader(SessionId));// ("<SessionId>" + SessionId + "</SessionId>");
            XmlBuilder.Append("</soapenv:Header>");
            XmlBuilder.Append("<soapenv:Body>");
            XmlBuilder.Append("<PNR_AddMultiElements>");
            XmlBuilder.Append("<reservationInfo>");
            XmlBuilder.Append("<reservation>");
            XmlBuilder.Append("<controlNumber>" + PNR + "</controlNumber>");
            XmlBuilder.Append("</reservation>");
            XmlBuilder.Append("</reservationInfo>");
            XmlBuilder.Append("<pnrActions>");
            XmlBuilder.Append("<optionCode>0</optionCode>");
            XmlBuilder.Append(" </pnrActions>");
            XmlBuilder.Append("</PNR_AddMultiElements>");
            XmlBuilder.Append("</soapenv:Body>");
            XmlBuilder.Append("</soapenv:Envelope>");
            return XmlBuilder.ToString();
        }
        public DataSet PnrDetailsReply(string PNRNO)
        {
            DataSet dspnr = new DataSet();
            DataTable dt1 = new DataTable("CancelStatus");
            try
            {
                StringBuilder XmlBuilder = new StringBuilder();
                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:vls='http://xml.amadeus.com/VLSSLQ_06_1_1A'>");
                XmlBuilder.Append("<soapenv:Header>");
                XmlBuilder.Append("<wbs:Session>");
                XmlBuilder.Append("<wbs:SessionId></wbs:SessionId>");
                XmlBuilder.Append("<wbs:SequenceNumber></wbs:SequenceNumber>");
                XmlBuilder.Append("<wbs:SecurityToken></wbs:SecurityToken>");
                XmlBuilder.Append("</wbs:Session>");
                XmlBuilder.Append("</soapenv:Header>");
                XmlBuilder.Append("<soapenv:Body>");
                XmlBuilder.Append("<Security_Authenticate>");
                XmlBuilder.Append("<userIdentifier>");
                XmlBuilder.Append("<originIdentification>");
                XmlBuilder.Append("<sourceOffice>DELVS32VB</sourceOffice>");//DELVS384Q
                XmlBuilder.Append("</originIdentification>");
                XmlBuilder.Append("<originatorTypeCode>U</originatorTypeCode>");
                XmlBuilder.Append("<originator>WSLOK</originator>");
                XmlBuilder.Append("</userIdentifier>");
                XmlBuilder.Append("<dutyCode>");
                XmlBuilder.Append("<dutyCodeDetails>");
                XmlBuilder.Append("<referenceQualifier>DUT</referenceQualifier>");
                XmlBuilder.Append("<referenceIdentifier>SU</referenceIdentifier>");
                XmlBuilder.Append("</dutyCodeDetails>");
                XmlBuilder.Append("</dutyCode>");
                XmlBuilder.Append("<systemDetails>");
                XmlBuilder.Append("<organizationDetails>");
                XmlBuilder.Append("<organizationId>NMC-INDIA</organizationId>");//MCG-MMT
                XmlBuilder.Append("</organizationDetails>");
                XmlBuilder.Append("</systemDetails>");
                XmlBuilder.Append("<passwordInfo>");
                XmlBuilder.Append("<dataLength>8</dataLength>");
                XmlBuilder.Append("<dataType>E</dataType>");
                XmlBuilder.Append("<binaryData>clJwVjhGOVg=</binaryData>");
                XmlBuilder.Append("</passwordInfo>");
                XmlBuilder.Append("</Security_Authenticate>");
                XmlBuilder.Append("</soapenv:Body>");
                XmlBuilder.Append("</soapenv:Envelope>");
                string strsessionid = "";
                strsessionid = PostXml(XmlBuilder.ToString(), "http://webservices.amadeus.com/1ASIWLOK/VLSSLQ_06_1_1A");
                //XNamespace ns = "http://webservices.amadeus.com/definitions";
                //XNamespace soap = "http://schemas.xmlsoap.org/soap/envelope/";
                //XDocument xdoc = XDocument.Parse(strsessionid);
                //string session = (xdoc.Descendants(soap + "Header")).Descendants(ns + "SessionId").First().Value;
                XNamespace ns = "http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd";// "http://webservices.amadeus.com/definitions";
                XNamespace soap = "http://schemas.xmlsoap.org/soap/envelope/";
                XDocument xdoc = XDocument.Parse(strsessionid);
                string session = xdoc.Descendants(ns + "SessionId").First().Value + "|" + xdoc.Descendants(ns + "SequenceNumber").First().Value + "|" + xdoc.Descendants(ns + "SecurityToken").First().Value;//(xdoc.Descendants(soap + "Header")).Descendants(ns + "Session").Descendants("SessionId").First().Value;               
                string[] SID = session.Split('|');
                string SessionId = SID[0] + "|" + Convert.ToInt32(Convert.ToInt32(SID[1]) + 1) + "|" + SID[2];
                string sessionrequest = GetSessionXML(SessionId, PNRNO);//3X533V
                string PNR = "";
                try
                {
                    PNR = PostXml(sessionrequest, "http://webservices.amadeus.com/1ASIWLOK/PNRADD_08_2_1A");

                    //Session Out
                    StringBuilder SOUT = new StringBuilder();
                    SOUT.Append("<?xml version='1.0' encoding='utf-8'?>");
                    SOUT.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:vls='http://xml.amadeus.com/VLSSOQ_04_1_1A'>");
                    SOUT.Append("<soapenv:Header>");
                    SOUT.Append(GetSessionHeader(SessionId));//XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                    SOUT.Append("</soapenv:Header>");
                    SOUT.Append("<soapenv:Body>");
                    SOUT.Append("<Security_SignOut/>");
                    SOUT.Append("</soapenv:Body>");
                    SOUT.Append("</soapenv:Envelope>");
                    PostXml(SOUT.ToString(), "http://webservices.amadeus.com/1ASIWLOK/VLSSOQ_04_1_1A");

                }
                catch (Exception ex)
                {

                }
                if (PNR != "")
                {
                    XNamespace ns1 = "http://xml.amadeus.com/PNRACC_08_2_1A";
                    XDocument xdoc1 = XDocument.Parse(PNR);
                    var PnrRoot = (xdoc1.Descendants(soap + "Body")).Descendants(ns1 + "PNR_Reply").First();
                    Hashtable PnrDetails = new Hashtable();
                    // XDocument xdoc2 = XDocument.Parse(PnrRoot.ToString().Remove(11, 45));
                    XDocument xdoc2 = XDocument.Parse(PnrRoot.ToString().Replace("xmlns=\"http://xml.amadeus.com/PNRACC_08_2_1A\"", ""));
                    var FLIGHTSEGMENT = from IMP in xdoc2.Descendants("itineraryInfo")
                                        where IMP.Descendants("relatedProduct").Elements("status").Any() == true
                                        select new
                                        {
                                            loc1 = IMP.Descendants("boardpointDetail").Elements("cityCode").First().Value,
                                            loc2 = IMP.Descendants("offpointDetail").Elements("cityCode").First().Value,
                                            Depdate = IMP.Descendants("product").Elements("depDate").First().Value, //IMP.Element("Dt").Value.ToString().Substring(6, 2) + IMP.Element("Dt").Value.ToString().Substring(4, 2) + IMP.Element("Dt").Value.ToString().Substring(2, 2),//.Substring(5,6),
                                            Deptime = IMP.Descendants("product").Elements("depTime").First().Value,
                                            ArrDate = IMP.Descendants("product").Elements("arrDate").First().Value,
                                            Arrtime = IMP.Descendants("product").Elements("arrTime").First().Value,
                                            FlightNumber = IMP.Descendants("productDetails").Elements("identification").First().Value,
                                            RBD = IMP.Descendants("productDetails").Elements("classOfService").First().Value,
                                            Airline = IMP.Descendants("companyDetail").Elements("identification").First().Value
                                        };


                    var PAX = from IMP in xdoc2.Descendants("travellerInfo").Descendants("passengerData").Descendants("travellerInformation")
                              select new
                              {
                                  Traveller = IMP,
                              };
                    ArrayList PAXLIST = new ArrayList();
                    ArrayList ECTPAXLIST = new ArrayList();
                    foreach (var abc in PAX)
                    {
                        string SirName = (abc.Traveller.Element("traveller").Element("surname").Value);
                        foreach (var px in abc.Traveller.Descendants("passenger"))
                        {
                            string[] FN = px.Element("firstName").Value.ToString().Split(' ');
                            string FirstName = "";
                            if (FN.Length > 1)
                            {
                                // FirstName = FN[1].ToString() + " " + FN[0].ToString();
                                FirstName = FN[FN.Length - 1].ToString() + " ";
                                for (int l = 0; l <= FN.Length - 2; l++)
                                {
                                    FirstName = FirstName + FN[l].ToString() + " ";
                                }
                                FirstName = FirstName.Trim();
                            }
                            else
                            {
                                FirstName = FN[0].ToString();
                            }
                            PAXLIST.Add(
                             new PXNAME
                             {
                                 FName = FirstName,
                                 LName = SirName,

                             });
                        }
                    }
                    //Check Status
                    var ChkStatus = from IMP in xdoc2.Descendants("originDestinationDetails").Descendants("itineraryInfo")
                                    select new
                                    {
                                        chkstatus = IMP.Descendants("relatedProduct").Elements("status").First().Value
                                    };
                    string chkstatus = "";
                    string flag = "0";
                    foreach (var ii in ChkStatus)
                    {
                        chkstatus = chkstatus + ii;
                    }
                    DataTable Segment = new DataTable("SegmentDetails");
                    Segment = LINQToDataTable(FLIGHTSEGMENT);
                    DataTable PaxName = new DataTable("TktDetails");
                    PaxName = ConvertArrayToDatatable(PAXLIST);

                    if (Segment.Rows.Count > 0)
                    {
                        if (chkstatus.Contains("HK") || chkstatus.Contains("KL") || chkstatus.Contains("KK") || chkstatus.Contains("TK"))
                        {
                            flag = "0";
                        }
                        else
                        {
                            flag = "1";
                            //break; // TODO: might not be correct. Was : Exit For
                        }
                        if (PnrRoot.ToString().Contains("<originDestinationDetails>") && flag == "0")
                        {
                            dspnr.Tables.Add(Segment);
                            dspnr.Tables.Add(PaxName);
                        }
                        else
                        {
                            dt1.Columns.Add("CancelStatus", typeof(String));
                            dt1.Rows.Add("Either The PNR Has Been Canceled Or Some Problem In The PNR");
                            dspnr.Tables.Add(dt1);
                            //return "<PnrDetailsReply><errorMessage>Either The PNR Has Been Canceled Or Some Problem In The PNR </errorMessage><errorMessage>Please Check Offline </errorMessage></PnrDetailsReply>";
                        }
                    }
                }
                else
                {
                    dt1.Columns.Add("CancelStatus", typeof(String));
                    dt1.Rows.Add("Either The PNR Has Been Canceled Or Some Problem In The PNR");
                    dspnr.Tables.Add(dt1);
                }

            }
            catch (Exception ex)
            {
                dt1.Columns.Add("CancelStatus", typeof(String));
                dt1.Rows.Add("Either The PNR Has Been Canceled Or Some Problem In The PNR");
                dspnr.Tables.Add(dt1);

            }


            return dspnr;
        }
        private string GetSessionHeader(string SH)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            string[] ss = SH.Split('|');
            XmlBuilder.Append("<wbs:Session>");
            XmlBuilder.Append("<wbs:SessionId>" + ss[0].ToString() + "</wbs:SessionId>");
            XmlBuilder.Append("<wbs:SequenceNumber>" + ss[1].ToString() + "</wbs:SequenceNumber>");
            XmlBuilder.Append("<wbs:SecurityToken>" + ss[2].ToString() + "</wbs:SecurityToken>");
            XmlBuilder.Append("</wbs:Session>");
            return XmlBuilder.ToString();
        }
        public DataTable LINQToDataTable<T>(IEnumerable<T> varlist)
        {
            DataTable dtReturn;
            dtReturn = new DataTable("SegmentDetails");
            // column names 
            System.Reflection.PropertyInfo[] oProps = null;

            if (varlist == null) return dtReturn;

            foreach (T rec in varlist)
            {
                // Use reflection to get property names, to create table, Only first time, others 
                //will follow 
                if (oProps == null)
                {
                    oProps = ((Type)rec.GetType()).GetProperties();
                    foreach (PropertyInfo pi in oProps)
                    {
                        Type colType = pi.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                        == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                    }
                }

                DataRow dr = dtReturn.NewRow();

                foreach (PropertyInfo pi in oProps)
                {
                    dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue
                    (rec, null);
                }

                dtReturn.Rows.Add(dr);
            }
            return dtReturn;
        }
        public DataTable ConvertArrayToDatatable(ArrayList arrList)
        {
            DataTable dt = new DataTable();
            try
            {
                if (arrList.Count > 0)
                {
                    Type arrype = arrList[0].GetType();
                    dt = new DataTable("TktDetails");

                    foreach (PropertyInfo propInfo in arrype.GetProperties())
                    {
                        dt.Columns.Add(new DataColumn(propInfo.Name));
                    }

                    foreach (object obj in arrList)
                    {
                        DataRow dr = dt.NewRow();

                        foreach (DataColumn dc in dt.Columns)
                        {
                            dr[dc.ColumnName] = obj.GetType().GetProperty(dc.ColumnName).GetValue(obj, null);
                        }
                        dt.Rows.Add(dr);

                    }
                }


                return dt;
            }
            catch (Exception ex)
            {
                return dt;
            }

        }
        public class PXNAME
        {
            public string FName { get; set; }
            public string LName { get; set; }

        }
        public class PXNAMEREAL
        {
            public string FstName { get; set; }
            public string LstName { get; set; }

        }
    }
}
