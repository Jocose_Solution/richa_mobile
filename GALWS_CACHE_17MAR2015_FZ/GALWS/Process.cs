﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using STD.Shared;
using System.Collections;


namespace STD.BAL
{
    /// <summary>
    /// Summary description for Process
    /// </summary>
    public class Process
    {
        public string connectionString {get;set; }
        public Process()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public ArrayList GetFltSearchResponse(FlightSearch searchParam)
        {
            FltResult objFltRes = new FltResult(searchParam);
            objFltRes.connectionString = connectionString;
            TransContext context = new TransContext(objFltRes);
            return context.ContextCall<ArrayList>();
        }
        public ArrayList GetFltSearchResponseCoupon(FlightSearch searchParam)
        {
            FltResult objFltRes = new FltResult(searchParam);
            objFltRes.connectionString = connectionString;
            TransContext context = new TransContext(objFltRes);
            return context.ContextCallCoupon<ArrayList>();
        }
    }
}