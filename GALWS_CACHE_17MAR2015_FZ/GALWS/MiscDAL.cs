﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Data;
using STD.Shared;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace STD.DAL
{
    /// <summary>
    /// Credentials for all the providers
    /// </summary>
    public class Credentials
    {
        private SqlDatabase DBHelper;

        public Credentials(string connectionString)
        {
            DBHelper = new SqlDatabase(connectionString);
        }
        /// <summary>
        /// Get the credentials for API/GDS Provider
        /// </summary>
        /// <returns>list of credentials</returns>
        //public List<CredentialList> GetServiceCredentials()
        //{
        //    List<CredentialList> SrvCrdlist = new List<CredentialList>();
        //    DbCommand sqlcmd = new SqlCommand();
        //    sqlcmd.CommandText = "SP_DIST_SELECT_SRVCREDENTIALS";
        //    sqlcmd.CommandType = CommandType.StoredProcedure;
        //    IDataReader idr = DBHelper.ExecuteReader(sqlcmd);

        //    while (idr.Read())
        //    {
        //        CredentialList objCrd = new CredentialList();
        //        objCrd.UserID = Convert.ToString(idr["USERID"]).Trim();
        //        objCrd.CorporateID = Convert.ToString(idr["CORPORATEID"]).Trim();
        //        objCrd.Password = Convert.ToString(idr["PASSWORD"]).Trim();
        //        objCrd.LoginID = Convert.ToString(idr["LOGINID"]).Trim();
        //        objCrd.LoginPWD = Convert.ToString(idr["LOGINPWD"]).Trim();
        //        objCrd.Port = Convert.ToInt16(idr["PORT"]);
        //        objCrd.AvailabilityURL = Convert.ToString(idr["SERVERURLORIP"]).Trim();
        //        objCrd.BookingURL = Convert.ToString(idr["BKGSERVERURLORIP"]).Trim();
        //        objCrd.Provider = Convert.ToString(idr["PROVIDER"]).Trim();
        //        objCrd.APISource = Convert.ToString(idr["APISOURCE"]).Trim();
        //        objCrd.CarrierAcc = Convert.ToString(idr["CARRIERACC"]).Trim();
        //        objCrd.ServerIP = Convert.ToString(idr["SERVERIP"]).Trim();
        //        objCrd.INFBasic = Convert.ToDouble(idr["INFBASIC"]);
        //        objCrd.INFTax = Convert.ToDouble(idr["INFTAX"]);
        //        SrvCrdlist.Add(objCrd);

        //    }
        //    idr.Close();
        //    idr.Dispose();
        //    return SrvCrdlist;
        //}

        public List<CredentialList> GetServiceCredentials(string Provider)
        {
            List<CredentialList> SrvCrdlist = new List<CredentialList>();
            try
            {
                DbCommand sqlcmd = new SqlCommand();
                sqlcmd.CommandText = "GetSrvCredentials";
                sqlcmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(sqlcmd, "@Provider", DbType.String, Provider);
                IDataReader idr = DBHelper.ExecuteReader(sqlcmd);

                while (idr.Read())
                {
                    CredentialList objCrd = new CredentialList();
                    objCrd.UserID = Convert.ToString(idr["UserID"]).Trim();
                    objCrd.CorporateID = Convert.ToString(idr["CorporateID"]).Trim();
                    objCrd.Password = Convert.ToString(idr["Password"]).Trim();
                    objCrd.LoginID = Convert.ToString(idr["LoginID"]).Trim();
                    objCrd.LoginPWD = Convert.ToString(idr["LoginPwd"]).Trim();
                    objCrd.Port = Convert.ToInt32(idr["Port"]);
                    objCrd.AvailabilityURL = Convert.ToString(idr["ServerUrlOrIP"]).Trim();
                    objCrd.BookingURL = Convert.ToString(idr["BkgServerUrlOrIP"]).Trim();
                    objCrd.Provider = Convert.ToString(idr["Provider"]).Trim();
                    objCrd.APISource = Convert.ToString(idr["APISource"]).Trim();
                    objCrd.CarrierAcc = Convert.ToString(idr["CarrierAcc"]).Trim();
                    objCrd.ServerIP = Convert.ToString(idr["ServerIP"]).Trim();
                    objCrd.INFBasic = Convert.ToDouble(idr["InfBasic"]);
                    objCrd.INFTax = Convert.ToDouble(idr["InfTax"]);
                    objCrd.SearchType = Convert.ToString(idr["SearchType"]).Trim();

                    objCrd.Status = Convert.ToBoolean(idr["Status"]);
                    objCrd.ResultFrom = Convert.ToString(idr["ResultFrom"]).Trim();
                    objCrd.CrdType = Convert.ToString(idr["CrdType"]).Trim();

                    objCrd.Trip = Convert.ToString(idr["TripType"]).Trim();
                    objCrd.AirlineCode = Convert.ToString(idr["AirlineCode"]).Trim();

                    objCrd.Cache = Convert.ToBoolean(idr["Cache"]);
                    objCrd.WebResult = Convert.ToBoolean(idr["WebResult"]);
                    SrvCrdlist.Add(objCrd);

                }
                idr.Close();
                idr.Dispose();
            }
            catch (Exception ex)
            {
                var abc = ex.Message.ToString();
            }
            return SrvCrdlist;
        }




        public List<CredentialList> GetCouponCRD()
        {
            List<CredentialList> SrvCrdlist = new List<CredentialList>();
            try
            {
                DbCommand sqlcmd = new SqlCommand();
                sqlcmd.CommandText = "usp_Get_Coupon_Crd";
                sqlcmd.CommandType = CommandType.StoredProcedure;
                 IDataReader idr = DBHelper.ExecuteReader(sqlcmd);

                while (idr.Read())
                {
                    CredentialList objCrd = new CredentialList();
                    objCrd.UserID = Convert.ToString(idr["UserID"]).Trim();
                    objCrd.CorporateID = Convert.ToString(idr["CorporateID"]).Trim();
                    objCrd.Password = Convert.ToString(idr["Pass"]).Trim();
                    objCrd.Provider = Convert.ToString(idr["Airline"]).Trim();
                    //objCrd.LoginPWD = Convert.ToString(idr["LoginPwd"]).Trim();
                    //objCrd.Port = Convert.ToInt32(idr["Port"]);
                    //objCrd.AvailabilityURL = Convert.ToString(idr["ServerUrlOrIP"]).Trim();
                    //objCrd.BookingURL = Convert.ToString(idr["BkgServerUrlOrIP"]).Trim();
                    //objCrd.Provider = Convert.ToString(idr["Provider"]).Trim();
                    //objCrd.APISource = Convert.ToString(idr["APISource"]).Trim();
                    //objCrd.CarrierAcc = Convert.ToString(idr["CarrierAcc"]).Trim();
                    //objCrd.ServerIP = Convert.ToString(idr["ServerIP"]).Trim();
                    //objCrd.INFBasic = Convert.ToDouble(idr["InfBasic"]);
                    //objCrd.INFTax = Convert.ToDouble(idr["InfTax"]);
                    //objCrd.SearchType = Convert.ToString(idr["SearchType"]).Trim();
                    SrvCrdlist.Add(objCrd);

                }
                idr.Close();
                idr.Dispose();
            }
            catch (Exception ex)
            {
                var abc = ex.Message.ToString();
            }
            return SrvCrdlist;
        }




        public List<CredentialList> GetGALBookingCredentials(string Provider, string Trip, string VC, string crdType)
        {
            List<CredentialList> SrvCrdlist = new List<CredentialList>();
            DbCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = "SP_GETCREDENTIAL_PNRCREATION";
            sqlcmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(sqlcmd, "@Trip", DbType.String, Trip);
            DBHelper.AddInParameter(sqlcmd, "@VC", DbType.String, VC);
            DBHelper.AddInParameter(sqlcmd, "@PROVIDER", DbType.String, Provider);
            DBHelper.AddInParameter(sqlcmd, "@CrdType", DbType.String, crdType);
            IDataReader idr = DBHelper.ExecuteReader(sqlcmd);

            while (idr.Read())
            {
                CredentialList objCrd = new CredentialList();
                objCrd.UserID = Convert.ToString(idr["UserID"]).Trim();
                objCrd.CorporateID = Convert.ToString(idr["CorporateID"]).Trim();
                objCrd.Password = Convert.ToString(idr["Password"]).Trim();
                objCrd.LoginID = Convert.ToString(idr["LoginID"]).Trim();
                objCrd.LoginPWD = Convert.ToString(idr["LoginPwd"]).Trim();
                objCrd.Port = Convert.ToInt32(idr["Port"]);
                objCrd.AvailabilityURL = Convert.ToString(idr["ServerUrlOrIP"]).Trim();
                objCrd.BookingURL = Convert.ToString(idr["BkgServerUrlOrIP"]).Trim();
                objCrd.Provider = Convert.ToString(idr["Provider"]).Trim();
                objCrd.APISource = Convert.ToString(idr["APISource"]).Trim();
                objCrd.CarrierAcc = Convert.ToString(idr["CarrierAcc"]).Trim();
                objCrd.ServerIP = Convert.ToString(idr["ServerIP"]).Trim();
                objCrd.INFBasic = Convert.ToDouble(idr["InfBasic"]);
                objCrd.INFTax = Convert.ToDouble(idr["InfTax"]);
                objCrd.SearchType = Convert.ToString(idr["SearchType"]).Trim();
                SrvCrdlist.Add(objCrd);

            }
            idr.Close();
            idr.Dispose();
            return SrvCrdlist;
        }
        public List<CredentialList> GetGALBookingCredentialsSeat(string Provider, string Trip, string VC, string crdType)
        {
            List<CredentialList> SrvCrdlist = new List<CredentialList>();
            DbCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = "SP_GETCREDENTIAL_PNRCREATION_V1";
            sqlcmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(sqlcmd, "@Trip", DbType.String, Trip);
            DBHelper.AddInParameter(sqlcmd, "@VC", DbType.String, VC);
            DBHelper.AddInParameter(sqlcmd, "@PROVIDER", DbType.String, Provider);
            DBHelper.AddInParameter(sqlcmd, "@CrdType", DbType.String, crdType);
            IDataReader idr = DBHelper.ExecuteReader(sqlcmd);

            while (idr.Read())
            {
                CredentialList objCrd = new CredentialList();
                objCrd.UserID = Convert.ToString(idr["UserID"]).Trim();
                objCrd.CorporateID = Convert.ToString(idr["CorporateID"]).Trim();
                objCrd.Password = Convert.ToString(idr["Password"]).Trim();
                objCrd.LoginID = Convert.ToString(idr["LoginID"]).Trim();
                objCrd.LoginPWD = Convert.ToString(idr["LoginPwd"]).Trim();
                objCrd.Port = Convert.ToInt32(idr["Port"]);
                objCrd.AvailabilityURL = Convert.ToString(idr["ServerUrlOrIP"]).Trim();
                objCrd.BookingURL = Convert.ToString(idr["BkgServerUrlOrIP"]).Trim();
                objCrd.Provider = Convert.ToString(idr["Provider"]).Trim();
                objCrd.APISource = Convert.ToString(idr["APISource"]).Trim();
                objCrd.CarrierAcc = Convert.ToString(idr["CarrierAcc"]).Trim();
                objCrd.ServerIP = Convert.ToString(idr["ServerIP"]).Trim();
                objCrd.INFBasic = Convert.ToDouble(idr["InfBasic"]);
                objCrd.INFTax = Convert.ToDouble(idr["InfTax"]);
                objCrd.SearchType = Convert.ToString(idr["SearchType"]).Trim();
                SrvCrdlist.Add(objCrd);

            }
            idr.Close();
            idr.Dispose();
            return SrvCrdlist;
        }
        public List<CredentialList> GetGalTicketingCrd(string Provider, string Trip, string VC, string crdType)
        {
            List<CredentialList> TktCrdlist = new List<CredentialList>();
            DbCommand sqlcmd = new SqlCommand();
            sqlcmd.CommandText = "GetTicketingCrdGAL";
            sqlcmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(sqlcmd, "@trip", DbType.String, Trip);
            DBHelper.AddInParameter(sqlcmd, "@VC", DbType.String, VC);
           // DBHelper.AddInParameter(sqlcmd, "@PROVIDER", DbType.String, Provider);
            DBHelper.AddInParameter(sqlcmd, "@CrdType", DbType.String, crdType);
            IDataReader idr = DBHelper.ExecuteReader(sqlcmd);
            while (idr.Read())
            {               
                CredentialList objCrd = new CredentialList();
                objCrd.UserID = Convert.ToString(idr["USERID"]).Trim();
                objCrd.CorporateID = Convert.ToString(idr["HAP"]).Trim();
                objCrd.Password = Convert.ToString(idr["PWD"]).Trim();
                objCrd.LoginID = "";// Convert.ToString(idr["LoginID"]).Trim();
                objCrd.LoginPWD = ""; //Convert.ToString(idr["LoginPwd"]).Trim();
                objCrd.Port = 0;//Convert.ToInt32(idr["Port"]);
                objCrd.AvailabilityURL =Convert.ToString(idr["URL"]).Trim(); //;//Convert.ToString(idr["ServerUrlOrIP"]).Trim();
                objCrd.BookingURL = Convert.ToString(idr["URL"]).Trim(); //Convert.ToString(idr["BkgServerUrlOrIP"]).Trim();
                objCrd.Provider = "1G";//Convert.ToString(idr["Provider"]).Trim();
                objCrd.APISource = "";// Convert.ToString(idr["APISource"]).Trim();
                objCrd.CarrierAcc = Convert.ToString(idr["PCC"]).Trim();//Convert.ToString(idr["CarrierAcc"]).Trim();
                objCrd.ServerIP = "";// Convert.ToString(idr["ServerIP"]).Trim();
                objCrd.INFBasic = 0;//Convert.ToDouble(idr["InfBasic"]);
                objCrd.INFTax = 0;//Convert.ToDouble(idr["InfTax"]);
                objCrd.SearchType = "BKG";//Convert.ToString(idr["SearchType"]).Trim();
                TktCrdlist.Add(objCrd);

            }
            idr.Close();
            idr.Dispose();
            return TktCrdlist;
        }
    }
    /// <summary>
    /// Class for Airline charges and Markups
    /// </summary>
    public class FltCharges
    {
        private SqlDatabase DBHelper;

        public FltCharges(string connectionString)
        {
            DBHelper = new SqlDatabase(connectionString);
        }
        public List<FltSrvChargeList> GetSrvCharges(string jrnyType)
        {
            List<FltSrvChargeList> SrvList = new List<FltSrvChargeList>();
            try
            {
                DbCommand DBCmd = new SqlCommand();
                DBCmd.CommandText = "ServiceChargeGAL";
                DBCmd.CommandType = CommandType.StoredProcedure;
                //DBHelper.AddInParameter(DBCmd, "@Airline", DbType.String, "");
                DBHelper.AddInParameter(DBCmd, "@Trip", DbType.String, jrnyType);
                IDataReader idr = DBHelper.ExecuteReader(DBCmd);
                //List<FltSrvChargeList> SrvList = new List<FltSrvChargeList>();
                while (idr.Read())
                {
                    FltSrvChargeList objSrvlst = new FltSrvChargeList();
                    objSrvlst.AirlineCode = idr["AIRLINECODE"].ToString();
                    objSrvlst.SrviceTax = (decimal)idr["SRVTAX"];
                    objSrvlst.TransactionFee = (decimal)idr["TRANFEE"];                    
                    objSrvlst.IATACommissiom = (decimal)idr["IATACOMM"];
                    SrvList.Add(objSrvlst);
                }
                idr.Close();
                idr.Dispose();
            }
            catch(Exception ex)
            {
                
            }
           
            return SrvList;
        }
    }


    public class FltDeal
    {
        private SqlDatabase DBHelper;

        public FltDeal(string connectionString)
        {
            DBHelper = new SqlDatabase(connectionString);
        }

        #region Code For PF,TC,DC,PC
        public DataTable GetCodeforPForDCFromDB(string CodeType, string Trip, string GroupType, string AgentId, string AirCode, string OrderId, string OrginAirport, string DestAirport, string OrginCountry, string DestCountry)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = new SqlCommand();
            try
            {
                //cmd.CommandText = "USP_FLT_Get_PForTCOrDCorPC";
                cmd.CommandText = "SpGetFlightDealCode";                
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@CodeType", DbType.String, CodeType);
                DBHelper.AddInParameter(cmd, "@TripType", DbType.String, Trip);
                DBHelper.AddInParameter(cmd, "@GroupType", DbType.String, GroupType);               
                DBHelper.AddInParameter(cmd, "@AgentId", DbType.String, AgentId);
                DBHelper.AddInParameter(cmd, "@AirCode", DbType.String, AirCode);
                DBHelper.AddInParameter(cmd, "@OrderId", DbType.String, OrderId);
                DBHelper.AddInParameter(cmd, "@OrginAirport", DbType.String, OrginAirport);
                DBHelper.AddInParameter(cmd, "@DestAirport", DbType.String, DestAirport);
                DBHelper.AddInParameter(cmd, "@OrginCountry", DbType.String, OrginCountry);
                DBHelper.AddInParameter(cmd, "@DestCountry", DbType.String, DestCountry);                
                dt.Load(DBHelper.ExecuteReader(cmd));
                cmd.Dispose();
            }
            catch { }
            return dt;
        }
        //public DataTable GetCodeforPForDCFromDB(string CorpCode, string Trip, string CodeType, string VC, string OrderId)
        //{
        //    DataTable dt = new DataTable();
        //    DbCommand cmd = new SqlCommand();
        //    try
        //    {
        //        //cmd.CommandText = "USP_FLT_Get_PForTCOrDCorPC";
        //        cmd.CommandText = "SpGetFlightDealCode";                
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        DBHelper.AddInParameter(cmd, "@TCCode", DbType.String, CorpCode);
        //        DBHelper.AddInParameter(cmd, "@Trip", DbType.String, Trip);
        //        DBHelper.AddInParameter(cmd, "@CodeType", DbType.String, CodeType);
        //        DBHelper.AddInParameter(cmd, "@VC", DbType.String, VC);
        //        DBHelper.AddInParameter(cmd, "@OrderId", DbType.String, OrderId);
        //        dt.Load(DBHelper.ExecuteReader(cmd));
        //        cmd.Dispose();
        //    }
        //    catch { }
        //    return dt;
        //}
        #endregion


        #region Get Search BlocK datatable
        public DataTable GetSearchResultBlockReccords(string CorpCode, string Trip, string CodeType, string VC, string OrderId)
        {
            DataTable dt = new DataTable();
            DbCommand cmd = new SqlCommand();
            try
            {               
                cmd.CommandText = "SpGetFlightDealCode";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@TCCode", DbType.String, CorpCode);
                DBHelper.AddInParameter(cmd, "@Trip", DbType.String, Trip);
                DBHelper.AddInParameter(cmd, "@CodeType", DbType.String, CodeType);
                DBHelper.AddInParameter(cmd, "@VC", DbType.String, VC);
                DBHelper.AddInParameter(cmd, "@OrderId", DbType.String, OrderId);
                dt.Load(DBHelper.ExecuteReader(cmd));
                cmd.Dispose();
            }
            catch(Exception ex) 
            {
            }
            return dt;
        }
        #endregion
    }

}