﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STD.BAL
{

    public interface ITrans
    {
        T ProcessRequest<T>() where T : class;
        T ProcessRequestCoupon<T>() where T : class;
    }
}