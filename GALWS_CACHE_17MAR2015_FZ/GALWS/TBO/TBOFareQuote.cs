﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STD.BAL.TBO.FareQuote
{
   public class Error
{
    public int ErrorCode { get; set; }
    public string ErrorMessage { get; set; }
}

public class ChargeBU
{
    public string key { get; set; }
    public double value { get; set; }
}

public class Fare
{
    public string Currency { get; set; }
    public decimal BaseFare { get; set; }
    public decimal  Tax { get; set; }
    public decimal YQTax { get; set; }
    public decimal AdditionalTxnFee { get; set; }
    public decimal AdditionalTxnFeeOfrd { get; set; }
    public decimal AdditionalTxnFeePub { get; set; }
    public double OtherCharges { get; set; }
    public List<ChargeBU> ChargeBU { get; set; }
    public decimal Discount { get; set; }
    public double PublishedFare { get; set; }
    public double CommissionEarned { get; set; }
    public double PLBEarned { get; set; }
    public double IncentiveEarned { get; set; }
    public double OfferedFare { get; set; }
    public double TdsOnCommission { get; set; }
    public double TdsOnPLB { get; set; }
    public double TdsOnIncentive { get; set; }
    public decimal ServiceFee { get; set; }
}

public class FareBreakdown
{
    public string Currency { get; set; }
    public int PassengerType { get; set; }
    public int PassengerCount { get; set; }
    public decimal BaseFare { get; set; }
    public decimal Tax { get; set; }
    public decimal YQTax { get; set; }
    public decimal AdditionalTxnFee { get; set; }
    public decimal AdditionalTxnFeeOfrd { get; set; }
    public decimal AdditionalTxnFeePub { get; set; }
}

public class FareRule
{
    public string Origin { get; set; }
    public string Destination { get; set; }
    public string Airline { get; set; }
    public string FareBasisCode { get; set; }
    public string FareRuleDetail { get; set; }
    public string FareRestriction { get; set; }
}

public class Results
{
    public string ResultIndex { get; set; }
    public int Source { get; set; }
    public bool IsLCC { get; set; }
    public bool IsRefundable { get; set; }
    public string AirlineRemark { get; set; }
    public Fare Fare { get; set; }
    public List<FareBreakdown> FareBreakdown { get; set; }
    public List<List<Segment>> Segments { get; set; }
    public object LastTicketDate { get; set; }
    public object TicketAdvisory { get; set; }
    public List<FareRule> FareRules { get; set; }
    public string AirlineCode { get; set; }
    public string ValidatingAirline { get; set; }
}

  public class Segment
    {
        public int TripIndicator { get; set; }
        public int SegmentIndicator { get; set; }
        public Airline Airline { get; set; }
        public string AirlinePNR { get; set; }
        public Origin Origin { get; set; }
        public Destination Destination { get; set; }
        public int Duration { get; set; }
        public int GroundTime { get; set; }
        public int Mile { get; set; }
        public bool StopOver { get; set; }
        public string StopPoint { get; set; }
        public string StopPointArrivalTime { get; set; }
        public string StopPointDepartureTime { get; set; }
        public string Craft { get; set; }
        public bool IsETicketEligible { get; set; }
        public string FlightStatus { get; set; }
        public string Status { get; set; }
    }

public class Response
{
    public Error Error { get; set; }
    public bool IsPriceChanged { get; set; }
    public int ResponseStatus { get; set; }
    public Results Results { get; set; }
    public string TraceId { get; set; }
}

public class FareQuoteResp
{
    public Response Response { get; set; }
}
}
