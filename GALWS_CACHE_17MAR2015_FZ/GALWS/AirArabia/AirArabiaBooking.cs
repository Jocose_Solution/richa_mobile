﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Threading.Tasks;
using ITZERRORLOG;

namespace GALWS.AirArabia
{
   public class AirArabiaBooking
    {
       public string AirAraiaBookingDetails(DataSet Credencilas, DataTable Fltdtlds, DataSet PaxDs, DataSet HeaderDs, ref ArrayList TktNoArray, DataSet AgencyDs)
       {
           string PNR = "";
           try
           {
               string BookingRequest=AirArabiaBookingRequest(Credencilas.Tables[0].Rows[0]["UserID"].ToString(), Credencilas.Tables[0].Rows[0]["Password"].ToString(),Credencilas.Tables[0].Rows[0]["CorporateID"].ToString(),  Fltdtlds,   PaxDs,  HeaderDs,  AgencyDs);
               string BookingResponse = AirArabiaUtility.AirArabiaPostXML(Credencilas.Tables[0].Rows[0]["LoginID"].ToString(), BookingRequest, Fltdtlds.Rows[0]["Searchvalue"].ToString());
               AirArabiaUtility.SaveFile(BookingRequest, "BookingReq_" + Fltdtlds.Rows[0]["Track_id"].ToString());
               AirArabiaUtility.SaveFile(BookingResponse, "BookingResp_" + Fltdtlds.Rows[0]["Track_id"].ToString());
 
               XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/";
               XNamespace ns1 = "http://www.opentravel.org/OTA/2003/05";
               if ( BookingResponse != "" )
               {
                   if (BookingResponse.Contains("<ns1:Success />") || BookingResponse.Contains("<ns1:Success/>"))
                   {
                       XDocument AirArabiaDocument = XDocument.Parse(BookingResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                       XElement BookingResult = AirArabiaDocument.Element(s + "Envelope").Element(s + "Body").Element(ns1 + "OTA_AirBookRS").Element(ns1 + "AirReservation");
                       PNR = BookingResult.Element(ns1 + "BookingReferenceID").Attribute("ID").Value;
                       TktNoArray = new ArrayList();

                       IEnumerable<XElement> Passengers = BookingResult.Element(ns1 + "TravelerInfo").Elements(ns1 + "AirTraveler");
                       foreach (var Passenger in Passengers)
                       {
                           TktNoArray.Add(Passenger.Element(ns1 + "TravelerRefNumber").Attribute("RPH").Value.Replace("G9|",""));
                       }
                   }
                   else
                       PNR = "AirArabia-FQ";
               }
               else
                   PNR =  "AirArabia-FQ";
           }
           catch (Exception ex)
           {
               ExecptionLogger.FileHandling("AirAraiaBookingDetails", "Error_001", ex, "AirArabiaFlight");
               PNR = PNR + "-FQ";
           }
           return PNR;
       }
       private string AirArabiaBookingRequest(string UserId, string Password, string Agencyid, DataTable Fltdtlds,  DataSet PaxDs, DataSet HeaderDs, DataSet AgencyDs )
       {
           StringBuilder objreqXml = new StringBuilder();
           try
           {
               DataRow[] ADTPax = PaxDs.Tables[0].Select("PaxType='ADT'", "PaxId ASC");
               DataRow[] CHDPax = PaxDs.Tables[0].Select("PaxType='CHD'", "PaxId ASC");
               DataRow[] INFPax = PaxDs.Tables[0].Select("PaxType='INF'", "PaxId ASC");
               //  //   SNNO(7)       Dim Originalrate As Decimal = Convert.ToDecimal(FltDs.Tables(0).Rows(0)("OriginalTF"))
               string[] SNO= Fltdtlds.Rows[0]["sno"].ToString().Split(':');
               #region Booking Request
               objreqXml.Append("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
               objreqXml.Append("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
               objreqXml.Append("<soap:Header>");
               objreqXml.Append("<wsse:Security soap:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
               objreqXml.Append("<wsse:UsernameToken wsu:Id=\"UsernameToken-17855236\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">");
               objreqXml.Append("<wsse:Username>" + UserId + "</wsse:Username>");
               objreqXml.Append("<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + Password + "</wsse:Password>");
               objreqXml.Append("</wsse:UsernameToken>");
               objreqXml.Append("</wsse:Security>");
               objreqXml.Append("</soap:Header>");
               objreqXml.Append("<soap:Body xmlns:ns1=\"http://www.isaaviation.com/thinair/webservices/OTA/Extensions/2003/05\" xmlns:ns2=\"http://www.opentravel.org/OTA/2003/05\">");
               objreqXml.Append("<ns2:OTA_AirBookRQ EchoToken=\"" + SNO[3] + "\" PrimaryLangID=\"en-us\" SequenceNmbr=\"1\" TimeStamp=\"" + DateTime.Now.AddMinutes(2).ToString("yyyy-MM-ddTHH:mm:ss") + "\" TransactionIdentifier=\"" + SNO[0] + "\" Version=\"" + SNO[4] + "\">");
               objreqXml.Append("<ns2:POS>");
               objreqXml.Append("<ns2:Source TerminalID=\"TestUser/Test Runner\">");
               objreqXml.Append("<ns2:RequestorID ID=\"" + UserId + "\" Type=\"4\" />");
               objreqXml.Append("<ns2:BookingChannel Type=\"12\" />");
               objreqXml.Append("</ns2:Source>");
               objreqXml.Append("</ns2:POS>");
               objreqXml.Append("<ns2:AirItinerary>");
               objreqXml.Append("<ns2:OriginDestinationOptions>");
#region Sector info
               for (int m = 0; m < Fltdtlds.Rows.Count; m++)
               {
                   string[] SNONew = Fltdtlds.Rows[m]["sno"].ToString().Split(':');
                   objreqXml.Append("<ns2:OriginDestinationOption>");
                   objreqXml.Append("<ns2:FlightSegment ArrivalDateTime=\"" + Convert.ToDateTime(Fltdtlds.Rows[m]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" DepartureDateTime=\"" + Convert.ToDateTime(Fltdtlds.Rows[m]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + Fltdtlds.Rows[m]["FlightIdentification"].ToString() + "\" RPH=\"" + SNONew[1] + "\">");
                   objreqXml.Append("<ns2:DepartureAirport LocationCode=\"" + Fltdtlds.Rows[m]["DepartureLocation"].ToString() + "\" Terminal=\"TerminalX\" />");
                   objreqXml.Append("<ns2:ArrivalAirport LocationCode=\"" + Fltdtlds.Rows[m]["ArrivalLocation"].ToString() + "\" Terminal=\"TerminalX\" />");
                   objreqXml.Append("</ns2:FlightSegment>");
                   objreqXml.Append("</ns2:OriginDestinationOption>");
               }
#endregion
               objreqXml.Append("</ns2:OriginDestinationOptions>");
               objreqXml.Append("</ns2:AirItinerary>");
               objreqXml.Append("<ns2:TravelerInfo>");
#region Pax Information
#region Adult
                for (int i = 0; i < ADTPax.Length; i++)
                {
                    objreqXml.Append(string.Format("<ns2:AirTraveler "));
                    if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["DOB"])) && Convert.ToString(ADTPax[i]["DOB"]).Trim().Length > 7)
                        objreqXml.Append("BirthDate=\"" + ADTPax[i]["DOB"].ToString().Split('/')[2] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[1] + "-" + ADTPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00\" ");
                    objreqXml.Append(" PassengerTypeCode=\"ADT\">");
                    objreqXml.Append("<ns2:PersonName>");
                    objreqXml.Append("<ns2:GivenName>" + ADTPax[i]["FName"].ToString());
                    if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["MName"])))
                        objreqXml.Append(" " + ADTPax[i]["MName"].ToString());
                    objreqXml.Append("</ns2:GivenName>");
                    objreqXml.Append("<ns2:Surname>" + ADTPax[i]["LName"].ToString() + "</ns2:Surname>");
                    objreqXml.Append("<ns2:NameTitle>" + ADTPax[i]["Title"].ToString().ToUpper() + "</ns2:NameTitle>");
                    objreqXml.Append("</ns2:PersonName>");
                    string countyocde = "IN";
                    if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[i]["NationalityCode"])))
                        countyocde=ADTPax[i]["NationalityCode"].ToString();
                    objreqXml.Append("<ns2:Telephone AreaCityCode=\""+ HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString().Substring(0,2) +"\" CountryAccessCode=\"091\" PhoneNumber=\"" + HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString().Substring(2, HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString().Length - 2) + "\"/>");
                    objreqXml.Append("<ns2:Address>");
                    objreqXml.Append("<ns2:CountryName Code=\"" + countyocde + "\"/>");
                    objreqXml.Append("</ns2:Address>");
                    objreqXml.Append("<ns2:Document DocHolderNationality=\"" + countyocde + "\"/>");
                    objreqXml.Append("<ns2:TravelerRefNumber RPH=\"A" + (i + 1).ToString() + "\"/>");
                    objreqXml.Append(string.Format("</ns2:AirTraveler>"));
                }
#endregion
#region Child
               if (CHDPax.Length > 0)
               {

                   for (int i = 0; i < CHDPax.Length; i++)
                   {
                        objreqXml.Append(string.Format("<ns2:AirTraveler "));
                    if (!string.IsNullOrEmpty(Convert.ToString(CHDPax[i]["DOB"])) && Convert.ToString(CHDPax[i]["DOB"]).Trim().Length > 7)
                        objreqXml.Append("BirthDate=\"" + CHDPax[i]["DOB"].ToString().Split('/')[2] + "-" + CHDPax[i]["DOB"].ToString().Split('/')[1] + "-" + CHDPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00\" ");
                    objreqXml.Append(" PassengerTypeCode=\"CHD\">");
                    objreqXml.Append("<ns2:PersonName>");
                    objreqXml.Append("<ns2:GivenName>" + CHDPax[i]["FName"].ToString());
                    if (!string.IsNullOrEmpty(Convert.ToString(CHDPax[i]["MName"])))
                        objreqXml.Append(" " + CHDPax[i]["MName"].ToString());
                    objreqXml.Append("</ns2:GivenName>");
                    objreqXml.Append("<ns2:Surname>" + CHDPax[i]["LName"].ToString() + "</ns2:Surname>");
                    objreqXml.Append("<ns2:NameTitle>" + CHDPax[i]["Title"].ToString().ToUpper() + "</ns2:NameTitle>");
                    objreqXml.Append("</ns2:PersonName>");
                    string countyocde="IN";
                    if (!string.IsNullOrEmpty(Convert.ToString(CHDPax[i]["NationalityCode"])))
                        countyocde=CHDPax[i]["NationalityCode"].ToString();
                    objreqXml.Append("<ns2:Telephone AreaCityCode=\"" + HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString().Substring(0, 2) + "\" CountryAccessCode=\"091\" PhoneNumber=\"" + HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString().Substring(2, HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString().Length - 2) + "\"/>");
                    objreqXml.Append("<ns2:Address>");
                    objreqXml.Append("<ns2:CountryName Code=\"" + countyocde + "\"/>");
                    objreqXml.Append("</ns2:Address>");
                    objreqXml.Append("<ns2:Document DocHolderNationality=\"" + countyocde + "\"/>");
                    objreqXml.Append("<ns2:TravelerRefNumber RPH=\"C" + (ADTPax.Length + i + 1).ToString() + "\"/>");
                    objreqXml.Append(string.Format("</ns2:AirTraveler>"));
                   }
               }
#endregion
#region Infant
               if (INFPax.Length > 0)
               {
                   for (int i = 0; i < INFPax.Length; i++)
                   {
                         objreqXml.Append("<ns2:AirTraveler ");
                        if (!string.IsNullOrEmpty(Convert.ToString(INFPax[i]["DOB"])) && Convert.ToString(INFPax[i]["DOB"]).Trim().Length > 7)
                            objreqXml.Append("BirthDate=\"" + INFPax[i]["DOB"].ToString().Split('/')[2] + "-" + INFPax[i]["DOB"].ToString().Split('/')[1] + "-" + INFPax[i]["DOB"].ToString().Split('/')[0] + "T00:00:00\" ");
                        objreqXml.Append(" PassengerTypeCode=\"INF\">");
                        objreqXml.Append("<ns2:PersonName>");
                        objreqXml.Append("<ns2:GivenName>" + INFPax[i]["FName"].ToString());
                        if (!string.IsNullOrEmpty(Convert.ToString(INFPax[i]["MName"])))
                            objreqXml.Append(" " + INFPax[i]["MName"].ToString());
                        objreqXml.Append("</ns2:GivenName>");
                        objreqXml.Append("<ns2:Surname>" + INFPax[i]["LName"].ToString() + "</ns2:Surname>");
                        objreqXml.Append("<ns2:NameTitle>" + INFPax[i]["Title"].ToString().ToUpper() + "</ns2:NameTitle>");
                        objreqXml.Append("</ns2:PersonName>");
                        objreqXml.Append("<ns2:TravelerRefNumber RPH=\"I" + (ADTPax.Length + CHDPax.Length + i + 1) + "/A" + (ADTPax.Length + i) + "\" /> ");

                        objreqXml.Append(string.Format("</ns2:AirTraveler>"));
                   }
               }
#endregion
#endregion
               objreqXml.Append("</ns2:TravelerInfo>");
               objreqXml.Append("<ns2:Fulfillment>");
               objreqXml.Append("<ns2:PaymentDetails>");
               objreqXml.Append("<ns2:PaymentDetail>");
               objreqXml.Append("<ns2:DirectBill>");
               objreqXml.Append("<ns2:CompanyName Code=\"" + Agencyid + "\" />");
               objreqXml.Append("</ns2:DirectBill>");
               objreqXml.Append("<ns2:PaymentAmount Amount=\"" + Math.Round(Convert.ToDecimal(Fltdtlds.Rows[0]["OriginalTF"]), 2).ToString() + "\" CurrencyCode=\"" + SNO[6] + "\" DecimalPlaces=\"2\" />");
               objreqXml.Append("</ns2:PaymentDetail>");
               objreqXml.Append("</ns2:PaymentDetails>");
               objreqXml.Append("</ns2:Fulfillment>");
               objreqXml.Append("</ns2:OTA_AirBookRQ>");
               objreqXml.Append("<ns1:AAAirBookRQExt>");
               objreqXml.Append("<ns1:ContactInfo>");
               objreqXml.Append("<ns1:PersonName>");
               objreqXml.Append("<ns1:Title>" + ADTPax[0]["Title"].ToString().ToUpper() + "</ns1:Title>");
               objreqXml.Append("<ns1:FirstName>" + ADTPax[0]["FName"].ToString());
               if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[0]["MName"])))
                   objreqXml.Append(" " + ADTPax[0]["MName"].ToString());
               objreqXml.Append("</ns1:FirstName>");
               objreqXml.Append("<ns1:LastName>" + ADTPax[0]["LName"].ToString() + "</ns1:LastName>");
               objreqXml.Append("</ns1:PersonName>");
               objreqXml.Append("<ns1:Telephone>");
               if (HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString().Length > 4)
               {
                   objreqXml.Append("<ns1:PhoneNumber>" + HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString().Substring(2, HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString().Length - 2) + "</ns1:PhoneNumber>");
                   objreqXml.Append("<ns1:CountryCode>91</ns1:CountryCode>");
                   objreqXml.Append("<ns1:AreaCode>" + HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString().Substring(0, 2) + "</ns1:AreaCode>");
               }
               else if (AgencyDs.Tables[0].Rows[0]["Mobile"].ToString().Length > 4 && HeaderDs.Tables[0].Rows[0]["PgMobile"].ToString().Length < 5)
               {
                   objreqXml.Append("<ns1:PhoneNumber>" + AgencyDs.Tables[0].Rows[0]["Mobile"].ToString().Substring(2, AgencyDs.Tables[0].Rows[0]["Mobile"].ToString().Length - 2) + "</ns1:PhoneNumber>");
                   objreqXml.Append("<ns1:CountryCode>91</ns1:CountryCode>");
                   objreqXml.Append("<ns1:AreaCode>" + AgencyDs.Tables[0].Rows[0]["Mobile"].ToString().Substring(0, 2) + "</ns1:AreaCode>");
               }
               objreqXml.Append("</ns1:Telephone>");
               //objreqXml.Append("<ns1:Email>07seaz@gmail.com</ns1:Email>");

               if (HeaderDs.Tables[0].Rows[0]["PgEmail"].ToString().Length > 11)
                   objreqXml.Append("<ns1:Email>" + HeaderDs.Tables[0].Rows[0]["PgEmail"].ToString() + "</ns1:Email>");
               else 
                   objreqXml.Append("<ns1:Email>07seaz@gmail.com</ns1:Email>");


               objreqXml.Append("<ns1:Address>");
               objreqXml.Append("<ns1:CountryName>");
               objreqXml.Append("<ns1:CountryName>India</ns1:CountryName>");
               objreqXml.Append("<ns1:CountryCode>IN</ns1:CountryCode>");
               objreqXml.Append("</ns1:CountryName>");
               if (HeaderDs.Tables[0].Rows[0]["GSTNO"].ToString().Length > 13)
                   objreqXml.Append("<ns1:StateCode>" + HeaderDs.Tables[0].Rows[0]["GSTNO"].ToString().Substring(0, 2) + "</ns1:StateCode>");
               objreqXml.Append("<ns1:CityName>New Delhi</ns1:CityName>");              
               objreqXml.Append("</ns1:Address>");
               if (HeaderDs.Tables[0].Rows[0]["GSTNO"].ToString().Length > 13)
                   objreqXml.Append("<ns1:taxRegNo>" + HeaderDs.Tables[0].Rows[0]["GSTNO"].ToString() + "</ns1:taxRegNo>");
               objreqXml.Append("</ns1:ContactInfo>");
               objreqXml.Append("</ns1:AAAirBookRQExt>");
               objreqXml.Append("</soap:Body>");
               objreqXml.Append("</soap:Envelope>");
               //
               #endregion
           }
           catch (Exception ex)
           {
               ExecptionLogger.FileHandling("AirArabiaBookingRequest", "Error_001", ex, "AirArabiaFlight");
           }
           return objreqXml.ToString();
       }

       public string PricingWithMealBaggege(DataSet Credencilas, DataTable fltdt, DataTable MealBages, DataTable PaxDt, string GSTNo, ref Dictionary<string, string> xml)
        {
             string TotalCost = "FAILURE";
             string PricingWithbaggegeReq = "", PricingWithbaggegeResp = "";
           try
           {
               if (fltdt.Rows.Count > 0)
               {
                   PricingWithbaggegeReq = PricingWithbaggegeRequest(Credencilas.Tables[0].Rows[0]["UserID"].ToString(), Credencilas.Tables[0].Rows[0]["Password"].ToString(), fltdt, MealBages, PaxDt, GSTNo);
                   AirArabiaUtility.SaveFile(PricingWithbaggegeReq, "PricingWithbaggegeReq_" + fltdt.Rows[0]["Track_id"].ToString());
                   PricingWithbaggegeResp = AirArabiaUtility.AirArabiaPostXML(Credencilas.Tables[0].Rows[0]["ServerUrlOrIP"].ToString(), PricingWithbaggegeReq, fltdt.Rows[0]["Searchvalue"].ToString());
                   AirArabiaUtility.SaveFile(PricingWithbaggegeResp, "PricingWithbaggegeRes_" + fltdt.Rows[0]["Track_id"].ToString());

                   if (PricingWithbaggegeResp.Contains("<ns1:Success />") || PricingWithbaggegeResp.Contains("<ns1:Success/>"))
                   {
                       XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/";
                       XNamespace ns1 = "http://www.opentravel.org/OTA/2003/05";
                       XDocument AirArabiaDocument = XDocument.Parse(PricingWithbaggegeResp.Replace("xmlns=\"http://tempuri.org/\"", string.Empty).Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                       XElement PricedItineraries = AirArabiaDocument.Element(s + "Envelope").Element(s + "Body").Element(ns1 + "OTA_AirPriceRS").Element(ns1 + "PricedItineraries");
                       TotalCost = PricedItineraries.Element(ns1 + "PricedItinerary").Element(ns1 + "AirItineraryPricingInfo").Element(ns1 + "ItinTotalFare").Element(ns1 + "TotalFareWithCCFee").Attribute("Amount").Value.Trim();
                   }

               }
           }
           catch(Exception ex)
           {

               ExecptionLogger.FileHandling("PricingWithbaggege", "Error_001", ex, "AirArabiaiaFlight");
                xml.Add("UPPAXREQ", "");
                xml.Add("UPPAXRES", "");
                xml.Add("UCCONREQ", "");
                xml.Add("UCCONRES", "");
                xml.Add("STATEREQ", "");
                xml.Add("STATERES", "");
                xml.Add("APBREQ", "");
                xml.Add("APBRES", "");
                xml.Add("BC-REQ", "");
                xml.Add("BC-RES", "");
                xml.Add("OTHER", ex.Message);
            }
            finally
            {
                xml.Add("SSR", PricingWithbaggegeReq + "<br>" + PricingWithbaggegeResp);
            }
           return TotalCost;
       }

        public string PricingWithbaggegeRequest(string Username, string Password, DataTable fltdt, DataTable MealBages, DataTable PaxDt, string GSTNo)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                string[] SNO= fltdt.Rows[0]["sno"].ToString().Split(':');
                DataRow[] newFltDsIN = fltdt.Select("TripType='R'");
            
                sb.Append("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                sb.Append("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
                sb.Append("<soap:Header>");
                sb.Append("<wsse:Security soap:mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">");
                sb.Append("<wsse:UsernameToken wsu:Id=\"UsernameToken-32124385\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">");
                sb.Append("<wsse:Username>" + Username + "</wsse:Username>");
                sb.Append("<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" + Password + "</wsse:Password>");
                sb.Append("</wsse:UsernameToken>");
                sb.Append("</wsse:Security>");
                sb.Append("</soap:Header>");
                sb.Append("<soap:Body xmlns:ns1=\"http://www.opentravel.org/OTA/2003/05\">");
                sb.Append("<ns1:OTA_AirPriceRQ EchoToken=\"" + SNO[3] + "\" PrimaryLangID=\"en-us\" SequenceNmbr=\"1\" TimeStamp=\"" + DateTime.Now.AddMinutes(2).ToString("yyyy-MM-ddTHH:mm:ss") + "\" TransactionIdentifier=\"" + SNO[0] + "\" Version=\"" + SNO[4] + "\">");
                sb.Append("<ns1:POS>");
                sb.Append("<ns1:Source TerminalID=\"TestUser/Test Runner\">");
                sb.Append("<ns1:RequestorID ID=\"" + Username + "\" Type=\"4\"/>");
                sb.Append("<ns1:BookingChannel Type=\"12\" />");
                sb.Append("</ns1:Source>");
                sb.Append("</ns1:POS>");
                if (newFltDsIN.Length > 0)
                    sb.Append("<ns1:AirItinerary DirectionInd=\"Return\">");
                else
                    sb.Append("<ns1:AirItinerary DirectionInd=\"OneWay\">");
               sb.Append("<ns1:OriginDestinationOptions>");

               for (int lg = 0; lg < fltdt.Rows.Count; lg++)
               {
                   string[] NewSNO= fltdt.Rows[lg]["sno"].ToString().Split(':');
                   sb.Append("<ns1:OriginDestinationOption>");
                   sb.Append("<ns1:FlightSegment ArrivalDateTime=\"" + Convert.ToDateTime(fltdt.Rows[lg]["arrdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" DepartureDateTime=\"" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "\" RPH=\"" + NewSNO[1] + "\">");
                   sb.Append("<ns1:DepartureAirport LocationCode=\"" + fltdt.Rows[lg]["DepartureLocation"].ToString() + "\" Terminal=\"TerminalX\"/>");
                   sb.Append("<ns1:ArrivalAirport LocationCode=\"" + fltdt.Rows[lg]["ArrivalLocation"].ToString() + "\" Terminal=\"TerminalX\"/>");
                   sb.Append("</ns1:FlightSegment>");
                   sb.Append("</ns1:OriginDestinationOption>");
               }
                  
                sb.Append("</ns1:OriginDestinationOptions>");
                sb.Append("</ns1:AirItinerary>");
                sb.Append(" <ns1:TravelerInfoSummary>");
                sb.Append("<ns1:AirTravelerAvail>");
                sb.Append("<ns1:PassengerTypeQuantity Code=\"ADT\" Quantity=\"" + fltdt.Rows[0]["Adult"].ToString() + "\"/>");
                if (Convert.ToInt32(fltdt.Rows[0]["Child"]) > 0)
                    sb.Append("<ns1:PassengerTypeQuantity Code=\"CHD\" Quantity=\"" + fltdt.Rows[0]["Child"].ToString() + "\"/>");
                if (Convert.ToInt32(fltdt.Rows[0]["Infant"]) > 0)
                    sb.Append("<ns1:PassengerTypeQuantity Code=\"INF\" Quantity=\"" + fltdt.Rows[0]["Infant"].ToString() + "\"/>");
                sb.Append("</ns1:AirTravelerAvail>");
                sb.Append("<ns1:SpecialReqDetails>");
                if (MealBages !=null)
                {
                 if (MealBages.Rows.Count > 0)
                 {
                     #region When Meal and baggage are selected
                     DataRow[] MealRows = MealBages.Select("MealCode <> ''", "PaxID ASC");
                     DataRow[] BaggageRows = MealBages.Select("BaggageCode <> ''", "PaxID ASC");
                   
                     #region Baggage Request
                     sb.Append("<ns1:BaggageRequests>");
                     if (BaggageRows.Length > 0)
                     {
                         int adt = 0, chd = 0; string PaxExistAdt = "", PaxExistChd = "";
                         #region For Adult
                         for (int ssr = 0; ssr < MealBages.Rows.Count; ssr++)
                         {
                             if (MealBages.Rows[ssr]["BaggageCode"].ToString() != "")
                             {
                                 DataRow[] Paxdtrow = PaxDt.Select("PaxId=" + MealBages.Rows[ssr]["PaxID"].ToString() + "");
                                 if (Paxdtrow[0]["PaxType"].ToString() == "ADT" && PaxExistAdt != Paxdtrow[0]["PaxId"].ToString())
                                 {
                                     adt++; 
                                     PaxExistAdt = Paxdtrow[0]["PaxId"].ToString();
                                 }
                                 //else if (Paxdtrow[0]["PaxType"].ToString() == "CHD" && PaxExistChd != Paxdtrow[0]["PaxId"].ToString())
                                 //{
                                 //    chd++; 
                                 //    PaxExistChd = Paxdtrow[0]["PaxId"].ToString();
                                 //}
                                 DataRow[] BaggageDatarow = fltdt.Select("TripType = '" + MealBages.Rows[ssr]["TripType"].ToString().Trim() + "'");

                                 for (int lg = 0; lg < BaggageDatarow.Length; lg++)
                                 {
                                     string[] NewSNO = BaggageDatarow[lg]["sno"].ToString().Split(':');
                                     if (Paxdtrow[0]["PaxType"].ToString() == "ADT")
                                         sb.Append("<ns1:BaggageRequest baggageCode=\"" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "\" TravelerRefNumberRPHList=\"A" + adt.ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(BaggageDatarow[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + BaggageDatarow[lg]["FlightIdentification"].ToString() + "\" />");
                                    // else if (Paxdtrow[0]["PaxType"].ToString() == "CHD")
                                     //    sb.Append("<ns1:BaggageRequest baggageCode=\"" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "\" TravelerRefNumberRPHList=\"C" + (adt + chd).ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(BaggageDatarow[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + BaggageDatarow[lg]["FlightIdentification"].ToString() + "\" />");       
                                 }
                               // string[] BaggageArray = MealBages.Rows[ssr]["BaggageDesc"].ToString().ToString().Split('_'); ;
                               // DataRow[] BaggageDatarow = fltdt.Select("DepartureLocation = '" + BaggageArray[1] + "' AND ArrivalLocation = '" + BaggageArray[2] + "'");
                                //string[] NewSNO = BaggageDatarow[0]["sno"].ToString().Split(':');
                                //if (Paxdtrow[0]["PaxType"].ToString() == "ADT")
                                //    sb.Append("<ns1:BaggageRequest baggageCode=\"" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "\" TravelerRefNumberRPHList=\"A" + adt.ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(BaggageDatarow[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + BaggageDatarow[0]["FlightIdentification"].ToString() + "\" />");
                                //else if (Paxdtrow[0]["PaxType"].ToString() == "CHD")
                                //    sb.Append("<ns1:BaggageRequest baggageCode=\"" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "\" TravelerRefNumberRPHList=\"C" + (adt + chd).ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(BaggageDatarow[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + BaggageDatarow[0]["FlightIdentification"].ToString() + "\" />");
                                
                                // //for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                                // //{
                                // //    string[] NewSNO = fltdt.Rows[lg]["sno"].ToString().Split(':');
                                // //    if (Paxdtrow[0]["PaxType"].ToString() == "ADT")
                                // //        sb.Append("<ns1:BaggageRequest baggageCode=\"" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "\" TravelerRefNumberRPHList=\"A" + adt.ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "\" />");
                                // //    else if (Paxdtrow[0]["PaxType"].ToString() == "CHD")
                                // //        sb.Append("<ns1:BaggageRequest baggageCode=\"" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "\" TravelerRefNumberRPHList=\"C" + (adt + chd).ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "\" />");
                                // //}
                             }
                         }
                         #endregion
                         #region For Childs
                         for (int ssr = 0; ssr < MealBages.Rows.Count; ssr++)
                         {
                             if (MealBages.Rows[ssr]["BaggageCode"].ToString() != "")
                             {
                                 DataRow[] Paxdtrow = PaxDt.Select("PaxId=" + MealBages.Rows[ssr]["PaxID"].ToString() + "");
                                 if (Paxdtrow[0]["PaxType"].ToString() == "CHD" && PaxExistChd != Paxdtrow[0]["PaxId"].ToString())
                                 {
                                     chd++;
                                     PaxExistChd = Paxdtrow[0]["PaxId"].ToString();
                                 }
                                 DataRow[] BaggageDatarow = fltdt.Select("TripType = '" + MealBages.Rows[ssr]["TripType"].ToString().Trim() + "'");

                                 for (int lg = 0; lg < BaggageDatarow.Length; lg++)
                                 {
                                     string[] NewSNO = BaggageDatarow[lg]["sno"].ToString().Split(':');
                                     if (Paxdtrow[0]["PaxType"].ToString() == "CHD")
                                         sb.Append("<ns1:BaggageRequest baggageCode=\"" + MealBages.Rows[ssr]["BaggageCode"].ToString() + "\" TravelerRefNumberRPHList=\"C" + (adt + chd).ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(BaggageDatarow[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + BaggageDatarow[lg]["FlightIdentification"].ToString() + "\" />");
                                 }
                             }
                         }
                         #endregion
                         if (MealBages.Rows.Count != Convert.ToInt32(fltdt.Rows[0]["Child"]))
                         {
                             for (int ssr = 0; ssr < Convert.ToInt32(fltdt.Rows[0]["Child"]) - MealBages.Rows.Count; ssr++)
                            {
                                chd++;
                                for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                                {
                                    string[] NewSNO = fltdt.Rows[lg]["sno"].ToString().Split(':');

                                    sb.Append("<ns1:BaggageRequest baggageCode=\"No Bag\" TravelerRefNumberRPHList=\"C" + (adt + chd).ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "\" />");
                                }
                            }
                         }
                         if (MealBages.Rows.Count != Convert.ToInt32(fltdt.Rows[0]["Adult"]))
                         {
                             for (int ssr = 0; ssr < Convert.ToInt32(fltdt.Rows[0]["Adult"]) - MealBages.Rows.Count; ssr++)
                             {
                                 adt++;
                                 for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                                 {
                                     string[] NewSNO = fltdt.Rows[lg]["sno"].ToString().Split(':');

                                     sb.Append("<ns1:BaggageRequest baggageCode=\"No Bag\" TravelerRefNumberRPHList=\"A" + adt.ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "\" />");
                                 }
                             }
                         }
                     }
                     else
                     {
                         int adt = 0, chd = 0;
                         for (int ssr = 0; ssr < PaxDt.Rows.Count; ssr++)
                         {
                             if (PaxDt.Rows[ssr]["PaxType"].ToString() == "ADT")
                                 adt++;
                             else if (PaxDt.Rows[ssr]["PaxType"].ToString() == "CHD")
                                 chd++;
                             for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                             {
                                 string[] NewSNO = fltdt.Rows[lg]["sno"].ToString().Split(':');
                                 if (PaxDt.Rows[ssr]["PaxType"].ToString() == "ADT")
                                     sb.Append("<ns1:BaggageRequest baggageCode=\"No Bag\" TravelerRefNumberRPHList=\"A" + adt.ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "\" />");
                                 else if (PaxDt.Rows[ssr]["PaxType"].ToString() == "CHD")
                                     sb.Append("<ns1:BaggageRequest baggageCode=\"No Bag\" TravelerRefNumberRPHList=\"C" + (adt + chd).ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "\" />");
                             }
                         }
                     }
                     sb.Append("</ns1:BaggageRequests>");
                     #endregion
                     #region Meal Request
                     if (MealRows.Length > 0)
                     {
                         int adt1 = 0, chd1 = 0; string PaxExistAdt = "", PaxExistChd = "";
                         sb.Append("<ns1:MealRequests>");
                         #region For Adult
                         for (int ssr = 0; ssr < MealRows.Length; ssr++)
                         {
                             if (MealRows[ssr]["MealCode"].ToString() != "")
                             {
                                 DataRow[] Paxdtrow = PaxDt.Select("PaxId=" + MealRows[ssr]["PaxID"].ToString() + "");
                                 if (Paxdtrow[0]["PaxType"].ToString() == "ADT" && PaxExistAdt != Paxdtrow[0]["PaxId"].ToString())
                                 {
                                     adt1++; 
                                     PaxExistAdt=Paxdtrow[0]["PaxId"].ToString();
                                 }
                                 //else if (Paxdtrow[0]["PaxType"].ToString() == "CHD" && PaxExistChd != Paxdtrow[0]["PaxId"].ToString())
                                 //{
                                 //    chd1++; 
                                 //    PaxExistChd = Paxdtrow[0]["PaxId"].ToString();
                                 //}
                                 string[] MealArray = MealBages.Rows[ssr]["MealDesc"].ToString().ToString().Split('_'); ;
                                 DataRow[] MealDatarow = fltdt.Select("DepartureLocation = '" + MealArray[1] + "' AND ArrivalLocation = '" + MealArray[2] + "'");
                                 string[] NewSNO = MealDatarow[0]["sno"].ToString().Split(':');
                                 if (Paxdtrow[0]["PaxType"].ToString() == "ADT")
                                     sb.Append("<ns1:MealRequest mealCode=\"" + MealRows[ssr]["MealCode"].ToString() + "\" mealQuantity=\"1\" TravelerRefNumberRPHList=\"A" + adt1.ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(MealDatarow[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + MealDatarow[0]["FlightIdentification"].ToString() + "\" />");
                                 //else if (Paxdtrow[0]["PaxType"].ToString() == "CHD")
                                 //    sb.Append("<ns1:MealRequest mealCode=\"" + MealRows[ssr]["MealCode"].ToString() + "\" mealQuantity=\"1\" TravelerRefNumberRPHList=\"C" + (adt1 + chd1).ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(MealDatarow[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + MealDatarow[0]["FlightIdentification"].ToString() + "\" />");
                                 
                                 //for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                                 //{
                                 //    string[] NewSNO = fltdt.Rows[lg]["sno"].ToString().Split(':');
                                 //    if (Paxdtrow[0]["PaxType"].ToString() == "ADT")
                                 //        sb.Append("<ns1:MealRequest mealCode=\"" + MealRows[ssr]["MealCode"].ToString() + "\" mealQuantity=\"1\" TravelerRefNumberRPHList=\"A" + adt1.ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "\" />");
                                 //    else if (Paxdtrow[0]["PaxType"].ToString() == "CHD")
                                 //        sb.Append("<ns1:MealRequest mealCode=\"" + MealRows[ssr]["MealCode"].ToString() + "\" mealQuantity=\"1\" TravelerRefNumberRPHList=\"C" + (adt1 + chd).ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "\" />");
                                 //}
                             }
                         }
                         #endregion
                         #region For Child
                         for (int ssr = 0; ssr < MealRows.Length; ssr++)
                         {
                             if (MealRows[ssr]["MealCode"].ToString() != "")
                             {
                                 DataRow[] Paxdtrow = PaxDt.Select("PaxId=" + MealRows[ssr]["PaxID"].ToString() + "");
                                 if (Paxdtrow[0]["PaxType"].ToString() == "CHD" && PaxExistChd != Paxdtrow[0]["PaxId"].ToString())
                                 {
                                     chd1++;
                                     PaxExistChd = Paxdtrow[0]["PaxId"].ToString();
                                 }
                                 string[] MealArray = MealBages.Rows[ssr]["MealDesc"].ToString().ToString().Split('_'); ;
                                 DataRow[] MealDatarow = fltdt.Select("DepartureLocation = '" + MealArray[1] + "' AND ArrivalLocation = '" + MealArray[2] + "'");
                                 string[] NewSNO = MealDatarow[0]["sno"].ToString().Split(':');
                                 if (Paxdtrow[0]["PaxType"].ToString() == "CHD")
                                     sb.Append("<ns1:MealRequest mealCode=\"" + MealRows[ssr]["MealCode"].ToString() + "\" mealQuantity=\"1\" TravelerRefNumberRPHList=\"C" + (adt1 + chd1).ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(MealDatarow[0]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + MealDatarow[0]["FlightIdentification"].ToString() + "\" />");
                             }
                         }
                         #endregion
                         sb.Append("</ns1:MealRequests>");
                     }
                     #endregion
                     #endregion
                 }
                 else
                 {
                     #region When SSR Not Selected  
                     sb.Append("<ns1:BaggageRequests>");
                     DataRow[] AdtPaxdt = PaxDt.Select("PaxType='ADT'");
                     DataRow[] ChdPaxdt = PaxDt.Select("PaxType='CHD'");
                     for (int ssr = 0; ssr < AdtPaxdt.Length; ssr++)
                     {
                         for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                         {
                             string[] NewSNO = fltdt.Rows[lg]["sno"].ToString().Split(':');
                             sb.Append("<ns1:BaggageRequest baggageCode=\"No Bag\" TravelerRefNumberRPHList=\"A" + (ssr + 1).ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "\" />");
                         }
                     }
                     for (int ssr = 0; ssr < ChdPaxdt.Length; ssr++)
                     {
                         for (int lg = 0; lg < fltdt.Rows.Count; lg++)
                         {
                             string[] NewSNO = fltdt.Rows[lg]["sno"].ToString().Split(':');
                             sb.Append("<ns1:BaggageRequest baggageCode=\"No Bag\" TravelerRefNumberRPHList=\"C" + (AdtPaxdt.Length + ssr + 1).ToString() + "\" FlightRefNumberRPHList=\"" + NewSNO[1] + "\" DepartureDate=\"" + Convert.ToDateTime(fltdt.Rows[lg]["depdatelcc"]).ToString("yyyy-MM-ddTHH:mm:ss") + "\" FlightNumber=\"" + fltdt.Rows[lg]["FlightIdentification"].ToString() + "\" />");
                         }
                     }
                     sb.Append("</ns1:BaggageRequests>");
                     #endregion
                 }
                }
                sb.Append("</ns1:SpecialReqDetails>");
                sb.Append("</ns1:TravelerInfoSummary>");
                if (GSTNo !="")
                {
                    sb.Append("<ns1:ServiceTaxCriteriaOptions><ns1:CountryCode>IN</ns1:CountryCode><ns1:StateCode>" + GSTNo.Substring(0,2) + "</ns1:StateCode>");
                    sb.Append("<ns1:TaxRegistrationNo>" + GSTNo + "</ns1:TaxRegistrationNo></ns1:ServiceTaxCriteriaOptions>");
                }
                sb.Append("</ns1:OTA_AirPriceRQ>");
                sb.Append("</soap:Body>");
                sb.Append("</soap:Envelope>");
            }
            catch(Exception ex)
            {
                ExecptionLogger.FileHandling("PricingWithbaggegeRequest", "Error_001", ex, "AirArabiaFlight");
            }
            return sb.ToString();
        }
        
   

    }
}
