﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
////using System.Threading.Tasks;
using System.Data;
namespace ITZLib
{
    public class ErrorLogs
    {
        public class ErrorLog
        {
            private static string LogDirectory = String.Empty;
            public bool WriteErrorLogLoginResp(string logMessage,string logFor)
            {
                bool successStatus = false;
                LogDirectory = ConfigurationManager.AppSettings["LogDirectory"].ToString();
                DateTime currentDateTime = DateTime.Now;
                string currentDateTimeString = currentDateTime.ToString();
                CheckCreateLogDirectory(LogDirectory);
                string logLine = BuildLogLine(currentDateTime, logMessage);
                LogDirectory = (LogDirectory + logFor.Trim() + LogFileNameLoginResp(currentDateTime) + ".txt");
                lock (typeof(ErrorLog))
                {
                    StreamWriter m_logSWriter = null;
                    try
                    {
                        m_logSWriter = new StreamWriter(LogDirectory, true);
                        m_logSWriter.WriteLine(logLine);
                        successStatus = true;
                    }
                    catch
                    {
                    }
                    finally
                    {
                        if (m_logSWriter != null)
                        {
                            m_logSWriter.Close();
                        }
                    }
                }
                return successStatus;
            }
            public bool WriteErrorLog(string logMessage)
            {
                bool successStatus = false;
                LogDirectory = ConfigurationManager.AppSettings["LogDirectory"].ToString();
                DateTime currentDateTime = DateTime.Now;
                string currentDateTimeString = currentDateTime.ToString();
                CheckCreateLogDirectory(LogDirectory);
                string logLine = BuildLogLine(currentDateTime, logMessage);
                LogDirectory = (LogDirectory + "Log_" + LogFileName(DateTime.Now) + ".txt");
                lock (typeof(ErrorLog))
                {
                    StreamWriter m_logSWriter = null;
                    try
                    {
                        m_logSWriter = new StreamWriter(LogDirectory, true);
                        m_logSWriter.WriteLine(logLine);
                        successStatus = true;
                    }
                    catch
                    {
                    }
                    finally
                    {
                        if (m_logSWriter != null)
                        {
                            m_logSWriter.Close();
                        }
                    }
                }
                return successStatus;
            }
            private static bool CheckCreateLogDirectory(string logPath)
            {
                bool loggingDirectoryExists = false;
                DirectoryInfo dirInfo = new DirectoryInfo(logPath);
                if (dirInfo.Exists)
                {
                    loggingDirectoryExists = true;
                }
                else
                {
                    try
                    {
                        Directory.CreateDirectory(logPath);
                        loggingDirectoryExists = true;
                    }
                    catch
                    {
                        // Logging failure
                    }
                }
                return loggingDirectoryExists;
            }
            private static string BuildLogLine(DateTime currentDateTime, string logMessage)
            {
                StringBuilder loglineStringBuilder = new StringBuilder();
                loglineStringBuilder.Append(LogFileEntryDateTime(currentDateTime));
                loglineStringBuilder.Append("\t");
                loglineStringBuilder.Append(logMessage);
                return loglineStringBuilder.ToString();
            }
            public static string LogFileEntryDateTime(DateTime currentDateTime)
            {
                return currentDateTime.ToString("dd-MM-yyyy HH:mm:ss");
            }
            private static string LogFileName(DateTime currentDateTime)
            {
                return currentDateTime.ToString("dd_MM_yyyy");
            }
            private static string LogFileNameLoginResp(DateTime currentDateTime)
            {
                return currentDateTime.ToString("dd_MM_yyyy(HH.mm.sss)");
            }
        }
    }
    public class login
    {
        public static DataTable loginparam(string username, string password)
        {
            DataTable dt = new DataTable();
            string constr = ConfigurationManager.ConnectionStrings["CONSTRING"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
                {
                    using (SqlCommand cmd = new SqlCommand("USP_REG_LOGIN"))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@username", username.Trim());
                        cmd.Parameters.AddWithValue("@password", password.Trim());
                        cmd.Parameters.AddWithValue("@Email", "");
                        cmd.Parameters.AddWithValue("@sqlparam", "0");
                        cmd.Connection = con;
                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
           
            return dt;
        }
    }
}
                   