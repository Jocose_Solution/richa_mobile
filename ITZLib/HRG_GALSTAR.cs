﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Configuration;

namespace ITZLib
{
    public class HRG_GALSTAR
    {
        public static void HRG_Serv(string filepath)
        {
            string strConnString = System.Configuration.ConfigurationManager.ConnectionStrings["CONSTRING"].ConnectionString;
            //string strConnString = "Data Source=172.18.80.75;Initial Catalog=HRG_TO_GAL;User ID=sa;Password=Password123;Max Pool Size=1000";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand sqlcmd = new SqlCommand();
            string Pathexl03 = "";
            string Pathexl07 = "";
            string excelConnectionString = "";
            string FileNamePath = "";
            Pathexl07 = filepath;
            if (Pathexl07 == "G:\\HRG_to_GALSTAR\\HRGProfile.xlsx")
            {
                FileNamePath = Pathexl07;
            }
            else if (Pathexl03 == "G:\\HRG_to_GALSTAR\\HRGProfile.xls")
            {
                FileNamePath = Pathexl03;
            }
            try
            {
                string Extension = Path.GetExtension(FileNamePath);
                if (Extension == ".xlsx")
                {
                    excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileNamePath + ";Extended Properties=Excel 12.0;Persist Security Info=False";
                }
                else if (Extension == ".xls")
                {
                    excelConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileNamePath + ";Extended Properties=Excel 8.0; Persist Security Info=False";
                }
                OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                excelConnection.Open();
                DataTable dt = new DataTable();
                dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (dt == null)
                {
                }
                String[] excelSheets = new String[dt.Rows.Count];
                int t = 0;
                foreach (DataRow row in dt.Rows)
                {
                    excelSheets[t] = row["TABLE_NAME"].ToString();
                    t++;
                }
                OleDbConnection MainexcelConnection = new OleDbConnection(excelConnectionString);
                DataSet ds = new DataSet();
                string query = string.Format("Select [EmployeeNo],[UsernameFormat],[Username],[Title],"
               + "[Grade],[Surname],[Firstname],[JobTitle],[AddressKey],[IsDeliveryAddress],[Address],[AddressZipCode],[AddressCountry],[AssociatedPortalRole],[PassportNationality],"
               + "[Gender],[DateOfBirth],[Ref1],[Ref2],[Ref3],[Ref4],[Ref5],[Ref6],[Ref7],[Ref8],[Ref9],[Ref10],[Ref11],[Ref12],[Ref13],[Ref14],[Ref15],[Ref16],[Ref17],[Ref18],"
               + "[Ref19],[Ref20],[PrimaryEmailAddress],[TelephoneNumber],[MobileNumber],[AdditionalEmailAddress],[CorporateCardName],[CorpCardnumber],[CorpCardValidFrom],"
               + "[CorpCardValidTo],[External/FederatedGID],[HRGCompanyKeyFormat],[HRGCompanyKey],[DateLeft],[ResidencePhone],[FaxNumber],[PassportNumber],[PassportExpiryDate],"
               + "[PassportIssueCountry],[FrequentFlierNumber],[SeatRequest],[MealRequest],[ProfileCreatedBy],[ProfileCreatedDate],[ProfileUpdatedBy],[ProfileUpdatedDate],"
               + "[LinkToCompanyAddress],[LinkToTravellerAddress] from [{0}]", excelSheets[0]);
                OleDbCommand cmd = new OleDbCommand(query, MainexcelConnection);
                MainexcelConnection.Open();
                OleDbDataReader dReader = cmd.ExecuteReader();
                SqlBulkCopy sqlBulk = new SqlBulkCopy(strConnString);
                //Give your Destination table name
                sqlBulk.DestinationTableName = "Excel_table";

                //sqlBulk.ColumnMappings.Add("MobileNumber".Trim().ToString(), "MobileNumber".ToString());

                sqlBulk.WriteToServer(dReader);
                excelConnection.Close();

                if (Extension == ".xlsx" || Extension == ".xls")
                {
                    con.Open();
                    sqlcmd.Connection = con;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "spx_ImportFromExcel";
                    sqlcmd.Parameters.Add("@Extension", SqlDbType.VarChar).Value = Extension.Trim().ToString();
                    sqlcmd.ExecuteNonQuery();
                }
                else
                {
                    con.Close();
                }
                try
                {
                }
                catch (Exception ex1)
                {
                    throw ex1;
                }
            }
            finally
            {
                //File.Delete(filepath);
            }
        }
        //public static void Txt_to_dbtable(string filepath)
        //{
        //    DataTable dt = new DataTable();
        //    dt.Columns.AddRange(new DataColumn[63]{new DataColumn("EmployeeNo"),new DataColumn("UsernameFormat"),new DataColumn("Username"),new DataColumn("Title"),
        //                        new DataColumn("Grade"),new DataColumn("Surname"),new DataColumn("Firstname"),new DataColumn("JobTitle"),new DataColumn("AddressKey"),
        //                        new DataColumn("IsDeliveryAddress"),new DataColumn("Address"),new DataColumn("AddressZipCode"),new DataColumn("AddressCountry"), 
        //                        new DataColumn("AssociatedPortalRole"),new DataColumn("PassportNationality"),new DataColumn("Gender"),new DataColumn("DateOfBirth"), 
        //                        new DataColumn("Ref1"),new DataColumn("Ref2"),new DataColumn("Ref3"),new DataColumn("Ref4"),new DataColumn("Ref5"),new DataColumn("Ref6"), 
        //                        new DataColumn("Ref7"),new DataColumn("Ref8"),new DataColumn("Ref9"),new DataColumn("Ref10"),new DataColumn("Ref11"),new DataColumn("Ref12"),
        //                        new DataColumn("Ref13"),new DataColumn("Ref14"),new DataColumn("Ref15"),new DataColumn("Ref16"),new DataColumn("Ref17"),new DataColumn("Ref18"),
        //                        new DataColumn("Ref19"),new DataColumn("Ref20"),new DataColumn("PrimaryEmailAddress"),new DataColumn("TelephoneNumber"),new DataColumn("MobileNumber"),
        //                        new DataColumn("AdditionalEmailAddress"),new DataColumn("CorporateCardName"),new DataColumn("CorpCardnumber"),new DataColumn("CorpCardValidFrom"), 
        //                        new DataColumn("CorpCardValidTo"),new DataColumn("External/FederatedGID"),new DataColumn("HRGCompanyKeyFormat"),new DataColumn("HRGCompanyKey"), 
        //                        new DataColumn("DateLeft"),new DataColumn("ResidencePhone"),new DataColumn("FaxNumber"),new DataColumn("PassportNumber"),new DataColumn("PassportExpiryDate"), 
        //                        new DataColumn("PassportIssueCountry"),new DataColumn("FrequentFlierNumber"),new DataColumn("SeatRequest"),new DataColumn("MealRequest"), 
        //                        new DataColumn("ProfileCreatedBy"),new DataColumn("ProfileCreatedDate"),new DataColumn("ProfileUpdatedBy"),new DataColumn("ProfileUpdatedDate"), 
        //                        new DataColumn("LinkToCompanyAddress"),new DataColumn("LinkToTravellerAddress")});

        //    StringBuilder sb = new StringBuilder();
        //    List<string> list = new List<string>();

        //    using (StreamReader sr = new StreamReader(filepath))
        //    {
        //        while (sr.Peek() >= 0)
        //        {
        //            list.Add(sr.ReadLine());
        //        }
        //    }
        //    for (int i = 1; i < list.Count; i++)
        //    {
        //        string[] str = list[i].Split('|');
        //        dt.Rows.Add(str[0].Replace("\"", ""), str[1].Replace("\"", ""), str[2].Replace("\"", ""), str[3].Replace("\"", ""), str[4].Replace("\"", ""),
        //                    str[5].Replace("\"", ""), str[6].Replace("\"", ""), str[7].Replace("\"", ""), str[8].Replace("\"", ""), str[9].Replace("\"", ""),
        //                    str[10].Replace("\"", ""), str[11].Replace("\"", ""), str[12].Replace("\"", ""), str[13].Replace("\"", ""), str[14].Replace("\"", ""),
        //                    str[15].Replace("\"", ""), str[16].Replace("\"", ""), str[17].Replace("\"", ""), str[18].Replace("\"", ""), str[19].Replace("\"", ""),
        //                    str[20].Replace("\"", ""), str[21].Replace("\"", ""), str[22].Replace("\"", ""), str[23].Replace("\"", ""), str[24].Replace("\"", ""),
        //                    str[25].Replace("\"", ""), str[26].Replace("\"", ""), str[27].Replace("\"", ""), str[28].Replace("\"", ""), str[29].Replace("\"", ""),
        //                    str[30].Replace("\"", ""), str[31].Replace("\"", ""), str[32].Replace("\"", ""), str[33].Replace("\"", ""), str[34].Replace("\"", ""),
        //                    str[35].Replace("\"", ""), str[36].Replace("\"", ""), str[37].Replace("\"", ""), str[38].Replace("\"", ""), str[39].Replace("\"", ""),
        //                    str[40].Replace("\"", ""), str[41].Replace("\"", ""), str[42].Replace("\"", ""), str[43].Replace("\"", ""), str[44].Replace("\"", ""),
        //                    str[45].Replace("\"", ""), str[46].Replace("\"", ""), str[47].Replace("\"", ""), str[48].Replace("\"", ""), str[49].Replace("\"", ""),
        //                    str[50].Replace("\"", ""), str[51].Replace("\"", ""), str[52].Replace("\"", ""), str[53].Replace("\"", ""), str[54].Replace("\"", ""),
        //                    str[55].Replace("\"", ""), str[56].Replace("\"", ""), str[57].Replace("\"", ""), str[58].Replace("\"", ""), str[59].Replace("\"", ""),
        //                    str[60].Replace("\"", ""), str[61].Replace("\"", ""), str[62].Replace("\"", "")
        //            );
        //    }
        //    string constr = ConfigurationManager.ConnectionStrings["CONSTRING"].ConnectionString;
        //    SqlConnection con = new SqlConnection(constr);
        //    try
        //    {
        //        con.Open();
        //        SqlBulkCopy bulkCopy = new SqlBulkCopy(con, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction, null);
        //        bulkCopy.DestinationTableName = "Excel_table";
        //        bulkCopy.WriteToServer(dt);
        //        con.Close();
        //        using (SqlCommand sqlcmd = new SqlCommand())
        //        {
        //            string extn = ".xlsx";
        //            sqlcmd.Connection = con;
        //            con.Open();
        //            sqlcmd.CommandType = CommandType.StoredProcedure;
        //            sqlcmd.CommandText = "spx_ImportFromExcel";
        //            sqlcmd.Parameters.Add("@Extension", SqlDbType.VarChar).Value = extn.ToString();
        //            sqlcmd.ExecuteNonQuery();
        //            con.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}
         