﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Web;
using System.Xml;
using System.Threading;
using System.Configuration;
using System.Text.RegularExpressions;
using System.ComponentModel;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
namespace BS_BAL
{
    public class RedBusService
    {
        private string baseUrl; private string consumerKey; private string consumerSecret;
        BS_DAL.SharedDAL shareddal; string msg = ""; EXCEPTION_LOG.ErrorLog erlog;
        SharedBAL sharedbal = new SharedBAL();
        #region[GET Credentials]
        private DataTable getCredentials()
        {
            DataTable dt = new DataTable();
            try
            {
                shareddal = new BS_DAL.SharedDAL();
                dt = shareddal.getOauthCred();
                return dt;
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
                return dt;
            }
        }
        #endregion
        #region[SET Credentials]
        public RedBusService()
        {
            DataTable dt = new DataTable();
            dt = getCredentials();
            this.baseUrl = dt.Rows[0]["Oauth_BaseUrl"].ToString().Trim();
            this.consumerKey = dt.Rows[0]["Oauth_ConsumerKey"].ToString().Trim();
            this.consumerSecret = dt.Rows[0]["Oauth_ConsumerSecret"].ToString().Trim();
        }
        #endregion
        #region[GET RedBus source List]
        public List<BS_SHARED.SHARED> getRBSrcList()
        {
            List<BS_SHARED.SHARED> getList = new List<BS_SHARED.SHARED>();
            DataSet ds = new DataSet();
            try
            {
                string srcList = GetRequest(ConfigurationManager.AppSettings["AllSrc"].ToString());
                if (srcList.Trim() != "Error")
                {
                    ds = convertJsonStringToDataSet(srcList);
                    getList = ConvertDatasetToList(ds, "", "", "");
                }

            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return getList;
        }
        #endregion
        #region[GET Redbus destination List]


        public List<BS_SHARED.SHARED> getRBDestList(string srcID, string src, string providername)
        {
            DataSet ds = new DataSet(); List<BS_SHARED.SHARED> RBList = new List<BS_SHARED.SHARED>();
            try
            {
                string result = GetRequest(ConfigurationManager.AppSettings["AllDest"].ToString() + "source=" + srcID.Trim());
                if (result.Contains("Error") == false && result!="")
                {
                    ds = convertJsonStringToDataSet(result);
                    if (ds.Tables.Count != 0)
                    {
                        RBList = ConvertDatasetToList(ds, srcID.Trim(), src, providername);
                    }
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return RBList;
        }
        #endregion
        #region[RedBus Result]
        public List<BS_SHARED.SHARED> getRBJourneyList(BS_SHARED.SHARED shared)
        {
            string avaltrip = ""; string path = ""; List<BS_SHARED.SHARED> getList = new List<BS_SHARED.SHARED>();
            List<BS_SHARED.SHARED> farelist = new List<BS_SHARED.SHARED>(); sharedbal = new SharedBAL();
            try
            {
                path = ConfigurationManager.AppSettings["AvalTrip"].ToString().Trim() + "source=" + shared.srcID.Trim() + "&" + "destination=" + shared.destID.Trim() + "&" + "doj=" + shared.journeyDate.Trim();
                
                avaltrip = GetRequest(path);
                try { }
                catch (Exception ex) { }
                if (avaltrip.Trim() != "Error" && avaltrip.Trim() != "null")
                {
                    JObject array = JObject.Parse(avaltrip);
                    string type = array["availableTrips"].Type.ToString();
                    if (type != "Array")
                    {
                        try
                        {
                            JObject objTrip = (JObject)array["availableTrips"];
                            BS_SHARED.SHARED sharedd = new BS_SHARED.SHARED();
                            //sharedd.arrTime = objTrip["arrivalTime"].ToString();
                            sharedd.remainingSeat = int.Parse(objTrip["availableSeats"].ToString());
                            if (objTrip["boardingTimes"] != null)
                            {
                                if (objTrip["boardingTimes"].Type.ToString() == "Array")
                                {
                                    JArray bp = (JArray)objTrip["boardingTimes"];
                                    string[] bpArr = new string[bp.Count];
                                    for (int n = 0; n <= bp.Count - 1; n++)
                                    {
                                        bpArr[n] = bp[n].ToString();
                                    }
                                    sharedd.bdPoint = bpArr;
                                }
                                else
                                {
                                    JObject objbp = (JObject)objTrip["boardingTimes"];
                                    string[] bpArr = new string[1];
                                    // bpArr[0] = "{\"bpId\":\"" + objbp["bpId"].ToString() + "\",\"location\":\"" + objbp["location"].ToString() + "\",\"time\":\"" + objbp["time"].ToString() + "\"}";
                                    bpArr[0] = "{\"bpId\":\"" + objbp["bpId"].ToString() + "\",\"bpName\":\"" + objbp["bpName"].ToString() + "\",\"location\":\"" + objbp["location"].ToString() + "\",\"prime\":\"" + objbp["prime"].ToString() + "\",\"time\":\"" + objbp["time"].ToString() + "\"}";
                                    sharedd.bdPoint = bpArr;
                                }
                            }
                            if (objTrip["droppingTimes"] != null)
                            {
                                if (objTrip["droppingTimes"].Type.ToString() == "Array")
                                {
                                    JArray dp = (JArray)objTrip["droppingTimes"];

                                    string[] dpArr = new string[dp.Count];

                                    for (int m = 0; m <= dp.Count - 1; m++)
                                    {
                                        dpArr[m] = dp[m].ToString();
                                    }
                                    sharedd.drPoint = dpArr;
                                }
                                else
                                {
                                    JObject objdp = (JObject)objTrip["droppingTimes"];
                                    string[] dpArr = new string[1];
                                    // dpArr[0] = "{\"bpId\":\"" + objdp["bpId"].ToString() + "\",\"location\":\"" + objdp["location"].ToString() + "\",\"time\":\"" + objdp["time"].ToString() + "\"}";
                                    dpArr[0] = "{\"bpId\":\"" + objdp["bpId"].ToString() + "\",\"bpName\":\"" + objdp["bpName"].ToString() + "\",\"location\":\"" + objdp["location"].ToString() + "\",\"prime\":\"" + objdp["prime"].ToString() + "\",\"time\":\"" + objdp["time"].ToString() + "\"}";
                                    sharedd.drPoint = dpArr;
                                }
                            }
                            sharedd.serviceType = objTrip["busType"].ToString();
                            sharedd.canPolicy_RB = objTrip["cancellationPolicy"].ToString();
                            if (Convert.ToInt32(objTrip["arrivalTime"].ToString()) == -1)
                                sharedd.arrTime = objTrip["arrivalTime"].ToString();
                            else
                                sharedd.arrTime = setHourstoMinutes(objTrip["arrivalTime"].ToString());//  Convert.ToDateTime(shared.journeyDate).AddMinutes(Convert.ToInt32(objTrip["arrivalTime"].ToString())).ToString("hh:mm tt");
                            if (Convert.ToInt32(objTrip["departureTime"].ToString()) == -1)
                                sharedd.departTime = objTrip["departureTime"].ToString();
                            else
                                sharedd.departTime = setHourstoMinutes(objTrip["departureTime"].ToString());//  Convert.ToDateTime(shared.journeyDate).AddMinutes(Convert.ToInt32(objTrip["departureTime"].ToString())).ToString("hh:mm tt");
                            //sharedd.departTime = objTrip["departureTime"].ToString();
                            if (objTrip["fares"] != null)
                            {
                                if (objTrip["fares"].Type.ToString() == "Array")
                                {
                                    JArray arrFare = (JArray)objTrip["fares"];
                                    string FArr = "";
                                    sharedd.seat_Originalfare = new string[arrFare.Count];
                                    sharedd.Arr_adcomm = new decimal[arrFare.Count];
                                    sharedd.Arr_taTds = new decimal[arrFare.Count];
                                    sharedd.Arr_serviceChrg = new decimal[arrFare.Count];
                                    sharedd.Arr_taNetFare = new decimal[arrFare.Count];
                                    sharedd.Arr_taTotFare = new decimal[arrFare.Count];
                                    sharedd.Arr_totFare = new decimal[arrFare.Count];
                                    for (int n = 0; n <= arrFare.Count - 1; n++)
                                    {
                                        FArr += arrFare[n].ToString() + ",";
                                        sharedd.seat_Originalfare[n] = arrFare[n].ToString();
                                    }
                                    FArr = FArr.Remove(FArr.LastIndexOf(","));
                                    // farelist = sharedbal.getMarkUp(shared.agentID.Trim(), FArr);
                                    shared.fare = FArr;
                                    shared.traveler = objTrip["travels"].ToString();
                                    farelist = sharedbal.getCommissionList(shared);
                                    sharedd.seat_farewithMarkp = new string[farelist.Count];

                                    for (int b = 0; b <= farelist.Count - 1; b++)
                                    {
                                        // sharedd.seat_farewithMarkp[b] = Convert.ToString(Math.Round(farelist[b].mrkpFare, 0));
                                        sharedd.seat_farewithMarkp[b] = Convert.ToString(Math.Round(farelist[b].taNetFare, 0));
                                        sharedd.Arr_adcomm[b] = farelist[b].adcomm;
                                        sharedd.Arr_taTds[b] = farelist[b].taTds;
                                        sharedd.Arr_serviceChrg[b] = farelist[b].serviceChrg;
                                        sharedd.Arr_taNetFare[b] = farelist[b].taNetFare;
                                        sharedd.Arr_taTotFare[b] = farelist[b].taTotFare;
                                        sharedd.Arr_totFare[b] = farelist[b].totFare;
                                    }
                                }
                                else
                                {
                                    //farelist = sharedbal.getMarkUp(shared.agentID.Trim(), objTrip["fares"].ToString());
                                    shared.fare = objTrip["fares"].ToString();
                                    shared.traveler = objTrip["travels"].ToString();
                                    farelist = sharedbal.getCommissionList(shared);
                                    sharedd.seat_farewithMarkp = new string[farelist.Count];
                                    sharedd.Arr_adcomm = new decimal[farelist.Count];
                                    sharedd.Arr_taTds = new decimal[farelist.Count];
                                    sharedd.Arr_serviceChrg = new decimal[farelist.Count];
                                    sharedd.Arr_taNetFare = new decimal[farelist.Count];
                                    sharedd.Arr_taTotFare = new decimal[farelist.Count];
                                    sharedd.Arr_totFare = new decimal[farelist.Count];

                                    sharedd.seat_Originalfare = new string[1];
                                    sharedd.seat_Originalfare[0] = objTrip["fares"].ToString();
                                    sharedd.seat_farewithMarkp[0] = Convert.ToString(Math.Round(farelist[0].taNetFare, 0));
                                    sharedd.Arr_adcomm[0] = farelist[0].adcomm;
                                    sharedd.Arr_taTds[0] = farelist[0].taTds;
                                    sharedd.Arr_serviceChrg[0] = farelist[0].serviceChrg;
                                    sharedd.Arr_taNetFare[0] = farelist[0].taNetFare;
                                    sharedd.Arr_taTotFare[0] = farelist[0].taTotFare;
                                    sharedd.Arr_totFare[0] = farelist[0].totFare;
                                }
                            }
                            sharedd.serviceID = objTrip["id"].ToString();
                            sharedd.idproofReq = objTrip["idProofRequired"].ToString();
                            sharedd.traveler = objTrip["travels"].ToString();
                            sharedd.partialCanAllowed = objTrip["partialCancellationAllowed"].ToString();
                            int durTime1 = 0;

                            if (objTrip["arrivalTime"].ToString() == "-1")
                            {
                                sharedd.Dur_Time = "NA";
                            }
                            else
                            {
                                durTime1 = Convert.ToInt32(objTrip["arrivalTime"].ToString()) - Convert.ToInt32(objTrip["departureTime"].ToString());
                                int ddMM1 = durTime1 % 60;
                                int ddHH1 = durTime1 / 60;
                                //sharedd.Dur_Time = Convert.ToString(ddHH1 + ":" + ddMM1 + ":" + "00");
                                string newddmm1 = "", newddhh1 = "";

                                if (ddMM1 < 10)
                                {
                                    newddmm1 = "0" + ddMM1;
                                }
                                if (ddHH1 < 10)
                                {
                                    newddhh1 = "0" + ddHH1;
                                }
                                if (newddmm1 != "" && newddhh1 == "")
                                {
                                    sharedd.Dur_Time = Convert.ToString(ddHH1 + ":" + newddmm1 + ":" + "00");
                                }
                                else if (newddhh1 != "" && newddmm1 == "")
                                {
                                    sharedd.Dur_Time = Convert.ToString(newddhh1 + ":" + ddMM1 + ":" + "00");
                                }
                                else if (newddhh1 != "" && newddmm1 == "")
                                {
                                    sharedd.Dur_Time = Convert.ToString(newddhh1 + ":" + newddmm1 + ":" + "00");
                                }
                                else
                                {
                                    sharedd.Dur_Time = Convert.ToString(ddHH1 + ":" + ddMM1 + ":" + "00");
                                }
                            }
                            sharedd.provider_name = "RB";
                            if (sharedd.drPoint != null && sharedd.bdPoint != null)
                                getList.Add(sharedd);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    else
                    {
                        JArray tripid = (JArray)array["availableTrips"];
                        try
                        {
                            for (int k = 0; k <= tripid.Count - 1; k++)
                            {
                                if (tripid[k]["boardingTimes"] != null && tripid[k]["droppingTimes"] != null)
                                {
                                    BS_SHARED.SHARED sharedd = new BS_SHARED.SHARED();
                                    //sharedd.arrTime = tripid[k]["arrivalTime"].ToString();
                                    sharedd.remainingSeat = int.Parse(tripid[k]["availableSeats"].ToString());
                                    if (tripid[k]["boardingTimes"] != null)
                                    {
                                        if (tripid[k]["boardingTimes"].Type.ToString() == "Array")
                                        {
                                            JArray bp = (JArray)tripid[k]["boardingTimes"];
                                            string[] bpArr = new string[bp.Count];
                                            for (int n = 0; n <= bp.Count - 1; n++)
                                            {
                                                bpArr[n] = bp[n].ToString();
                                            }
                                            sharedd.bdPoint = bpArr;
                                        }
                                        else
                                        {
                                            JObject objbp = (JObject)tripid[k]["boardingTimes"];
                                            string[] bpArr = new string[1];
                                            bpArr[0] = "{\"bpId\":\"" + objbp["bpId"].ToString() + "\",\"bpName\":\"" + objbp["bpName"].ToString() + "\",\"location\":\"" + objbp["location"].ToString() + "\",\"prime\":\"" + objbp["prime"].ToString() + "\",\"time\":\"" + objbp["time"].ToString() + "\"}";
                                            sharedd.bdPoint = bpArr;
                                        }
                                    }
                                    if (tripid[k]["droppingTimes"] != null)
                                    {
                                        if (tripid[k]["droppingTimes"].Type.ToString() == "Array")
                                        {
                                            JArray dp = (JArray)tripid[k]["droppingTimes"];

                                            string[] dpArr = new string[dp.Count];

                                            for (int m = 0; m <= dp.Count - 1; m++)
                                            {
                                                dpArr[m] = dp[m].ToString();
                                            }
                                            sharedd.drPoint = dpArr;
                                        }
                                        else
                                        {
                                            JObject objdp = (JObject)tripid[k]["droppingTimes"];
                                            string[] dpArr = new string[1];
                                            dpArr[0] = "{\"bpId\":\"" + objdp["bpId"].ToString() + "\",\"bpName\":\"" + objdp["bpName"].ToString() + "\",\"location\":\"" + objdp["location"].ToString() + "\",\"prime\":\"" + objdp["prime"].ToString() + "\",\"time\":\"" + objdp["time"].ToString() + "\"}";

                                            //   dpArr[0] = "{\"bpId\":\"" + objdp["bpId"].ToString() + "\",\"location\":\"" + objdp["location"].ToString() + "\",\"time\":\"" + objdp["time"].ToString() + "\"}";
                                            sharedd.drPoint = dpArr;
                                        }
                                    }
                                    sharedd.serviceType = tripid[k]["busType"].ToString();
                                    sharedd.canPolicy_RB = tripid[k]["cancellationPolicy"].ToString();
                                    if (Convert.ToInt32(tripid[k]["arrivalTime"].ToString()) == -1)
                                        sharedd.arrTime = tripid[k]["arrivalTime"].ToString();
                                    else
                                        sharedd.arrTime = setHourstoMinutes(tripid[k]["arrivalTime"].ToString());//  Convert.ToDateTime(shared.journeyDate).AddMinutes(Convert.ToInt32(tripid[k]["arrivalTime"].ToString())).ToString("hh:mm tt");
                                    if (Convert.ToInt32(tripid[k]["departureTime"].ToString()) == -1)
                                        sharedd.departTime = tripid[k]["departureTime"].ToString();
                                    else
                                        sharedd.departTime = setHourstoMinutes(tripid[k]["departureTime"].ToString());// Convert.ToDateTime(shared.journeyDate).AddMinutes(Convert.ToInt32(tripid[k]["departureTime"].ToString())).ToString("hh:mm tt");
                                    //sharedd.departTime = tripid[k]["departureTime"].ToString();
                                    if (tripid[k]["fares"] != null)
                                    {
                                        if (tripid[k]["fares"].Type.ToString() == "Array")
                                        {
                                            JArray arrFare = (JArray)tripid[k]["fares"];
                                            string FArr = "";
                                            sharedd.seat_Originalfare = new string[arrFare.Count];                                                                                                                                           
                                            sharedd.Arr_adcomm = new decimal[arrFare.Count];
                                            sharedd.Arr_taTds = new decimal[arrFare.Count];
                                            sharedd.Arr_serviceChrg = new decimal[arrFare.Count];
                                            sharedd.Arr_taNetFare = new decimal[arrFare.Count];
                                            sharedd.Arr_taTotFare = new decimal[arrFare.Count];
                                            sharedd.Arr_totFare = new decimal[arrFare.Count];
                                            for (int n = 0; n <= arrFare.Count - 1; n++)
                                            {
                                                FArr += arrFare[n].ToString() + ",";
                                                sharedd.seat_Originalfare[n] = arrFare[n].ToString();
                                            }
                                            FArr = FArr.Remove(FArr.LastIndexOf(","));
                                            //  farelist = sharedbal.getMarkUp(shared.agentID.Trim(), FArr);
                                            shared.fare = FArr;
                                            shared.traveler = tripid[k]["travels"].ToString();
                                            farelist = sharedbal.getCommissionList(shared);
                                            sharedd.seat_farewithMarkp = new string[farelist.Count];

                                            for (int b = 0; b <= farelist.Count - 1; b++)
                                            {
                                                sharedd.seat_farewithMarkp[b] = Convert.ToString(Math.Round(farelist[b].taNetFare, 0));
                                                sharedd.Arr_adcomm[b] = farelist[b].adcomm;
                                                sharedd.Arr_taTds[b] = farelist[b].taTds;
                                                sharedd.Arr_serviceChrg[b] = farelist[b].serviceChrg;
                                                sharedd.Arr_taNetFare[b] = farelist[b].taNetFare;
                                                sharedd.Arr_taTotFare[b] = farelist[b].taTotFare;
                                                sharedd.Arr_totFare[b] = farelist[b].totFare;
                                                //serviceChrg = totmrkp, taNetFare = taNetTotal, taTotFare = totalfare, totFare = totalfare 
                                            }
                                        }
                                        else
                                        {
                                            // farelist = sharedbal.getMarkUp(shared.agentID.Trim(), tripid[k]["fares"].ToString());
                                            shared.fare = tripid[k]["fares"].ToString();
                                            shared.traveler = tripid[k]["travels"].ToString();
                                            farelist = sharedbal.getCommissionList(shared);
                                            sharedd.seat_farewithMarkp = new string[farelist.Count];
                                            sharedd.Arr_adcomm = new decimal[farelist.Count];
                                            sharedd.Arr_taTds = new decimal[farelist.Count];
                                            sharedd.Arr_serviceChrg = new decimal[farelist.Count];
                                            sharedd.Arr_taNetFare = new decimal[farelist.Count];
                                            sharedd.Arr_taTotFare = new decimal[farelist.Count];
                                            sharedd.Arr_totFare = new decimal[farelist.Count];
                                            sharedd.seat_Originalfare = new string[1];
                                            sharedd.seat_Originalfare[0] = tripid[k]["fares"].ToString();
                                            //sharedd.seat_farewithMarkp[0] = Convert.ToString(Math.Round(farelist[0].mrkpFare, 0));
                                            sharedd.seat_farewithMarkp[0] = Convert.ToString(Math.Round(farelist[0].taNetFare, 0));
                                            sharedd.Arr_adcomm[0] = farelist[0].adcomm;
                                            sharedd.Arr_taTds[0] = farelist[0].taTds;
                                            sharedd.Arr_serviceChrg[0] = farelist[0].serviceChrg;
                                            sharedd.Arr_taNetFare[0] = farelist[0].taNetFare;
                                            sharedd.Arr_taTotFare[0] = farelist[0].taTotFare;
                                            sharedd.Arr_totFare[0] = farelist[0].totFare;
                                        }
                                    }
                                    sharedd.serviceID = tripid[k]["id"].ToString();
                                    sharedd.idproofReq = tripid[k]["idProofRequired"].ToString();
                                    sharedd.traveler = tripid[k]["travels"].ToString();
                                    sharedd.partialCanAllowed = tripid[k]["partialCancellationAllowed"].ToString();
                                    int durTime = 0;
                                    try
                                    {
                                        if (tripid[k]["arrivalTime"].ToString() == "-1")
                                        {
                                            sharedd.Dur_Time = "NA";
                                        }
                                        else
                                        {
                                            durTime = Convert.ToInt32(tripid[k]["arrivalTime"].ToString()) - Convert.ToInt32(tripid[k]["departureTime"].ToString());
                                            int ddMM = durTime % 60;
                                            int ddHH = durTime / 60;
                                            //sharedd.Dur_Time = Convert.ToString(ddHH + ":" + ddMM + ":" + "00");
                                            string newddmm = "", newddhh = "";

                                            if (ddMM < 10)
                                            {
                                                newddmm = "0" + ddMM;
                                            }
                                            if (ddHH < 10)
                                            {
                                                newddhh = "0" + ddHH;
                                            }
                                            if (newddmm == "" && newddhh == "")
                                            {
                                                sharedd.Dur_Time = Convert.ToString(ddHH + ":" + ddMM + ":" + "00");
                                            }
                                            else if (newddmm != "" && newddhh == "")
                                            {
                                                sharedd.Dur_Time = Convert.ToString(ddHH + ":" + newddmm + ":" + "00");
                                            }
                                            else if (newddmm == "" && newddhh != "")
                                            {
                                                sharedd.Dur_Time = Convert.ToString(newddhh + ":" + ddMM + ":" + "00");
                                            }
                                            else if (newddmm != "" && newddhh != "")
                                            {
                                                sharedd.Dur_Time = Convert.ToString(newddhh + ":" + newddmm + ":" + "00");
                                            }
                                        }
                                        sharedd.provider_name = "RB";
                                        if (sharedd.drPoint != null && sharedd.bdPoint != null)
                                            getList.Add(sharedd);
                                    }
                                    catch (Exception ex)
                                    {
                                    }
                                }
                            }
                        }
                        catch (Exception ex) { }
                    }
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return getList;
        }
        #endregion
        #region[Create Request Header]
        private string formHeader(string requestUrl, string methodType)
        {
            DataTable dt = new DataTable(); string finalAuthHeader = "";
            try
            {
                OauthBase oauthBase = new OauthBase();
                string normalisedUrl = string.Empty;
                string normalisedParams = string.Empty;
                string authHeader = string.Empty;
                string timeStamp = oauthBase.GenerateTimeStamp();
                string nonce = oauthBase.GenerateNonce();

                //string requestWithAuth = oauthBase.GenerateSignature(new Uri(requestUrl), this.consumerKey, this.consumerSecret,
                //    "", "", methodType, timeStamp, nonce, OauthBase.SignatureTypes.HMACSHA1, out normalisedUrl, out normalisedParams, out authHeader);
                //finalAuthHeader = "OAuth oauth_nonce=\"" + nonce + "\",oauth_consumer_key=\"" + consumerKey + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\""
                //  + timeStamp + "\",oauth_version=\"1.0\",oauth_signature=\"" + HttpUtility.UrlEncode(requestWithAuth) + "\"";
                string requestWithAuth = oauthBase.GenerateSignature(new Uri(requestUrl), this.consumerKey, this.consumerSecret,
                "", "", methodType, timeStamp, nonce, OauthBase.SignatureTypes.HMACSHA1, out normalisedUrl, out normalisedParams, out authHeader);
               finalAuthHeader = "OAuth oauth_nonce=\"" + nonce + "\",oauth_consumer_key=\"" + consumerKey + "\",oauth_signature_method=\"HMAC-SHA1\",oauth_timestamp=\""
                   + timeStamp + "\",oauth_version=\"1.0\",oauth_signature=\"" + HttpUtility.UrlEncode(requestWithAuth) + "\"";
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return finalAuthHeader;
        }


    
        #endregion
        #region[Get Request]
        public string GetRequest(string requestUrl)
        {
            string completeUrl = baseUrl + requestUrl;
            string header = ""; string responseString = "";
            try
            {
                
                HttpWebRequest request1 = WebRequest.Create(completeUrl) as HttpWebRequest;
                request1.ContentType = "application/json";
                request1.Method = "GET";
                header = formHeader(completeUrl, "GET");
                request1.Headers.Add(HttpRequestHeader.Authorization, header);
                request1.Timeout = 300000;
                HttpWebResponse httpWebResponse = (HttpWebResponse)request1.GetResponse();
                StreamReader reader = new StreamReader(httpWebResponse.GetResponseStream());
                responseString = reader.ReadToEnd();
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
                responseString = "Error";
            }
            return responseString;
        }
        #endregion
        #region[Post Request]
        private string GetPostRequest(string requestUrl, string requestBody)
        {
            string completeUrl = baseUrl + requestUrl;
            string header = ""; string response = "";
            var timeout = 4 * 60 * 1000;
            try
            {
                try
                {
                    HttpWebRequest request = WebRequest.Create(completeUrl) as HttpWebRequest;
                    request.ContentType = "application/json";
                    request.Method = "POST";
                    header = formHeader(completeUrl, "POST");
                    request.Timeout = timeout;
                    request.Headers.Add(HttpRequestHeader.Authorization, header);
                    StreamWriter requestWriter = new StreamWriter(request.GetRequestStream());
                    requestWriter.Write(requestBody);
                    requestWriter.Close();
                    HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse();
                    StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                    response = responseReader.ReadToEnd();
                }
                catch (WebException ex)
                {
                    //reading the custom messages sent by the server
                    using (var reader = new StreamReader(ex.Response.GetResponseStream()))
                    {
                        return reader.ReadToEnd();
                        response = "Error";
                    }
                }
                catch (Exception ex)
                {
                    return "Failed with exception message:" + ex.Message;
                    response = "Error";
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
                response = "Error";
            }
            return response;
        }
        #endregion
        #region[convert json to dataset]
        public DataSet convertJsonStringToDataSet(string jsonString)
        {
            DataSet ds = new DataSet();
            if (jsonString != "" && jsonString.Contains("Error")==false)
            {
                XmlDocument xd = new XmlDocument();
                jsonString = "{ \"rootNode\": {" + jsonString.Trim().TrimStart('{').TrimEnd('}') + "} }";
                xd = (XmlDocument)JsonConvert.DeserializeXmlNode(jsonString);
                ds.ReadXml(new XmlNodeReader(xd));
                
            }
            return ds;
        }
        #endregion
        #region[convert dataset to list]
        private List<BS_SHARED.SHARED> ConvertDatasetToList(DataSet ds, string srcid, string src, string providername)
        {
            List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>();
            try
            {
                if (srcid.Trim() == "")
                {
                    for (int t = 0; t <= ds.Tables[0].Rows.Count - 1; t++)
                    {
                        list.Add(new BS_SHARED.SHARED { src = ds.Tables[0].Rows[t]["name"].ToString(), srcID = ds.Tables[0].Rows[t]["id"].ToString(), provider_name = "RB" });
                    }
                }
                else
                {
                    for (int t = 0; t <= ds.Tables[0].Rows.Count - 1; t++)
                    {
                        list.Add(new BS_SHARED.SHARED { src = src, dest = ds.Tables[0].Rows[t]["name"].ToString(), srcID = srcid, destID = ds.Tables[0].Rows[t]["id"].ToString(), provider_name = providername });
                    }
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return list;
        }
        #endregion
        #region[Get Source ID]
        public DataTable getSrcID(BS_SHARED.SHARED shared)
        {
            DataTable dt = new DataTable();
            shareddal = new BS_DAL.SharedDAL();
            try
            {
                dt = shareddal.getSRCDEST_ID(shared);
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
                return dt;
            }
            return dt;
        }
        #endregion
        #region[RedBus layout]

        public string RBSeatLayout(BS_SHARED.SHARED shared)
        {
            string tripDetails = ""; List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>();
            sharedbal = new SharedBAL(); bool UP_LPlength = false;
            string layout_lower = "<table cellpadding='0' cellspacing='0'  id='seatTbllower_RB' class='tbl' border='0px'>";
            string layout_upper = "<table cellpadding='0' cellspacing='0'  id='seatTblupper_RB' class='tbl' border='0px'>";
            string seatLayout = "<table width='100%' cellpadding='0' cellspacing='0'>";
            seatLayout += "<tr>";
            seatLayout += "<td class='seatLayoutheaderSelect'>Select Your Seat:</td>";
            seatLayout += "</tr>";
            seatLayout += "<tr>";
            DateTime date = Convert.ToDateTime(shared.journeyDate.Trim().Replace("-", "/"));
            seatLayout += "<td class='seatLayoutheader'>" + shared.src.Trim() + " To " + shared.dest.Trim() + " on, " + date.DayOfWeek + " " + date.Day + " " + date.ToString("MMM") + " " + date.Year + " (" + shared.traveler + ")</td>";
            seatLayout += "</tr>";
            try
            {
                tripDetails = GetRequest(ConfigurationManager.AppSettings["TripDetails"].ToString() + "id=" + shared.serviceID.Trim());
                if (tripDetails.Trim() != "Error")
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    RootObject objRootObject = new RootObject();
                    RootObject LIRootObject = new RootObject();
                    RootObject LIURootObject = new RootObject();
                    RootObject collection = serializer.Deserialize<RootObject>(tripDetails.ToString());
                    objRootObject.seats = collection.seats;
                    string[] maxRowcol = getMaxRowCol(objRootObject);
                    int lr = 0; int Ur = 0;
                    for (int R = 0; R <= Convert.ToInt32(maxRowcol[0]); R++)
                    {
                        LIRootObject.seats = objRootObject.seats.OrderBy(x => Convert.ToInt32(x.column)).Where(x => x.row.ToString() == Convert.ToString(R)).ToList().Where(u => u.zIndex.ToString() == "0").ToList();
                        LIURootObject.seats = objRootObject.seats.OrderBy(u => Convert.ToInt32(u.column)).Where(u => u.row.ToString() == Convert.ToString(R)).ToList().Where(u => u.zIndex.ToString() == "1").ToList();
                        if (LIRootObject.seats.Count != 0)
                        {
                            lr++;
                            layout_lower += SET_UPPER_LOWER(LIRootObject, Convert.ToInt32(maxRowcol[1]), list, shared);
                        }
                        if (LIURootObject.seats.Count != 0)
                        {
                            Ur++;
                            UP_LPlength = true;
                            layout_upper += SET_UPPER_LOWER(LIURootObject, Convert.ToInt32(maxRowcol[1]), list, shared);
                        }
                        if (lr == 2)
                        {
                            if (LIRootObject.seats.Count < 1)
                                layout_lower += "<tr><td colspan='" + Convert.ToInt32(Convert.ToInt32(maxRowcol[1]) * 2) + 1 + "'><div  style='height:30px;width:30px;'>&nbsp;</div></td></tr>";
                        }
                        if (Ur == 2)
                        {
                            if (LIURootObject.seats.Count < 1)
                                layout_upper += "<tr><td colspan='" + Convert.ToInt32(Convert.ToInt32(maxRowcol[1]) * 2) + 1 + "'><div  style='height:30px;width:30px;'>&nbsp;</div></td></tr>";

                        }

                    }
                    layout_lower += "</table>";
                    layout_upper += "</table>";
                    if (UP_LPlength == true)
                    {
                        seatLayout += "<tr>";
                        seatLayout += "<td><span style='font-weight:bold; width:30%; color:#175d80; font-size:12px;'>Upper</span>";
                        seatLayout += layout_upper;
                        seatLayout += "</td>";
                        seatLayout += "</tr>";
                        seatLayout += "<tr>";
                        seatLayout += "<td><span style='font-weight:bold; width:30%; color:#175d80; font-size:12px;'>Lower</span>";
                        seatLayout += layout_lower;
                        seatLayout += "</td>";
                        seatLayout += "</tr>";
                        seatLayout += "</table>";
                    }
                    else
                    {
                        seatLayout += "<tr>";
                        seatLayout += "<td><span style='font-weight:bold; width:30%; color:#175d80; font-size:12px;'></span>";
                        seatLayout += layout_lower;
                        seatLayout += "</td>";
                        seatLayout += "</tr>";
                        seatLayout += "</table>";
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
                seatLayout = "Error";
            }
            return seatLayout;
        }
        #endregion
        public string SET_UPPER_LOWER(RootObject LIRootObjects, int maxrow, List<BS_SHARED.SHARED> list, BS_SHARED.SHARED shared)
        {
            string birthLayout = "";
            birthLayout += "<tr>";
            int s = 0;

            for (int C = 0; C <= maxrow; C++)
            {
                if (C == Convert.ToInt32(LIRootObjects.seats[s].column.ToString().Trim()))
                {
                    shared.fare = LIRootObjects.seats[s].fare.ToString().Trim();
                    list = sharedbal.getCommissionList(shared);
                    //sharedbal.getMarkUp(shared.agentID.Trim(), LIRootObjects.seats[s].fare.ToString().Trim());

                    string CheckConditions = ""; CheckConditions = LIRootObjects.seats[s].zIndex.ToString() + "," + LIRootObjects.seats[s].length.ToString() + "," + LIRootObjects.seats[s].width.ToString();
                    string ladiesSeat = ""; ladiesSeat = LIRootObjects.seats[s].ladiesSeat.ToString();
                    string available = ""; available = LIRootObjects.seats[s].available.ToString();
                    // for (int m = 0; m < shared.seatfarewithMarkp.Trim().Split(',').Length; m++)
                    //for (int m = 0; m < list.Count; m++)
                    //{
                    //  if (Math.Round(Convert.ToDecimal(shared.seatfare.Trim().Split(',')[m]), 0) == Math.Round(Convert.ToDecimal(LIRootObjects.seats[s].fare.ToString()), 0) || 0 == Math.Round(Convert.ToDecimal(LIRootObjects.seats[s].fare.ToString()), 0))
                    string divShow = "";
                    divShow += "<div style='line-height:30px;'><div>Seat No:" + LIRootObjects.seats[s].name.ToString() + "</div><div>Fare:" + Convert.ToString(Math.Round(list[0].taTotFare, 0)).Trim().Trim() + "</div>";
                    divShow += "<div></div><div></div><div>" + LIRootObjects.seats[s].serviceTaxAbsolute + "</div><div></div><div>" + LIRootObjects.seats[s].fare + "</div><div></div></div>";
                    birthLayout += "<td class='ES'><div class='" + SetClassName(CheckConditions, ladiesSeat, available) + "'  title='Seat No:" + LIRootObjects.seats[s].name.ToString() + " | Fare:" + Convert.ToString(Math.Round(list[0].taTotFare, 0)).Trim().Trim() + "' rel='Seat No:" + LIRootObjects.seats[s].name.ToString() + " | Fare:" + LIRootObjects.seats[s].fare.ToString() + "| Provider:" + shared.provider_name + "'></div><br /><div id='" + LIRootObjects.seats[s].name.Replace(" ", "").Replace("(", "").Replace(")", "") + "' class='divSeatshow'>" + divShow + "</div></td>";
                    // }
                    if (C < Convert.ToInt32(LIRootObjects.seats[Convert.ToInt32(LIRootObjects.seats.Count - 1)].column.ToString().Trim()))
                        s = s + 1;
                }
                else
                {
                    birthLayout += "<td><div>&nbsp;</div></td>";
                }
            }
            birthLayout += "</tr>";


            return birthLayout;
        }

        //public string SET_UPPER_LOWER(RootObject LIRootObjects, int maxrow, List<BS_SHARED.SHARED> list, BS_SHARED.SHARED shared)
        //{
        //    string birthLayout = "";
        //    birthLayout += "<tr>";
        //    int s = 0;

        //    for (int C = 0; C <= maxrow; C++)
        //    {
        //        if (C == Convert.ToInt32(LIRootObjects.seats[s].column.ToString().Trim()))
        //        {
        //            shared.fare = LIRootObjects.seats[s].fare.ToString().Trim();
        //            list = sharedbal.getCommissionList(shared);
        //            //sharedbal.getMarkUp(shared.agentID.Trim(), LIRootObjects.seats[s].fare.ToString().Trim());

        //            string CheckConditions = ""; CheckConditions = LIRootObjects.seats[s].zIndex.ToString() + "," + LIRootObjects.seats[s].length.ToString() + "," + LIRootObjects.seats[s].width.ToString();
        //            string ladiesSeat = ""; ladiesSeat = LIRootObjects.seats[s].ladiesSeat.ToString();
        //            string available = ""; available = LIRootObjects.seats[s].available.ToString();
        //            // for (int m = 0; m < shared.seatfarewithMarkp.Trim().Split(',').Length; m++)
        //            //for (int m = 0; m < list.Count; m++)
        //            //{
        //            //  if (Math.Round(Convert.ToDecimal(shared.seatfare.Trim().Split(',')[m]), 0) == Math.Round(Convert.ToDecimal(LIRootObjects.seats[s].fare.ToString()), 0) || 0 == Math.Round(Convert.ToDecimal(LIRootObjects.seats[s].fare.ToString()), 0))
        //            birthLayout += "<td><div class='" + SetClassName(CheckConditions, ladiesSeat, available) + "'  title='Seat No:" + LIRootObjects.seats[s].name.ToString() + " | Fare:" + Convert.ToString(Math.Round(list[0].taTotFare, 0)).Trim().Trim() + "' rel='Seat No:" + LIRootObjects.seats[s].name.ToString() + " | Fare:" + LIRootObjects.seats[s].fare.ToString() + "| Provider:" + shared.provider_name + "'></div></td>";
        //            // }
        //            if (C < Convert.ToInt32(LIRootObjects.seats[Convert.ToInt32(LIRootObjects.seats.Count - 1)].column.ToString().Trim()))
        //                s = s + 1;
        //        }
        //        else
        //        {
        //            birthLayout += "<td><div>&nbsp;</div></td>";
        //        }
        //    }
        //    birthLayout += "</tr>";


        //    return birthLayout;
        //}
        #region[Get Max row and Col]




        private string[] getMaxRowCol(RootObject listObj)
        {
            string[] row = new string[listObj.seats.Count];
            string[] col = new string[listObj.seats.Count];
            string[] max = new string[2];
            try
            {
                var a = from s in listObj.seats
                        select new { row = int.Parse(s.row.ToString()), col = int.Parse(s.column.ToString()) };
                max[0] = Convert.ToString(a.Select(x => x.row).Max());
                max[1] = Convert.ToString(a.Select(x => x.col).Max());
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return max;
        }
        #endregion
        #region[Block Seat]
        public string getBlockKey(string list)
        {
            string key = "";
            try
            {
                key = GetPostRequest(ConfigurationManager.AppSettings["BlockTicket"].ToString().Trim(), list);
            
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return key;
        }
        #endregion
        #region[Book Seat]
        public List<BS_SHARED.SHARED> getFinalTicket_RB(BS_SHARED.SHARED shared)
        {
            List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>();
            DataSet ds = new DataSet(); string response = "";
            try
            {                
                string ticketno = GetPostRequest(ConfigurationManager.AppSettings["BookTicket"].ToString().Trim() + "blockKey=" + shared.blockKey.Trim(), "");
                //string ticketno = GetPostRequest(ConfigurationManager.AppSettings["BookTicket"].ToString().Trim() + "blockKey=" + "r7iH3Odeve", "");
                if (ticketno != "" && ticketno.Contains("Error")==false)
                {
                    //var strssdsd = "";
                    //response = strssdsd;//getTicketDetails(ticketno.Trim());
                    response = getTicketDetails(ticketno.Trim());
                    if (response.Contains("Error")==false && response != "")
                    {
                        shareddal.UPDATEBOOKING_STATUS(shared.orderID, "Booked", ticketno);
                        ds = convertJsonStringToDataSet(response);
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                list.Add(new BS_SHARED.SHARED { bookres = response.Trim(), blockKey = shared.blockKey.Trim(), tin = ds.Tables[0].Rows[0]["tin"].ToString(), pnr = ds.Tables[0].Rows[0]["pnr"].ToString(), partialCancel = ds.Tables[0].Rows[0]["partialCancellationAllowed"].ToString(), status = "success" });
                            }
                        }
                        else
                        {
                            list.Add(new BS_SHARED.SHARED { bookres = response.Trim(), status = "Fail", TicketNoRes = ticketno });
                            shared.bookreq = "not";
                            shared.TicketNoRes = ticketno;
                            shared.bookres = response.Trim();
                            shareddal.updateBookResponse(shared);
                        }
                    }
                    else
                    {
                        list.Add(new BS_SHARED.SHARED { bookres = response.Trim(), status = "Fail",TicketNoRes = ticketno });
                        shared.bookreq = "not";
                        shared.TicketNoRes = ticketno;
                        shared.bookres = response.Trim();
                        shareddal.updateBookResponse(shared);
                    }
                }
                else
                {
                    list.Add(new BS_SHARED.SHARED { bookres = response.Trim(), status = "Fail", TicketNoRes = ticketno });
                    shared.bookreq = "not";
                    shared.TicketNoRes = ticketno;
                    shared.bookres = response.Trim();
                    shareddal.updateBookResponse(shared);
                }
            }
            catch (Exception ex)
            {
                shared.TicketNoRes = ConfigurationManager.AppSettings["BookTicket"].ToString().Trim() + "blockKey=" + shared.blockKey;
                shared.bookreq = "not"; //ConfigurationManager.AppSettings["BookTicket"].ToString().Trim() + "blockKey=" + shared.blockKey;
                shared.bookres = response.Trim() + "_Error_" + ex.Message;
                shareddal.updateBookResponse(shared);
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
            }
            return list;
        }
        #endregion
        #region[get cancellation data]
        public string getcancelData(string tin)
        {
            string canceldata = ""; DataSet ds = new DataSet();
            string cancellable = "";
            string url = ConfigurationManager.AppSettings["CancelData"].ToString();
            try
            {
                canceldata = GetRequest(url + "tin=" + tin);
                if (canceldata.Trim() != "Error")
                {
                    ds = convertJsonStringToDataSet(canceldata);
                    cancellable = ds.Tables[0].Rows[0]["cancellable"].ToString();
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
                cancellable = "Error";
            }
            return cancellable;
        }
        #endregion
        #region[cancel ticket]
        private string[] cancelTicket(BS_SHARED.SHARED shared)
        {
            string url = ConfigurationManager.AppSettings["CancelTicket"].ToString();
            string cancelstr = ""; string cancelRes = ""; string[] s; string[] canstr = new string[4];
            string ss = ""; DataSet ds = new DataSet(); shareddal = new BS_DAL.SharedDAL();
            try
            {
                if (shared.seat.IndexOf(",") > 0)
                {
                    s = shared.seat.Split(',');
                    for (int y = 0; y <= s.Length - 1; y++)
                    {
                        ss += "\"" + s[y].Trim() + "\"" + ",";
                    }
                    ss = ss.Remove(ss.LastIndexOf(","));
                    cancelstr = "{\"tin\":\"" + shared.tin + "\",\"seatsToCancel\":[" + ss.Trim() + "]}";
                    shared.canrequest = cancelstr;
                    #region[save cancel request into db]
                    shareddal.InsertUpdateCanREQ(shared, "Insert");
                    #endregion
                    cancelRes = GetPostRequest(url, cancelstr);
                    //Changes 07-08/2014
                    if (cancelRes != "" && cancelRes.Contains("Error") == false)
                    {
                        #region[save cancel request into db]
                        shared.canresponse = cancelRes;
                        shareddal.InsertUpdateCanREQ(shared, "Update");
                        #endregion
                        ds = convertJsonStringToDataSet(cancelRes);
                        canstr[0] = "Success";
                        canstr[1] = ds.Tables[0].Rows[0]["cancellationCharge"].ToString();
                        canstr[2] = ds.Tables[0].Rows[0]["refundAmount"].ToString();
                        canstr[3] = ds.Tables[0].Rows[0]["tin"].ToString();
                    }
                    else  //Add new Changes 07-08/2014
                    {
                        #region[save cancel request into db]
                        shared.canresponse = cancelRes;
                        shareddal.InsertUpdateCanREQ(shared, "Update");
                        canstr[0] = "Fail";
                        canstr[1] = cancelRes;
                        #endregion
                    }
                }
                else
                {
                    cancelstr = "{\"tin\":\"" + shared.tin + "\",\"seatsToCancel\":[\"" + shared.seat + "\"]}";
                    shared.canrequest = cancelstr;
                    #region[save cancel request into db]
                    shareddal.InsertUpdateCanREQ(shared, "Insert");
                    #endregion
                    cancelRes = GetPostRequest(url, cancelstr);
                    if (cancelRes != "" && cancelRes.Contains("Error") == false)
                    {
                        #region[save cancel request into db]
                        shared.canresponse = cancelRes;
                        shareddal.InsertUpdateCanREQ(shared, "Update");
                        #endregion
                        ds = convertJsonStringToDataSet(cancelRes);
                        //Change 07-08/2014

                        canstr[0] = "Success";
                        canstr[1] = ds.Tables[0].Rows[0]["cancellationCharge"].ToString();
                        canstr[2] = ds.Tables[0].Rows[0]["refundAmount"].ToString();
                        canstr[3] = ds.Tables[0].Rows[0]["tin"].ToString();

                    }
                    else  //Add new Changes 07-08/2014
                    {
                        #region[save cancel request into db]
                        shared.canresponse = cancelRes;
                        shareddal.InsertUpdateCanREQ(shared, "Update");
                        canstr[0] = "Fail";
                        canstr[1] = cancelRes;
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                canstr[0] = "Fail";
            }
            return canstr;
        }
        #endregion
        //private string[] cancelTicket(BS_SHARED.SHARED shared)
        //{
        //    string url = ConfigurationManager.AppSettings["CancelTicket"].ToString();
        //    string cancelstr = ""; string cancelRes = ""; string[] s; string[] canstr = new string[4];
        //    string ss = ""; DataSet ds = new DataSet(); shareddal = new BS_DAL.SharedDAL();
        //    try
        //    {
        //        if (shared.seat.IndexOf(",") > 0)
        //        {
        //            s = shared.seat.Split(',');
        //            for (int y = 0; y <= s.Length - 1; y++)
        //            {
        //                ss += "\"" + s[y].Trim() + "\"" + ",";
        //            }
        //            ss = ss.Remove(ss.LastIndexOf(","));
        //            cancelstr = "{\"tin\":\"" + shared.tin + "\",\"seatsToCancel\":[" + ss.Trim() + "]}";
        //            shared.canrequest = cancelstr;
        //            #region[save cancel request into db]
        //            shareddal.InsertUpdateCanREQ(shared, "Insert");
        //            #endregion
        //            cancelRes = GetPostRequest(url, cancelstr);
        //            if (cancelRes != "")
        //            {
        //                #region[save cancel request into db]
        //                shared.canresponse = cancelRes;
        //                shareddal.InsertUpdateCanREQ(shared, "Update");
        //                #endregion
        //                ds = convertJsonStringToDataSet(cancelRes);
        //            }
        //        }
        //        else
        //        {
        //            cancelstr = "{\"tin\":\"" + shared.tin + "\",\"seatsToCancel\":[\"" + shared.seat + "\"]}";
        //            shared.canrequest = cancelstr;
        //            #region[save cancel request into db]
        //            shareddal.InsertUpdateCanREQ(shared, "Insert");
        //            #endregion
        //            cancelRes = GetPostRequest(url, cancelstr);
        //            if (cancelRes != "")
        //            {
        //                #region[save cancel request into db]
        //                shared.canresponse = cancelRes;
        //                shareddal.InsertUpdateCanREQ(shared, "Update");
        //                #endregion
        //                ds = convertJsonStringToDataSet(cancelRes);
        //            }
        //        }
        //        canstr[0] = "Success";
        //        canstr[1] = ds.Tables[0].Rows[0]["cancellationCharge"].ToString();
        //        canstr[2] = ds.Tables[0].Rows[0]["refundAmount"].ToString();
        //        canstr[3] = ds.Tables[0].Rows[0]["tin"].ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        canstr[0] = "Fail";
        //    }
        //    return canstr;
        //}
        #region[Calculate Refund]
        public string[] getcancelTicketStatus(ref BS_SHARED.SHARED shared)
        {
            string[] cancelResponse = null; shareddal = new BS_DAL.SharedDAL();
            Double cancharge = 0;
            BS_DAL.SharedDAL shareds = new BS_DAL.SharedDAL();
            DataSet dsSrvCharge = shareds.GetBusServiceChrg();
            double busfare = 0; bool flagg = false; string[] cancelResp = new string[4];
            try
            {
                if (shared.seat.IndexOf(",") > 0 && shared.netfare.IndexOf(",") > 0)
                {
                    cancelResponse = cancelTicket(shared);
                    //New Change 7-08-2014
                    if (cancelResponse.Length < 0 || cancelResponse[0] == "Fail")
                    {   //update canceldetail to field as api cancel status as failed
                        return cancelResponse;
                    }
                    else
                    {
                        string[] d = shared.journeyDate.Split('-');
                        string[] policy = shared.canPolicy.Split(';');
                        string time = shared.canTime.Replace("(", "").Replace(")", "");
                        string date = d[0].Trim() + "/" + d[1].Trim() + "/" + d[2].Trim() + " " + time;
                        DateTime dt = Convert.ToDateTime(date);
                        DateTime dt1 = DateTime.Now;
                        TimeSpan duration = dt - dt1;
                        double timediff = duration.TotalMilliseconds;
                        int hour = 60 * 60 * 1000;
                        int diff = (int)Math.Abs(timediff / hour);
                        for (int q = 0; q <= policy.Length - 1; q++)
                        {
                            if (policy[q] != "")
                            {
                                string[] s = policy[q].Split(':');
                                if (diff > Convert.ToInt32(s[0].Trim()) && diff < Convert.ToInt32(s[1].Trim()))
                                {
                                    flagg = true;

                                    Refund(ref  shared, s, shared.provider_name, dsSrvCharge);
                                    //string[] spltfare = shared.fare.Split(',');
                                    //string[] splitnetfare = shared.netfare.Split(',');
                                    //for (int b = 0; b <= spltfare.Length - 1; b++)
                                    //{
                                    //    busfare = Convert.ToDouble((Convert.ToDouble(spltfare[b]) * Convert.ToInt32(s[2].Trim())) / 100);
                                    //    shared.cancelRecharge += Convert.ToString(busfare) + ",";
                                    //    shared.refundAmt += Convert.ToString(Convert.ToDouble(splitnetfare[b]) - busfare) + ",";
                                    //}

                                   //cancharge = Convert.ToDouble(sharedbal.GetServiceCharges(shared.fare, shared.provider_name));


                                      
                                    //-------------------------------insert into database----------------------------------//
                                    ////shareddal.updateCanchrgandRefund(shared);
                                    ////shared.refundAmt = shared.refundAmt.Remove(shared.refundAmt.LastIndexOf(","));
                                    ////string[] addfare = shared.refundAmt.Split(',');
                                    ////for (int x = 0; x <= addfare.Length - 1; x++)
                                    ////{
                                    ////    shared.addAmt += Convert.ToDecimal(addfare[x].Trim());
                                    ////}
                                    ////shared.avalBal = shareddal.deductAndaddfareAmt(shared, "Add");
                                    ////shareddal.insertLedgerDetails(shared, "Add");

                                    //--------------------------------------end--------------------------------------------//
                                    //----------------------------------yatra credit note integratrion---------------------------//
                                    //YatraBilling.BUS_YATRA bs = new YatraBilling.BUS_YATRA();
                                    //bool flag = bs.IntegrateYatra(common.orderID, common.agentID, common.tin, "C");
                                    //---------------------------------------end-------------------------------------------------//
                                }
                                if (flagg == false)
                                {
                                    if (s[1] == "-1")
                                    {

                                        Refund(ref  shared, s, shared.provider_name, dsSrvCharge);

                                        //string[] spltfare = shared.fare.Split(',');
                                        //string[] splitnetfare = shared.netfare.Split(',');
                                        //for (int b = 0; b <= spltfare.Length - 1; b++)
                                        //{
                                        //    busfare = Convert.ToDouble((Convert.ToDouble(spltfare[b]) * Convert.ToInt32(s[2].Trim())) / 100);
                                        //    shared.cancelRecharge += Convert.ToString(busfare) + ",";
                                        //    shared.refundAmt += Convert.ToString(Convert.ToDouble(splitnetfare[b]) - busfare) + ",";
                                        //}
                                        //-------------------------------insert into database----------------------------------//
                                        ////shareddal.updateCanchrgandRefund(shared);
                                        ////shared.refundAmt = shared.refundAmt.Remove(shared.refundAmt.LastIndexOf(","));
                                        ////string[] addfare = shared.refundAmt.Split(',');
                                        ////for (int x = 0; x <= addfare.Length - 1; x++)
                                        ////{
                                        ////    shared.addAmt += Convert.ToDecimal(addfare[x].Trim());
                                        ////}
                                        ////shared.avalBal = shareddal.deductAndaddfareAmt(shared, "Add");
                                        ////shareddal.insertLedgerDetails(shared, "Add");

                                        //--------------------------------------end--------------------------------------------//

                                        //----------------------------------yatra credit note integratrion---------------------------//
                                        //YatraBilling.BUS_YATRA bs = new YatraBilling.BUS_YATRA();
                                        //bool flag = bs.IntegrateYatra(common.orderID, common.agentID, common.tin, "C");
                                        //---------------------------------------end-------------------------------------------------//
                                    }
                                }
                            }
                        }
                    }
                    cancelResp[0] = cancelResponse[0].Trim();//sucess/fail//
                    cancelResp[1] = cancelResponse[1].Trim();//cancellation charge//
                    cancelResp[2] = Convert.ToString(shared.addAmt);//refund amount//
                    cancelResp[3] = cancelResponse[3].Trim();//ticket no//
                }
                else
                {
                    cancelResponse = cancelTicket(shared);

                    if (cancelResponse.Length < 0 || cancelResponse[0] == "Fail")
                    {
                        return cancelResponse;
                    }
                    else
                    {
                        string[] d = shared.journeyDate.Split('-');
                        string[] policy = shared.canPolicy.Split(';');
                        string time = shared.canTime.Replace("(", "").Replace(")", "");
                        string date = d[0].Trim() + "/" + d[1].Trim() + "/" + d[2].Trim() + " " + time;
                        DateTime dt = Convert.ToDateTime(date);
                        DateTime dt1 = DateTime.Now;
                        TimeSpan duration = dt - dt1;
                        double timediff = duration.TotalMilliseconds;
                        int hour = 60 * 60 * 1000;
                        int diff = (int)Math.Abs(timediff / hour);
                        for (int p = 0; p <= policy.Length - 1; p++)
                        {
                            if (policy[p] != "")
                            {
                                string[] s = policy[p].Split(':');
                                if (diff > Convert.ToInt32(s[0].Trim()) && diff < Convert.ToInt32(s[1].Trim()))
                                {
                                    flagg = true;

                                    Refund(ref  shared, s, shared.provider_name, dsSrvCharge);

                                    //busfare = Convert.ToDouble((Convert.ToDouble(shared.fare) * Convert.ToInt32(s[2].Trim())) / 100);
                                    //shared.cancelRecharge = Convert.ToString(busfare);
                                    //shared.refundAmt = Convert.ToString(Convert.ToDouble(shared.netfare) - busfare);

                                    //-------------------------------insert into database----------------------------------//
                                    ////shareddal.updateCanchrgandRefund(shared);
                                    ////shared.addAmt = Convert.ToDecimal(shared.refundAmt);

                                    ////shared.avalBal = shareddal.deductAndaddfareAmt(shared, "Add");
                                    ////shareddal.insertLedgerDetails(shared, "Add");
                                    //--------------------------------------end--------------------------------------------//

                                    //----------------------------------yatra credit note integratrion---------------------------//
                                    // YatraBilling.BUS_YATRA bs = new YatraBilling.BUS_YATRA();
                                    //bool flag = bs.IntegrateYatra(common.orderID, common.agentID, common.tin, "C");
                                    //---------------------------------------end-------------------------------------------------//
                                }
                                if (flagg == false)
                                {
                                    if (s[1] == "-1")
                                    {
                                        Refund(ref  shared, s, shared.provider_name, dsSrvCharge);


                                        //busfare = Convert.ToDouble((Convert.ToDouble(shared.fare) * Convert.ToInt32(s[2].Trim())) / 100);
                                        //shared.cancelRecharge = Convert.ToString(busfare);
                                        //shared.refundAmt = Convert.ToString(Convert.ToDouble(shared.netfare) - busfare);
                                        //-------------------------------insert into database----------------------------------//
                                        ////shareddal.updateCanchrgandRefund(shared);
                                        ////shared.addAmt = Convert.ToDecimal(shared.refundAmt);

                                        ////shared.avalBal = shareddal.deductAndaddfareAmt(shared, "Add");
                                        ////shareddal.insertLedgerDetails(shared, "Add");
                                        //--------------------------------------end--------------------------------------------//
                                        //----------------------------------yatra credit note integratrion---------------------------//
                                        //YatraBilling.BUS_YATRA bs = new YatraBilling.BUS_YATRA();
                                        // bool flag = bs.IntegrateYatra(common.orderID, common.agentID, common.tin, "C");
                                        //---------------------------------------end-------------------------------------------------//
                                    }
                                }
                            }
                        }
                    }
                    cancelResp[0] = cancelResponse[0].Trim();//sucess/fail//
                    cancelResp[1] = cancelResponse[1].Trim();//cancellation charge//
                    cancelResp[2] = Convert.ToString(shared.addAmt);//refund amount//
                    cancelResp[3] = cancelResponse[3].Trim();//ticket no//
                }
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
                cancelResp[0] = cancelResponse[0].Trim();
            }
            return cancelResp;
        }
        #endregion
        #region[Get Ticket Details]
        private string getTicketDetails(string tin)
        {
            string resp = "";
            try
            {
                resp = GetRequest(ConfigurationManager.AppSettings["GetTicket"].ToString().Trim() + "tin=" + tin.Trim());
            }
            catch (Exception ex)
            {
                erlog = new EXCEPTION_LOG.ErrorLog();
                erlog.writeErrorLog(ex, "SHARED_BAL");
                resp = "Error";
            }
            return resp;
        }
        #endregion
        public string SetClassName(string CheckConditions, string ladiesSeat, string available)
        {
            string divclass = "";
            if (ladiesSeat == "true")    // check for Female
            {
                if (available == "true")
                {
                    if (CheckConditions == "0,1,1" || CheckConditions == "1,1,1")
                        divclass = "divLadies";
                    else if (CheckConditions == "0,1,2" || CheckConditions == "1,1,2")
                        divclass = "divVertiSleperLadies";
                    else if (CheckConditions == "0,2,1" || CheckConditions == "1,2,1")
                        divclass = "divHoriSleperLadies";
                }
                else
                {
                    if (CheckConditions == "0,1,1" || CheckConditions == "1,1,1")
                        divclass = "divBlockladies";
                    else if (CheckConditions == "0,1,2" || CheckConditions == "1,1,2")
                        divclass = "divVertiSleperBlockladies";
                    else if (CheckConditions == "0,2,1" || CheckConditions == "1,2,1")
                        divclass = "divHoriSleperBlockladies";
                }
            }
            else
            {
                if (available == "true")
                {
                    if (CheckConditions == "0,1,1" || CheckConditions == "1,1,1")
                        divclass = "divAval";
                    else if (CheckConditions == "0,1,2" || CheckConditions == "1,1,2")
                        divclass = "divVertiSleperAval";
                    else if (CheckConditions == "0,2,1" || CheckConditions == "1,2,1")
                        divclass = "divHoriSleperAval";
                }
                else
                {
                    if (CheckConditions == "0,1,1" || CheckConditions == "1,1,1")
                        divclass = "divBlock";
                    else if (CheckConditions == "0,1,2" || CheckConditions == "1,1,2")
                        divclass = "divVertiSleperBlock";
                    else if (CheckConditions == "0,2,1" || CheckConditions == "1,2,1")
                        divclass = "divHoriSleperBlock";
                }
            }
            return divclass;
        }
        public string setHourstoMinutes(string time)
        {
            string TimeFormate = "";
            int timestring = Convert.ToInt32(time);
            int hour = timestring / 60;
            int minute = timestring % 60;
            int Day = hour / 24;
            int DayHour = hour % 24;
            string strDayHour = "";
            string strminute = "";
            if (Day == 1)
            {
                if (DayHour >= 12)
                {
                    DayHour = DayHour - 12;
                    if (DayHour < 10)
                        strDayHour = "0" + DayHour;
                    else
                        strDayHour = Convert.ToString(DayHour);
                    if (minute < 10)
                        strminute = "0" + minute;
                    else
                        strminute = Convert.ToString(minute);
                    TimeFormate = strDayHour + ':' + strminute + " " + "PM";
                }
                else
                {
                    if (DayHour < 10)
                        strDayHour = "0" + DayHour;
                    else
                        strDayHour = Convert.ToString(DayHour);
                    if (minute < 10)
                        strminute = "0" + minute;
                    else
                        strminute = Convert.ToString(minute);
                    TimeFormate = strDayHour + ':' + strminute + " " + "AM";
                }
            }
            else
            {
                if (minute == 0)
                {
                    if (DayHour >= 12)
                    {
                        DayHour = DayHour - 12;
                        if (DayHour < 10)
                            strDayHour = "0" + DayHour;
                        else
                            strDayHour = Convert.ToString(DayHour);
                        TimeFormate = strDayHour + ':' + minute + "0" + " " + "PM";
                    }
                    else
                    {
                        if (DayHour < 10)
                            strDayHour = "0" + DayHour;
                        else
                            strDayHour = Convert.ToString(DayHour);
                        TimeFormate = strDayHour + ':' + minute + "0" + " " + "AM";
                    }
                }
                else
                {
                    if (DayHour >= 12)
                    {
                        DayHour = DayHour - 12;
                        if (DayHour < 10)
                            strDayHour = "0" + DayHour;
                        else
                            strDayHour = Convert.ToString(DayHour);
                        if (minute < 10)
                            strminute = "0" + minute;
                        else
                            strminute = Convert.ToString(minute);
                        TimeFormate = strDayHour + ':' + strminute + " " + "PM";
                    }
                    else
                    {
                        if (DayHour < 10)
                            strDayHour = "0" + DayHour;
                        else
                            strDayHour = Convert.ToString(DayHour);
                        if (minute < 10)
                            strminute = "0" + minute;
                        else
                            strminute = Convert.ToString(minute);
                        TimeFormate = strDayHour + ':' + strminute + " " + "AM";
                    }
                }
            }
            return TimeFormate;
        }
        public string GetBoardingPoints(string tripId, string boardingId)
        {
            return GetRequest(ConfigurationManager.AppSettings["BoardingPoint"].ToString().Trim() + "id=" + boardingId + "&tripId=" + tripId + "");
        }


        public void Refund(ref BS_SHARED.SHARED shared, string[] s, string provider,DataSet dsSrvCharge)
        {

            string[] spltfare = shared.fare.Split(',');
            string[] splitnetfare = shared.netfare.Split(',');

            string  RefundSrvCharge = "0";

            for (int b = 0; b <= spltfare.Length - 1; b++)
            {
               // BS_SHARED.SHARED TicketWiseshared =( BS_SHARED.SHARED) shared.Clone();

                 RefundSrvCharge = sharedbal.GetServiceCharges(spltfare[b], shared.provider_name, dsSrvCharge);
                 shared.serviceID    += RefundSrvCharge + ",";
                Double busfare = Convert.ToDouble((Convert.ToDouble(spltfare[b]) * Convert.ToInt32(s[2].Trim())) / 100);
                shared.cancelRecharge += Convert.ToString(busfare)+ ",";
                shared.refundAmt += Convert.ToString(Convert.ToDouble(splitnetfare[b]) - busfare -  Convert.ToDouble(RefundSrvCharge)) + ",";
                shared.addAmt += Convert.ToDecimal(Convert.ToDouble(splitnetfare[b]) - busfare - Convert.ToDouble(RefundSrvCharge));
               
            }


          

                   


          //  }
           




            //-----------------------------------end-------------------------------------------------------





           
        }

    }
}
public class Seat
{
    public string available { get; set; }
    public string column { get; set; }
    public string fare { get; set; }
    public string ladiesSeat { get; set; }
    public string length { get; set; }
    public string name { get; set; }
    public string row { get; set; }
    public string width { get; set; }
    public string zIndex { get; set; }
    public string serviceTaxAbsolute { get; set; }
}

public class RootObject
{
    public List<Seat> seats { get; set; }
}

